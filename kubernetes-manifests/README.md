This directory contains all relevant kubernetes resource manifests.

* `./` directory contains any rendered files, which may be used for production releases if they exist
* `skaffold/` directory contains the base kubernetes resource manifests used directly by skaffold for build, dev, etc
* `supplemental/` directory contains supplemental manifests that could be useful, though not required i.e. for a standalone service

To render production manifest files, use the `scripts/reder_artifacts.sh`.


