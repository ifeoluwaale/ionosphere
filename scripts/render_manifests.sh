#!/bin/bash

registry=${IMAGE_REGISTRY:-"registry.gitlab.com/cyverse"}
cur_branch=$(git branch --show-current)
manifest="kubernetes-manifests/ion.yaml"

echo "This script will render kubernetes manifests, useful for production or release environments"

if [ -z "$IMAGE_RELEASE" ]; then
    if [ -z "$cur_branch" ]; then
        IMAGE_RELEASE="latest"
        echo "fyi - IMAGE_RELEASE was not found, using 'latest' tag to render"
    else
        IMAGE_RELEASE=$cur_branch
        echo "Tagging manifests with branch: $cur_branch"
    fi
fi

skaffold render --default-repo $registry --profile prod --output $manifest --digest-source=tag
