import { useCallback, useEffect, useState } from "react";
import { useCredentials, useClouds, useAPI } from "../contexts";

const CLOUD_CREDENTIAL_TYPES = ["openstack", "aws", "kubernetes"];

/**
 * Map provider type to credential type.
 * @param provider
 * @returns {*|string}
 */
const cloudCredentialTypeForProvider = (provider) => {
  switch (provider.type) {
    case "openstack":
      return provider.type;
    case "aws":
      return provider.type;
    case "kubernetes":
      return "kubeconfig";
    default:
      return "";
  }
};

/**
 * Custom React hook for getting credential for selected cloud/project combination.
 *
 * @param {string} cloudId (required)
 * @param {string} projectName (required if cloud is openstack)
 * @returns {string} credentialId
 *
 * @reference https://react.dev/learn/reusing-logic-with-custom-hooks
 */
export const useCredential = (cloudId, projectName) => {
  const [credentials] = useCredentials();

  const cloudCredentials = credentials.filter((c) => {
    return CLOUD_CREDENTIAL_TYPES.includes(c.type);
  });

  const [clouds] = useClouds();
  const api = useAPI();

  const [credentialId, setCredentialId] = useState("");

  const getCredential = useCallback(
    async (cloudId, projectName) => {
      const fetchProjectIDMapping = async (cloudId, projectName) => {
        const projectList = await api.openstackProjects(cloudId);
        const matchedProject = projectList.find((p) => p.name === projectName);
        if (matchedProject) {
          return matchedProject.id;
        } else {
          return "";
        }
      };

      if (cloudId && cloudCredentials.length > 0) {
        // get selected cloud by ID
        const cloud = clouds.find((c) => c.id === cloudId);
        const expectedCredType = cloudCredentialTypeForProvider(cloud);

        let projectUUID = "";

        // if openstack, get UUID for selected project
        if (cloud.type === "openstack") {
          // project is required for openstack, if not provided, clear credential
          if (!projectName) {
            setCredentialId("");
            return;
          }

          projectUUID = await fetchProjectIDMapping(cloudId, projectName);
        }

        // filter credentials for expected type
        const typeCredentials = credentials?.filter(
          (c) => c.type === expectedCredType
        );

        // filter credentials for selected cloud
        const cloudCredentials = typeCredentials.filter((c) => {
          return (
            c.tags_kv["cacao_provider"] === cloudId || c.tags.includes(cloudId)
          );
        });

        // find the credential for selected project (if openstack)
        let credentialId = cloudCredentials.find(
          (c) => c.tags_kv["cacao_openstack_project_id"] === projectUUID
        )?.id;

        if (!credentialId) {
          credentialId = cloudCredentials.find(
            (c) => c.id.includes(projectName) || c.name.includes(projectName)
          )?.id;
        }

        if (credentialId) {
          setCredentialId(credentialId);
        } else {
          setCredentialId("");
        }
      } else {
        setCredentialId("");
      }
    },
    [cloudCredentials, clouds, credentials, api]
  );

  // When cloudId or projectName changes, find the matching credential
  useEffect(() => {
    getCredential(cloudId, projectName);
  }, [cloudId, projectName, getCredential]);

  return credentialId;
};
