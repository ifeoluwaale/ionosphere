import { useEffect, useState } from "react";
import { useAPI } from "../contexts";
import { js2SortByImageByName } from "../utils";

/**
 * Custom React hook to fetch and manage images from an API, based on the provided cloud, credential, and region IDs.
 * The hook handles the fetching process, maintains the state of the images, loading status, and any errors.
 *
 * @param {string} cloudId - The ID of the cloud provider.
 * @param {string} credentialId - The ID of the credential.
 * @param {string} regionId - The ID of the region.
 * @returns {Object} An object containing the fetched images, a loading status, and any errors encountered.
 */
export const useImages = (cloudId, credentialId, regionId) => {
  const api = useAPI();

  const [images, setImages] = useState([]);
  const [loadingImages, setLoadingImages] = useState(false);
  const [error, setError] = useState();

  /**
   * Fetches images and provides loading state.
   */
  const fetchImages = async () => {
    setLoadingImages(true);

    try {
      // pass credential to filter images by allocation
      const fetchedImages = await api.providerImages(cloudId, {
        credential: credentialId,
        region: regionId,
      });
      // fetchedImages = fetchedImages.filter((i) => i.visibility === "public");
      fetchedImages.sort(js2SortByImageByName);
      setImages(fetchedImages);
    } catch (e) {
      setError(e);
    } finally {
      setLoadingImages(false);
    }
  };

  useEffect(() => {
    if (!!credentialId && !!regionId) {
      setTimeout(fetchImages, 0);
    }
  }, [credentialId, regionId]);

  return {
    images,
    loadingImages,
    imagesError: error,
  };
};
