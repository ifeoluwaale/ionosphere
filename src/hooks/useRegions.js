import { useEffect, useState } from "react";
import { useAPI } from "../contexts";

/**
 * Custom React hook to fetch and manage a list of regions from an API based on the
 * given cloud ID and credential ID.
 *
 * @param {string} cloudId - The ID of the cloud provider.
 * @param {string} credentialId - The ID of the credential.
 * @returns {Object} An object containing the fetched regions, and a loading state indicator.
 */
export const useRegions = (cloudId, credentialId) => {
  const [regions, setRegions] = useState([]);
  const [loading, setLoading] = useState(true);
  const api = useAPI();

  /**
   * Fetches regions and provides loading state.
   */
  const fetchRegions = async () => {
    setLoading(true);
    try {
      const regions = await api.providerRegions(cloudId, {
        credential: credentialId,
      });
      setRegions(regions);
    } catch (e) {
      console.log(e);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    if (!!credentialId) {
      setTimeout(fetchRegions, 0);
    }
  }, [credentialId]);

  return [regions, loading];
};
