import {
  Home as HomeIcon,
  Storage as StorageIcon,
  Category as CategoryIcon,
  Event as EventIcon,
  Lock as LockIcon,
  Group as GroupIcon,
  Help as HelpIcon,
  VpnKey as VpnKeyIcon,
  Cloud as CloudIcon,
  Widgets as WidgetsIcon,
  Folder as FolderIcon,
} from "@mui/icons-material";
import SvgForumIcon from "./components/icons/forumIcon";
import SvgLearnIcon from "./components/icons/learnIcon";
import SvgCalIcon from "./components/icons/calendarIcon";
import SvgPoliciesIcon from "./components/icons/policiesIcon";
import SvgWikiIcon from "./components/icons/wikiIcon";
import SvgCloudIcon from "./components/icons/cloudIcon";
import SvgMapIcon from "./components/icons/mapIcon";
import SvgVideoIcon from "./components/icons/videoIcon";
import SvgFlagIcon from "./components/icons/flagIcon";
import { mdiRocketLaunch } from "@mdi/js";
import { mdiSchool } from "@mdi/js";
import { mdiBookshelf } from "@mdi/js";
import Icon from "@mdi/react";

const menuItems = [
  {
    label: "Home",
    icon: <HomeIcon />,
    path: "/home",
    items: [],
  },
  {
    label: "Deployments",
    icon: <Icon path={mdiRocketLaunch} size={1} />,
    path: "/deployments",
    items: [],
  },
  // {
  //   label: 'Storage',
  //   icon: <StorageIcon fontSize="large" />,
  //   path: '/storage',
  //   items: [],
  // },
  {
    label: "Credentials",
    icon: <VpnKeyIcon />,
    path: "/credentials",
    items: [],
  },
  {
    label: "Templates",
    icon: <WidgetsIcon />,
    path: "/templates",
    items: [],
  },
  {
    label: "Help",
    icon: <HelpIcon />,
    path: "/help",
    items: [
      {
        category: "learn",
        label: "Product Tour",
        path: null,
        description:
          "Coming Soon: Take a tour to get introduced to the features of Jetstream2",
        icon: <SvgMapIcon width="20px" height="20px" />,
      },
      {
        category: "learn",
        label: "Platform Guide",
        path: "https://docs.jetstream-cloud.org/",
        description: "Learn more about Jetstream2 by using the platform guide.",
        icon: <Icon path={mdiBookshelf} size={0.8} />,
      },
      // {
      //   category: 'learn',
      //   label: 'Webinars',
      //   path: '',
      //   description:
      //     'View webinars about Jetstream2.',
      //   icon: <SvgVideoIcon />,
      // },
      {
        category: "learn",
        label: "Learning Center",
        path: "https://docs.jetstream-cloud.org/",
        description:
          "Explore our learning materials in the popular “Read the Docs” formatting.",
        icon: <Icon path={mdiSchool} size={0.8} />,
      },
      {
        category: "support",
        label: "Policies",
        path: "https://docs.jetstream-cloud.org/",
        description: "Jetstream2 policies that apply to all users.",
        icon: <SvgPoliciesIcon width="20px" height="20px" />,
      },
      // {
      //   category: 'support',
      //   label: 'Jetstream2 Wiki',
      //   path: 'https://cyverse.atlassian.net/wiki',
      //   description: 'A space for collaboration.',
      //   icon: <SvgWikiIcon />,
      // },
      {
        category: "support",
        label: "FAQ",
        path: "https://docs.jetstream-cloud.org/faq/general-faq/",
        description: "Answers to frequenty asked questions about Jetstream2",
        icon: <SvgForumIcon width="20px" height="20px" />,
      },
      // {
      //   category: 'support',
      //   label: 'Maintenance Calendar',
      //   path: 'https://cyverse.org/maintenance',
      //   description: 'Check for scheduled downtime for maintenance.',
      //   icon: <SvgCalIcon />,
      // },
    ],
  },
  {
    label: "Administrative",
    icon: <LockIcon fontSize="large" />,
    path: "/administrative",
    restricted: true,
    items: [
      {
        label: "Users",
        icon: <GroupIcon fontSize="medium" />,
        path: "/administrative/users",
        description:
          "Search across all users and view details about individual users.",
      },
      {
        label: "Workspaces",
        icon: <FolderIcon fontSize="medium" />,
        path: "/administrative/workspaces",
        description: "Search across all workspaces.",
      },
      {
        label: "Templates",
        icon: <CategoryIcon fontSize="medium" />,
        path: "/administrative/templates",
        description: "Search across all templates.",
      },
      {
        label: "Clouds",
        icon: <CloudIcon fontSize="medium" />,
        path: "/administrative/clouds",
        description: "View and manage clouds.",
      },
      {
        label: "Events",
        icon: <EventIcon fontSize="medium" />,
        path: "/administrative/events",
        description: "View and filter events.",
      },
    ],
  },
];

const getMenuItem = (label) => {
  return menuItems.find((item) => item.label === label);
};

export { menuItems, getMenuItem };
