/**
 * Contains controllers for express routes associated with user
 * navigation in the nextJS application.
 */

const app = require("../server");
const nextHandler = app.getRequestHandler();
const navService = require("../services/nav.service");

// Handles when user lands on base url
const getLanding = (req, res) => {
  navService.getLanding(req, res);
};

// Public routes
const getPublicRoutes = (req, res) => {
  return nextHandler(req, res);
};

// Private routes (authenticated required)
const getPrivateRoutes = (req, res) => {
  return nextHandler(req, res);
};

// Handle callback from openstack with unscoped token
const openstackCallbackHandler = (req, res) => {
  return navService.openstackCallbackHandler(req, res);
};

module.exports = {
  getLanding,
  getPublicRoutes,
  getPrivateRoutes,
  openstackCallbackHandler,
};
