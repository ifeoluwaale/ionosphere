const passport = require("passport");
const GlobusStrategy = require("passport-globus").Strategy;
const KeycloakStrategy = require("@exlinc/keycloak-passport");
const OAuth2Strategy = require("passport-oauth2");

const jwt = require("jsonwebtoken");
const cookieSession = require("cookie-session");

// get auth provider from environment var
function getProvider() {
  return process.env.ION_AUTH_TYPE;
}

// authentication setup
function init(opts) {
  // These are needed for storing the user in the session
  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (user, done) {
    done(null, user);
  });

  const provider = getProvider();

  if (provider === "keycloak") initKeycloak(opts);
  else if (provider === "globus") initGlobus(opts);
  else if (provider === "cilogon") initCILogon(opts);
  else console.log("Authentication is disabled");
}

function middleware(app) {
  const provider = getProvider();

  if (!provider) return;

  // Initialize Passport
  app.use(
    cookieSession({
      name: "session",
      secret: process.env.SESSION_SECRET,
      maxAge: 24 * 60 * 60 * 1000, // 24 hours
    })
  );

  app.use(passport.initialize());
  app.use(passport.session());

  // Define scope for access token
  let options = {};

  if (provider === "globus") {
    options.scope = [
      "email",
      "profile",
      "openid",
      "https://auth.globus.org/scopes/b184f54f-4df1-45cd-8618-f6704bf1147c/all",
    ];
  } else if (provider === "cilogon") {
    options.scope = ["openid", "email", "profile", "org.cilogon.userinfo"];
  }

  // Define route to initiate authentication
  app.get(`/auth/${provider}/login`, passport.authenticate(provider, options));

  // Define route to finish authentication
  app.get(
    `/auth/${provider}/callback`,
    passport.authenticate(provider, {
      successRedirect: "/",
      failureRedirect: "/401",
    })
  );

  // Alias for login endpoint above
  app.get("/login", requireAuth, (_, res) => {
    //res.redirect("/")
  });

  // Passport logout: http://www.passportjs.org/docs/logout/
  app.get("/logout", async (req, res) => {
    // Remove session cookie
    req.session = null;

    if (getProvider() === "globus") {
      console.log("Revoking token", req.user.accessToken);
      try {
        const res = await fetch(
          "https://auth.globus.org/v2/oauth2/token/revoke",
          {
            //FIXME hardcoded URL
            method: "POST",
            headers: {
              Authorization:
                "Basic " +
                Buffer.from(
                  process.env.GLOBUS_CLIENT + ":" + process.env.GLOBUS_SECRET
                ).toString("base64"),
              "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            },
            body: "token=" + encodeURIComponent(req.user.accessToken),
          }
        );
        console.log(await res.json());
      } catch (e) {
        console.error("Token revocation failed:");
        console.error(e);
      }
    } else if (getProvider() === "cilogon") {
      console.log("Revoking token", req.user.accessToken);
      try {
        const res = await fetch("https://cilogon.org/oauth2/revoke", {
          //FIXME hardcoded URL
          method: "POST",
          headers: {
            Authorization:
              "Basic " +
              Buffer.from(
                process.env.OAUTH2_CLIENT_ID +
                  ":" +
                  process.env.OAUTH2_CLIENT_SECRET
              ).toString("base64"),
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
          },
          body: "token=" + encodeURIComponent(req.user.accessToken),
        });
        // console.log(res);
      } catch (e) {
        console.error("Token revocation failed:");
        console.error(e);
      }
    }

    req.logout();
    res.redirect("/");
  });
}

/**
 * Initialize Globus strategy for passport.
 */
function initGlobus({ onLogin }) {
  console.log("Initializing Globus auth");

  // Verify required config params
  const REQUIRED_PARAMS = ["OAUTH2_CLIENT_ID", "OAUTH2_CLIENT_SECRET"];
  for (const p of REQUIRED_PARAMS)
    if (!(p in process.env) || typeof process.env[p] === "undefined")
      throw Error("Missing required Globus configuration parameter: " + p);

  // Add authentication strategy
  passport.use(
    "globus",
    new GlobusStrategy(
      {
        state: true, // remove this if not using sessions
        clientID: process.env.OAUTH2_CLIENT_ID,
        clientSecret: process.env.OAUTH2_CLIENT_SECRET,
        callbackURL: `${process.env.UI_BASE_URL}/auth/globus/callback`,
      },
      function (accessToken, refreshToken, params, profile, done) {
        // `profile` is empty as Globus has no generic profile URL,
        // but rather returns a json web token in the params. so we
        // set the profile to a decoded params.id_token
        console.log(
          "accessToken:",
          accessToken,
          "params:",
          params,
          "profile:",
          profile
        );
        profile = jwt.decode(params.id_token);
        if (onLogin) onLogin(accessToken, profile);
        return done(null, { accessToken, profile }); // stored in req.user object
      }
    )
  );
}

/**
 * Initialize Keycloak strategy for passport.
 */
function initKeycloak({ onLogin }) {
  console.log("Initializing Keycloak auth");

  // Verify required config params
  const REQUIRED_PARAMS = [
    "KEYCLOAK_URL",
    "KEYCLOAK_REALM",
    "OAUTH2_CLIENT_ID",
    "OAUTH2_CLIENT_SECRET",
  ];

  for (const p of REQUIRED_PARAMS)
    if (!(p in process.env) || typeof process.env[p] === "undefined")
      throw Error("Missing required Keycloak configuration parameter: " + p);

  // Add authentication strategy
  passport.use(
    "keycloak",
    new KeycloakStrategy(
      {
        host: process.env.KEYCLOAK_URL,
        realm: process.env.KEYCLOAK_REALM,
        clientID: process.env.OAUTH2_CLIENT_ID,
        clientSecret: process.env.OAUTH2_CLIENT_SECRET,
        callbackURL: `${process.env.UI_BASE_URL}/auth/keycloak/callback`,

        //FIXME not present in example, see KeycloakEnvConfig
        authorizationURL: `${process.env.KEYCLOAK_URL}/auth/realms/${process.env.KEYCLOAK_REALM}/protocol/openid-connect/auth`,
        tokenURL: `${process.env.KEYCLOAK_URL}/auth/realms/${process.env.KEYCLOAK_REALM}/protocol/openid-connect/token`,
        userInfoURL: `${process.env.KEYCLOAK_URL}/auth/realms/${process.env.KEYCLOAK_REALM}/protocol/openid-connect/userinfo`,
      },
      (req, accessToken, refreshToken, profile, done) => {
        if (onLogin) onLogin(accessToken, profile);
        return done(null, { accessToken, profile }); // stored in req.user object
      }
    )
  );
}

/**
 * Initialize CILogon strategy for passport.
 */
function initCILogon({ onLogin }) {
  console.log("Initializing CILogon auth");

  // Verify required config params
  const REQUIRED_PARAMS = ["OAUTH2_CLIENT_ID", "OAUTH2_CLIENT_SECRET"];

  for (const p of REQUIRED_PARAMS)
    if (!(p in process.env) || typeof process.env[p] === "undefined")
      throw Error("Missing required OAuth2 configuration parameter: " + p);

  // Add authentication strategy
  passport.use(
    "cilogon",
    new OAuth2Strategy(
      {
        clientID: process.env.OAUTH2_CLIENT_ID,
        clientSecret: process.env.OAUTH2_CLIENT_SECRET,
        callbackURL: `${process.env.UI_BASE_URL}/auth/cilogon/callback`,
        authorizationURL: "https://cilogon.org/authorize",
        tokenURL: "https://cilogon.org/oauth2/token",
      },
      function (accessToken, refreshToken, profile, cb) {
        console.log(
          "accessToken:",
          accessToken,
          "refreshToken:",
          refreshToken,
          "profile:",
          profile
        );

        if (onLogin) {
          onLogin(accessToken, profile);
        }

        return cb(null, { accessToken, profile }); // stored in req.user object
      }
    )
  );
}

function requireAuth(req, res, next) {
  //console.log('isAuthenticated: ', req?.isAuthenticated())

  const provider = getProvider();

  // if user is not authenticated, direct them to login with auth provider
  if (provider && !req.isAuthenticated()) {
    res.redirect(`/auth/${provider}/login`);
  }
  return next();
}

module.exports = { init, middleware, requireAuth };
