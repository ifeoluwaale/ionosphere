/**
 * Contains services for express controllers associated with
 * user navigation within the nextJS application.
 */

const app = require("../server");
const url = require("url");

/**
 * Handles when user lands on base url. If user is logged in,
 * they are directed to the home page. Otherwise, they are
 * sent to the login/welcome page.
 *
 * @param {Object} req request
 * @param {Object} res response
 */
const getLanding = (req, res) => {
  if (
    process.env.API_SIMPLE_TOKEN ||
    process.env.DEV_AUTH_TOKEN ||
    req.isAuthenticated()
  )
    app.render(req, res, "/home");
  else app.render(req, res, "/welcome");
};

/**
 * Handles callback from js2 openstack containing unscoped token during
 * credential creation wizard.
 *
 * @param {Object} req request - body contains unscoped token
 * @param {Object} res response
 *
 * @returns redirects user back to credential wizard if POST request
 * is from openstack, otherwise returns 403
 */
const openstackCallbackHandler = (req, res) => {
  const referer = req.headers.referer;

  // if POST request came from openstack, save token in cookie
  // and redirect user to credential wizard
  if (referer === "https://js2.jetstream-cloud.org:5000/") {
    let options = {
      maxAge: 1000 * 60 * 15, // expires after 15 minutes
      httpOnly: true, // cookie is only accessible by the web server
      secure: true, // httpsonly
    };

    // Set secure httponly cookie
    res.cookie("openstackToken", req.body.token, options);

    // redirect user to credential wizard
    return res.redirect(
      url.format({
        pathname: "/credentials",
        query: { provider: "Jetstream 2" },
      })
    );

    // otherwise, referrer is unauthorized to make POST request
  } else {
    return res.status(403).end();
  }
};

module.exports = {
  getLanding,
  openstackCallbackHandler,
};
