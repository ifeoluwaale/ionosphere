/*
 * Cacao API Client Adapter
 *
 * Could someday be turned into an npm package for external users.
 *
 * OpenAPI Specification: https://gitlab.com/cyverse/nafigos/-/blob/master/docs/openapi/openapi.yaml
 *
 */

const axios = require("axios");
const qs = require("qs");

class Cacao {
  constructor(params) {
    this.baseUrl = params ? params.baseUrl : "/api";
    this.token = params ? params.token : null;
    this.debug = params ? params.debug : false;
  }

  request(options) {
    if (!options.headers) options.headers = {};
    options.headers["Content-Type"] = "application/json";
    if (this.token) options.headers["Authorization"] = this.token;

    options.timeout = 60 * 1000;

    // Add custom parameter serializer to encode spaces with %20 instead of '+'
    options.paramsSerializer = (params) => qs.stringify(params);

    if (this.debug)
      console.log(`axios request: ${options.method} ${options.url}`);
    return axios.request(options);
  }

  async get(path, params) {
    // params can contain optional "offset" & "limit" properties
    const res = await this.request({
      method: "get",
      url: `${this.baseUrl}${path}`,
      params,
    });
    return res.data;
  }

  async put(path, data) {
    const res = await this.request({
      method: "put",
      url: `${this.baseUrl}${path}`,
      data,
    });
    return res.data;
  }

  async post(path, data, params) {
    const res = await this.request({
      method: "post",
      url: `${this.baseUrl}${path}`,
      data,
      params,
    });
    return res.data;
  }

  async patch(path, data) {
    const res = await this.request({
      method: "patch",
      url: `${this.baseUrl}${path}`,
      data,
    });
    return res.data;
  }

  async delete(path) {
    const res = await this.request({
      method: "delete",
      url: `${this.baseUrl}${path}`,
    });
    return res.data;
  }

  /*
   * User endpoints
   */

  async users(params) {
    return await this.get(`/users`, params);
  }

  async createUser(params) {
    return await this.post(`/users`, params);
  }

  async user(username) {
    return await this.get(`/users/${username || "mine"}`);
  }

  async updateUser(username) {
    return await this.patch(`/users/${username || "mine"}`);
  }

  async deleteUser(username) {
    return await this.delete(`/users/${username}`);
  }

  /*
   * Workspace endpoints
   */

  async workspaces(params) {
    return await this.get(`/workspaces`, params);
  }

  async createWorkspace(params) {
    return await this.post(`/workspaces`, params);
  }

  async workspace(id) {
    return await this.get(`/workspaces/${id}`);
  }

  async updateWorkspace(id, params) {
    return await this.patch(`/workspaces/${id}`, params);
  }

  async deleteWorkspace(id) {
    return await this.delete(`/workspaces/${id}`);
  }

  async workspaceDeployments(id) {
    return await this.get(`/workspaces/${id}/deployments`);
  }

  async workspaceShares(id, params) {
    return await this.get(`/workspaces/${id}/shares`, params);
  }

  /*
   * Deployment endpoints
   */

  async deployments(params) {
    return await this.get(`/deployments`, params);
  }

  async createDeployment(params) {
    return await this.post(`/deployments`, params);
  }

  async deployment(id) {
    return await this.get(`/deployments/${id}`);
  }

  async deploymentDeletionLogs(id) {
    return await this.get(`/deployments/${id}/logs/deletion`);
  }

  async updateDeployment(id, params) {
    return await this.patch(`/deployments/${id}`, params);
  }

  async deleteDeployment(id) {
    return await this.delete(`/deployments/${id}`);
  }

  async deploymentBuilds(id, params) {
    return await this.get(`/deployments/${id}/builds`, params);
  }

  async createDeploymentBuild(id, params) {
    return await this.post(`/deployments/${id}/builds`, params);
  }

  async deploymentBuild(deploymentId, buildId) {
    return await this.get(`/deployments/${deploymentId}/builds/${buildId}`);
  }

  async deploymentRuns(id, params) {
    return await this.get(`/deployments/${id}/runs`, params);
  }

  async createDeploymentRun(id, params) {
    return await this.post(`/deployments/${id}/runs`, params);
  }

  async deploymentRun(deploymentId, runId) {
    return await this.get(`/deployments/${deploymentId}/runs/${runId}`);
  }

  async deploymentRunLogs(deploymentId, runId, logsType) {
    return await this.get(
      `/deployments/${deploymentId}/runs/${runId}/logs/${logsType}`
    );
  }

  async deploymentVolumeAttach(deploymentId, volumeId) {
    return await this.post(
      `/deployments/${deploymentId}/volume_attach/${volumeId}`
    );
  }

  async deploymentVolumeDetach(deploymentId, volumeId) {
    return await this.post(
      `/deployments/${deploymentId}/volume_detach/${volumeId}`
    );
  }

  async version() {
    return await this.get(`/version`);
  }

  /*
   * Volume endpoints
   */

  async volumes(params) {
    return await this.get(`/volumes`, params);
  }

  async createVolume(params) {
    return await this.post(`/volumes`, params);
  }

  async volume(id) {
    return await this.get(`/volumes/${id}`);
  }

  async updateVolume(id, params) {
    return await this.patch(`/volumes/${id}`, params);
  }

  async deleteVolume(id) {
    return await this.delete(`/volumes/${id}`);
  }

  /*
   * Provider endpoints
   */

  async providers(params) {
    return await this.get(`/providers`, params);
  }

  async createProvider(params) {
    return await this.post(`/providers`, params);
  }

  async provider(id) {
    return await this.get(`/providers/${id}`);
  }

  async updateProvider(id, params) {
    return await this.patch(`/providers/${id}`, params);
  }

  async deleteProvider(id) {
    return await this.delete(`/providers/${id}`);
  }

  /**
   * list images
   * @param {string} id provider ID
   * @param {Object} params, query parameter to pass along, require "region"
   */
  async providerImages(id, params) {
    return await this.get(`/providers/${id}/images`, params).then((images) => {
      // filter out windows images
      if (!Array.isArray(images) || !images.length) {
        return [];
      }
      let result = [];
      for (const i in images) {
        const img = images[i];
        if (img.name && !img.name.toLowerCase().includes("windows")) {
          result.push(img);
        }
      }
      return result;
    });
  }

  async providerImage(providerId, imageId, param) {
    return await this.get(`/providers/${providerId}/images/${imageId}`, param);
  }

  /**
   * list flavors
   * @param {string} id provider ID
   * @param {Object} params, query parameter to pass along, require "region"
   */
  async providerFlavors(id, params) {
    return await this.get(`/providers/${id}/flavors`, params);
  }

  async providerProjects(id, params) {
    return await this.get(`/providers/${id}/projects`, params);
  }

  async providerRegions(id, params) {
    return await this.get(`/providers/${id}/regions`, params);
  }

  /*
   * Openstack endpoints (use unscoped token)
   */

  async openstackProjects(id, params) {
    return await this.get(`/providers/${id}/projects`, params);
  }

  async openstackCredentials(id, params) {
    return await this.get(`/providers/${id}/applicationCredentials`, params);
  }

  async createOpenstackCredential(id, data, params) {
    return await this.post(
      `/providers/${id}/applicationCredentials`,
      data,
      params
    );
  }

  async deleteOpenstackCredential(providerId, credentialId) {
    return await this.delete(
      `/providers/${providerId}/applicationCredentials/${credentialId}`
    );
  }

  /*
   * Projects endpoints
   */

  async js2projects(params) {
    return await this.get(`/js2projects`, params);
  }

  async js2allocations(params) {
    return await this.get(`/js2projects`, params);
  }

  /*
   * Credential endpoints
   */

  async credentials(params) {
    return await this.get(`/credentials`, params);
  }

  async createCredential(params) {
    return await this.post(`/credentials`, params);
  }

  async credential(id) {
    return await this.get(`/credentials/${id}`);
  }

  async updateCredential(id, params) {
    return await this.patch(`/credentials/${id}`, params);
  }

  async deleteCredential(id) {
    return await this.delete(`/credentials/${id}`);
  }

  async credentialTypes() {
    return await this.get(`/credential_types`);
  }

  /*
   * Template endpoints
   */

  async templates(params) {
    return await this.get(`/templates`, params);
  }

  async createTemplate(params) {
    return await this.post(`/templates`, params);
  }

  async template(id) {
    return await this.get(`/templates/${id}`);
  }

  async updateTemplate(id, params) {
    return await this.patch(`/templates/${id}`, params);
  }

  async deleteTemplate(id) {
    return await this.delete(`/templates/${id}`);
  }

  async templateTypes(params) {
    return await this.get(`/templates/types`, params);
  }

  async templateType(name) {
    return await this.get(`/templates/types/${name}`);
  }

  /*
   * Interactive Session endpoints
   */

  async isessions(params) {
    return await this.get(`/isessions`, params);
  }

  async createISession(params) {
    return await this.post(`/isessions`, params);
  }

  async isession(id) {
    return await this.get(`/isessions/${id}`);
  }

  async deleteISession(id) {
    return await this.delete(`/isessions/${id}`);
  }

  /*
   * API Token endpoints
   */
  async createToken(params) {
    return await this.post(`/tokens`, params);
  }
  async getTokens() {
    return await this.get("/tokens");
  }
  async getToken(id) {
    return await this.get(`/tokens/${id}`);
  }
  async getTokenTypes() {
    return await this.get("/tokens/types");
  }
  async updateToken(id, params) {
    return await this.patch(`/tokens/${id}`, params);
  }
  async revokeToken(id) {
    return await this.post(`/tokens/${id}/revoke`);
  }
  async deleteToken(id) {
    return await this.delete(`/tokens/${id}`);
  }

  /*
   * Utility functions
   */
  errorMessage(err) {
    return err?.response?.data?.message || err?.message || err;
  }
}

module.exports = Cacao;
