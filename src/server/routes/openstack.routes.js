const express = require("express");
const bodyParser = require("body-parser");
const auth = require("../auth");

const openstackControllers = require("../controllers/openstack.controllers");

const router = express.Router();

// Get openstack projects
router.get(
  "/get-projects",
  bodyParser.json(),
  auth.requireAuth,
  openstackControllers.getProjects
);

// Create openstack credentials
router.get(
  "/create-credentials",
  bodyParser.json(),
  auth.requireAuth,
  openstackControllers.createCredentials
);

module.exports = router;
