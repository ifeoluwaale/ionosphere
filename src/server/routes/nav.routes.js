const express = require("express");
const bodyParser = require("body-parser");
const auth = require("../auth");

const navControllers = require("../controllers/nav.controllers");

const router = express.Router();

// Handle when user lands on base url
router.get("/", bodyParser.json(), navControllers.getLanding);

// Handle POST request from openstack containing unscoped token
router.post(
  "/",
  bodyParser.urlencoded({ extended: true }),
  navControllers.openstackCallbackHandler
);

// Public routes
router.get(
  ["/_next/*", "/*.(svg|ico|png|gif|jpg)", "/welcome"],
  bodyParser.json(),
  navControllers.getPublicRoutes
);

// Private routes (authenticated required)
router.get(
  "*",
  bodyParser.json(),
  auth.requireAuth,
  navControllers.getPrivateRoutes
);

module.exports = router;
