import React from "react";

const CloudsContext = React.createContext();
CloudsContext.displayName = "Clouds";

function useClouds() {
  const context = React.useContext(CloudsContext);
  if (!context) {
    throw new Error(`useClouds must be used within a CloudsProvider`);
  }
  return context;
}

//FIXME
// function withClouds(WrappedComponent) {
//   return (
//     <WrappedComponent clouds={useClouds()} {...this.props} />
//   )
// }

function CloudsProvider(props) {
  const [clouds, setClouds] = React.useState(props.clouds);
  const value = React.useMemo(() => [clouds, setClouds], [clouds]);
  return <CloudsContext.Provider value={value} {...props} />;
}

export { CloudsProvider, useClouds };
