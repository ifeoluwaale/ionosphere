import { createContext, useContext, useState, useMemo } from "react";

const WorkspacesContext = createContext();
WorkspacesContext.displayName = "Workspaces";

function useWorkspaces() {
  const context = useContext(WorkspacesContext);
  if (!context) {
    throw new Error(`useWorkspaces must be used within a WorkspacesProvider`);
  }
  return context;
}

function WorkspacesProvider(props) {
  const [workspaces, setWorkspaces] = useState(props.workspaces);
  const value = useMemo(() => [workspaces, setWorkspaces], [workspaces]);
  return <WorkspacesContext.Provider value={value} {...props} />;
}

export { WorkspacesProvider, useWorkspaces };
