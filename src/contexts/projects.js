import React from "react";

const ProjectsContext = React.createContext();
ProjectsContext.displayName = "Projects";

function useProjects() {
  const context = React.useContext(ProjectsContext);
  if (!context) {
    throw new Error(`useProjects must be used within a ProjectsProvider`);
  }
  return context;
}

function ProjectsProvider(props) {
  const [projects, setProjects] = React.useState(props.projects);
  const value = React.useMemo(() => [projects, setProjects], [projects]);
  return <ProjectsContext.Provider value={value} {...props} />;
}

export { ProjectsProvider, useProjects };
