import { createTheme } from "@mui/material/styles";
import { blue } from "@mui/material/colors";
let theme = createTheme({
  typography: {
    fontFamily: ["Inter", "sans-serif"].join(","),
  },
  palette: {
    primary: {
      main: blue[900],
    },
    secondary: {
      main: "#CD491A",
    },
    success: {
      main: "#078063",
    },
    info: {
      main: "#86B3D1",
    },
    warning: {
      main: "#ffa726",
    },
    error: {
      main: "#f45d48",
    },
    sidebar: {
      background: "#eaddcf",
      itemColor: "#5c4033de",
      itemBackground: "#eaddcf",
      selectedItemColor: "#CD491A",
      selectedItemBackground: "#FAF6F2",
    },
    appBar: {
      background: "#ffffff",
      userAvatar: "#5c4033de",
    },
    background: {
      default: "#FAF6F2",
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          boxShadow: "none",
          "&:hover": {
            boxShadow: "none",
          },
          "&:active": {
            boxShadow: "none",
          },
        },
      },
    },
    MuiAvatar: {
      styleOverrides: {
        colorDefault: {
          color: "#ffffff",
          background: "#987654",
        },
      },
    },
    MuiCard: {
      defaultProps: {
        elevation: 0,
      },
      styleOverrides: {
        root: {
          border: ".5px solid rgb(0 0 0 / 10%)",
          borderRadius: "6px",
          boxShadow:
            "rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px",
        },
      },
    },
    MuiMenu: {
      defaultProps: {
        PaperProps: {
          variant: "outlined",
          elevation: 0,
          style: {
            boxShadow: "none",
          },
        },
      },
    },
    MuiAppBar: {
      styleOverrides: {
        root: {
          backgroundColor: "#ffffff",
          color: "#000000",
          borderBottom: "1px solid #e0e0e0",
        },
      },
      defaultProps: {
        elevation: 0,
      },
    },
  },
});

theme = createTheme(theme, {
  palette: {
    raspberry: theme.palette.augmentColor({
      color: {
        main: "#c74073",
      },
      name: "raspberry",
    }),
    mint: theme.palette.augmentColor({
      color: {
        main: "#3EB489",
      },
      name: "mint",
    }),
  },
});

export default theme;
