import dessertWords from "@/data/dessertWords";

// Set default image and flavor
const DEFAULT_IMAGE = "Featured-Ubuntu22";
const DEFAULT_FLAVOR = "m3.tiny";

/**
 * Generates a random deployment name by combining two unique words.
 *
 * @returns {String} - A string representing a random deployment name,
 * composed of two words separated by a hyphen.
 */
export const getRandomDeploymentName = () => {
  const randomWord1 = getRandomUniqueWord();
  const randomWord2 = getRandomUniqueWord();

  function getRandomUniqueWord() {
    let randomWord;
    let isUnique = false;

    while (!isUnique) {
      randomWord =
        dessertWords[Math.floor(Math.random() * dessertWords.length)];

      // Check if the random word is unique
      if (randomWord !== randomWord1 && randomWord !== randomWord2) {
        isUnique = true;
      }
    }

    return randomWord;
  }

  return [randomWord1, randomWord2].join("-");
};

/**
 * Recursive function to flatten one "step" from CACAO template.ui_metadata.
 *
 * @param {Object} step - step in CACAO template.ui_metadata
 * @returns {Array} - Array of fields.
 */
const flattenStep = (step) => {
  // Array to hold the flattened fields
  let fields = [];

  // Recursive internal function to handle the flattening
  function internalFlatten(currentItem) {
    // If current item is a field, add it to the flat list
    if (currentItem.name && currentItem.ui_label) {
      fields.push(currentItem);
    }

    // If this item has more items nested, recurse on each of them
    if ("items" in currentItem) {
      currentItem.items.forEach((item) => {
        internalFlatten(item);
      });
    }
  }
  // Check if the step has 'items' and start the recursion if it does
  if ("items" in step) {
    step.items.forEach((item) => {
      internalFlatten(item);
    });
  }

  return fields;
};

/**
 * CACAO template ui_metadata schema allows for the nesting of fields within steps, rows,
 * and other groupings.
 *
 * This function recursively flattens the ui_metadata to get a list of all fields
 * from all steps.
 *
 * @param {Array} steps CACAO template ui_metadata.steps
 * @returns {Array} flattened list of fields
 */
export const flattenUIMetadata = (steps) => {
  const fields = [];

  // from ui_metadata.steps, get all subitems
  steps.forEach((step) => {
    const items = flattenStep(step);
    items.forEach((item) => {
      fields.push(item);
    });
  });

  return fields;
};

/**
 * Given a field from template.ui_metadata, find the matching entry in
 * template.metadata.parameters.
 * @param {*} fieldName Name of parameter.
 * @param {Object} metadata CACAO template.metadata.parameters list
 * @returns Metadata for parameter.
 */
const getFieldMetadata = (fieldName, parameters) => {
  return parameters.find((p) => p.name === fieldName);
};

/**
 * Pulling from both template metadata and ui_metadata, derives list of fields with
 * consolidated attributes needed to render form.
 * @param {Object} template
 * @returns {Array} List of fields.
 */

export const getFormFieldsFromTemplate = (template) => {
  // Flatten template.ui_metadata.steps into a list of fields
  // At this point, each field has a `name` and `ui_label` only.
  const fields = flattenUIMetadata(template.ui_metadata.steps);

  /**
   * Filter out fields that should not be displayed on the form.
   *
   * - power_state is only shown on edit
   * - region is displayed in cloud/provider/region menu instead
   */
  const formFields = fields.filter((f) => f.name !== "region");

  formFields.forEach((field) => {
    // get metadata for UI field
    const metadata = getFieldMetadata(field.name, template.metadata.parameters);

    // set field type
    field.type = metadata.type;

    // if a default value is provided, use it
    if (metadata.default) {
      field.default = metadata.default;
    }

    // TODO: set following based on template metadata (not present currently)
    field.required = true;
    if (field.name === "instance_count") {
      field.min = 1;
    }
    if (field.name === "instance_name") {
      field.max_length = 32;
    }
  });

  return formFields;
};

/**
 * Initialize form values from template metadata.
 * @param {*} template
 */
export const initializeFormValuesFromTemplate = (template, images, flavors) => {
  // get flattened list of form fields from template
  const formFields = getFormFieldsFromTemplate(template);

  let initialFormValues = {};

  // iterate over form fields and set initial value based on param metadata
  formFields.forEach((field) => {
    // if a default value is provided, use it
    if (field.default) {
      initialFormValues[field.name] = field.default;
    }

    // instance_name is a special case, generate a random name
    else if (field.name === "instance_name") {
      initialFormValues["instance_name"] = getRandomDeploymentName();
    }

    // otherwise, set initial value based on param type
    else {
      switch (field.type) {
        case "cacao_provider_image_name":
          // set to default image if available, otherwise first image in list
          const default_image = images.find(
            (i) => i.name === DEFAULT_IMAGE
          ).name;
          initialFormValues[field.name] = default_image
            ? default_image
            : images[0].name;
          break;
        case "cacao_provider_flavor":
          // set to default flavor if available, otherwise first flavor in list
          const default_flavor = flavors.find(
            (f) => f.name === DEFAULT_FLAVOR
          ).name;
          initialFormValues[field.name] = default_flavor
            ? default_flavor
            : flavors[0].name;
          break;
        case "integer":
          initialFormValues[field.name] = 0;
          break;
        case "string":
          initialFormValues[field.name] = "";
          break;
        default:
          initialFormValues[field.name] = null;
      }
    }
  });

  return initialFormValues;
};
