import { FormControl, TextField } from "@mui/material";
import { useContext } from "react";
import { FormikContext } from "../contexts/FormikContext";

export default function IntegerField({ field }) {
  const formik = useContext(FormikContext);

  return (
    <FormControl variant="outlined" fullWidth>
      <TextField
        id={field.name}
        name={field.name}
        label={field.ui_label}
        required={field.required}
        type="number"
        min={field.min ? field.min : undefined}
        max={field.max ? field.max : undefined}
        step={1}
        value={
          formik.values[field.name] || formik.values[field.name] === 0
            ? formik.values[field.name]
            : ""
        }
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        error={!!formik.errors[field.name]}
        helperText={formik.errors[field.name]}
        variant="outlined"
      />
    </FormControl>
  );
}
