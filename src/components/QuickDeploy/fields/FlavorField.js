import {
  Box,
  Chip,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Typography,
} from "@mui/material";
import { useContext } from "react";
import { FormikContext } from "../contexts/FormikContext";

export default function FlavorField({ field, flavors }) {
  const formik = useContext(FormikContext);

  return (
    <FormControl variant="outlined" fullWidth>
      <InputLabel required={field.required}>{field.ui_label}</InputLabel>
      <Select
        id={field.name}
        name={field.name}
        label={field.ui_label}
        required={field.required}
        value={formik.values[field.name] || ""}
        onChange={formik.handleChange}
        renderValue={(option) => (
          <>
            {option}
            {option.startsWith("g3") && (
              <Chip
                size="small"
                label="GPU"
                style={{ marginLeft: 5, marginTop: "-0.45em" }}
              />
            )}
          </>
        )}
      >
        {flavors.map((flavor, index) => (
          <MenuItem key={index} value={flavor.name}>
            <Box display="flex" alignItems="center">
              <Typography style={{ fontWeight: "bold", width: "8em" }}>
                {flavor.name}
              </Typography>
              <Box style={{ fontWeight: "bold", width: "3.5em" }}>
                {flavor.name.startsWith("g3") && (
                  <Chip size="small" label="GPU" />
                )}
              </Box>
              <Typography variant="body2" style={{ width: "4.375rem" }}>
                vCPU: {flavor.vcpus}
              </Typography>
              <Typography variant="body2" style={{ width: "5.9375rem" }}>
                RAM: {flavor.ram}
              </Typography>
              <Typography variant="body2" style={{ width: "3.75rem" }}>
                Disk: {flavor.disk}
              </Typography>
            </Box>
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}
