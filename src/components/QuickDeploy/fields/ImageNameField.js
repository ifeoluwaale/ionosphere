import {
  Chip,
  FormControl,
  ListItem,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import Autocomplete, { createFilterOptions } from "@mui/material/Autocomplete";
import { useContext, useMemo } from "react";
import { FormikContext } from "../contexts/FormikContext";

export default function ImageNameField({ field, images }) {
  const formik = useContext(FormikContext);

  // get selected image object from image name
  const selected = useMemo(
    () => images.find((image) => image.name === formik.values[field.name]),
    [formik.values[field.name]]
  );

  return (
    <FormControl variant="outlined" fullWidth>
      <Autocomplete
        id={field.name}
        name={field.name}
        value={selected || images[0]}
        options={images}
        getOptionLabel={(option) => option.name}
        onChange={(e, v) => {
          formik.setFieldValue(field.name, v?.name || "");
        }}
        onBlur={() => formik.setFieldTouched(field.name, true)}
        renderInput={(params) => {
          return (
            <TextField
              {...params}
              label={field.ui_label}
              variant="outlined"
              required={field.required}
            />
          );
        }}
        renderOption={(props, option) => (
          <ListItem key={option.name} {...props} sx={{ display: "flex" }}>
            <Typography>{option.name}</Typography>
            {option.tags?.length > 0 && (
              <Stack direction="row" spacing={2} marginLeft={2}>
                {option.tags
                  .sort((a, b) =>
                    a.toLowerCase().localeCompare(b.toLowerCase())
                  )
                  .map((tag, index) => (
                    <Chip
                      key={option.name + index}
                      label={tag}
                      size="small"
                      style={{
                        fontSize: "0.7rem",
                        margin: "2px 2px",
                      }}
                    />
                  ))}
              </Stack>
            )}
          </ListItem>
        )}
        filterOptions={createFilterOptions({
          stringify: (option) =>
            option.name +
            (option.tags?.length > 0 ? " " + option.tags.join(" ") : ""),
        })}
      />
    </FormControl>
  );
}
