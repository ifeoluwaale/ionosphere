/**
 * Actions File
 * ------------
 * This file contains action creators for Quick Deploy. Action creators are functions
 * that return action objects, which can be dispatched to a reducer to update state.
 *
 * Each action creator should be exported and can then be used wherever actions need to
 * be dispatched from.
 */

import {
  SET_CLOUD_ID,
  SET_PROJECT_ID,
  SET_REGION_ID,
  SET_IMAGES,
  SET_FLAVORS,
  SET_FIELD_LIST,
  SET_INITIAL_FORM_VALUES,
  SET_VALIDATION_SCHEMA,
} from "./actionTypes";

export const setCloudId = (payload) => ({
  type: SET_CLOUD_ID,
  payload,
});

export const setProjectId = (payload) => ({
  type: SET_PROJECT_ID,
  payload,
});

export const setRegionId = (payload) => ({
  type: SET_REGION_ID,
  payload,
});

export const setImages = (payload) => ({
  type: SET_IMAGES,
  payload,
});

export const setFlavors = (payload) => ({
  type: SET_FLAVORS,
  payload,
});

export const setFormFields = (payload) => ({
  type: SET_FIELD_LIST,
  payload,
});

export const setValidationSchema = (payload) => ({
  type: SET_VALIDATION_SCHEMA,
  payload,
});

export const setInitialFormValues = (payload) => ({
  type: SET_INITIAL_FORM_VALUES,
  payload,
});
