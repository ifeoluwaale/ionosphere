/**
 * Action Types File
 * -----------------
 * This file contains the action types that can be dispatched to update Quick Deploy state.
 * Action types are constants that give a name to our actions so that they can be called
 * consistently throughout the app.
 *
 * Using action types ensures that reducers can identify actions and handle them accordingly.
 * Each action type should be exported and then can be used in corresponding action creators
 * and reducers.
 */

export const SET_CLOUD_ID = "SET_CLOUD_ID";
export const SET_PROJECT_ID = "SET_PROJECT_ID";
export const SET_REGION_ID = "SET_REGION_ID";
export const SET_IMAGES = "SET_IMAGES";
export const SET_FLAVORS = "SET_FLAVORS";
export const SET_FIELD_LIST = "SET_FIELD_LIST";
export const SET_INITIAL_FORM_VALUES = "SET_INITIAL_FORM_VALUES";
export const SET_VALIDATION_SCHEMA = "SET_VALIDATION_SCHEMA";
