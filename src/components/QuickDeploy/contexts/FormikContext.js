/**
 * Custom FormikContext
 * --------------------
 *
 * The QuickDeploy form uses a custom Formik Context instead of the built-in
 * Formik context to allow cross-component communication.
 *
 * In this case, the <form> is located in DialogContent, while the submit button
 * is located the DialogActions, so the submit button is located outside the
 * form. This custom Formik context allows the submit button to trigger form
 * submission from outside the <form>.
 *
 * Wrapping both the DialogContent & DialogActions in the built-in Formik context
 * did not work, presumably due to the asynchronous form initialization process.
 */

import React from "react";

const FormikContext = React.createContext();

const FormikContextProvider = ({ value, children }) => {
  return (
    <FormikContext.Provider value={value}>{children}</FormikContext.Provider>
  );
};

export { FormikContext, FormikContextProvider };
