import { createContext, useContext, useEffect, useReducer } from "react";
import { object } from "yup";

import { useCredential } from "@/hooks";

import quickDeployReducer from "../reducers/quickDeployReducer";
import { setFormFields, setValidationSchema } from "../actions/actions";
import { getFormFieldsFromTemplate } from "../utils/initializeForm";
import generateYupSchema from "../utils/validationSchema";

export const QuickDeployContext = createContext(null);
export const QuickDeployDispatchContext = createContext(null);

/**
 * QuickDeployProvider is a React Context that provides the current
 * state for Quick Deploy.
 *
 * Quick Deploy wizard state is managed with a React reducer.
 *
 * @param {Object} value (see below for params)
 * @param {JSX.Element} children
 * @returns Deployment wizard state
 */

export function QuickDeployProvider({ children, initialValues }) {
  // initialize state from initialValues prop, or use defaults
  const {
    cloudId = "",
    projectId = "",
    template = null,
    regionId = "",
    formValues = {},
  } = initialValues;

  const initialState = {
    cloudId,
    projectId,
    template,
    regionId,
    formValues,
  };

  // initialize QuickDeploy reducer
  const [state, dispatch] = useReducer(quickDeployReducer, initialState);

  // get credential ID for selected cloud & project
  const credentialId = useCredential(state.cloudId, state.projectId);

  /**
   * When a template is selected, generate the list of form fields &
   * yup validation schema & update Quick Deploy state.
   */
  useEffect(() => {
    if (state.template) {
      const fields = getFormFieldsFromTemplate(state.template);
      const schema = generateYupSchema(fields);

      dispatch(setValidationSchema(object().shape(schema)));
      dispatch(setFormFields(fields));
    }
  }, [state.template]);

  return (
    <QuickDeployContext.Provider
      value={{ ...state, credentialId: credentialId }}
    >
      <QuickDeployDispatchContext.Provider value={dispatch}>
        {children}
      </QuickDeployDispatchContext.Provider>
    </QuickDeployContext.Provider>
  );
}

/**
 * React hook to access Quick Deploy wizard state.
 */
export function useQuickDeploy() {
  return useContext(QuickDeployContext);
}

/**
 * React hook to access dispatch function for updating
 * Quick Deploy wizard state.
 */
export function useQuickDeployDispatch() {
  return useContext(QuickDeployDispatchContext);
}
