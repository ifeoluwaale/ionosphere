/**
 * DialogSteps Component
 * ---------------------
 * 
 * This component orchestrates the step-by-step logic for the QuickDeploy dialog,
 * which includes a credential check, the Quick Deploy form, and a success message.
 *
 * If the user does not have a cloud credential or SSH key, an interface to add an
 * SSH key and credential is displayed. Otherwise, the user is taken directly to the
 * Quick Deploy form. Upon successful submission, the `QuickDeploySuccess`
 * message is displayed.

 * Props:
 * - `handleClose`: Function to call when the dialog should be closed.
 * - `templateType`: Template type (e.g. openstack_terraform).
 */

import { useRef, useState } from "react";
import { Button, DialogActions, DialogContent, Stack } from "@mui/material";
import { useRouter } from "next/router";
import Link from "next/link";

import { useCredentials } from "@/contexts";
import DeployPrerequisiteForm from "@/components/forms/DeployPrerequisiteForm";
import QuickDeployForm from "./QuickDeployForm";
import DeploymentConfirmation from "../../Deployment/DeploymentConfirmation";
import { findCredentialForTemplate } from "@/utils";

export default function DialogSteps({ handleClose, templateType }) {
  const [credentials] = useCredentials();
  const router = useRouter();

  // Check if user has credential for selected template
  const hasCredential = useRef(
    findCredentialForTemplate(templateType, credentials) !== null
  );

  // Check if user has SSH key
  const hasSSHKey = useRef(
    credentials.filter((cred) => cred.type === "ssh").length > 0
  );

  /**
   * If users don't have a credential or SSH key, start at the credential step.
   * Otherwise, start at the QuickDeploy form.
   */
  const startStep = useRef(
    !hasCredential.current || !hasSSHKey.current ? 0 : 1
  );

  const [activeStep, setActiveStep] = useState(startStep.current);

  return (
    <>
      {activeStep === 0 && (
        <>
          <DialogContent>
            <Stack>
              <DeployPrerequisiteForm />
            </Stack>
          </DialogContent>
          <DialogActions>
            <Button
              variant="outlined"
              onClick={() => setActiveStep(1)}
              disabled={!hasCredential.current}
            >
              Continue
            </Button>
          </DialogActions>
        </>
      )}

      {activeStep === 1 && (
        <QuickDeployForm
          setActiveStep={setActiveStep}
          startStep={startStep}
        ></QuickDeployForm>
      )}

      {activeStep === 2 && (
        <>
          <DialogContent>
            <DeploymentConfirmation />
          </DialogContent>
          <DialogActions>
            <Stack direction="row" spacing={1}>
              <Button variant="outlined" onClick={handleClose}>
                Close
              </Button>
              <Link href="/deployments" passHref>
                <Button
                  variant="contained"
                  color="primary"
                  mr={2}
                  onClick={() => {
                    handleClose();
                    router.push("/deployments");
                  }}
                >
                  View Deployments
                </Button>
              </Link>
            </Stack>
          </DialogActions>
        </>
      )}
    </>
  );
}
