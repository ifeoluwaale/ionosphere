import { Box, Stack } from "@mui/material";

import {
  FlavorField,
  ImageNameField,
  IntegerField,
  ShortTextField,
} from "../fields";

/**
 * Renders a set of dynamic fields for the Quick Deploy form, based on the provided field list.
 * It maps each field type to a specific component, providing the necessary data for each field.
 *
 * @param {Array} props.fieldList - Array of field objects to render, each object includes field type and other properties.
 * @param {Array} props.images - Array of image options for the 'cacao_provider_image_name' field type.
 * @param {Array} props.flavors - Array of flavor options for the 'cacao_provider_flavor' field type.
 * @returns {JSX.Element} - A stack of fields rendered according to their types.
 */
export default function QuickDeployFields({ fieldList, images, flavors }) {
  const renderField = (field) => {
    switch (field.type) {
      case "cacao_provider_image_name":
        return <ImageNameField field={field} images={images} />;
      case "cacao_provider_flavor":
        return <FlavorField field={field} flavors={flavors} />;
      case "integer":
        return <IntegerField field={field} />;
      case "string":
        return <ShortTextField field={field} />;
      default:
        return <></>;
    }
  };

  return (
    <Stack spacing={2}>
      {fieldList.map((field, index) => {
        // power_state is only rendered when editing an existing deployment
        if (field.name !== "power_state") {
          return <Box key={index}>{renderField(field)}</Box>;
        }
      })}
    </Stack>
  );
}
