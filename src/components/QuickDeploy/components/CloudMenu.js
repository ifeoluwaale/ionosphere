/**
 * CloudMenu Component
 * -----------------------
 *
 * This component provides a user interface for selecting a cloud, project,
 * and region from dropdown menus. Dropdown state is managed by the QuickDeploy
 * context.
 *
 * Region defaults to IU.
 *
 * Skeleton screens are displayed while the data is loading, providing a smoother
 * user experience.
 */

import { useEffect } from "react";
import { Box, Grid, Skeleton } from "@mui/material";

import { useClouds, useProjects } from "@/contexts";
import { useRegions } from "@/hooks/useRegions";

import {
  useQuickDeployDispatch,
  useQuickDeploy,
} from "../contexts/QuickDeployContext";
import { setCloudId, setProjectId, setRegionId } from "../actions/actions";
import LabeledDropdownMenu from "@/components/Common/Form/LabeledDropdownMenu";

export default function CloudMenu() {
  // Get clouds and projects from context
  const [clouds] = useClouds();
  const [projects] = useProjects();

  // QuickDeploy context & update functions
  const deployment = useQuickDeploy();
  const dispatch = useQuickDeployDispatch();

  // Get regions for selected cloud and credential
  const [regions] = useRegions(deployment.cloudId, deployment.credentialId);

  // Once regions are loaded, set the default region to IU
  useEffect(() => {
    if (regions.length) {
      const defaultRegion = regions.find((r) => r.id === "IU") || regions[0];
      dispatch(setRegionId(defaultRegion.id));
    }
  }, [regions]);

  return (
    <>
      {!!regions.length ? (
        <Grid container spacing={2}>
          <Grid item sm={4} xs={6}>
            <LabeledDropdownMenu
              label={"Cloud"}
              selectedId={deployment.cloudId}
              options={clouds}
              handleSelect={(selected) => dispatch(setCloudId(selected.id))}
            />
          </Grid>
          <Grid item sm={4} xs={6}>
            <LabeledDropdownMenu
              label={"Project"}
              selectedId={deployment.projectId}
              options={projects.map((p) => {
                return { id: p.title, name: p.title };
              })}
              handleSelect={(selected) => {
                dispatch(setProjectId(selected.id));
                dispatch(setRegionId(""));
              }}
            />
          </Grid>
          <Grid item sm={4} xs={6}>
            <LabeledDropdownMenu
              label={"Region"}
              selectedId={deployment.regionId}
              options={regions?.map((r) => {
                return { id: r.id, name: r.name };
              })}
              handleSelect={(selected) => dispatch(setRegionId(selected.id))}
              // show skeleton if regions are still loading
              displayEmpty
              renderValue={(value) => {
                return value || <Skeleton animation="wave" />;
              }}
            />
          </Grid>
        </Grid>
      ) : (
        <Box spacing={2}>
          <Skeleton variant="rounded" height={75} animation="wave" />
        </Box>
      )}
    </>
  );
}
