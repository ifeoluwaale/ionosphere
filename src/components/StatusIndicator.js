import { ListItemText, Stack, Typography } from "@mui/material";
import {
  Settings as SettingsIcon,
  Error as ErrorIcon,
  Warning as WarningIcon,
  PlayArrow as PlayIcon,
  DeleteSweep as DeleteSweepIcon,
  Stop as StopIcon,
  PowerSettingsNew as PowerIcon,
} from "@mui/icons-material";
import { timeDiffString } from "../utils";

export default function StatusIndicator({ deployment }) {
  let color = "#303030";
  let icon, label;

  const getStatus = (deployment) => {
    let power_state = deployment.current_run.parameters?.find(
      (p) => p.key === "power_state"
    )?.value;
    let pending_status = deployment.pending_status;
    if (pending_status && pending_status !== "none") {
      if (
        pending_status === "creating" &&
        power_state &&
        power_state === "shelved_offloaded"
      )
        return "shelving";
      if (
        pending_status === "creating" &&
        power_state &&
        power_state === "shutoff"
      )
        return "shutting_off";
      return pending_status;
    }
    if (power_state && power_state !== "active") return power_state;
    return deployment.current_status;
  };

  let status = getStatus(deployment);
  switch (status) {
    case "creating":
      color = "#e9692c";
      icon = <SettingsIcon fontSize="small" style={{ fill: color }} />;
      label = "Starting";
      break;
    case "active":
      color = "#388e3c";
      icon = <PlayIcon style={{ fill: color }} />;
      label = `Active (${
        deployment.current_run
          ? timeDiffString(deployment.current_run.start, Date.now())
          : "unknown"
      })`;
      break;
    case "errored":
      color = "#ff0000";
      icon = <ErrorIcon style={{ fill: color }} />;
      label = "Errored";
      break;
    case "creation_errored":
      color = "#f57c00";
      icon = <WarningIcon style={{ fill: color }} />;
      label = "Errored";
      break;
    case "deleting":
      color = "#696969";
      icon = <DeleteSweepIcon fontSize="small" style={{ fill: color }} />;
      label = "Deleting";
      break;
    case "deletion_errored":
      color = "#f57c00";
      icon = <WarningIcon style={{ fill: color }} />;
      label = "Deletion Errored";
      break;
    case "shelving":
      icon = <StopIcon style={{ fill: color }} />;
      label = "Shelving";
      color = "#000";
      break;
    case "shelved_offloaded":
      icon = <StopIcon style={{ fill: color }} />;
      label = "Shelved";
      color = "#303030";
      break;
    case "shutting_off":
      icon = <PowerIcon style={{ fill: color }} />;
      label = "Shutting Off";
      break;
    case "shutoff":
      icon = <PowerIcon style={{ fill: color }} />;
      label = "Shut Off";
      color = "#000";
      break;
    default:
      label = status || "Status unknown";
      color = "#696969";
  }

  return (
    <Stack direction={"row"} spacing={1} alignItems={"center"}>
      {icon}
      <Typography
        variant="overline"
        style={{ fontSize: "0.8rem", fontWeight: 500 }}
        color={color}
      >
        {label}
      </Typography>
    </Stack>
  );
}
