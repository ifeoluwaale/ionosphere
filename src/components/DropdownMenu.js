import React from "react";
import { Button, Typography, Menu, MenuItem } from "@mui/material";
import { ArrowDropDown as DownIcon } from "@mui/icons-material";
/**
 * Simple customizable dropdown menu.
 */
const DropdownMenu = ({
  label,
  header,
  icon,
  options,
  onClick,
  enabled = true,
}) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSelect = (option) => {
    handleClose();
    onClick && onClick(option);
  };

  return (
    <div>
      <Button
        variant="outlined"
        color="primary"
        onClick={handleClick}
        startIcon={icon}
        endIcon={<DownIcon />}
      >
        <Typography style={{ textTransform: "uppercase" }}>{label}</Typography>
      </Button>
      <Menu anchorEl={anchorEl} open={enabled && open} onClose={handleClose}>
        <div sx={{ padding: "1rem", paddingTop: "2rem" }}>
          <MenuItem disabled style={{ textDecoration: "underline" }}>
            {header}
          </MenuItem>
          {options?.length > 0 ? (
            options.map((option, index) => (
              <MenuItem key={index} onClick={() => handleSelect(option)}>
                {option.name}
              </MenuItem>
            ))
          ) : (
            <MenuItem disabled>None</MenuItem>
          )}
        </div>
      </Menu>
    </div>
  );
};

export default DropdownMenu;
