/**
 * CreateApiTokenDialog Component
 * ------------------------------
 *
 * This component is used to display a dialog for creating a new API token.
 *
 * First, token types are fetched from the API. Once the token types are
 * fetched, the CreateApiTokenForm component is rendered.
 */

import { useEffect, useState } from "react";
import { Box } from "@mui/material";

import { useAPI } from "@/contexts";
import IconDialogTitle from "@/components/Common/Dialog/IconDialogTitle";
import PaddedDialog from "@/components/Common/Dialog/PaddedDialog";
import CreateApiTokenForm from "./CreateApiTokenForm";
import FormLoadingSkeleton from "@/components/Common/Form/FormLoadingSkeleton";

export default function CreateApiTokenDialog({ open, handleClose }) {
  const api = useAPI();
  const [loadingTokenTypes, setLoadingTokenTypes] = useState(true);
  const [tokenTypes, setTokenTypes] = useState([]);

  // fetch token type choices from API
  useEffect(() => {
    api.getTokenTypes().then((res) => {
      setTokenTypes(res);
      setLoadingTokenTypes(false);
    });
  }, []);

  return (
    <PaddedDialog
      open={open}
      maxWidth="sm"
      fullWidth
      aria-labelledby="create-api-token"
      PaperProps={{
        sx: {
          minHeight: "500px",
        },
      }}
    >
      <IconDialogTitle
        title="Create API Token"
        handleClose={handleClose}
      ></IconDialogTitle>

      {!loadingTokenTypes && !!tokenTypes.length && (
        <CreateApiTokenForm tokenTypes={tokenTypes} handleClose={handleClose} />
      )}
      {loadingTokenTypes && (
        <Box margin={4}>
          <FormLoadingSkeleton rows={4} />
        </Box>
      )}
    </PaddedDialog>
  );
}
