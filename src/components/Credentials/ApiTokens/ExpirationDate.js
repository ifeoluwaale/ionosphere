/**
 * ExpirationDate Component
 * ------------------------
 *
 * This component is used to display the expiration date of a credential. It shows
 * the date in MM/DD/YY format and visually indicates the urgency of expiration.
 * If the expiration date is within the next 30 days, the date is highlighted in
 * warning color (yellow) and accompanied by a warning icon. If the expiration
 * is within the next 14 days, it is displayed in error color (red) to indicate
 * a more urgent expiration timeline. In case the expiration date has already passed,
 * the text changes to indicate that the credential has expired, also in red.
 *
 * Props:
 *   expirationDate - A date string representing the expiration date of the credential.
 *
 * The component uses dayjs for date manipulation.
 */

import dayjs from "dayjs";
import WarningIcon from "@mui/icons-material/Warning";
import { Box, Typography, useTheme } from "@mui/material";

function ExpirationDate({ expirationDate }) {
  const theme = useTheme();
  const today = dayjs();
  const expiration = dayjs(expirationDate);
  const daysUntilExpiration = expiration.diff(today, "day");

  let color = "inherit"; // Default color
  let displayText = `Expires ${expiration.format("MM/DD/YY")}`;
  let showWarning = false;

  if (expiration.isBefore(today)) {
    // Already expired
    color = theme.palette.error.main;
    displayText = `Expired ${expiration.format("MM/DD/YY")}`;
  } else if (daysUntilExpiration <= 14) {
    // Expires in the next 2 weeks
    color = theme.palette.error.main; // Use error color
    showWarning = true;
  } else if (daysUntilExpiration <= 30) {
    // Expires in the next 30 days
    color = theme.palette.warning.main; // Use warning color
    showWarning = true;
  }

  return (
    <Box display="flex" alignItems="center" sx={{ color }}>
      {showWarning && <WarningIcon sx={{ color }} fontSize="small" />}
      <Typography variant="subtitle" marginLeft={showWarning ? 1 : 0}>
        {displayText}
      </Typography>
    </Box>
  );
}

export default ExpirationDate;
