// Credential Category constants
export const sshCredentialType = "ssh";
export const apiTokenCredentialType = "apiToken";
export const cloudCredentialType = "cloud";

// Cloud credential type constants
export const aws = "aws";
export const openstack = "openstack";

const cloudCredentialTypes = [aws, openstack];

export const credentialCategories = [
  {
    value: cloudCredentialType,
    singular_label: "Cloud Credential",
    plural_label: "Cloud Credentials",
    filter: (c) => cloudCredentialTypes.includes(c.type),
  },
  {
    value: sshCredentialType,
    singular_label: "SSH Key",
    plural_label: "SSH Keys",
    filter: (c) => c.type === sshCredentialType,
  },
  {
    value: apiTokenCredentialType,
    singular_label: "API Token",
    plural_label: "API Tokens",
  },
];
