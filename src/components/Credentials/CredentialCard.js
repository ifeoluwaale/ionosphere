import { useState } from "react";
import {
  Avatar,
  Box,
  Button,
  CardActions,
  CardContent,
  CardHeader,
  Chip,
  Grid,
  Stack,
  Typography,
} from "@mui/material";
import {
  Delete as DeleteIcon,
  Edit as EditIcon,
  VpnKey as CredentialIcon,
} from "@mui/icons-material";
import { apiTokenCredentialType } from "./CredentialTypes";
import HoverCard from "../Common/HoverCard";

import ExpirationDate from "./ApiTokens/ExpirationDate";
import { Date } from "../Common/DateTime";

const CredentialCard = ({
  id,
  name,
  description,
  type,
  created_at,
  updated_at,
  tags_kv,
  expiration,
  onDelete,
  setEditCredentialId,
  credentialCategory,
}) => {
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);

  return (
    <HoverCard
      sx={{ minWidth: "44px" }}
      style={{
        display: "flex",
        flexDirection: "column",
        height: "100%",
        width: "100%",
      }}
    >
      <CardHeader
        avatar={
          <Avatar sx={{ width: "2rem", height: "2rem" }}>
            <CredentialIcon fontSize="small" />
          </Avatar>
        }
        title={
          <Grid container justifyContent="space-between" alignItems="center">
            <Grid item>
              <Typography>{name ? name : id}</Typography>
            </Grid>
            {expiration && (
              <Grid item>
                <ExpirationDate expirationDate={expiration} />
              </Grid>
            )}
          </Grid>
        }
        subheader={
          <Typography variant="caption">
            {type.toUpperCase()}{" "}
            {credentialCategory === apiTokenCredentialType && " API TOKEN"}
          </Typography>
        }
      />
      <CardContent style={{ flexGrow: 1 }}>
        {description && (
          <Box mb={2}>
            <Typography variant="body2" fontWeight="bold">
              Description
            </Typography>
            <Typography variant="body2">{description}</Typography>
          </Box>
        )}
        <Box>
          <Typography variant="body2" fontWeight="bold">
            Tags
          </Typography>
          <CredentialKeyValueTags tags_kv={tags_kv} />
        </Box>
        <Box mt={2}>
          <Typography variant="caption" color="textPrimary">
            Created <Date datetime={created_at} />, last updated{" "}
            <Date datetime={updated_at} />.
          </Typography>
        </Box>
      </CardContent>
      <CardActions>
        <Stack direction={"row"} spacing={1}>
          {setEditCredentialId && (
            <Button
              onClick={() => setEditCredentialId(id)}
              startIcon={<EditIcon />}
            >
              Edit
            </Button>
          )}
          {!showDeleteConfirmation ? (
            <Button
              color="raspberry"
              startIcon={<DeleteIcon />}
              onClick={() => setShowDeleteConfirmation(true)}
            >
              Delete
            </Button>
          ) : (
            <Box display="flex" alignItems="center" pl={2}>
              <Typography>Are you sure?</Typography>
              <Button
                sx={{ marginLeft: "1em" }}
                onClick={(e) => setShowDeleteConfirmation(false) || onDelete(e)}
                color="raspberry"
              >
                Yes
              </Button>
              <Button onClick={() => setShowDeleteConfirmation(false)}>
                Cancel
              </Button>
            </Box>
          )}
        </Stack>
      </CardActions>
    </HoverCard>
  );
};

/**
 * Render key-value tags in a Grid
 * @param {Object} tags_kv Tags that are key value pairs
 * @returns {JSX.Element}
 * @constructor
 */
const CredentialKeyValueTags = ({ tags_kv }) => {
  if (!tags_kv) {
    return <Typography variant="caption">No tags.</Typography>;
  }
  return (
    <Grid container spacing={1} direction="row" mt={1}>
      {Object.keys(tags_kv).map((tag_key, index) => {
        const label =
          tag_key + (tags_kv[tag_key] ? " | " + tags_kv[tag_key] : "");
        return (
          <Grid item key={index}>
            <Chip label={label} size="small" />
          </Grid>
        );
      })}
    </Grid>
  );
};

export default CredentialCard;
