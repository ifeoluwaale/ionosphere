import { Alert, Box, Grid } from "@mui/material";

import CredentialCard from "./CredentialCard";
import { credentialCategories } from "./CredentialTypes";

const CredentialList = ({
  credentials,
  credentialCategory,
  onDelete,
  setEditCredentialId,
}) => {
  return (
    <Box mt={2}>
      {credentials.length > 0 ? (
        <Grid container spacing={3} direction="row" justify="flex-end">
          {credentials.map((credential, index) => (
            <Grid
              item
              key={index}
              xs={12}
              sm={12}
              md={8}
              lg={6}
              xl={4}
              style={{ display: "flex" }}
            >
              <CredentialCard
                style={{ width: "100%" }}
                key={index}
                {...credential}
                onDelete={() => onDelete(credential)}
                setEditCredentialId={setEditCredentialId}
                credentialCategory={credentialCategory}
              />
            </Grid>
          ))}
        </Grid>
      ) : (
        <Box mt={1}>
          <Alert severity="error">
            You do not have any{" "}
            {
              credentialCategories.find((c) => c.value === credentialCategory)
                .plural_label
            }
            . Select "Add Credential" to create one.
          </Alert>
        </Box>
      )}
    </Box>
  );
};

export default CredentialList;
