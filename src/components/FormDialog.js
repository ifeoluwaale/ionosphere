import { useState } from "react";
import {
  isEmail,
  isNumeric,
  isAlphanumeric,
  isLowercase,
  isDate,
  isEmpty,
} from "validator";
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
} from "@mui/material";

const FormDialog = ({ title, open, fields, handleClose, handleSubmit }) => {
  const [values, setValues] = useState({});
  const [errors, setErrors] = useState({});

  const validateField = (field, value) => {
    if ((field.is_required || field.required) && isEmpty(value))
      return "This field is required";
    if (field.type === "email" && !isEmail(value))
      return "A valid email address is required";
    if (field.type === "number" && (!isNumeric(value) || value <= 0))
      return "A valid numeric value is required";
    if (field.type === "date" && !isDate(value))
      return "A valid date value is required";
    if (field.type === "password") {
      const error = validatePassword(value);
      if (error) return error;
    }
    if (field.type === "username") {
      if (value.length < 5)
        return "Usernames must be at least 5 characters long";
      if (value.length > 150)
        return "Usernames must be less than 150 characters long";
      if (!isAlphanumeric(value) || !isLowercase(value))
        return "Usernames must be all lowercase and only contain letters and numbers (a-z, 0-9)";
    }

    return null;
  };

  function validatePassword(value) {
    if (value.length < 10) {
      return "password too short";
    }
    return null;
  }

  const handleChange = (e) => {
    const error = validateField(e.target, e.target.value);
    setErrors({ ...errors, [e.target.id]: error });
    setValues({ ...values, [e.target.id]: e.target.value });
  };

  return (
    <Dialog open={open} onClose={handleClose} fullWidth>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        {fields &&
          fields.map((field, index) => (
            <TextField
              key={index}
              // autoFocus={index === 0}
              margin="normal"
              fullWidth
              error={!!errors[field.id]}
              helperText={errors[field.id]}
              onChange={handleChange}
              {...field}
            />
          ))}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} variant="outlined">
          Cancel
        </Button>
        <Button
          variant="contained"
          color="primary"
          disabled={Object.values(errors).some((e) => e) || !handleSubmit}
          onClick={() => {
            handleClose();
            handleSubmit(values);
          }}
        >
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default FormDialog;
