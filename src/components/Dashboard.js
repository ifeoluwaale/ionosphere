import { useState, useEffect } from "react";
import {
  Alert,
  AlertTitle,
  Box,
  Container,
  CssBaseline,
  IconButton,
  Snackbar,
  Toolbar,
  Tooltip,
  Typography,
} from "@mui/material";

import Image from "next/image";

import useMediaQuery from "@mui/material/useMediaQuery";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import MenuIcon from "@mui/icons-material/Menu";

import { useTheme } from "@mui/material/styles";

import { AppBar, Drawer, DrawerHeader } from "./Common/MiniDrawer";
import NavMenu from "./NavMenu";
import UserMenu from "./UserMenu";
import PageHeader from "./PageHeader";
import { CustomIntercom } from "./CustomIntercom";
import Footer from "./Footer";
import { QuickDeployButton } from "./QuickDeploy/components/QuickDeployAvatar";
import QuickDeployDialog from "./QuickDeploy/components/QuickDeployDialog";

import { useUser, useError, useConfig, useAPI } from "../contexts";

const DRAWER_WIDTH = 240;
const QUICK_DEPLOY_TEMPLATE_NAME = "openstack-single-image";

function Dashboard(props) {
  const theme = useTheme();
  const [user] = useUser();
  const config = useConfig();
  const api = useAPI();

  const [error, setError] = useError();
  const [showQuickDeploy, setShowQuickDeploy] = useState(false);
  const [quickDeployTemplate, setQuickDeployTemplate] = useState([]);
  const [open, setOpen] = useState(false);

  const isSmallScreen = useMediaQuery((theme) => theme.breakpoints.down("sm"));

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (isSmallScreen) {
      setOpen(false);
    }
  }, [isSmallScreen]);

  // Get the default quick deploy template
  const fetchTemplates = async () => {
    const templates = await api.templates();
    const defaultTemplate = templates.find(
      (t) => t.name === QUICK_DEPLOY_TEMPLATE_NAME
    );
    setQuickDeployTemplate(defaultTemplate ? defaultTemplate : null);
  };

  useEffect(() => {
    fetchTemplates();
  }, []);

  const handleQuickDeployClose = () => {
    setShowQuickDeploy(false);
  };

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar
        position="fixed"
        open={open}
        sx={{
          backgroundColor: theme.palette.appBar.background,
          borderBottom: "1px solid #e0e0e0",
        }}
      >
        <Toolbar>
          <IconButton
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: 5,
              ...(open && { display: "none" }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Box
            sx={{ width: "100%" }}
            display="flex"
            justifyContent={"space-between"}
            alignItems="center"
          >
            <Box>
              <Image
                src="/images/cacaoLogo.svg"
                width="100px"
                height="50px"
                priority={true}
              />
            </Box>
            {config.NODE_ENV !== "production" && (
              <Typography color="brown">LOCAL DEVELOPMENT</Typography>
            )}
            {config.CACAO_DEV_MODE === "true" && <div>DEVELOPMENT VERSION</div>}
            {config.INTERCOM_APP_ID && (
              <CustomIntercom
                appId={config.INTERCOM_APP_ID}
                companyId={config.INTERCOM_COMPANY_ID}
              />
            )}
            <Box>
              <Box
                display={"flex"}
                alignContent={"center"}
                justifyContent={"center"}
                alignItems={"center"}
              >
                {quickDeployTemplate && (
                  <Tooltip title="Quick Deploy">
                    <Box>
                      <QuickDeployButton
                        onClick={() => setShowQuickDeploy(true)}
                      />
                    </Box>
                  </Tooltip>
                )}
                <Box>
                  <UserMenu user={user} />
                </Box>
              </Box>
            </Box>
          </Box>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        open={open}
        PaperProps={{
          sx: {
            backgroundColor: theme.palette.sidebar.background,
          },
        }}
      >
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </DrawerHeader>
        <NavMenu showStaff={props.showStaff} open={open} />
      </Drawer>

      {/* page contents */}
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          p: 3,
          width: { sm: `calc(100% - ${DRAWER_WIDTH}px)` },
        }}
      >
        <Toolbar />
        <Container maxWidth="xl">
          <PageHeader
            title={props.title}
            breadcrumbs={props.breadcrumbs}
            parts={props.parts}
            back={props.back}
            actions={props.actions}
          />
          {props.children}
        </Container>
        <Box
          component="footer"
          sx={{
            padding: "4rem",
            marginTop: "auto", // Pushes the footer to the bottom
          }}
        >
          <Footer />
        </Box>
      </Box>

      {/* error display */}
      {error && (
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
          open={!!error}
        >
          <Alert
            elevation={6}
            variant="filled"
            severity="error"
            onClose={() => setError(null)}
          >
            <AlertTitle>Oops! An error occurred:</AlertTitle>
            {error}
          </Alert>
        </Snackbar>
      )}

      {/* quick deploy dialog */}
      {showQuickDeploy && (
        <QuickDeployDialog
          open={true}
          handleClose={handleQuickDeployClose}
          template={quickDeployTemplate}
        />
      )}
    </Box>
  );
}

export default Dashboard;
