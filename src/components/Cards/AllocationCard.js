import React from "react";
import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardHeader,
  LinearProgress,
  Tooltip,
  Typography,
} from "@mui/material";
import { DataUsage as DataUsageIcon } from "@mui/icons-material";
import { ArrowDropUp } from "@mui/icons-material";

const AllocationTicker = ({ percentage }) => {
  return (
    <div>
      <Box sx={{ position: "relative", width: "100%", padding: ".2rem" }}>
        <Box
          sx={{
            position: "absolute",
            top: "0px",
            left: `${percentage}%`,
          }}
        >
          <Box display={"flex"} flexDirection={"column"} alignItems={"center"}>
            <Tooltip
              title="Today"
              slotProps={{
                popper: {
                  modifiers: [
                    {
                      name: "offset",
                      options: {
                        offset: [0, -65],
                      },
                    },
                  ],
                },
              }}
            >
              <ArrowDropUp fontSize="large" />
            </Tooltip>
          </Box>
        </Box>
      </Box>
    </div>
  );
};

const AllocationMessage = ({ remainingTimeString, endDate }) => {
  return (
    <Box
      m={1}
      sx={{ width: "100%" }}
      display={"flex"}
      justifyContent={"center"}
      flexDirection={"column"}
      alignItems={"center"}
    >
      <Box>
        <Typography variant="body2" style={{ textAlign: "center" }}>
          You have {remainingTimeString} left of your current allocation.
        </Typography>
      </Box>
      <Box>
        <Typography variant="caption" style={{ textAlign: "center" }}>
          Allocation expires on {endDate}.
        </Typography>
      </Box>
    </Box>
  );
};

const AllocationProgress = ({
  computeAllocated,
  computeUsed,
  resource,
  index,
}) => {
  function calculatePercentage() {
    return (computeUsed / computeAllocated) * 100;
  }
  let pct = calculatePercentage();
  pct = Math.round(pct);

  let pctWidth = calculatePercentage();
  pctWidth = Math.min(pctWidth, 100);
  pctWidth = Math.round(pctWidth);

  const pctToColor = (pct) => {
    if (pct <= 49) return "primary";
    else if (pct >= 50 && pct < 75) return "warning";
    else if (pct >= 75) return "error";
    else return "primary";
  };

  return (
    <div>
      <Box
        display="flex"
        justifyContent="space-between"
        sx={{ mt: index !== 0 && 2 }}
      >
        <Box display="flex" justifyContent="flex-start">
          {resource.startsWith("jetstream2.indiana") && (
            <Typography variant="caption">CPU</Typography>
          )}
          {resource.startsWith("jetstream2-gpu.indiana") && (
            <Typography variant="caption">GPU</Typography>
          )}
          {resource.startsWith("jetstream2-lm.indiana") && (
            <Typography variant="caption">Large Memory</Typography>
          )}
          {/* { resource.startsWith('jetstream2-storage.indiana') && <Typography variant="caption">Storage</Typography>} */}
        </Box>
        <Box display="flex" justifyContent="flex-end">
          <Typography variant="caption" style={{ marginRight: "10px" }}>
            {Math.round(computeUsed).toLocaleString()} /{" "}
            {Math.round(computeAllocated).toLocaleString()} SUs
          </Typography>
          <Typography variant="caption">{pct}% Used</Typography>
        </Box>
      </Box>
      <LinearProgress
        value={pctWidth}
        variant="determinate"
        color={pctToColor(pct)}
      />
    </div>
  );
};

const AllocationCard = ({ title, description, allocations }) => {
  const start = allocations[0].start;
  const end = allocations[0].end;

  let dateStart = new Date(start);
  let dateEnd = new Date(end);
  let endDate = dateEnd.toDateString();
  // Get current date
  const now = new Date();
  // Difference between the start of the year and now in milliseconds
  const diff = now - dateStart;
  // Total duration of the year in milliseconds
  const yearDuration = dateEnd - dateStart;
  // Number of milliseconds in one day
  const oneDay = 1000 * 60 * 60 * 24;
  // Current day number
  const dayNumber = Math.floor(diff / oneDay) + 1;
  // Total number of days in the year
  const totalDays = Math.floor(yearDuration / oneDay) + 1;
  // Percentage of the year that has passed
  const unroundedPercentage = (dayNumber / totalDays) * 100;
  const percentage = Math.floor(unroundedPercentage);
  // Remaining time in months and days
  const remainingDays = totalDays - dayNumber;
  const remainingMonths = Math.floor(remainingDays / 30.44);
  const remainingDaysInMonth = Math.floor(remainingDays % 30.44);

  const monthString = remainingMonths === 1 ? "month" : "months";
  const dayString = remainingDaysInMonth === 1 ? "day" : "days";

  const remainingTimeString =
    remainingMonths > 0
      ? `${remainingMonths} ${monthString} and ${remainingDaysInMonth} ${dayString}`
      : `${remainingDaysInMonth} ${dayString}`;

  return (
    <Card
      style={{
        display: "flex",
        flexDirection: "column",
        height: "100%",
        width: "100%",
      }}
    >
      <CardHeader
        title={<Typography>{title}</Typography>}
        subheader={description}
        avatar={
          <Avatar sx={{ width: "2rem", height: "2rem" }}>
            <DataUsageIcon fontSize="small" />
          </Avatar>
        }
      />
      <CardContent style={{ flexGrow: 1 }}>
        {allocations
          .filter(
            (alloc) => alloc.resource !== "jetstream2-storage.indiana.xsede.org"
          )
          .map((alloc, index) => (
            <AllocationProgress
              key={index}
              index={index}
              computeAllocated={alloc.compute_allocated}
              computeUsed={alloc.compute_used}
              storageAllocated={alloc.storage_allocated}
              resource={alloc.resource}
            />
          ))}
        <Box>
          <AllocationTicker
            percentage={percentage}
            dateEnd={dateEnd}
            dateStart={dateStart}
          />
        </Box>
      </CardContent>
      <AllocationMessage
        remainingTimeString={remainingTimeString}
        endDate={endDate}
      />
    </Card>
  );
};

export default AllocationCard;
