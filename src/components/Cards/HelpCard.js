import {
  CardHeader,
  CardContent,
  CardActions,
  Typography,
  Avatar,
} from "@mui/material";
import HoverCard from "../Common/HoverCard";

import { blue } from "@mui/material/colors";

const HelpCard = ({
  largeHeader,
  title,
  subtitle,
  description,
  iconUrl,
  icon,
  action,
}) => {
  // use icon or iconUrl but not both

  return (
    <HoverCard>
      <CardHeader
        style={{ height: largeHeader ? "7em" : "5em" }}
        avatar={
          (icon || iconUrl) && (
            <Avatar
              alt={title}
              src={iconUrl}
              variant="circle"
              sx={{ width: "2rem", height: "2rem" }}
            >
              {icon}
            </Avatar>
          )
        }
        title={title}
        subheader={subtitle}
      />
      <CardContent sx={{ minHeight: "6em" }}>
        <Typography variant="body2" color="textPrimary" component="p">
          {description}
        </Typography>
      </CardContent>
      <CardActions>{action}</CardActions>
    </HoverCard>
  );
};

export default HelpCard;
