import React from "react";
import {
  Card,
  Box,
  Grid,
  CardContent,
  CardActions,
  Avatar,
  Button,
  Typography,
} from "@mui/material";
import { Folder as FolderIcon, Cloud as CloudIcon } from "@mui/icons-material";
import { useClouds } from "../../contexts";

export default function WorkspaceCard({ name, default_provider_id }) {
  const [clouds] = useClouds();

  const cloud = clouds.find((c) => c.id === default_provider_id);

  // IF NO DEPLOYMENTS FOUND DISPLAY "NO DEPLOYMENTS ADDED"
  // IF MORE THAN 0 FOUND THEN NEED TO SHOW AMOUNT OF DEPLOYMENTS IN THE WORKSPACE

  return (
    <Card>
      <Box m={2}>
        <Grid
          container
          item
          xs={12}
          justifyContent="space-between"
          alignItems="flex-start"
        >
          <Grid item>
            <Box
              display="flex"
              flexWrap="wrap"
              alignSelf="flex-end"
              alignItems="center"
            >
              <Box mr={2}>
                <Avatar>
                  <FolderIcon />
                </Avatar>
              </Box>
              <Typography
                component="h1"
                variant="h5"
                style={{ paddingRight: "1em" }}
              >
                {name}
              </Typography>
            </Box>
          </Grid>
          {cloud?.name && (
            <Grid item>
              <Box
                display="flex"
                flexWrap="wrap"
                alignSelf="flex-start"
                alignItems="center"
                mt={1}
              >
                <CloudIcon
                  color="primary"
                  fontSize="small"
                  style={{ marginRight: ".5em" }}
                />
                <Typography color="primary" variant="subtitle2">
                  {cloud?.name.toUpperCase() || "<unknown>"}
                </Typography>
              </Box>
            </Grid>
          )}
        </Grid>
      </Box>
      <CardContent>
        <Typography variant="body1">Deployments:</Typography>
        <Typography variant="overline">
          2 running, 1 stopped, 1 errored
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" color="primary">
          View Details
        </Button>
      </CardActions>
    </Card>
  );
}
