import React from "react";
import {
  Card,
  Box,
  Button,
  CardContent,
  CardActions,
  Avatar,
  Typography,
  CardHeader,
} from "@mui/material";
import { StatusIndicator } from "../../components";
import { useClouds, useCredentials } from "../../contexts";
import { mdiRocketLaunch } from "@mdi/js";
import Icon from "@mdi/react";
import dayjs from "dayjs";

export default function DeploymentCard({ deployment }) {
  const [clouds] = useClouds();

  const cloud = clouds.find((c) => c.id === deployment.primary_provider_id);

  const createdDate = dayjs(deployment.created_at).format(
    "MMM D, YYYY, h:mm A"
  );

  return (
    <Card sx={{ height: "98%" }}>
      <CardHeader
        title={
          <Typography
            style={{
              paddingRight: "1em",
              overflow: "hidden",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
            }}
          >
            {deployment.name || "<Unnamed>"}
          </Typography>
        }
        avatar={
          <Avatar sx={{ width: "2rem", height: "2rem" }}>
            <Icon path={mdiRocketLaunch} size={0.8} />
          </Avatar>
        }
        action={
          <Box
            display="flex"
            flexWrap="wrap"
            alignSelf="flex-start"
            alignItems="center"
            mt={1}
          ></Box>
        }
        subheader={
          <Box
            display="flex"
            flexWrap="wrap"
            alignSelf="flex-end"
            alignItems="center"
          >
            <Typography color="primary" variant="subtitle2">
              {cloud?.name.toUpperCase() || "<unknown>"}
            </Typography>
          </Box>
        }
      />
      <CardContent>
        <StatusIndicator deployment={deployment} />
        <Box mt={2}>
          <Typography variant="caption" color="textPrimary">
            Created {createdDate}
          </Typography>
        </Box>
      </CardContent>
      <CardActions>
        <Box display="flex" justifyContent="flex-end" width="100%">
          <Button size="small">View Details</Button>
        </Box>
      </CardActions>
    </Card>
  );
}
