import {
  Backdrop,
  CircularProgress,
  Grid,
  Button,
  Typography,
} from "@mui/material";
import Dashboard from "./Dashboard";
import { useUser, useConfig } from "../contexts";
import CacaoJSLogo from "./icons/cacaojsLogo";

/**
 * The RestrictUserPage is displayed if a env variable RESTRICT_USERS is true
 * and the currently logged in user is not in the ALLOWED_USERS list.
 */
const RestrictUserPage = () => {
  return (
    <Grid
      container
      spacing={4}
      direction="column"
      alignItems="center"
      justifyContent="center"
      style={{ minHeight: "90vh" }}
    >
      <Grid item>
        <CacaoJSLogo width={400}></CacaoJSLogo>
      </Grid>
      <Grid item>
        <Typography
          variant="h5"
          style={{
            marginTop: "10px",
            color: "rgb(71, 69, 83)",
          }}
        >
          You do not have permission to access this site.
        </Typography>
      </Grid>
      <Grid item>
        <Button
          variant="contained"
          size="large"
          href="/logout"
          style={{
            width: "10em",
            color: "white",
            backgroundColor: "#990000",
            fontWeight: "bold",
          }}
        >
          Logout
        </Button>
      </Grid>
    </Grid>
  );
};

const Layout = (props) => {
  const config = useConfig();
  const [user] = useUser();

  // get config for restricting users
  const restrictUsers = config.RESTRICT_USERS === "true";
  let allowedUsers = [];

  if (restrictUsers && config.ALLOWED_USERS) {
    allowedUsers = config.ALLOWED_USERS.split(",");
  }

  return (
    <div>
      {/* If RESTRICT_USERS is false or nonexistent, display site to everyone. */}
      {!restrictUsers && <Dashboard {...props} />}
      {/* If RESTRICT_USERS is true, only allow users in ALLOWED_USERS to access the site. */}
      {restrictUsers && (
        <>
          {allowedUsers.includes(user.username) ? (
            <Dashboard {...props} />
          ) : (
            <RestrictUserPage></RestrictUserPage>
          )}
        </>
      )}

      <Backdrop open={!!props.busy} style={{ zIndex: 999, color: "#fff" }}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
};

export default Layout;
