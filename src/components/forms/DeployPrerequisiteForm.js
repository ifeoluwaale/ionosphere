/**
 * DeployPrerequisiteForm Component
 * --------------------------------
 *
 * Users should have certain prerequisites before they can deploy a CACAO template,
 * including a cloud credential and an SSH key.
 *
 * This component displays a list of prerequisite items based on the user's current
 * credentials, indicating whether each requirement has been fulfilled. If not,
 * the user can add the necessary credentials directly through the provided dialog
 * interface.
 *
 * Each prerequisite is a `Prerequisite` component which takes a prerequisite object
 * as a prop and renders it with appropriate status and action.
 *
 * Prerequisites include:
 * - Importing a cloud-specific credential.
 * - Importing an SSH key for remote deployment access.
 */

import { useState } from "react";
import { Box, Button, Chip, Paper, Stack, Typography } from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";

import { useAPI, useCredentials } from "@/contexts";
import AddCredentialDialog from "./AddCredentialDialog";
import ProviderAuthDialog from "./ProviderAuthDialog";

export default function DeployPrerequisiteForm() {
  const [credentials, setCredentials] = useCredentials();
  const [addCredentialType, setAddCredentialType] = useState();
  const api = useAPI();

  const prerequisites = [
    {
      action: "Import a Credential",
      actionButtonText: "Add now",
      actionFunction: () => setAddCredentialType("cloud"),
      description: "Import a credential to link to your preferred cloud.",
      requireLevel: "Required",
      requireChipColor: "error",
      fulfilled:
        credentials.filter((cred) => cred.type === "openstack").length > 0,
    },
    {
      action: "Import an SSH Key",
      actionButtonText: "Add now",
      actionFunction: () => setAddCredentialType("ssh"),
      description:
        "Required if you wish to access deployments via SSH. If you do not add an SSH Key, you'll only be able to access your deployment via webshell or web desktop.",
      requireLevel: "Recommended",
      requireChipColor: "primary",
      fulfilled: credentials.filter((cred) => cred.type === "ssh").length > 0,
    },
  ];

  const fetchCredentials = async () => {
    try {
      const res = await api.credentials();
      setCredentials(res);
    } catch (error) {
      console.log("Could not retrieve credential list", error);
    } finally {
      setAddCredentialType();
    }
  };

  return (
    <>
      {addCredentialType === "ssh" && (
        <AddCredentialDialog
          open={true}
          type={addCredentialType}
          credentials={credentials}
          handleClose={() => setAddCredentialType()}
          handleCredentialCreate={() => fetchCredentials()}
        />
      )}

      {addCredentialType === "cloud" && (
        <ProviderAuthDialog
          open={true}
          handleClose={() => setAddCredentialType()}
          setAddCredentialType={setAddCredentialType}
          handleSubmit={() => setAddCredentialType()}
        />
      )}

      <Box>
        <Stack spacing={2}>
          <Box>
            <Typography>
              Hold up! Let's make sure you have everything you need to launch a
              deployment.
            </Typography>
          </Box>
          <Stack spacing={2}>
            {prerequisites.map((p, index) => {
              return <Prerequisite prerequisite={p} key={index}></Prerequisite>;
            })}
          </Stack>
        </Stack>
      </Box>
    </>
  );
}

/**
 * Prerequisite Component
 * ----------------------
 *
 * A single item component that represents a prerequisite action within the
 * `DeployPrerequisiteForm`.
 *
 * It displays an action (e.g., 'Import a Credential'), a description, and a status indicator.
 * If the action is not fulfilled, it provides a button to perform the required action.
 *
 * Props:
 * - prerequisite: An object containing the details of the prerequisite, such as the action name,
 *   description, fulfillment status, and associated function to execute the action.
 *
 * The status is visually represented by a 'Chip' that displays 'Done!' if the prerequisite
 * is fulfilled, or the level of requirement (e.g., 'Required', 'Recommended') otherwise.
 */
function Prerequisite(props) {
  const { prerequisite } = props;

  return (
    <Paper variant="outlined">
      <Box padding={2}>
        <Grid container spacing={3}>
          <Grid sm={9}>
            <Stack spacing={2}>
              <Stack direction="row" spacing={2} alignItems={"center"}>
                <Typography variant="h6">{prerequisite.action}</Typography>
                {prerequisite.fulfilled ? (
                  <Chip
                    label="Done!"
                    variant="outlined"
                    color="success"
                    size="small"
                    icon={<CheckCircleIcon />}
                  />
                ) : (
                  <Chip
                    label={prerequisite.requireLevel}
                    variant="outlined"
                    color={prerequisite.requireChipColor}
                    size="small"
                  />
                )}
              </Stack>
              <Typography variant="subtitle">
                {prerequisite.description}
              </Typography>
            </Stack>
          </Grid>
          <Grid
            sm={3}
            display="flex"
            justifyContent="right"
            alignItems="flex-start"
          >
            {!prerequisite.fulfilled && (
              <Button variant="contained" onClick={prerequisite.actionFunction}>
                {prerequisite.actionButtonText}
              </Button>
            )}
          </Grid>
        </Grid>
      </Box>
    </Paper>
  );
}
