import {
  Box,
  Button,
  Chip,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  DialogContentText,
  Grid,
  IconButton,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import { Add as AddIcon, Close as CloseIcon } from "@mui/icons-material";

import AddTagDialog from "./AddCredentialTagDialog";
import { useState, useReducer } from "react";

// Prefixes for CACAO-created tags, which users cannot delete.
const CACAO_TAG_PREFIXES = ["cacao_", "provider-", "application", "os_"];

/**
 * Update function for tagState, which tracks tags added and deleted by the user.
 * @see https://react.dev/reference/react/useReducer
 *
 * @param {Object} tagState Current tag state (read-only)
 * @param {Object} action Action to perform on tag state. Can be "deleteExisting", "createNew", or "deleteNew".
 * @returns {Object} New tag state
 */
function tagStateReducer(tagState, action) {
  switch (action.type) {
    case "deleteExisting":
      // add tag to "delete" list
      return {
        ...tagState,
        tagsToDelete: {
          ...tagState.tagsToDelete,
          [action.key]: {},
        },
      };
    case "createNew":
      // add tag to "add" list
      return {
        ...tagState,
        tagsToAdd: {
          ...tagState.tagsToAdd,
          [action.key]: action.value,
        },
      };
    case "deleteNew":
      // remove tag from "add" list
      let tagsToAdd = tagState.tagsToAdd;
      delete tagsToAdd[action.key];

      return {
        ...tagState,
        tagsToAdd: tagsToAdd,
      };

    default:
      throw new Error();
  }
}

/**
 * Renders a dialog for editing a credential.
 */
export default function EditCredentialDialog({
  open,
  title,
  handleClose,
  handleEdit,
  credential,
}) {
  // populates name field
  const [credentialName, setCredentialName] = useState(credential.name);

  // manages deletion and addition of tags
  const [tagState, dispatchTagState] = useReducer(tagStateReducer, {
    tagsToDelete: {},
    tagsToAdd: {},
  });

  const [loading, setLoading] = useState(false);
  const [showAddTagDialog, setShowAddTagDialog] = useState(false);

  // closes Add Tag Dialog
  const closeAddTagDialog = () => {
    setShowAddTagDialog(false);
  };

  // builds request for editing credential
  const editCredential = () => {
    setLoading(true);

    let credentialUpdate = { name: credentialName };

    if (Object.keys(tagState.tagsToDelete).length) {
      credentialUpdate["delete_tags"] = tagState.tagsToDelete;
    }

    if (Object.keys(tagState.tagsToAdd).length) {
      credentialUpdate["update_tags"] = tagState.tagsToAdd;
    }
    handleEdit(credential.id, credentialUpdate);
  };

  return (
    <Dialog open={open} aria-labelledby="edit-credential-dialog">
      <AddTagDialog
        open={showAddTagDialog}
        credential={credential}
        handleClose={closeAddTagDialog}
        dispatchTagState={dispatchTagState}
        title={"New Tag"}
      />
      <DialogTitle id="edit-credential-dialog">
        {title}{" "}
        <IconButton
          aria-label="close"
          onClick={handleClose}
          sx={{
            position: "absolute",
            right: 12,
            top: 12,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent dividers={true}>
        <Stack spacing={3} paddingY={1}>
          <DialogContentText fontSize={14}>
            To update the name of your credential, please enter the new name
            below.
          </DialogContentText>

          <TextField
            id="newCredential"
            name="newCredential"
            onChange={(e) => {
              setCredentialName(e.target.value);
            }}
            value={credentialName}
            style={{ width: 500 }}
            label="Credential Name"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
            InputProps={{ maxLength: 100 }}
          />

          <Box>
            <Typography fontWeight={500}>Manage Tags</Typography>
            <DialogContentText marginY={1} fontSize={14}>
              Some tags are required by CACAO and cannot be removed.
            </DialogContentText>
            <Grid container direction="row" mt={2}>
              {/* First display credential's existing tags. */}
              {credential.tags_kv && (
                <>
                  {Object.keys(credential.tags_kv)
                    .filter((tagKey) => !(tagKey in tagState.tagsToDelete))
                    .map((tagKey, index) => {
                      let cacaoRequiredTag = false;

                      // Disable delete if tag is required by CACAO.
                      CACAO_TAG_PREFIXES.forEach((prefix) => {
                        if (tagKey.startsWith(prefix)) {
                          cacaoRequiredTag = true;
                        }
                      });

                      return (
                        <KeyValueTag
                          tagKey={tagKey}
                          tagValue={credential.tags_kv[tagKey]}
                          index={index}
                          onDelete={
                            cacaoRequiredTag
                              ? null
                              : () =>
                                  dispatchTagState({
                                    type: "deleteExisting",
                                    key: tagKey,
                                  })
                          }
                        ></KeyValueTag>
                      );
                    })}
                </>
              )}
              {/* Then display new tags (not yet saved to credential). */}
              {Object.keys(tagState.tagsToAdd).length > 0 && (
                <>
                  {Object.keys(tagState.tagsToAdd).map((tagKey, index) => {
                    return (
                      <KeyValueTag
                        tagKey={tagKey}
                        tagValue={tagState.tagsToAdd[tagKey]}
                        index={
                          credential.tags_kv
                            ? Object.keys(credential.tags_kv).length + index
                            : index
                        }
                        onDelete={() =>
                          dispatchTagState({
                            type: "deleteNew",
                            key: tagKey,
                          })
                        }
                      ></KeyValueTag>
                    );
                  })}
                </>
              )}
              {/* Then display 'New Tag' button. */}
              <Grid item>
                <Chip
                  label="New Tag"
                  onClick={() => setShowAddTagDialog(true)}
                  icon={<AddIcon />}
                  size="small"
                  color="primary"
                  variant="outlined"
                />
              </Grid>
            </Grid>
          </Box>
        </Stack>
      </DialogContent>
      <DialogActions>
        <Box mr={2}>
          <Button onClick={handleClose}>Cancel</Button>
        </Box>
        <LoadingButton
          loading={loading}
          disabled={
            !Object.keys(tagState.tagsToDelete).length &&
            !Object.keys(tagState.tagsToAdd).length &&
            credentialName === credential.name
          }
          variant="contained"
          onClick={editCredential}
        >
          Save Changes
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
}

/**
 * Renders a single key-value tag as a Chip component.
 * @param {string} tagKey key of tag
 * @param {string} tagValue value of tag
 * @param {number} index index of tag in list of tags
 * @param {Function} onDelete function to call when tag is deleted
 * @returns {JSX.Element}
 */
const KeyValueTag = ({ tagKey, tagValue, index, onDelete }) => {
  const label = tagKey + (tagValue ? " | " + tagValue : "");

  return (
    <Grid item key={index} mr={1} mb={1}>
      <Chip label={label} size="small" onDelete={onDelete} />
    </Grid>
  );
};
