import { Box, Typography, TextField, MenuItem, Chip } from "@mui/material";
import Autocomplete, { createFilterOptions } from "@mui/material/Autocomplete";
import { datetimeString } from "../../utils";

const TemplateField = (props) => {
  let type = props.type;
  let options = props.enum;

  if (props.type === "cacao_username") return <></>; // hidden

  if (props.type === "cacao_provider_flavor") {
    type = "string";
    options = props.data.flavors.map((f) => f.name);
  }

  if (props.type === "cacao_provider_image") {
    type = "autocomplete";
    options = props.data.images;
  }

  if (props.type === "cacao_provider_project") {
    return <></>; // hidden
    // type = 'string'
    // options = props.data.projects.map(f => f.project_code)
  }

  if (props.type === "cacao_provider_key_pair") return <></>; //TODO

  if (props.type === "cacao_provider_external_network") return <></>; //TODO

  if (props.type === "cacao_cloud_init") return <></>; //TODO

  if (type === "string") {
    return (
      <TextField
        sx={{ marginBottom: "1em" }}
        required
        fullWidth
        variant="outlined"
        select={options?.length > 0}
        id={props.id}
        label={props.ui_label}
        placeholder={props.description}
        // helperText=
        defaultValue={props.default}
        onChange={(e) => props.onChange(props.id, e.target.value)}
      >
        {options?.map((option, index) => (
          <MenuItem key={index} value={option}>
            {option}
          </MenuItem>
        ))}
      </TextField>
    );
  }

  if (type === "integer") {
    return (
      <TextField
        sx={{ marginBottom: "1em" }}
        type="number"
        variant="outlined"
        required
        fullWidth
        id={props.id}
        label={props.ui_label}
        placeholder={props.description}
        // helperText=
        defaultValue={props.default}
        onChange={(e) => props.onChange(props.id, e.target.value)}
      />
    );
  }

  if (type === "autocomplete") {
    return (
      <Autocomplete
        sx={{ marginBottom: "1em" }}
        id={props.id}
        // value={images && images.find(image => image.id === values['imageId'])}
        options={options || []}
        getOptionLabel={(option) => option.name}
        onChange={(event, option) =>
          option && props.onChange(props.id, option.id)
        }
        renderInput={(params) => (
          <TextField
            {...params}
            label={props.ui_label}
            placeholder={props.description}
            fullWidth
            variant="outlined"
          />
        )}
        renderOption={(props, option) => (
          <li {...props}>
            <Box sx={{ display: "flex" }}>
              <Typography>{option.name}</Typography>
            </Box>
            {option.updated_at && (
              <Box>
                <Typography variant="caption">
                  {datetimeString(option.updated_at)}
                  {option.owner ? " by " + option.owner : ""}
                </Typography>
              </Box>
            )}
            {option.tags?.length > 0 && (
              <Box>
                {option.tags
                  .sort((a, b) =>
                    a.toLowerCase().localeCompare(b.toLowerCase())
                  )
                  .map((tag, index) => (
                    <Chip
                      key={index}
                      label={tag}
                      size="small"
                      style={{ fontSize: "0.7rem", margin: "2px 2px" }}
                    />
                  ))}
              </Box>
            )}
          </li>
        )}
        filterOptions={createFilterOptions({
          stringify: (option) =>
            option.name +
            (option.owner ? " " + option.owner : "") +
            (option.tags?.length > 0 ? " " + option.tags.join(" ") : ""),
        })}
      />
    );
  }

  return <></>;
};

export default function TemplateForm({
  parameters,
  images,
  flavors,
  projects,
  onChange,
}) {
  console.log("template parameters:", parameters);
  return (
    <form autoComplete="off">
      {Object.keys(parameters).map((id, index) => (
        <div key={index}>
          <TemplateField
            id={id}
            {...parameters[id]}
            data={{ images, flavors, projects }}
            onChange={onChange}
          />
        </div>
      ))}
    </form>
  );
}
