import React from "react";
import {
  Button,
  Typography,
  Stepper,
  Stack,
  Step,
  StepLabel,
} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";

const FormStepper = ({ activeStep, steps }) => {
  if (steps.length <= 1) return <></>;

  return (
    <Stepper>
      {steps.map((title, index) => (
        <Step
          key={index}
          completed={activeStep > index}
          active={activeStep === index}
        >
          <StepLabel>{title}</StepLabel>
        </Step>
      ))}
    </Stepper>
  );
};

const FormControls = ({
  step,
  disabled,
  activeStep,
  numSteps,
  backHandler,
  nextHandler,
  submitHandler,
  closeHandler,
  isSubmitting,
}) => {
  const isLastStep = activeStep === numSteps - 1;

  return (
    <div>
      {activeStep === numSteps ? (
        <div>
          <Typography>All steps completed - you&apos;re finished</Typography>
        </div>
      ) : (
        <Stack spacing={1} direction={"row"}>
          {numSteps > 1 && activeStep > 0 && !step.disablePrev && (
            <Button
              sx={{ width: "8rem" }}
              onClick={backHandler}
              variant="outlined"
            >
              {step.prevText ? step.prevText : "Previous"}
            </Button>
          )}{" "}
          {step.disableNext ? (
            <Button
              sx={{ width: "8rem" }}
              variant="contained"
              color="primary"
              onClick={closeHandler}
            >
              Close
            </Button>
          ) : (
            <LoadingButton
              sx={{ width: "8rem" }}
              disabled={disabled}
              variant="contained"
              color="primary"
              onClick={isLastStep ? submitHandler : nextHandler}
              loading={isSubmitting}
            >
              {isLastStep
                ? step.submitText
                  ? step.submitText
                  : "Submit"
                : step.nextText
                ? step.nextText
                : "Next"}
            </LoadingButton>
          )}
        </Stack>
      )}
    </div>
  );
};

export { FormStepper, FormControls };
