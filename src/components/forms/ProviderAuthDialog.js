/* eslint-disable no-unused-vars */
import { useState, useEffect } from "react";
import {
  Alert,
  AppBar,
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogContent,
  Grid,
  IconButton,
  List,
  ListItemButton,
  Toolbar,
  Tooltip,
  Typography,
} from "@mui/material";
import {
  Close as CloseIcon,
  CheckCircle as CheckCircleIcon,
  Warning as WarningIcon,
} from "@mui/icons-material";

import { FormStepper, FormControls } from "./FormStepper";
import {
  useAPI,
  useClouds,
  useConfig,
  useCredentials,
  useProjects,
} from "@/contexts";
import axios from "axios";
/**
 * Step 1 of Credential Creation Wizard. Allows user to select cloud
 * to create credentials for.
 */
const SelectCloud = ({
  clouds,
  selectedCloudId,
  onSelect,
  setAddCredentialType,
}) => {
  return (
    <>
      <Box display="flex" justifyContent="center">
        <List
          style={{ minWidth: "35em" }}
          subheader={
            <Box
              display="flex"
              justifyContent="center"
              alignContent={"center"}
              alignItems="center"
              mb={2}
            >
              <Box>
                <Typography
                  style={{
                    fontSize: "1.25rem",
                    marginRight: "1em",
                  }}
                >
                  Select Cloud{" "}
                </Typography>
              </Box>
              <Box>
                <Alert severity="warning">
                  <Typography variant="caption">
                    You will be redirected to ACCESS-CI for credential creation.
                  </Typography>
                </Alert>
              </Box>
            </Box>
          }
        >
          <div>
            <Grid container dispay="flex" direction="row">
              {clouds &&
                clouds.map((cloud, index) => (
                  <Grid key={index} item md={6} sm={12} xs={12}>
                    <ListItemButton
                      onClick={() => onSelect("selectedCloudId", cloud.id)}
                      selected={selectedCloudId === cloud.id}
                      style={{
                        border: "1px solid #dfdfdf",
                        minHeight: "1rem",
                        borderRadius: "5px",
                      }}
                    >
                      <Box
                        p={1}
                        ml={1}
                        sx={{ margin: "auto", paddingX: "1rem" }}
                      >
                        <Typography align="center">{cloud.name}</Typography>
                      </Box>
                    </ListItemButton>
                  </Grid>
                ))}

              <Tooltip title="Not Available">
                <Grid item md={6} sm={12} xs={12}>
                  <ListItemButton disabled>
                    <Box p={1} ml={1} sx={{ margin: "auto", paddingX: "1rem" }}>
                      <Typography align="center">AWS</Typography>
                    </Box>
                  </ListItemButton>
                </Grid>
              </Tooltip>
              <Tooltip title="Not Available">
                <Grid item md={6} sm={12} xs={12}>
                  <ListItemButton disabled>
                    <Box p={1} ml={1} sx={{ margin: "auto", paddingX: "1rem" }}>
                      <Typography align="center">Google</Typography>
                    </Box>
                  </ListItemButton>
                </Grid>
              </Tooltip>
              <Tooltip title="Not Available">
                <Grid item md={6} sm={12} xs={12}>
                  <ListItemButton disabled>
                    <Box p={1} ml={1} sx={{ margin: "auto", paddingX: "1rem" }}>
                      <Typography align="center">Kubernetes</Typography>
                    </Box>
                  </ListItemButton>
                </Grid>
              </Tooltip>
            </Grid>
          </div>
        </List>
      </Box>

      <Box
        display="flex"
        justifyContent="center"
        alignContent="center"
        style={{ marginTop: "1rem" }}
      >
        <Button
          color="secondary"
          disableElevation
          onClick={() => setAddCredentialType("application")}
        >
          <Box align="center">
            <Typography variant="caption" align="center">
              Existing Credentials? Click here
            </Typography>{" "}
          </Box>
        </Button>
      </Box>
    </>
  );
};

/**
 * Step 2 of Credential Creation Wizard. Allows user to select js2
 * allocations/projects to create credentials for.
 */
const SelectProjects = (props) => {
  return (
    <>
      <Box sx={{ maxWidth: "90%", margin: "0 auto" }}>
        <Box
          display="flex"
          alignItems="center"
          justifyContent={"center"}
          mt={1}
          ml={2}
        >
          <Box>
            <Typography
              style={{
                fontSize: "1.25rem",
                marginRight: "1em",
              }}
            >
              Select Projects
            </Typography>
          </Box>
          <Box>
            <Alert severity="info">
              <Typography variant="caption">
                Projects with existing credentials cannot be selected.
              </Typography>
            </Alert>
          </Box>
        </Box>
        <Box display="flex" justifyContent="center" mt={2}>
          <Typography variant="body2">
            The following are ACCESS allocations linked to Jetstream2 projects
          </Typography>
        </Box>
        <List style={{ minWidth: "35em" }}>
          {props.projects?.length === 0 && (
            <Box
              display="flex"
              justifyContent="center"
              alignContent="center"
              mt={3}
            >
              <Typography>No projects found.</Typography>
            </Box>
          )}

          {props.projects?.length > 0 && (
            <>
              <Grid container spacing={1} dispay="flex" direction="row">
                <>
                  {props.projects.map((project, index) => (
                    <Grid item md={6} sm={12} xs={12} key={index}>
                      <ListItemButton
                        style={{
                          border: "1px solid #dfdfdf",
                          minHeight: "125px",
                          height: "100%",
                          borderRadius: "5px",
                        }}
                        dense
                        selected={props.selectedProjects.includes(project.id)}
                        disabled={project.hasCredential}
                        onClick={() => {
                          if (props.selectedProjects.includes(project.id)) {
                            props.handleRemove(project);
                          } else {
                            props.handleAdd(project);
                          }
                        }}
                      >
                        <Box
                          p={1}
                          ml={1}
                          sx={{ margin: "auto", paddingX: "1rem" }}
                        >
                          <Typography align="center" variant="h6">
                            {project.name}
                          </Typography>
                          <Box display="flex" align="center">
                            <Typography variant="body2" align="center">
                              {project.description}
                            </Typography>
                          </Box>
                        </Box>
                      </ListItemButton>
                    </Grid>
                  ))}
                </>
              </Grid>

              {props.canCreateCredentials === false && (
                <Box
                  display="flex"
                  justifyContent="center"
                  alignContent="center"
                  mt={3}
                >
                  <Typography align="center">
                    Nothing to do here! All projects already have credentials.
                  </Typography>
                </Box>
              )}
            </>
          )}
        </List>
      </Box>
    </>
  );
};

/**
 * Step 3 of Credential Creation Wizard. Tells user whether credentials
 * were created successfully.
 */
const ConfirmCreation = (props) => {
  return (
    <>
      <Box display="flex" justifyContent="center">
        <List
          style={{ minWidth: "35em" }}
          subheader={
            <Box display="flex">
              <Typography
                style={{
                  fontSize: "1.25rem",
                  marginBottom: "1em",
                }}
              >
                Summary
              </Typography>
            </Box>
          }
        >
          {props.failedCredentials?.length > 0 && (
            <div>
              <Box display="flex" alignItems="start" direction="row" pb={3}>
                <Box>
                  <WarningIcon
                    style={{
                      width: "3rem",
                      color: "orange",
                      marginLeft: "1rem",
                      marginRight: ".3rem",
                      marginTop: "-.1rem",
                    }}
                  />
                </Box>
                <Box>
                  <Typography variant="body1">
                    Error creating credentials for the following projects:
                  </Typography>
                  {props.failedCredentials.map((projectName, index) => (
                    <Typography
                      key={index}
                      variant="subtitle2"
                      style={{ marginTop: "1rem" }}
                    >
                      {projectName}
                    </Typography>
                  ))}
                </Box>
              </Box>
            </div>
          )}

          {props.createdCredentials?.length > 0 && (
            <div>
              <Box display="flex" alignItems="start" direction="row">
                <Box>
                  <CheckCircleIcon
                    style={{
                      width: "3rem",
                      color: "green",
                      marginLeft: "1rem",
                      marginRight: ".3rem",
                      marginTop: "-.1rem",
                    }}
                  />
                </Box>
                <Box>
                  <Typography variant="body1">
                    Success! Credentials created for the following projects:
                  </Typography>
                  {props.createdCredentials.map((projectName, index) => (
                    <Typography
                      key={index}
                      variant="subtitle2"
                      style={{ marginTop: "1rem" }}
                    >
                      {projectName}
                    </Typography>
                  ))}
                </Box>
              </Box>
            </div>
          )}
        </List>
      </Box>
    </>
  );
};

/**
 * Credential Creation Wizard.
 */
export default function ProviderAuthDialog(props) {
  const api = useAPI();
  const [clouds] = useClouds();
  const config = useConfig();
  const [projectsContext, setProjectsContext] = useProjects();
  const [credentials, setCredentials] = useCredentials();

  const [error, setError] = useState();
  const [busy, setBusy] = useState(false);
  const [busyMessage, setBusyMessage] = useState();
  const [projects, setProjects] = useState();
  const [canCreateCredentials, setCanCreateCredentials] = useState();

  const [createdCredentials, setCreatedCredentials] = useState();
  const [failedCredentials, setFailedCredentials] = useState();

  let initialValues;

  if (props.provider) {
    const providerId = clouds.find(
      (element) => element.name === props.provider
    ).id;
    initialValues = {
      selectedCloudId: providerId,
      selectedProjects: [],
    };
  } else {
    initialValues = {
      selectedCloudId: null,
      selectedProjects: [],
    };
  }

  const jetstreamId = clouds.find(
    (element) => element.name === "Jetstream 2"
  ).id;
  const [activeStep, setActiveStep] = useState(
    props.wizardStep ? props.wizardStep : 0
  );
  const [values, setValues] = useState(initialValues);
  const [errors] = useState({});
  const [isValid, setValid] = useState(false);

  const getProjects = async () => {
    setCanCreateCredentials();

    // clear any errors
    setError();
    setBusy(true);
    setBusyMessage("Getting projects");

    try {
      // get js2 projects (backend call to express)
      const res = await axios.get("/openstack/get-projects", {
        params: { selectedCloudId: values["selectedCloudId"] },
        withCredentials: true,
      });

      let projectList = res.data?.projectList;

      if (projectList && projectList.length) {
        // get CACAO-managed credentials
        const cacaoCredentials = credentials.filter(
          (cred) =>
            cred.tags_kv?.cacao_managed === "true" ||
            cred.tags?.includes("cacao_managed")
        );

        // Check each openstack project for a CACAO-managed credential.
        if (cacaoCredentials.length) {
          projectList.forEach((project, index, projectList) => {
            const projectCred = cacaoCredentials.find(
              (cred) => cred.tags_kv?.cacao_openstack_project_id === project.id
            );

            if (projectCred) {
              projectList[index].hasCredential = true;
            } else {
              projectList[index].hasCredential = false;
            }
          });
          // Otherwise, all projects are eligible for credential creation
        } else {
          projectList.forEach((project, index, projectList) => {
            projectList[index].hasCredential = false;
          });
        }

        setProjects(projectList);
      } else {
        setProjects([]);
      }

      /**
       * User can create a credential if at least one project does
       * not already have a Cacao-created credential.
       */
      const isEligible = (element) => !element.hasCredential;
      setCanCreateCredentials(projectList.some(isEligible));

      setBusy(false);
      setBusyMessage();
    } catch (e) {
      setError(e.message);
      setProjects();
      setBusy(false);
      setBusyMessage();
    }
  };

  // Fetch projects when user navigates to second step (project selection)
  useEffect(() => {
    if (
      props.provider === "Jetstream 2" &&
      props.wizardStep === 1 &&
      !config.isDevelopment
    ) {
      getProjects();
    }
  }, []);

  useEffect(() => {
    const validator = steps[activeStep].validator;
    setValid((!validator || validator(values)) && !error);
  }, [activeStep, values, errors, error]);

  const handleSelect = (id, value) => {
    const newValue = values[id] === value ? null : value; // unselect if selected
    setValues({ ...values, [id]: newValue });
  };

  const handleAdd = (project) => {
    const newProjects = values["selectedProjects"].slice();
    newProjects.push(project.id);
    setValues({ ...values, selectedProjects: newProjects });
  };

  const handleRemove = (project) => {
    const newProjects = values["selectedProjects"].filter(
      (p) => p !== project.id
    );
    setValues({ ...values, selectedProjects: newProjects });
  };

  const createCredentials = async () => {
    setBusy(true);
    setBusyMessage("Creating credentials");

    let credentialsToCreate = [];
    let projectNames = [];

    projects.forEach((project) => {
      if (values["selectedProjects"].includes(project.id)) {
        projectNames.push(project.name);
        credentialsToCreate.push({
          // 'name': '',
          scope: {
            project: {
              id: project.id,
            },
            domain: {
              id: project.domain_id,
            },
          },
        });
      }
    });

    let res;

    try {
      // create openstack credentials (backend call to express)
      res = await axios.get("/openstack/create-credentials", {
        params: {
          selectedCloudId: values["selectedCloudId"],
          credentialsToCreate: credentialsToCreate,
        },
        withCredentials: true,
      });
    } catch (error) {
      console.log(error);
      setError(error.response.data);
      setBusy(false);
      setBusyMessage();
    }

    // get creation status for each credential from response
    if (res) {
      let promiseResults = res.data.promiseResults;

      let successes = [];
      let failures = [];

      promiseResults.forEach((result, index) => {
        if (result.status === "fulfilled") {
          successes.push(projectNames[index]);
        } else {
          failures.push(projectNames[index]);
        }
      });

      // if credentials were created, force refresh of js2projects cache & update projects context
      if (successes.length) {
        const updatedProjects = await api.js2allocations({ nocache: true });
        const credentials = await api.credentials();
        setProjectsContext(updatedProjects);
        setCredentials(credentials);
      }

      setCreatedCredentials(successes);
      setFailedCredentials(failures);

      setBusy(false);
      setBusyMessage();
    }
  };

  const steps = [
    {
      title: "Select Cloud",
      validator: (values) => !validateSelect(values["selectedCloudId"]),
      render: (values) => (
        <SelectCloud
          clouds={clouds}
          selectedCloudId={values["selectedCloudId"]}
          onSelect={handleSelect}
          setAddCredentialType={props.setAddCredentialType}
        />
      ),
      nextText: "Next",
    },
    {
      title: "Select Projects",
      validator: (values) =>
        !validateSelectMultiple(values["selectedProjects"]),
      render: (values) => (
        <SelectProjects
          projects={projects}
          selectedProjects={values["selectedProjects"]}
          canCreateCredentials={canCreateCredentials}
          handleAdd={handleAdd}
          handleRemove={handleRemove}
        />
      ),
      nextText: canCreateCredentials === false ? "Close" : "Create",
      prevText: "Back",
      disableNext: canCreateCredentials === false,
    },
    {
      title: "Summary",
      render: () => (
        <ConfirmCreation
          createdCredentials={createdCredentials}
          failedCredentials={failedCredentials}
        />
      ),
      submitText: "OK",
      disablePrev: true,
    },
  ].filter((step) => step);

  const handleNext = async (values) => {
    // For credential creation step
    if (activeStep === 0 && values["selectedCloudId"] === jetstreamId) {
      if (config.isDevelopment && config.UNSCOPED_TOKEN) {
        setActiveStep((prevStep) => prevStep + 1);
        let response = await getProjects();
      } else {
        setBusy(true);
        window.location.replace(
          `https://js2.jetstream-cloud.org:5000/identity/v3/auth/OS-FEDERATION/websso/openid?origin=${window.location.origin}`
        );
      }
    } else if (activeStep === 1 && values["selectedProjects"]) {
      let response = await createCredentials();
      setActiveStep((prevStep) => prevStep + 1);
    } else {
      setActiveStep((prevStep) => prevStep + 1);
    }
  };

  const handleBack = () => {
    setError();
    setActiveStep((prevStep) => prevStep - 1);
  };

  const validateSelect = (selectedId) => {
    if (!selectedId) return "Please make a selection";
  };

  const validateSelectMultiple = (selected) => {
    if (!selected.length) return "Please make a selection";
  };

  return (
    <Dialog
      open={props.open}
      onClose={(_, reason) => {
        reason !== "backdropClick" && props.handleClose && props.handleClose();
      }}
      maxWidth="md"
      fullWidth
    >
      <AppBar sx={{ position: "relative" }}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={props.handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h6" sx={{ marginLeft: "16px", flex: 1 }}>
            New Cloud Credential
          </Typography>
          {/* <Box display="flex">
            <CloudOutlineIcon />
            <Typography style={{marginLeft: '0.5em'}}>{cloud.name.toUpperCase()} / {props.projectId}</Typography>
          </Box> */}
        </Toolbar>
      </AppBar>
      <DialogContent style={{ minHeight: "20vh" }}>
        <FormStepper
          activeStep={activeStep}
          steps={steps.map((s) => s.title)}
        />
        <Box mt={2} mb={2} sx={{ overflowX: "hidden", overflowY: "auto" }}>
          {!busy ? (
            <>
              {steps[activeStep].render(values)}
              {error && (
                <Box mt={2} display="flex" justifyContent="center">
                  <Alert severity="error">{error}</Alert>
                </Box>
              )}
            </>
          ) : (
            <>
              <Box sx={{ display: "flex", justifyContent: "center" }} p={2}>
                <div>
                  <CircularProgress size="3rem" />
                </div>
              </Box>
              <Box sx={{ display: "flex", justifyContent: "center" }} pb={3}>
                <div>
                  <Typography variant="body2">{busyMessage}</Typography>
                </div>
              </Box>
            </>
          )}
        </Box>
      </DialogContent>
      <Box display="flex" justifyContent="center" mt={2} mb={3}>
        <FormControls
          step={steps[activeStep]}
          disabled={!isValid || busy}
          activeStep={activeStep}
          numSteps={steps.length}
          nextHandler={handleNext.bind(null, values)}
          backHandler={handleBack.bind(null, values)}
          submitHandler={props.handleSubmit.bind(null, values)}
          closeHandler={props.handleClose}
        />
      </Box>
    </Dialog>
  );
}
