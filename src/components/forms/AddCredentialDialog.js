import { useState, useEffect } from "react";
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Box,
} from "@mui/material";

import LoadingButton from "@mui/lab/LoadingButton";

import { AddSshKeyForm, AddOpenStackApplicationForm } from "./CredentialForms";

import { useClouds, useCredentials, useAPI, useError } from "../../contexts";

export default function AddCredentialDialog({
  open,
  type,
  projects,
  handleClose,
  handleCredentialCreate,
}) {
  const [clouds] = useClouds();
  const [credentials, setCredentials] = useCredentials();
  const api = useAPI();
  const [_, setError] = useError();

  const initialValues = {
    username: projects?.length > 0 ? projects[0].owner : null,
    cloudId: clouds[0]?.id,
  };
  const [values, setValues] = useState(initialValues);
  const [isValid, setValid] = useState(false);
  const [hasConflict, setHasConflict] = useState(false);
  const [busy, setBusy] = useState(false);

  const handleCredentialError = (msg, error, hideDetails) => {
    setError(
      <>
        {msg}
        {error && !hideDetails && (
          <Box mt={2}>
            Details: {error.response ? error.response.data : error.message}
          </Box>
        )}
      </>
    );
  };

  const createCredentialRequest = (values) => {
    const cloud = clouds.find((c) => c.id === values.cloudId);
    const value = {
      OS_REGION_NAME: cloud.metadata.OS_REGION_NAME,
      OS_PROJECT_DOMAIN_ID: cloud.metadata.project_domain_uuid,
      OS_INTERFACE: cloud.metadata.OS_INTERFACE,
      OS_USER_DOMAIN_NAME: cloud.metadata.OS_USER_DOMAIN_NAME,
      OS_IDENTITY_API_VERSION: cloud.metadata.OS_IDENTITY_API_VERSION,
      OS_AUTH_URL: cloud.url,
    };

    if (values.type === "application") {
      const { id, cloudId, applicationId, applicationSecret } = values;
      return {
        id,
        type: "openstack",
        tags: ["application", cloudId],
        value: JSON.stringify({
          ...value,
          OS_AUTH_TYPE: "v3applicationcredential",
          OS_APPLICATION_CREDENTIAL_ID: applicationId,
          OS_APPLICATION_CREDENTIAL_SECRET: applicationSecret,
        }),
      };
    } else {
      // "ssh" type
      const { username, id, type, value } = values;
      return { username, id, type, value };
    }
  };

  const createCredential = async (values) => {
    const credId = values["id"];
    const credType = values["type"];

    let req = createCredentialRequest(values);
    console.log("createCredential:", req);

    setBusy(true);

    // Create credential
    try {
      await api.createCredential(req);
    } catch (error) {
      handleCredentialError("Error creating credential", error.response.data);
      setBusy(false);
      return;
    }

    // If new credential is an application credential, test that credential works and delete if not
    if (credType === "application") {
      let newCredentialId;

      // get ID of newly created credential
      try {
        const credentials = await api.credentials();
        newCredentialId = credentials.filter((c) => c.name === credId)[0].id;
      } catch (error) {
        console.log("Could not get credential ID to test credential.");
      }

      if (newCredentialId) {
        // test credential is valid with provider projects endpoint
        try {
          // TODO: replace with credential test endpoint when ready
          await api.providerProjects(values.cloudId, {
            credential: newCredentialId,
          });
        } catch (error) {
          console.log("credential test error");
          // if credential isn't valid, try to delete it
          try {
            await api.deleteCredential(newCredentialId);
          } catch (error) {
            console.log(error.response);
            handleCredentialError("Invalid credential", error.response.data);
          }
        }
      }
    }

    // close modal and refresh credentials list
    handleCredentialCreate();
  };

  const handleChange = async (type, e) => {
    setValues({
      ...values,
      [e.target.id || e.target.name]: e.target.value,
      type,
    });
  };

  const validate = (values) => {
    if (type === "ssh") return values["id"] && values["value"];
    if (type === "application")
      return (
        values["id"] &&
        values["cloudId"] &&
        values["applicationId"] &&
        values["applicationSecret"]
      );
  };

  const typeDescriptor = (type) => {
    if (type === "ssh") return "SSH Key";
    if (type === "application") return "Application";
  };

  useEffect(() => {
    setValid(validate(values));
    setHasConflict(
      credentials.some((c) => c.id === values["id"] || c.name === values["id"])
    );
  }, [credentials, validate, values]);

  return (
    <Dialog open={open} onClose={handleClose} maxWidth="sm">
      <DialogTitle>Add {typeDescriptor(type)} Credential</DialogTitle>
      <DialogContent>
        {type === "ssh" && (
          <AddSshKeyForm
            onChange={(e) => handleChange(type, e)}
            hasConflict={hasConflict}
          />
        )}
        {type === "application" && (
          <AddOpenStackApplicationForm
            onChange={(e) => handleChange(type, e)}
            hasConflict={hasConflict}
          />
        )}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <LoadingButton
          disabled={!isValid || hasConflict}
          color="primary"
          variant="contained"
          onClick={() => createCredential(values)}
          loading={busy}
        >
          Add
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
}
