import { Search } from "@mui/icons-material";
import { TextField, InputAdornment } from "@mui/material";

export default function TemplateSearch({ search, setSearch }) {
  return (
    <TextField
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <Search />
          </InputAdornment>
        ),
      }}
      variant="outlined"
      fullWidth
      label="Search Templates"
      value={search}
      onChange={(e) => setSearch(e.target.value)}
      placeholder=" Try 'docker', 'vms', 'llm', or 'jupyter' or clouds like 'aws' and 'openstack'"
    />
  );
}
