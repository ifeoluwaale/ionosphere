import {
  Code as CodeIcon,
  MoreVert as MoreVertIcon,
  RocketLaunch as RocketLaunchIcon,
  Share as ShareIcon,
  Widgets as WidgetsIcon,
} from "@mui/icons-material";
import {
  Avatar,
  Box,
  Button,
  CardActions,
  CardContent,
  CardHeader,
  Chip,
  Grid,
  IconButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Stack,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { DateTime } from "../Common/DateTime";
import { stringToColor } from "@/utils";
import HoverCard from "../Common/HoverCard";

/**
 * Card for displaying information about a CACAO template.
 */

export default function TemplateCard({ template, search, launchWizard }) {
  // Search term bolding, ignores empty strings after splitting
  function splitAndFilter(str, search) {
    return str.split(new RegExp(`(${search})`, "gi")).filter(Boolean);
  }
  const name = splitAndFilter(template.name, search);
  const description = splitAndFilter(template.metadata.description, search);
  const author = splitAndFilter(template.metadata.author, search);
  const type = splitAndFilter(template.metadata.template_type, search);
  const purpose = splitAndFilter(template.metadata.purpose, search);
  const owner = splitAndFilter(template.owner, search);

  return (
    <HoverCard
      style={{
        display: "flex",
        flexDirection: "column",
        height: "100%",
        width: "100%",
      }}
    >
      <CardHeader
        avatar={
          <Avatar
            sx={{
              bgcolor: stringToColor(template.name),
              width: "2rem",
              height: "2rem",
            }}
          >
            <WidgetsIcon fontSize="small" />
          </Avatar>
        }
        action={<TemplateActionMenu template={template} />}
        title={
          <Typography
            variant="body1"
            style={{ fontSize: "15px", fontWeight: 500 }}
            component="h2"
          >
            <HighlightedText text={name} search={search} />
          </Typography>
        }
        subheader={
          <Typography variant="caption">
            Author: <HighlightedText text={author} search={search} />
          </Typography>
        }
      />
      <CardContent style={{ flexGrow: 1 }}>
        <Stack spacing={3}>
          <Box>
            <HighlightedText text={description} search={search} />
          </Box>
          <Box>
            <Grid container spacing={1} direction="row">
              <Grid item>
                <Chip
                  label={<HighlightedText text={purpose} search={search} />}
                  size="small"
                />
              </Grid>
              <Grid item>
                <Chip
                  label={<HighlightedText text={type} search={search} />}
                  size="small"
                />
              </Grid>
              <Grid item>
                <Chip
                  label={template.public ? "public" : "private"}
                  size="small"
                />
              </Grid>
            </Grid>
          </Box>
          <Stack mt={2}>
            <Box>
              <Typography variant="caption">
                <Box display={"flex"}>
                  Last updated: <DateTime datetime={template.updated_at} />
                </Box>
              </Typography>
            </Box>
            <Box>
              <Typography variant="caption">
                <Box display={"flex"}>
                  Imported by: <HighlightedText text={owner} search={search} />
                </Box>
              </Typography>
            </Box>
          </Stack>
        </Stack>
      </CardContent>
      <CardActions>
        <Stack
          direction={"row"}
          justifyContent={"flex-end"}
          spacing={1}
          padding={1}
          sx={{ width: "100%" }}
        >
          <Button
            size="small"
            variant="text"
            color="primary"
            href={`/templates/${template.id}`}
          >
            Learn More
          </Button>
          <Button
            size="small"
            variant="outlined"
            color="primary"
            startIcon={<RocketLaunchIcon />}
            onClick={() => launchWizard(template.id)}
          >
            Deploy
          </Button>
        </Stack>
      </CardActions>
    </HoverCard>
  );
}

// Highlight and bold searched terms in the template card

function HighlightedText({ text, search }) {
  return text.map((part, index) =>
    part.toLowerCase() === search.toLowerCase() && search.length > 1 ? (
      <b
        key={index}
        style={{
          backgroundColor: "#fff38a",
          padding: "2px",
          borderRadius: "5px",
        }}
      >
        {part}
      </b>
    ) : (
      <span key={index}>{part}</span>
    )
  );
}

// Template Action Menu for Source Code

const TemplateActionMenu = ({ template }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <IconButton
        id="template-actions-button"
        aria-controls={open ? "template-actions" : undefined}
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="template-actions"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "template-actions-button",
        }}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <MenuItem onClick={handleClose}>
          <a
            href={template.source.uri}
            target="_blank"
            rel="noopener noreferrer"
            style={{
              textDecoration: "none",
              color: "inherit",
              display: "flex",
              alignItems: "center",
            }}
          >
            <ListItemIcon>
              <CodeIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="View Source Code" />
          </a>
        </MenuItem>
        {/* <MenuItem onClick={handleClose}>
          {" "}
          <ListItemIcon>
            <ShareIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText>Share</ListItemText>
        </MenuItem> */}
      </Menu>
    </div>
  );
};
