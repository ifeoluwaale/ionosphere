import React from "react";
import {
  Avatar,
  Box,
  Card,
  CardHeader,
  Divider,
  Grid,
  IconButton,
  styled,
  Tooltip,
  Typography,
} from "@mui/material";
import {
  Category as CategoryIcon,
  Layers as LayersIcon,
  Delete as DeleteIcon,
  Edit as EditIcon,
  Update as UpdateIcon,
} from "@mui/icons-material";
import StatusIndicator from "../StatusIndicator";
import Icon from "@mdi/react";
import { mdiRocketLaunch } from "@mdi/js";
import { datetimeString } from "../../utils";
import DeploymentErrorAlert from "./DeploymentErrorAlert";

const ActionButton = styled(IconButton)(({ theme }) => ({
  width: "2em",
  height: "2em",
  top: "0.2em",
}));

const DeploymentSummary = (props) => {
  const deployment = props.deployment;
  const run = deployment.current_run;
  const resources = run?.last_state?.resources;
  const numInstances =
    resources &&
    resources.filter((r) => r.type === "openstack_instance").length;

  return (
    <Card
      sx={{
        "&:hover": {
          backgroundColor: "#f2f2f2",
        },
        border: "none",
        paddingBottom: 1,
      }}
    >
      <CardHeader
        title={
          <Typography>
            <b>{deployment.name || "<Unnamed>"}</b>{" "}
            {run.parameters?.find((e) => e.key === "region").value}
          </Typography>
        }
        avatar={
          <Avatar sx={{ width: "2rem", height: "2rem" }}>
            <Icon path={mdiRocketLaunch} size={0.8} />
          </Avatar>
        }
        action={
          <Grid container alignItems="center">
            <Grid item>
              <Box display="flex" mx={4}>
                <StatusIndicator deployment={deployment} />
              </Box>
            </Grid>
            <Divider orientation="vertical" flexItem />
            <Grid item>
              <Tooltip title="Edit">
                <ActionButton
                  // disable edit if deployment is in transitional state
                  disabled={
                    deployment.pending_status &&
                    deployment.pending_status !== "none"
                  }
                  onClick={(e) => {
                    e.preventDefault();
                    props.handleEdit(deployment.id);
                  }}
                >
                  <EditIcon />
                </ActionButton>
              </Tooltip>
            </Grid>
            <Grid item>
              <Tooltip title="Delete">
                <ActionButton
                  // disable delete if deployment is in transitional state
                  disabled={
                    deployment.pending_status &&
                    deployment.pending_status !== "none"
                  }
                  onClick={(e) => {
                    e.preventDefault();
                    props.handleDelete(deployment.id);
                  }}
                >
                  <DeleteIcon />
                </ActionButton>
              </Tooltip>
            </Grid>
          </Grid>
        }
      />
      <Grid
        sx={{ marginLeft: "3rem", marginTop: "-.5rem" }}
        container
        display="flex"
        justifyContent="flex-start"
      >
        {numInstances > 0 && (
          <Grid item>
            <Box display={"flex"}>
              <Box mx={1}>
                <LayersIcon fontSize="small" />
              </Box>
              <Box>
                {" "}
                <Typography variant="body2" color="textSecondary">
                  Instances: {numInstances}
                </Typography>
              </Box>
            </Box>
          </Grid>
        )}
        {props.template && (
          <Grid item>
            <Box display={"flex"}>
              <Box mx={1}>
                <CategoryIcon fontSize="small" />
              </Box>
              <Box>
                <Typography variant="body2" color="textSecondary">
                  <span>
                    <b>Template</b>: {props.template.name}
                  </span>
                </Typography>
              </Box>
            </Box>
          </Grid>
        )}
        <Grid item>
          <Box display={"flex"}>
            <Box mx={1}>
              <UpdateIcon fontSize="small" />
            </Box>
            <Box>
              <Typography variant="body2" color="textSecondary">
                <span>
                  <b>Updated</b>: {datetimeString(deployment.updated_at)}
                </span>
              </Typography>
            </Box>
          </Box>
        </Grid>
        {deployment.current_status === "creation_errored" && (
          <Grid item md={12}>
            <DeploymentErrorAlert status={deployment.current_status} />
          </Grid>
        )}

        {/* <Grid item sx={{display: "inline-flex", whiteSpace: "nowrap",paddingX:'2rem'}}>
              <StorageIcon fontSize="small" />
              <Typography variant='body2' color="textSecondary">
                Storage: ?
              </Typography>
            </Grid>
            <Grid item sx={{display: "inline-flex", whiteSpace: "nowrap",paddingX:'2rem'}}>
              <ShareIcon fontSize="small" />
              <Typography variant='body2' color="textSecondary">
                File Shares: ?
              </Typography>
            </Grid> */}
      </Grid>
    </Card>
  );
};

export default DeploymentSummary;
