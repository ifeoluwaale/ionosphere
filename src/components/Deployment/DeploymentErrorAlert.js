import React from "react";
import { Alert, Box, Typography } from "@mui/material";

const DeploymentErrorAlert = () => {
  return (
    <Box m={2} display={"flex"} width="90%">
      <Alert severity="warning">
        <Typography variant="body2">
          Errored deployments may have partially created resources that consume
          allocation. Try deleting or redeploying. If the error persists,
          contact support.
        </Typography>
      </Alert>
    </Box>
  );
};

export default DeploymentErrorAlert;
