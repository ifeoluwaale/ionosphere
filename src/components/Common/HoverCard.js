import { styled, Card } from "@mui/material";

const HoverCard = styled(Card)({
  boxShadow:
    "rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px",
  "&:hover": {
    border: ".5px solid rgb(0 0 0 / 20%)",
    boxShadow:
      "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
  },
});
export default HoverCard;
