import dayjs from "dayjs";

export function DateTime({ datetime }) {
  return dayjs(datetime).format("M/D/YYYY, h:mm A");
}

export function Date({ datetime }) {
  return dayjs(datetime).format("M/D/YYYY");
}
