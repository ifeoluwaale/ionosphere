/**
 * IconDialogTitle Component
 * ----------------------------
 * This is a customizable DialogTitle component with an avatar icon
 * and title on the left, and a close button on the right.
 *
 * Accepts the following props:
 * title: string
 * handleClose: function
 * icon: MUI Avatar or Icon
 */

import { Box, DialogTitle, Divider, IconButton } from "@mui/material";
import { Close as CloseIcon } from "@mui/icons-material";

export default function IconDialogTitle({ title, handleClose, icon }) {
  return (
    <>
      <DialogTitle sx={{ m: 0, p: 2 }} id="icon-dialog-title">
        <div style={{ display: "flex", alignItems: "center" }}>
          {icon && <Box mr={2}>{icon}</Box>}
          {title}
        </div>
      </DialogTitle>

      <IconButton
        aria-label="close"
        onClick={handleClose}
        sx={{
          position: "absolute",
          right: 12,
          top: 12,
          color: (theme) => theme.palette.grey[500],
        }}
      >
        <CloseIcon />
      </IconButton>

      <Divider />
    </>
  );
}
