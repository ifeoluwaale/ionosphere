import {
  Alert,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
} from "@mui/material";

export default function ConfirmationDialog({
  open,
  title,
  confirmText,
  infoText,
  handleClose,
  handleSubmit,
}) {
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      fullWidth
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          <Typography mb={2} color="textPrimary" textAlign={"center"}>
            Are you sure?
          </Typography>
          <Alert severity="info"> {infoText}</Alert>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button
          color="primary"
          variant="contained"
          onClick={() => {
            handleClose();
            handleSubmit();
          }}
        >
          {confirmText}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
