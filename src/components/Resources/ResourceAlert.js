import { Alert, Box, Typography } from "@mui/material";

/**
 * Provider-specific alert for Deployments page.
 *
 * @param {string} cloudName
 * @returns JSX.Element
 */

const ResourceAlert = ({ cloudName }) => {
  return (
    <>
      {cloudName && cloudName === "Jetstream 2" && (
        <Alert severity="info">
          <Typography variant="caption">
            CACAO does not display all resources, only the resources managed by
            CACAO.
          </Typography>
        </Alert>
      )}
    </>
  );
};

export default ResourceAlert;
