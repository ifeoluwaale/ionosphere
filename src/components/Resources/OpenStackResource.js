import {
  Box,
  IconButton,
  Link,
  ListItem,
  ListItemText,
  Stack,
  styled,
  Tooltip,
  Typography,
} from "@mui/material";
import {
  Computer as ComputerIcon,
  Info as InfoIcon,
  PlayArrow as PlayIcon,
  PowerSettingsNew as PowerIcon,
  Stop as StopIcon,
} from "@mui/icons-material";
import { mdiContentSave, mdiConsole } from "@mdi/js";
import Icon from "@mdi/react";
import { CopyToClipboardButton } from "@/components";

function instanceTypeString(type) {
  if (type === "openstack_instance") return "OpenStack";
  return "UnknownType";
}

const ActionButton = styled(IconButton)(({ theme }) => ({
  color: theme.palette.primary.main,
  width: "2em",
  height: "2em",
  top: "0.2em",
}));

// https://docs.openstack.org/nova/latest/reference/vm-states.html
function InstanceStatusIndicator({ status }) {
  let color = "#000000";
  let icon, label;

  switch (status) {
    case "active":
      color = "#388e3c";
      icon = <PlayIcon fontSize="small" style={{ fill: color }} />;
      label = `Active`;
      break;
    case "shelved_offloaded":
      icon = <StopIcon fontSize="small" style={{ fill: color }} />;
      label = `Shelved`;
      break;
    case "shutoff":
      icon = <PowerIcon fontSize="small" style={{ fill: color }} />;
      label = "Shutoff";
      break;
    default:
      label = status || "Status unknown";
  }

  return (
    <span style={{ display: "flex", alignItems: "center" }}>
      {icon}
      <Typography variant="overline" style={{ marginLeft: "2px" }}>
        {label}
      </Typography>
    </span>
  );
}

const ProxyUrl = ({ ip }) => {
  const formattedIp = ip.replace(/\./g, "-");
  const proxyUrl = `https://${formattedIp}.js2proxy.cacao.run`;
  return (
    <Link href={proxyUrl} target="_blank" rel="noopener noreferrer">
      {proxyUrl}
    </Link>
  );
};

const Resource = (props) => {
  if (props.type !== "openstack_instance") {
    return <></>;
  }

  return (
    <ListItem>
      <ListItemText
        primary={
          <Box>
            <Box display="flex" alignItems="center">
              <div style={{ marginRight: "1em" }}>
                <Typography variant="body1">{props.attributes.name}</Typography>
              </div>
              <InstanceStatusIndicator status={props.state} />
            </Box>
            <Box display="flex" alignItems="center" mb={2}>
              <div style={{ marginRight: "1em" }}>
                <Typography variant="body2">
                  {instanceTypeString(props.type)} Instance
                </Typography>
              </div>
            </Box>
          </Box>
        }
        secondary={
          <Stack spacing={0.5}>
            <Box>
              <b>ID:</b> {props.id}
            </Box>

            <Box>
              <b>Image/Size:</b> {props.attributes.image_name} /{" "}
              {props.attributes.flavor_name}
            </Box>

            <Box>
              <b>IP Address:</b> {props.attributes.access_ip_v4 || "unknown"}{" "}
              (private){" "}
              {props.attributes.access_ip_v4 && (
                <CopyToClipboardButton text={props.attributes.access_ip_v4} />
              )}{" "}
              / {props.attributes.floating_ip || "unknown"} (public){" "}
              {props.attributes.floating_ip && (
                <CopyToClipboardButton text={props.attributes.floating_ip} />
              )}
            </Box>
            {props.attributes.floating_ip && (
              <Stack direction={"row"}>
                <Box>
                  <b>Proxy URL: </b>{" "}
                  <ProxyUrl ip={props.attributes.floating_ip} />
                </Box>
                <Tooltip
                  title={
                    <Typography variant="caption">
                      The proxy URL supports HTTP services only and cannot be
                      used for SSH connections. Port 80 is proxied by default.
                    </Typography>
                  }
                >
                  <InfoIcon
                    style={{ fontSize: 18, marginLeft: "0.6rem" }}
                  ></InfoIcon>
                </Tooltip>
              </Stack>
            )}
          </Stack>
        }
      />
      <Tooltip title="Webshell">
        <ActionButton
          onClick={() => props.sessionHandler({ protocol: "ssh", ...props })}
        >
          <Icon path={mdiConsole} size={1} />
        </ActionButton>
      </Tooltip>
      <Tooltip title="Webdesktop">
        <ActionButton
          onClick={() => props.sessionHandler({ protocol: "vnc", ...props })}
        >
          <ComputerIcon />
        </ActionButton>
      </Tooltip>
      <Tooltip title=" Image">
        <ActionButton>
          <Icon path={mdiContentSave} size={1} />
        </ActionButton>
      </Tooltip>
    </ListItem>
  );
};

export default Resource;
