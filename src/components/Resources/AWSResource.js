import {
  Box,
  Tooltip,
  Typography,
  ListItem,
  ListItemText,
  IconButton,
  styled,
} from "@mui/material";
import {
  Computer as ComputerIcon,
  PowerSettingsNew as PowerIcon,
  PlayArrow as PlayIcon,
} from "@mui/icons-material";
import { mdiContentSave, mdiConsole } from "@mdi/js";
import Icon from "@mdi/react";
import { CopyToClipboardButton } from "../../components";

const ActionButton = styled(IconButton)(({ theme }) => ({
  color: theme.palette.primary.main,
  width: "2em",
  height: "2em",
  top: "0.2em",
}));

function instanceTypeString(type) {
  if (type === "aws_instance") return "AWS";
  return "UnknownType";
}

// https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-lifecycle.html
function InstanceStatusIndicator({ status }) {
  let color = "#000000";
  let icon, label;

  switch (status) {
    case "running":
      color = "#388e3c";
      icon = <PlayIcon fontSize="small" style={{ fill: color }} />;
      label = `Running`;
      break;
    case "pending":
      label = `Pending`;
      break;
    case "stopping":
      icon = <PowerIcon fontSize="small" style={{ fill: color }} />;
      label = `Stopping`;
      break;
    case "stopped":
      icon = <PowerIcon fontSize="small" style={{ fill: color }} />;
      label = `Stopped`;
      break;
    case "terminated":
      icon = <PowerIcon fontSize="small" style={{ fill: color }} />;
      label = "Terminated";
      break;
    case "shutting-down":
      icon = <PowerIcon fontSize="small" style={{ fill: color }} />;
      label = `Shutting-down`;
      break;
    default:
      label = status || "Status unknown";
  }

  return (
    <span style={{ display: "flex", alignItems: "center" }}>
      {icon}
      <Typography variant="overline" style={{ marginLeft: "2px" }}>
        {label}
      </Typography>
    </span>
  );
}

const Resource = (props) => {
  if (props.type !== "aws_instance") {
    return <></>;
  }
  return (
    <ListItem>
      <ListItemText
        primary={
          <Box>
            <Box display="flex" alignItems="center">
              <div style={{ marginRight: "1em" }}>
                <Typography variant="body1">{props.attributes.name}</Typography>
              </div>
              <InstanceStatusIndicator
                status={props.attributes.instance_state}
              />
            </Box>
            <Box display="flex" alignItems="center" mb={2}>
              <div style={{ marginRight: "1em" }}>
                <Typography variant="body2">
                  {instanceTypeString(props.type)} Instance
                </Typography>
              </div>
            </Box>
          </Box>
        }
        secondary={
          <>
            <b>ID:</b> {props.id.toUpperCase()}
            <br></br>
            <b>IP Address:</b> {props.attributes.private_ip || "unknown"}{" "}
            (private){" "}
            {props.attributes.private_ip && (
              <CopyToClipboardButton text={props.attributes.private_ip} />
            )}{" "}
            / {props.attributes.public_ip || "unknown"} (public){" "}
            {props.attributes.public_ip && (
              <CopyToClipboardButton text={props.attributes.public_ip} />
            )}
            <br></br>
            <b>Image/Size:</b> {props.attributes.ami} /{" "}
            {props.attributes.instance_type}
          </>
        }
      />
      <Tooltip title="Webshell">
        <ActionButton
          onClick={() => props.sessionHandler({ protocol: "ssh", ...props })}
        >
          <Icon path={mdiConsole} size={1} />
        </ActionButton>
      </Tooltip>
      <Tooltip title="Webdesktop">
        <ActionButton
          onClick={() => props.sessionHandler({ protocol: "vnc", ...props })}
        >
          <ComputerIcon />
        </ActionButton>
      </Tooltip>
      <Tooltip title=" Image">
        <ActionButton>
          <Icon path={mdiContentSave} size={1} />
        </ActionButton>
      </Tooltip>
    </ListItem>
  );
};

/**
 * Match AWS instance with the elastic-ip associated with them, and
 * embed the public ip as an attribute on the AWS instance resource.
 *
 * preferably this should run on the server-side.
 * @param {[Object]} resources
 */
export function matchAWSInstanceIP(resources) {
  for (let i = 0; i < resources.length; i++) {
    if (resources[i].type !== "aws_instance") {
      continue;
    }
    let public_ip = null;
    for (let j = 0; j < resources.length; j++) {
      if (resources[j].type === "aws_eip_association") {
        if (resources[j].attributes.instance_id === resources[i].id) {
          public_ip = resources[j].attributes.public_ip;
          break;
        }
      }
    }
    if (public_ip) {
      resources[i].attributes.public_ip = public_ip;
    }
  }
}

export default Resource;
