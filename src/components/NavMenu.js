import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import {
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Tooltip,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { menuItems } from "../menuItems.js";

const NavButton = ({ item, open, selected }) => {
  const theme = useTheme();

  const ButtonContent = (
    <ListItemButton
      sx={{
        minHeight: 48,
        justifyContent: open ? "initial" : "center",
        borderRadius: "5px",
        margin: 1,
        "&.Mui-selected": {
          backgroundColor: theme.palette.sidebar.selectedItemBackground,
          "& .MuiListItemText-primary": {
            fontWeight: "600",
          },
          "&:hover": {
            backgroundColor: theme.palette.sidebar.selectedItemBackground,
          },
        },
        "&:hover": {
          "& .MuiListItemText-primary": {
            fontWeight: "600", // Make text bold on hover
          },
          "& .MuiListItemIcon-root": {
            transform: "scale(1.2)", // Scale up the icon on hover
          },
        },
        "& .MuiListItemIcon-root": {
          color: selected
            ? theme.palette.sidebar.selectedItemColor
            : theme.palette.sidebar.itemColor,
          transition: "transform 0.1s ease-in-out", // Transition for the transform
        },
        "& .MuiListItemText-primary": {
          fontWeight: "500",
          color: selected
            ? theme.palette.sidebar.selectedItemColor
            : theme.palette.sidebar.itemColor,
        },
      }}
      selected={selected}
    >
      <ListItemIcon
        sx={{
          minWidth: 0,
          mr: open ? 3 : "auto",
          justifyContent: "center",
        }}
      >
        {item.icon}
      </ListItemIcon>
      <ListItemText primary={item.label} sx={{ opacity: open ? 1 : 0 }} />
    </ListItemButton>
  );

  return (
    <Link href={item.path}>
      <ListItem key={item.label} disablePadding sx={{ display: "block" }}>
        {open ? (
          ButtonContent
        ) : (
          <Tooltip title={item.label} placement="right">
            {ButtonContent}
          </Tooltip>
        )}
      </ListItem>
    </Link>
  );
};

export default function NavMenu({ showStaff, open }) {
  const route = useRouter().route;
  const navItems = menuItems.filter((item) => !item.restricted || showStaff);

  return (
    <List>
      {navItems.map((item, index) => (
        <NavButton
          key={index}
          item={item}
          open={open}
          selected={route.startsWith(item.path)}
        />
      ))}
    </List>
  );
}
