import {
  Box,
  FormControl,
  FormControlLabel,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  Switch,
  Typography,
  ListItem,
  ListItemText,
} from "@mui/material";
import Checkbox from "./Checkbox";
import SizeGB from "./SizeGB";

export default function BootDisk({ wizard }) {
  if (!wizard.values.hasOwnProperty("config_boot_disk"))
    wizard.values["config_boot_disk"] = wizard.values["root_storage_size"] > 0;
  return (
    <>
      <Grid container justifyContent="flex-start" spacing={2}>
        <Grid item md={12}>
          <Box mb={3}>
            <FormControl variant="outlined" fullWidth>
              <FormControlLabel
                control={
                  <Switch
                    checked={wizard.values["config_boot_disk"]}
                    disabled={wizard.isEditing}
                    onChange={(e) => {
                      wizard.onChange("config_boot_disk", e.target.checked);
                    }}
                  />
                }
                label={
                  wizard.isEditing
                    ? "Boot disk configuration locked post-deployment"
                    : "Configure boot disk"
                }
              />
            </FormControl>
          </Box>
        </Grid>
      </Grid>

      <Grid
        container
        justifyContent="center"
        alignItems="flex-start"
        spacing={2}
      >
        {wizard.values["config_boot_disk"] && (
          <>
            <Grid item xs={12} sm={6} md={6}>
              <FormControl variant="outlined" fullWidth>
                <InputLabel id="root_storage_source" shrink>
                  Boot Source
                </InputLabel>
                <Select
                  name="root_storage_source"
                  id="root_storage_source"
                  disabled={
                    wizard.isEditing &&
                    !wizard.isParamEditable("root_storage_source")
                  }
                  value={wizard.values["root_storage_source"]}
                  label="Boot Source"
                  onChange={(e) =>
                    wizard.onChange("root_storage_source", e.target.value)
                  }
                >
                  <MenuItem value="image">Image</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
              <FormControl variant="outlined" fullWidth>
                <InputLabel id="root_storage_type" shrink>
                  Boot Type
                </InputLabel>
                <Select
                  name="root_storage_type"
                  id="root_storage_type"
                  disabled={
                    wizard.isEditing &&
                    !wizard.isParamEditable("root_storage_type")
                  }
                  value={wizard.values["root_storage_type"]}
                  label="Boot Type"
                  onChange={(e) =>
                    wizard.onChange("root_storage_type", e.target.value)
                  }
                >
                  <MenuItem value="local">Local</MenuItem>
                  <MenuItem value="volume">Volume</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              {wizard.values["root_storage_type"] === "volume" && (
                <SizeGB
                  disabled={
                    (wizard.isEditing &&
                      !wizard.isParamEditable("root_storage_size")) ||
                    wizard.values["root_storage_type"] !== "volume"
                  }
                  label={"Storage in GB"}
                  name={"root_storage_size"}
                  wizard={wizard}
                />
              )}
            </Grid>
            <Grid item xs={12}>
              {wizard.values["root_storage_type"] === "volume" && (
                <Checkbox
                  disabled={
                    (wizard.isEditing &&
                      !wizard.isParamEditable("root_storage_type")) ||
                    wizard.values["root_storage_type"] !== "volume"
                  }
                  label="Delete boot disk when VMs are deleted?"
                  name="root_storage_delete_on_termination"
                  wizard={wizard}
                />
              )}
            </Grid>
          </>
        )}
      </Grid>
    </>
  );
}

export function BootDiskAddParams(params) {
  params.push({ name: "root_storage_source", type: "string", editable: false });
  params.push({ name: "root_storage_type", type: "string", editable: false });
  params.push({ name: "root_storage_size", type: "integer", editable: false });
  params.push({
    name: "root_storage_delete_on_termination",
    type: "boolean",
    editable: false,
  });
}
export function BootDiskAddReviewItems(add_item, values) {
  if (!values["config_boot_disk"]) return;
  add_item("root_storage_source", "Boot Source");
  add_item("root_storage_type", "Boot Type");
  if (values["root_storage_type"] === "volume") {
    add_item("root_storage_size", "Boot Size");
    add_item(
      "root_storage_delete_on_termination",
      "Delete when VMs are deleted?"
    );
  }
}

// TODO: delete this after converting everything to use BootDiskAddReviewItems()
export function BootDiskReview({ values }) {
  return (
    <>
      {values.config_boot_disk && (
        <Grid
          container
          direction="row"
          justifyContent="flex-start"
          alignContent="center"
          alignItems="center"
        >
          <Grid xs={12} sm={12} md={6}>
            <ListItem>
              <ListItemText
                primary={
                  <Typography>
                    <Box component="span" sx={{ fontWeight: "bold" }}>
                      Boot Source:{" "}
                    </Box>
                    {values.root_storage_source}
                  </Typography>
                }
              />
            </ListItem>
          </Grid>
          <Grid xs={12} sm={12} md={6}>
            <ListItem>
              <ListItemText
                primary={
                  <Typography>
                    <Box component="span" sx={{ fontWeight: "bold" }}>
                      Boot Type:{" "}
                    </Box>
                    {values.root_storage_type}
                  </Typography>
                }
              />
            </ListItem>
          </Grid>
          {values["root_storage_type"] === "volume" && (
            <Grid xs={12} sm={12} md={6}>
              <ListItem>
                <ListItemText
                  primary={
                    <Typography>
                      <Box component="span" sx={{ fontWeight: "bold" }}>
                        Boot Size:{" "}
                      </Box>
                      {values.root_storage_size} GB
                    </Typography>
                  }
                />
              </ListItem>
            </Grid>
          )}
          <Grid xs={12} sm={12} md={6}>
            <ListItem>
              <ListItemText
                primary={
                  <Typography>
                    <Box component="span" sx={{ fontWeight: "bold" }}>
                      Delete when VMs are deleted?:{" "}
                    </Box>
                    {values.root_storage_delete_on_termination ? "Yes" : "No"}
                  </Typography>
                }
              />
            </ListItem>
          </Grid>
        </Grid>
      )}
    </>
  );
}

// TODO: delete this after everything is converted to using parameter defaults
export function BootDiskSetInitialValues(values) {
  if (!values.hasOwnProperty("root_storage_source"))
    values["root_storage_source"] = "image";
  if (!values.hasOwnProperty("root_storage_type"))
    values["root_storage_type"] = "local";
  if (!values.hasOwnProperty("root_storage_delete_on_termination"))
    values["root_storage_delete_on_termination"] = true;
  if (!values.hasOwnProperty("config_boot_disk"))
    values["config_boot_disk"] = values["root_storage_size"] > 0;
}

export function BootDiskSetRunParams(runParams, values) {
  runParams.push({
    key: "root_storage_source",
    value: values["config_boot_disk"] ? values["root_storage_source"] : "image",
  });
  runParams.push({
    key: "root_storage_type",
    value: values["config_boot_disk"] ? values["root_storage_type"] : "local",
  });
  runParams.push({
    key: "root_storage_size",
    value:
      values["config_boot_disk"] && values["root_storage_size"]
        ? values["root_storage_size"].toString()
        : "0",
  });
  runParams.push({
    key: "root_storage_delete_on_termination",
    value: values["config_boot_disk"]
      ? values["root_storage_delete_on_termination"]
        ? "true"
        : "false"
      : "false",
  });
}

export function BootDiskValidate(values) {
  return (
    !values["config_boot_disk"] ||
    values["root_storage_type"] === "local" ||
    values["root_storage_size"] > 0
  );
}
