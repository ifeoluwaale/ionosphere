import { Checkbox as MUICheckbox, FormControlLabel, Grid } from "@mui/material";

export default function Checkbox({ disabled, half, label, name, wizard }) {
  return (
    <Grid container justifyContent="center" alignItems="flex-start" spacing={1}>
      <Grid item xs={12} sm={half ? 6 : 12}>
        <FormControlLabel
          control={
            <MUICheckbox
              checked={wizard.values[name]}
              color="primary"
              disabled={
                (wizard.isEditing && !wizard.isParamEditable(name)) || disabled
              }
              name={name}
              onChange={(e) => wizard.onChange(name, e.target.checked)}
            />
          }
          label={label}
        />
      </Grid>
    </Grid>
  );
}
