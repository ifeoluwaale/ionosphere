import { Box, Typography } from "@mui/material";
import Alert from "@mui/material/Alert";
//import AlertIcon from '@mui/icons-material/Alert';

export default function Disclaimer() {
  return (
    <>
      <Box
        display="flex"
        justifyContent="center"
        alignContent="flex-start"
        alignItems="flex-start"
      >
        <Box display="flex" justifyContent="center" mb={2}>
          <Alert severity="info">
            Windows server images are not yet supported.
          </Alert>
        </Box>
      </Box>
    </>
  );
}
