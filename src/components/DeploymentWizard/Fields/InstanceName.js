import { FormControl, TextField } from "@mui/material";
import { ValidateDeploymentName } from "../../../utils";

export default function InstanceName({ item, wizard }) {
  let param = item && wizard.get_param?.(item);
  let name = param?.name || "instance_name";
  let value = wizard.values[name];
  let label = item?.ui_label || "Deployment Name";
  return (
    <FormControl variant="outlined" fullWidth>
      <TextField
        name={name}
        label={label}
        disabled={
          wizard.isEditing &&
          !(param?.editable || wizard.isParamEditable?.(name))
        }
        fullWidth
        required
        autoComplete="off"
        inputProps={{ maxLength: 32 }}
        error={value?.length > 0 && wizard.nameError !== null}
        helperText={
          (value?.length > 0 && wizard.nameError) || item?.helper_text
        }
        value={value || param?.default || ""}
        variant="outlined"
        onChange={(e) => {
          wizard.onChange(name, e.target.value);
          wizard.setNameError(ValidateDeploymentName(e.target.value));
        }}
      />
    </FormControl>
  );
}
