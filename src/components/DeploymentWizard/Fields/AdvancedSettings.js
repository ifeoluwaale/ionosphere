import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Link,
  Typography,
} from "@mui/material";
import { ExpandMore as ExpandMoreIcon } from "@mui/icons-material";

export default function AdvancedSettings({ item, children }) {
  return (
    <Accordion elevation={0}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="advanced-settings"
        id="advanced-settings-header"
      >
        <Typography>
          <Link
            sx={{
              color: "primary",
              textDecoration: "none",
              "&:hover": { textDecoration: "underline" },
            }}
          >
            {item?.title || "Advanced Settings"}
          </Link>
        </Typography>
      </AccordionSummary>
      <AccordionDetails sx={{ alignItems: "center", padding: "0px" }}>
        <Box>{children}</Box>
      </AccordionDetails>
    </Accordion>
  );
}
