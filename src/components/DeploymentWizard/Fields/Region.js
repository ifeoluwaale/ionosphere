import { FormControl, InputLabel, Select, MenuItem } from "@mui/material";

const Region = ({ item, wizard }) => {
  let param = item && wizard.get_param(item);
  let name = param?.name || "regionId";
  let value = wizard.values[name];
  let label = item?.ui_label || "Region";
  return (
    <FormControl variant="outlined" fullWidth>
      <InputLabel>{label}</InputLabel>
      <Select
        name={name}
        value={value}
        label={label}
        onChange={(e) => {
          wizard.setRegionId(e.target.value);
          wizard.onChange(name, e.target.value);
        }}
        renderValue={(option) => option}
        disabled={wizard.isEditing}
      >
        {wizard.regions?.map((region, index) => (
          <MenuItem key={index} value={region.id}>
            {region.name}
          </MenuItem>
        ))}
        {!wizard.regions && (
          <MenuItem key={0} value={value}>
            {value}
          </MenuItem>
        )}
      </Select>
    </FormControl>
  );
};

export default Region;
