import {
  Chip,
  FormControl,
  InputLabel,
  ListItem,
  TextField,
  Typography,
} from "@mui/material";
import Autocomplete, { createFilterOptions } from "@mui/material/Autocomplete";
import { datetimeString } from "../../../utils";
import Disclaimer from "./Disclaimer";

export default function ImageID({ item, disclaimer, wizard }) {
  let param = wizard.get_param?.(item);
  let name = param?.name || "image";
  let value = wizard.images?.find((image) => image.id === wizard.values[name]);
  let label = item?.ui_label || "Image";
  return (
    <>
      {!wizard.isEditing &&
        ((item &&
          wizard.get_item_parameter(item, "windows_disclaimer")?.value) ||
          disclaimer) && <Disclaimer />}
      <FormControl variant="outlined" fullWidth>
        <InputLabel />
        <Autocomplete
          name={name}
          disabled={
            wizard.isEditing &&
            !(param?.editable || wizard.isParamEditable?.(name))
          }
          value={value}
          options={wizard.images || []}
          getOptionLabel={(option) => option.name}
          onChange={(event, option) =>
            option && wizard.onChange(name, option.id)
          }
          renderInput={(params) => (
            <TextField {...params} label={label} variant="outlined" />
          )}
          renderOption={(props, option) => (
            <>
              <ListItem key={option.id} {...props} sx={{ display: "flex" }}>
                <Typography>{option.name}</Typography>
              </ListItem>
              {option.updated_at && (
                <ListItem key={option.updated_at}>
                  <Typography variant="caption">
                    {datetimeString(option.updated_at)}
                    {option.owner ? " by " + option.owner : ""}
                  </Typography>
                </ListItem>
              )}
              {option.tags?.length > 0 && (
                <ListItem>
                  {option.tags
                    .sort((a, b) =>
                      a.toLowerCase().localeCompare(b.toLowerCase())
                    )
                    .map((tag, index) => (
                      <Chip
                        key={option.id + index}
                        label={tag}
                        size="small"
                        style={{
                          fontSize: "0.7rem",
                          margin: "2px 2px",
                        }}
                      />
                    ))}
                </ListItem>
              )}
            </>
          )}
          filterOptions={createFilterOptions({
            stringify: (option) =>
              option.name +
              (option.owner ? " " + option.owner : "") +
              (option.tags?.length > 0 ? " " + option.tags.join(" ") : ""),
          })}
        />
      </FormControl>
    </>
  );
}
