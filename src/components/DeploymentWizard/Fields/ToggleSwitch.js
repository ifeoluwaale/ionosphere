import { FormControl, FormControlLabel, Switch } from "@mui/material";

export default function ToggleSwitch({
  item,
  name,
  label,
  helperText,
  required,
  wizard,
}) {
  let param = item && wizard.get_param?.(item);
  name = name || param?.name;
  label = item?.ui_label || label || name;
  required = param?.required || required;
  return (
    <FormControl variant="outlined" fullWidth>
      <FormControlLabel
        control={
          <Switch
            checked={wizard.values[name] || param?.default}
            disabled={
              wizard.isEditing &&
              !(param?.editable || wizard.isParamEditable?.(name))
            }
            onChange={(e) => {
              wizard.onChange(name, e.target.checked);
            }}
            required={required}
          />
        }
        label={label}
      />
    </FormControl>
  );
}
