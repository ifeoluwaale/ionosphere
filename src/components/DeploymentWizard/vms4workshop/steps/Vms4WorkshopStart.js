import {
  Box,
  Typography,
  Grid,
  Chip,
  TextField,
  FormControl,
  InputLabel,
  ListItem,
  Select,
  MenuItem,
  CircularProgress,
} from "@mui/material";
import Autocomplete, { createFilterOptions } from "@mui/material/Autocomplete";
import {
  datetimeString,
  filterFlavorsByMinValues,
  ValidateDeploymentName,
} from "../../../../utils";
import InfoIcon from "@mui/icons-material/Info";
import Disclaimer from "../../Fields/Disclaimer";
import BootDisk from "../../Fields/BootDisk";

const VMs4WorkshopParameters = ({
  values,
  onChange,
  nameError,
  setNameError,
  images,
  flavors,
  loadingImages,
  isEditing,
  isParamEditable,
}) => {
  const powerStateOptions = [
    "active",
    "shutoff",
    "suspend",
    "shelved_offloaded",
  ];

  flavors = filterFlavorsByMinValues(
    flavors,
    images?.find((image) => image.name === values["image_name"])?.min_disk,
    images?.find((image) => image.name === values["image_name"])?.min_ram
  );
  if (
    flavors?.length > 0 &&
    !flavors.find((flavor) => flavor.name === values["flavor"])
  )
    values["flavor"] = flavors[0].name;
  // expose boot disk parameters
  const wizard = {
    flavors: flavors,
    images: images,
    isEditing: isEditing,
    isParamEditable: isParamEditable,
    nameError: nameError,
    onChange: onChange,
    setNameError: setNameError,
    values: values,
  };

  return (
    <>
      {loadingImages && (
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <CircularProgress />
        </Box>
      )}
      {!loadingImages && (
        <Box ml={1} mr={1}>
          <Grid
            container
            justifyContent="center"
            alignItems="flex-start"
            spacing={2}
            mb={2}
          >
            <Grid item xs={12} sm={12} md={12}>
              <Disclaimer />
            </Grid>
            <Grid item xs={12} sm={isEditing ? 6 : 12} md={isEditing ? 6 : 8}>
              <TextField
                name="instance_name"
                label="Deployment Name"
                disabled={isEditing && !isParamEditable("instance_name")}
                fullWidth
                required
                autoComplete="off"
                inputProps={{ maxLength: 32 }}
                error={values["instance_name"].length > 0 && nameError}
                helperText={values["instance_name"].length > 0 && nameError}
                value={values["instance_name"]}
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => {
                  onChange("instance_name", e.target.value);
                  setNameError(ValidateDeploymentName(e.target.value));
                }}
              />
            </Grid>
            {/* Show power state if editing */}
            {isEditing && (
              <Grid item xs={12} sm={4} md={2}>
                <FormControl variant="outlined" fullWidth>
                  <InputLabel id="parameter-size" shrink>
                    Power State
                  </InputLabel>
                  <Select
                    name="power_state"
                    id="power_state"
                    disabled={isEditing && !isParamEditable("power_state")}
                    value={values["power_state"]}
                    label="Power State"
                    onChange={(e) => onChange("power_state", e.target.value)}
                    renderValue={(option) => option}
                  >
                    {powerStateOptions.map((option, index) => (
                      <MenuItem key={index} value={option}>
                        {option}
                      </MenuItem>
                    ))}
                  </Select>
                  {/* <FormHelperText>{errors['flavor']}</FormHelperText> */}
                </FormControl>
              </Grid>
            )}
          </Grid>
          <Grid
            container
            justifyContent="center"
            alignItems="flex-start"
            spacing={2}
            mb={2}
          >
            <Grid item xs={12} sm={6} md={4}>
              <FormControl variant="outlined" fullWidth>
                <InputLabel id="image_name" shrink />
                <Autocomplete
                  id="image_name"
                  name="image_name"
                  disabled={isEditing && !isParamEditable("image_name")}
                  value={
                    images &&
                    images.find((image) => image.name === values["image_name"])
                  }
                  options={images || []}
                  getOptionLabel={(option) => option.name}
                  onChange={(event, option) =>
                    option && onChange("image_name", option.name)
                  }
                  renderInput={(params) => (
                    <TextField {...params} label="Image" variant="outlined" />
                  )}
                  renderOption={(props, option) => (
                    <>
                      <ListItem {...props} sx={{ display: "flex" }}>
                        <Typography>{option.name}</Typography>
                      </ListItem>
                      {option.updated_at && (
                        <ListItem>
                          <Typography variant="caption">
                            {datetimeString(option.updated_at)}
                            {option.owner ? " by " + option.owner : ""}
                          </Typography>
                        </ListItem>
                      )}
                      {option.tags?.length > 0 && (
                        <ListItem>
                          {option.tags
                            .sort((a, b) =>
                              a.toLowerCase().localeCompare(b.toLowerCase())
                            )
                            .map((tag, index) => (
                              <Chip
                                key={index}
                                label={tag}
                                size="small"
                                style={{
                                  fontSize: "0.7rem",
                                  margin: "2px 2px",
                                }}
                              />
                            ))}
                        </ListItem>
                      )}
                    </>
                  )}
                  filterOptions={createFilterOptions({
                    stringify: (option) =>
                      option.name +
                      (option.owner ? " " + option.owner : "") +
                      (option.tags?.length > 0
                        ? " " + option.tags.join(" ")
                        : ""),
                  })}
                />
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <FormControl variant="outlined" fullWidth>
                <InputLabel id="parameter-size" shrink>
                  Size
                </InputLabel>
                <Select
                  name="flavor"
                  id="parameter-flavor"
                  disabled={isEditing && !isParamEditable("flavor")}
                  value={
                    (flavors &&
                      flavors.find((flavor) => flavor.name === values["flavor"])
                        ?.name) ||
                    "m3.medium"
                  }
                  label="Size"
                  onChange={(e) => onChange("flavor", e.target.value)}
                  renderValue={(option) => option}
                >
                  {flavors &&
                    flavors.map((flavor, index) => (
                      <MenuItem key={index} value={flavor.name}>
                        <Box display="flex" alignItems="center">
                          <Typography
                            style={{ fontWeight: "bold", width: "8em" }}
                          >
                            {flavor.name}
                          </Typography>
                          <Typography
                            variant="body2"
                            style={{ width: "4.375rem" }}
                          >
                            vCPU: {flavor.vcpus}
                          </Typography>
                          <Typography
                            variant="body2"
                            style={{ width: "5.9375rem" }}
                          >
                            RAM: {flavor.ram}
                          </Typography>
                          <Typography
                            variant="body2"
                            style={{ width: "3.75rem" }}
                          >
                            Disk: {flavor.disk}
                          </Typography>
                        </Box>
                      </MenuItem>
                    ))}
                </Select>
                {/* <FormHelperText>{errors['flavor']}</FormHelperText> */}
              </FormControl>
            </Grid>
          </Grid>
          <Grid
            container
            justifyContent="center"
            alignItems="flex-start"
            spacing={2}
            mb={2}
          >
            <Grid item xs={12} sm={6} md={4}>
              <FormControl variant="outlined" fullWidth>
                <TextField
                  id="students_per_instance"
                  name="students_per_instance"
                  disabled={
                    isEditing && !isParamEditable("students_per_instance")
                  }
                  label="No. of Students per VM"
                  type="number"
                  step="1"
                  value={values["students_per_instance"]}
                  onChange={(e) => {
                    // prevent decrementing below 1
                    if (e.target.value < 1) {
                      e.preventDefault();
                    } else {
                      onChange("students_per_instance", e.target.value);
                    }
                  }}
                  // prevent typing
                  onKeyDown={(e) => {
                    e.preventDefault();
                  }}
                  // hide cursor
                  style={{ caretColor: "transparent" }}
                  variant="outlined"
                  required
                  readOnly
                />
                {/* <FormHelperText>{errors['flavor']}</FormHelperText> */}
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <FormControl variant="outlined" fullWidth>
                <TextField
                  id="instance_instructor_count"
                  name="instance_instructor_count"
                  disabled={
                    isEditing && !isParamEditable("instance_instructor_count")
                  }
                  label="Total No. of Instructor VMs"
                  type="number"
                  step="1"
                  value={values["instance_instructor_count"]}
                  onChange={(e) => {
                    // prevent decrementing below 1
                    if (e.target.value < 1) {
                      e.preventDefault();
                    } else {
                      onChange("instance_instructor_count", e.target.value);
                    }
                  }}
                  // prevent typing
                  onKeyDown={(e) => {
                    e.preventDefault();
                  }}
                  // hide cursor
                  style={{ caretColor: "transparent" }}
                  variant="outlined"
                  required
                  readOnly
                />
                {/* <FormHelperText>{errors['flavor']}</FormHelperText> */}
              </FormControl>
            </Grid>
          </Grid>
          <Grid
            container
            direction="row"
            justifyContent="center"
            spacing={2}
            mb={2}
          >
            <Grid item xs={12} sm={6} md={4}>
              <FormControl variant="outlined" fullWidth>
                <TextField
                  id="spare_instances"
                  name="spare_instances"
                  // disabled={isEditing && !isParamEditable('spare_instances')}
                  label="No. of Spare VMs"
                  type="number"
                  step="1"
                  value={values["spare_instances"]}
                  onChange={(e) => {
                    // prevent decrementing below 1
                    if (e.target.value < 0) {
                      e.preventDefault();
                    } else {
                      onChange("spare_instances", e.target.value);
                    }
                  }}
                  // prevent typing
                  onKeyDown={(e) => {
                    e.preventDefault();
                  }}
                  // hide cursor
                  style={{ caretColor: "transparent" }}
                  variant="outlined"
                  required
                  readOnly
                />
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Box
                style={{ padding: ".5rem" }}
                display="flex"
                alignItems="flex-start"
              >
                <InfoIcon color="primary" style={{ marginRight: ".2rem" }} />
                <Typography variant="body2">
                  Adding spare VMs is optional and intended for extra students
                  joining during the workshop.{" "}
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} md={8}>
              <BootDisk wizard={wizard} />
            </Grid>
          </Grid>
        </Box>
      )}
    </>
  );
};

export default VMs4WorkshopParameters;
