import { Box, Stack, TextField, Typography } from "@mui/material";
import VMAssignmentInfo from "./VMAssignmentInfo";

export default function GenerateUsers({ values, onChange }) {
  return (
    <Box sx={{ maxWidth: "90%", margin: "0 auto" }}>
      <Stack spacing={3}>
        <Typography variant="h6" component="h2">
          Autogenerate Student Usernames
        </Typography>

        <Stack spacing={2}>
          <Typography>
            To autogenerate usernames, enter a username prefix and number of
            users. If you have already added users, you can skip this step.
          </Typography>
        </Stack>
        <Box display="flex" alignItems="flex-start" mb={4}>
          <Box sx={{ marginRight: "1rem", minWidth: "10rem" }}>
            <TextField
              id="generate_username_prefix"
              name="generate_username_prefix"
              value={values["generate_username_prefix"]}
              onChange={(e) =>
                onChange("generate_username_prefix", e.target.value)
              }
              label="Username Prefix"
              type="text"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Box>
          <Box sx={{ marginRight: "1rem", minWidth: "10rem" }}>
            <TextField
              id="generate_username_count"
              name="generate_username_count"
              value={values["generate_username_count"]}
              onChange={(e) =>
                onChange("generate_username_count", e.target.value)
              }
              label="Number of users"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Box>
        </Box>
        <Typography variant="subtitle2">
          Example: Prefix of "bios" and amount of three, you will get "bios1",
          "bios2", "bios3" for usernames.
        </Typography>

        <Box mt={2}>
          <VMAssignmentInfo></VMAssignmentInfo>
        </Box>
      </Stack>
    </Box>
  );
}
