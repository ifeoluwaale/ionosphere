import {
  Box,
  Grid,
  ListItem,
  ListItemText,
  Typography,
  Alert,
} from "@mui/material";
import { BootDiskReview } from "../../Fields/BootDisk";

const VMs4WorkshopReview = (props) => {
  const newUserList = props.buildUserList(props.values);
  const oldUserList = props.isEditing
    ? props.buildUserList(props.oldValues)
    : undefined;

  return (
    <Box sx={{ margin: "2rem" }}>
      {props.changedParams?.length > 0 && (
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignContent="center"
          alignItems="center"
        >
          <Grid xs={12} md={9}>
            <Alert severity="info" variant="outlined">
              <Typography>
                Please review your changes (highlighed in{" "}
                <Box
                  component={"span"}
                  sx={{
                    color: "#ff007f",
                    fontWeight: "bold",
                    textDecoration: "underline",
                  }}
                >
                  pink and underlined
                </Box>
                ).
              </Typography>{" "}
            </Alert>
          </Grid>
        </Grid>
      )}
      {props.changedParams?.length === 0 && (
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignContent="center"
          alignItems="flex-start"
        >
          <Grid xs={12}>
            <Box display="flex" justifyContent="center" mt={3}>
              <Typography style={{ color: "#ff007f" }}>
                No changes have been made.
              </Typography>
            </Box>
          </Grid>
        </Grid>
      )}
      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignContent="center"
        alignItems="flex-start"
      >
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color: props.changedParams?.includes("regionId")
                      ? "#ff007f"
                      : null,
                    textDecoration: props.changedParams?.includes("regionId")
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Region:{" "}
                  </Box>{" "}
                  {props.deploymentValues["regionId"]}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color: props.changedParams?.includes("instance_name")
                      ? "#ff007f"
                      : null,
                    textDecoration: props.changedParams?.includes(
                      "instance_name"
                    )
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Instance Name:{" "}
                  </Box>{" "}
                  {props.values.instance_name}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography color="inherit">
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Template:{" "}
                  </Box>{" "}
                  {props.template.name}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        {props.isEditing && (
          <Grid xs={12} sm={12} md={6}>
            <ListItem>
              <ListItemText
                primary={
                  <Typography
                    color="inherit"
                    style={{
                      color: props.changedParams?.includes("power_state")
                        ? "#ff007f"
                        : null,
                      textDecoration: props.changedParams?.includes(
                        "power_state"
                      )
                        ? "underline"
                        : null,
                    }}
                  >
                    <Box component="span" sx={{ fontWeight: "bold" }}>
                      Power State:{" "}
                    </Box>
                    {props.values.power_state}
                  </Typography>
                }
              />
            </ListItem>
          </Grid>
        )}
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color: props.changedParams?.includes("image_name")
                      ? "#ff007f"
                      : null,
                    textDecoration: props.changedParams?.includes("image_name")
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Image Name:{" "}
                  </Box>{" "}
                  {props.values.image_name}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color: props.changedParams?.includes("flavor")
                      ? "#ff007f"
                      : null,
                    textDecoration: props.changedParams?.includes("flavor")
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Master Flavor:{" "}
                  </Box>
                  {props.values.flavor}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color:
                      props.isEditing &&
                      oldUserList.length !== newUserList.length
                        ? "#ff007f"
                        : undefined,
                    textDecoration: props.changedParams?.includes("newUserList")
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Students:{" "}
                  </Box>
                  {newUserList.length}{" "}
                  {newUserList.length === 1 ? "student" : "students"}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color:
                      props.isEditing &&
                      props.changedParams?.includes("students_per_instance")
                        ? "#ff007f"
                        : undefined,
                    textDecoration: props.changedParams?.includes(
                      "students_per_instance"
                    )
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Students per VM:{" "}
                  </Box>
                  {props.values.students_per_instance}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color:
                      props.isEditing &&
                      props.oldValues.spare_instances !==
                        props.values.spare_instances
                        ? "#ff007f"
                        : undefined,
                    textDecoration: props.changedParams?.includes(
                      "spare_instances"
                    )
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Spare VMs:{" "}
                  </Box>
                  {props.values.spare_instances}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color: props.changedParams?.includes(
                      "instance_instructor_count"
                    )
                      ? "#ff007f"
                      : null,
                    textDecoration: props.changedParams?.includes(
                      "instance_instructor_count"
                    )
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Instructor VMs:{" "}
                  </Box>{" "}
                  {props.values.instance_instructor_count}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color: props.changedParams?.includes("csv_email")
                      ? "#ff007f"
                      : null,
                    textDecoration: props.changedParams?.includes("csv_email")
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Email for CSV:{" "}
                  </Box>{" "}
                  {props.values.csv_email}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color:
                      props.isEditing &&
                      (props.oldValues.instructors_ssh_keys.length !==
                        props.values.instructors_ssh_keys.length ||
                        !props.oldValues.instructors_ssh_keys.every((key) =>
                          props.values.instructors_ssh_keys.includes(key)
                        ))
                        ? "#ff007f"
                        : undefined,
                    textDecoration: props.changedParams?.includes(
                      "instructors_ssh_keys"
                    )
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Instructor SSH Keys:{" "}
                  </Box>{" "}
                  {props.values.instructors_ssh_keys.length}{" "}
                  {props.values.instructors_ssh_keys.length === 1
                    ? "key"
                    : "keys"}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        {props.values.enable_shared_name && (
          <Grid xs={12} sm={12} md={6}>
            <Box>
              <ListItem>
                <ListItemText
                  primary={
                    <Typography
                      style={{
                        color:
                          props.isEditing &&
                          props.changedParams?.includes("share_name")
                            ? "#ff007f"
                            : undefined,
                        textDecoration: props.changedParams?.includes(
                          "share_name"
                        )
                          ? "underline"
                          : null,
                      }}
                    >
                      <Box component="span" sx={{ fontWeight: "bold" }}>
                        Share Name:{" "}
                      </Box>{" "}
                      {props.values.share_name}
                    </Typography>
                  }
                />
              </ListItem>
            </Box>
          </Grid>
        )}
        <Grid xs={12} sm={12} md={6}>
          <Box>
            <ListItem>
              <ListItemText
                primary={
                  <Typography
                    style={{
                      color:
                        props.isEditing &&
                        props.changedParams?.includes("enable_shared_storage")
                          ? "#ff007f"
                          : undefined,
                      textDecoration: props.changedParams?.includes(
                        "enable_shared_storage"
                      )
                        ? "underline"
                        : null,
                    }}
                  >
                    <Box component="span" sx={{ fontWeight: "bold" }}>
                      Shared Storage:{" "}
                    </Box>{" "}
                    {props.values.enable_shared_storage ? "Yes" : "No"}
                  </Typography>
                }
              />
            </ListItem>
          </Box>
        </Grid>
        {props.values.share_mount_path &&
          props.values.share_mount_path.trim() !== "" && (
            <Grid xs={12} sm={12} md={6}>
              <Box>
                <ListItem>
                  <ListItemText
                    primary={
                      <Typography
                        style={{
                          color:
                            props.isEditing &&
                            props.changedParams?.includes("share_mount_path")
                              ? "#ff007f"
                              : undefined,
                          textDecoration:
                            props.isEditing &&
                            props.changedParams?.includes("share_mount_path")
                              ? "underline"
                              : null,
                        }}
                      >
                        <Box component="span" sx={{ fontWeight: "bold" }}>
                          Share Mount Path:{" "}
                        </Box>{" "}
                        {props.values.share_mount_path}
                      </Typography>
                    }
                  />
                </ListItem>
              </Box>
            </Grid>
          )}
        {props.values.share_access_key &&
          props.values.share_access_key.trim() !== " " && (
            <Grid xs={12} sm={12} md={6}>
              <Box>
                <ListItem>
                  <ListItemText
                    primary={
                      <Typography
                        style={{
                          color:
                            props.isEditing &&
                            props.changedParams?.includes("share_access_key")
                              ? "#ff007f"
                              : undefined,
                          textDecoration: props.changedParams?.includes(
                            "share_access_key"
                          )
                            ? "underline"
                            : null,
                        }}
                      >
                        <Box component="span" sx={{ fontWeight: "bold" }}>
                          Share Access Key:{" "}
                        </Box>{" "}
                        {props.values.share_access_key}
                      </Typography>
                    }
                  />
                </ListItem>
              </Box>
            </Grid>
          )}
        {props.values.share_access_to &&
          props.values.share_access_to.trim() !== " " && (
            <Grid xs={12} sm={12} md={6}>
              <Box>
                <ListItem>
                  <ListItemText
                    primary={
                      <Typography
                        style={{
                          color:
                            props.isEditing &&
                            props.changedParams?.includes("share_access_to")
                              ? "#ff007f"
                              : undefined,
                          textDecoration: props.changedParams?.includes(
                            "share_access_to"
                          )
                            ? "underline"
                            : null,
                        }}
                      >
                        <Box component="span" sx={{ fontWeight: "bold" }}>
                          Share Access To:{" "}
                        </Box>
                        {props.values.share_access_to}
                      </Typography>
                    }
                  />
                </ListItem>
              </Box>
            </Grid>
          )}
        {props.values.share_access_level &&
          props.values.share_access_level.trim() !== " " && (
            <Grid xs={12} sm={12} md={6}>
              <Box>
                <ListItem>
                  <ListItemText
                    primary={
                      <Typography
                        style={{
                          color:
                            props.isEditing &&
                            props.changedParams?.includes("share_access_level")
                              ? "#ff007f"
                              : undefined,
                          textDecoration: props.changedParams?.includes(
                            "share_access_level"
                          )
                            ? "underline"
                            : null,
                        }}
                      >
                        <Box component="span" sx={{ fontWeight: "bold" }}>
                          Share Access To:{" "}
                        </Box>{" "}
                        {props.values.share_access_level === "rw"
                          ? "Read/Write"
                          : "Read Only"}
                      </Typography>
                    }
                  />
                </ListItem>
              </Box>
            </Grid>
          )}
      </Grid>
      <BootDiskReview values={props.values} />
    </Box>
  );
};

export default VMs4WorkshopReview;
