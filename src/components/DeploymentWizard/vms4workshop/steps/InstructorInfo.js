import { useState } from "react";
import { Box, Button, Grid, Stack, TextField, Typography } from "@mui/material";
import AllowedKeys from "./AllowedKeys";
import VMAssignmentInfo from "./VMAssignmentInfo";

export default function InstuctorKeys({ values, onChange }) {
  const [keysToAddString, setKeysToAddString] = useState("");

  const addKeys = () => {
    // split keys string into array
    let keysToAdd = keysToAddString.split(",").map((element) => {
      return element.trim();
    });
    // add new keys to existing list
    onChange("instructors_ssh_keys", [
      ...values["instructors_ssh_keys"],
      ...keysToAdd,
    ]);
    // clear textfield
    setKeysToAddString("");
  };

  const removeKeys = (keysToRemove) => {
    let keys = values["instructors_ssh_keys"].filter((key) => {
      return !keysToRemove.includes(key);
    });

    onChange("instructors_ssh_keys", keys);
  };

  return (
    <Box sx={{ maxWidth: "90%", margin: "0 auto" }}>
      <Typography variant="h6" component="h2" style={{ fontSize: "1.25rem" }}>
        Instructor Info
      </Typography>

      <Box mt={2}>
        <Grid
          container
          spacing={3}
          direction="row"
          alignItems="flex-start"
          justifyContent="center"
        >
          <Grid item sm={12} md={6}>
            <Stack spacing={3}>
              <Stack spacing={2}>
                <Typography fontSize={14}>
                  First, choose a username that will be set up on all instructor
                  instances. Defaults to 'instructor'.
                </Typography>

                <TextField
                  label="Instructor Username"
                  name="instructor_username"
                  id="instructor_username"
                  value={values["instructor_username"]}
                  onChange={(e) =>
                    onChange("instructor_username", e.target.value)
                  }
                  inputProps={{ maxLength: 32 }}
                  variant="outlined"
                />
              </Stack>
              <Stack>
                <Typography fontSize={14}>
                  Next, add a public SSH key for each instructor. This will
                  allow access to all VMs.
                </Typography>
                <Box mt={2}>
                  <TextField
                    id="keysToAddString"
                    name="keysToAddString"
                    value={keysToAddString}
                    onChange={(e) => setKeysToAddString(e.target.value)}
                    multiline
                    label="Add SSH keys"
                    helperText="Separate keys with a comma."
                    variant="outlined"
                    fullWidth
                  />
                </Box>
                <Box align="right">
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => addKeys()}
                    disabled={!keysToAddString || keysToAddString.length === 0}
                  >
                    Add
                  </Button>
                </Box>
              </Stack>

              <VMAssignmentInfo></VMAssignmentInfo>
            </Stack>
          </Grid>
          <Grid item sm={12} md={6}>
            <AllowedKeys values={values} removeKeys={removeKeys} />
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}
