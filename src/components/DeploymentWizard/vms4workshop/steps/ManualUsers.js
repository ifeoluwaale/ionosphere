import { useState } from "react";
import {
  Box,
  Button,
  Divider,
  Grid,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import Papa from "papaparse";
import AllowedUsersList from "./AllowedUserList";
import VMAssignmentInfo from "./VMAssignmentInfo";

export default function ManualUsers({
  values,
  onChange,
  isEditing,
  isParamEditable,
}) {
  const [file, setFile] = useState(null);
  const [usersToAddString, setUsersToAddString] = useState("");

  // ---------------------- CSV --------------------------- //

  const removeUpload = () => {
    setFile(null);
  };

  const handleUpload = (event) => {
    const files = Array.from(event.target.files);
    const [file] = files;
    setFile(file);
    event.target.value = "";
  };

  const addUsersFromCsv = () => {
    Papa.parse(file, {
      header: true,
      complete: (results) => {
        let usersToAdd = [];
        results.data.forEach((row) => {
          if (row.username) {
            usersToAdd.push(row.username.trim());
          }
        });
        onChange("student_usernames", [
          ...values["student_usernames"],
          ...usersToAdd,
        ]);
        setFile(null);
      },
    });
  };

  // ------------------ Manual Add ----------------------- //

  const addUsers = () => {
    let usersToAdd = usersToAddString.split(",").map((element) => {
      return element.trim();
    });
    onChange("student_usernames", [
      ...values["student_usernames"],
      ...usersToAdd,
    ]);
    setUsersToAddString("");
  };

  const removeUsers = (usersToRemove) => {
    let users = values["student_usernames"].filter((user) => {
      return !usersToRemove.includes(user);
    });

    onChange("student_usernames", users);
  };

  return (
    <Box sx={{ maxWidth: "90%", margin: "0 auto" }}>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        sx={{ marginBottom: "1.5rem", gap: 0.5 }}
      >
        <Box sx={{ marginRight: "1rem" }}>
          <Typography
            variant="h6"
            component="h2"
            style={{ fontSize: "1.25rem" }}
          >
            Students
          </Typography>
        </Box>
        <Box textAlign="center">
          <VMAssignmentInfo></VMAssignmentInfo>
        </Box>
      </Box>
      <Box>
        <Grid
          container
          direction="row"
          justifyContent="flex-start"
          alignItems="flex-start"
        >
          <Grid item sm={12} md={6} pr={4}>
            <Stack spacing={3}>
              <Typography fontSize={14}>
                Add students by entering comma-separated usernames or uploading
                a CSV file. You can also skip to the next step to autogenerate
                usernames.
              </Typography>
              <Divider>
                <Typography variant={"subtitle2"}>
                  Comma-separated Usernames
                </Typography>
              </Divider>
              <Stack spacing={1}>
                <TextField
                  name="usersToAddString"
                  value={usersToAddString}
                  autoComplete="off"
                  id="outlined-helperText"
                  multiline
                  label="Usernames"
                  variant="outlined"
                  onChange={(e) => setUsersToAddString(e.target.value)}
                />
                <Box align="right">
                  <Button
                    color="primary"
                    size="small"
                    variant="contained"
                    onClick={() => addUsers()}
                    disabled={
                      !usersToAddString || usersToAddString.length === 0
                    }
                  >
                    Add
                  </Button>
                </Box>
              </Stack>

              <Divider>
                <Typography variant="subtitle2">Upload CSV File</Typography>
              </Divider>

              <Typography fontSize={14}>
                Upload a CSV file containing column 'username'.
              </Typography>

              <Stack spacing={1}>
                <TextField
                  variant="outlined"
                  disabled
                  value={file ? file.name : "No File Selected"}
                  sx={{
                    "& .MuiInputBase-input.Mui-disabled": {
                      WebkitTextFillColor: "#000000",
                    },
                  }}
                  InputProps={{
                    endAdornment: (
                      <Box>
                        {file ? (
                          <Button
                            size="small"
                            component="label"
                            onClick={removeUpload}
                          >
                            Remove
                          </Button>
                        ) : (
                          <Button
                            variant="contained"
                            size="small"
                            component="label"
                          >
                            Browse
                            <input
                              type="file"
                              hidden
                              accept=".csv"
                              multiple={false}
                              onChange={handleUpload}
                            />
                          </Button>
                        )}
                      </Box>
                    ),
                  }}
                />
                <Box align="right">
                  <Button
                    color="primary"
                    variant="contained"
                    disabled={!file}
                    onClick={addUsersFromCsv}
                    size="small"
                  >
                    Add Users from CSV
                  </Button>
                </Box>
              </Stack>
            </Stack>
          </Grid>
          <Grid item md={6} sm={12}>
            <AllowedUsersList
              student_usernames={values["student_usernames"]}
              removeUsers={removeUsers}
              isEditing={isEditing}
            ></AllowedUsersList>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}
