import { useState } from "react";

import {
  Box,
  Button,
  Checkbox,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Stack,
  Typography,
} from "@mui/material";

import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

export default function AllowedKeys({ values, removeKeys }) {
  const [checked, setChecked] = useState([]);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  return (
    <Stack>
      <Typography
        variant="body1"
        component="h3"
        style={{ marginBottom: ".5rem" }}
      >
        Added Keys
      </Typography>
      <Paper>
        <Box
          style={{
            height: "10rem",
            overflowY: "scroll",
            overflowX: "hidden",
          }}
        >
          <List dense sx={{ width: "100%", backgroundColor: "ffff" }}>
            {values["instructors_ssh_keys"].map((value, index) => (
              <ListItem key={index} onClick={handleToggle(value)}>
                <ListItemIcon>
                  <Checkbox
                    edge="start"
                    tabIndex={-1}
                    disableRipple
                    size="small"
                    checked={checked.indexOf(value) !== -1}
                  />
                </ListItemIcon>
                <ListItemText
                  primary={
                    value.length >= 50
                      ? value.substring(0, 24) +
                        "..." +
                        value.substring(value.length - 18)
                      : value
                  }
                />
              </ListItem>
            ))}
          </List>
        </Box>
      </Paper>
      <Box mt={2}>
        <Button
          color="inherit"
          onClick={() => {
            removeKeys(checked);
            setChecked([]);
          }}
          sx={{ color: "#ff007f" }}
          startIcon={<DeleteForeverIcon />}
        >
          Delete
        </Button>
      </Box>
    </Stack>
  );
}
