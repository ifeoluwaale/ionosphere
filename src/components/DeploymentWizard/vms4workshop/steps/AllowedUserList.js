import { useState } from "react";
import {
  Box,
  Button,
  Checkbox,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Stack,
  Typography,
} from "@mui/material";

import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

export default function AllowedUsersList(props) {
  const [checked, setChecked] = useState([]);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const removeUsers = () => {
    props.removeUsers(checked);
    setChecked([]);
  };

  return (
    <Stack>
      <Typography
        variant="body1"
        component="h3"
        style={{ marginBottom: ".5rem" }}
      >
        Added Usernames
      </Typography>
      <Paper>
        <Box
          style={{
            height: "15rem",
            overflowY: "scroll",
            overflowX: "hidden",
          }}
        >
          <List sx={{ width: "100%" }}>
            {props.student_usernames.map((value, index) => {
              const labelId = `checkbox-list-label-${value}`;

              return (
                <ListItem
                  key={index}
                  role={undefined}
                  dense
                  onClick={handleToggle(value)}
                >
                  <ListItemIcon>
                    <Checkbox
                      edge="start"
                      checked={checked.indexOf(value) !== -1}
                      tabIndex={-1}
                      disableRipple
                      inputProps={{
                        "aria-labelledby": labelId,
                      }}
                    />
                  </ListItemIcon>
                  <ListItemText id={labelId} primary={`${value}`} />
                </ListItem>
              );
            })}
          </List>
        </Box>
      </Paper>
      <Box mt={2}>
        <Button
          color="inherit"
          onClick={removeUsers}
          sx={{ color: "#ff007f" }}
          startIcon={<DeleteForeverIcon />}
          disabled={props.isEditing}
        >
          Delete
        </Button>
      </Box>
    </Stack>
  );
}
