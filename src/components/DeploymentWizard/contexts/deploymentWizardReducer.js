/**
 * Reducer File
 * ------------
 * This file contains the reducer for the Deployment Wizard. A reducer is a function that takes the current state and an action, and returns a new state based on the action type.
 *
 * Reducers specify how the application's state changes in response to actions received.
 * Actions only describe what happened, but don't describe how the application's state changes.
 *
 * Reducers should should not modify the input state. Instead, they should return a new state
 * object if changes are necessary.
 */

import {
  SET_CLOUD_ID,
  SET_PROJECT_ID,
  SET_REGION_ID,
  SET_IMAGES,
  SET_FLAVORS,
  SET_WORKSPACE_ID,
  SET_TEMPLATE,
  SET_SUBMITTING_DEPLOYMENT,
} from "./actions/actionTypes";

function deploymentReducer(state, action) {
  switch (action.type) {
    case SET_CLOUD_ID:
      return {
        ...state,
        cloudId: action.payload.id,
        cloudName: action.payload.name,
      };
    case SET_PROJECT_ID:
      return { ...state, projectId: action.payload };
    case SET_REGION_ID:
      return { ...state, regionId: action.payload };
    case SET_IMAGES:
      return { ...state, images: action.payload };
    case SET_FLAVORS:
      return { ...state, flavors: action.payload };
    case SET_WORKSPACE_ID:
      return { ...state, workspaceId: action.payload };
    case SET_TEMPLATE:
      return { ...state, template: action.payload };
    case SET_SUBMITTING_DEPLOYMENT:
      return { ...state, isSubmitting: action.payload };
    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
}

export default deploymentReducer;
