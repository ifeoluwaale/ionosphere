import { createContext, useContext, useEffect, useReducer } from "react";

import { useCredential, useRegions } from "@/hooks";
import deploymentWizardReducer from "./deploymentWizardReducer";
import { useClouds, useWorkspaces } from "@/contexts";

import { setWorkspaceId } from "./actions/actions";

export const DeploymentWizardContext = createContext(null);
export const DeploymentWizardDispatchContext = createContext(null);

/**
 * DeploymentWizardContext is a React Context that provides the current
 * state for the deployment wizard.
 *
 * Deployment wizard state is managed with a React reducer.
 *
 * @param {Object} value (see below for params)
 * @param {JSX.Element} children
 * @returns Deployment wizard state
 */

export function DeploymentWizardProvider({ children, initialValues }) {
  // initialize state from initialValues prop, or use defaults
  const {
    cloudId = "",
    cloudName = "",
    projectId = "",
    template = null,
    regionId = "",
    workspaceId = null,
  } = initialValues;

  const initialState = {
    cloudId,
    cloudName,
    template,
    cloudId,
    projectId,
    regionId,
    workspaceId,
  };

  const [workspaces] = useWorkspaces();

  // initialize deployment wizard reducer
  const [state, dispatch] = useReducer(deploymentWizardReducer, initialState);

  // get credential ID for selected cloud & project
  const credentialId = useCredential(state.cloudId, state.projectId);
  const [regions, loadingRegions] = useRegions(state.cloudId, credentialId);

  useEffect(() => {
    const workspaceId = workspaces.find(
      (w) =>
        w.default_provider_id === state.cloudId &&
        w.name === "Default Workspace"
    )?.id;

    dispatch(setWorkspaceId(workspaceId));
  }, [state.cloudId, state.projectId]);

  return (
    <DeploymentWizardContext.Provider
      value={{ ...state, credentialId, regions, loadingRegions }}
    >
      <DeploymentWizardDispatchContext.Provider value={dispatch}>
        {children}
      </DeploymentWizardDispatchContext.Provider>
    </DeploymentWizardContext.Provider>
  );
}

/**
 * React hook to access Quick Deploy wizard state.
 */
export function useDeploymentWizard() {
  return useContext(DeploymentWizardContext);
}

/**
 * React hook to access dispatch function for updating
 * Quick Deploy wizard state.
 */
export function useDeploymentWizardDispatch() {
  return useContext(DeploymentWizardDispatchContext);
}
