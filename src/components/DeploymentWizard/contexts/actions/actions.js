/**
 * Actions File
 * ------------
 * This file contains action creators for the deployment wizard. Action creators are functions
 * that return action objects, which can be dispatched to a reducer to update state.
 *
 * Each action creator should be exported and can then be used wherever actions need to
 * be dispatched from.
 */

import {
  SET_CLOUD_ID,
  SET_PROJECT_ID,
  SET_REGION_ID,
  SET_IMAGES,
  SET_FLAVORS,
  SET_WORKSPACE_ID,
  SET_TEMPLATE,
  SET_SUBMITTING_DEPLOYMENT,
} from "./actionTypes";

export const setCloudId = (payload) => ({
  type: SET_CLOUD_ID,
  payload,
});

export const setProjectId = (payload) => ({
  type: SET_PROJECT_ID,
  payload,
});

export const setRegionId = (payload) => ({
  type: SET_REGION_ID,
  payload,
});

export const setImages = (payload) => ({
  type: SET_IMAGES,
  payload,
});

export const setFlavors = (payload) => ({
  type: SET_FLAVORS,
  payload,
});

export const setWorkspaceId = (payload) => ({
  type: SET_WORKSPACE_ID,
  payload,
});

export const setTemplate = (payload) => ({
  type: SET_TEMPLATE,
  payload,
});

export const setSubmittingDeployment = (payload) => ({
  type: SET_SUBMITTING_DEPLOYMENT,
  payload,
});
