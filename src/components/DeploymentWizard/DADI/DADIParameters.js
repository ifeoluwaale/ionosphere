import { Box, Grid } from "@mui/material";
import Checkbox from "../Fields/Checkbox";
import InstanceName from "../Fields/InstanceName";
import PowerState from "../Fields/PowerState";
import Text from "../Fields/Text";

const DADIParameters = ({
  wizard,
  // flavors,
  // loadingImages,
}) => {
  return (
    <>
      {
        <Box ml={1} mr={1}>
          <Grid
            container
            justifyContent="center"
            alignItems="flex-start"
            spacing={2}
            mb={2}
          >
            <Grid
              item
              xs={12}
              sm={wizard.isEditing ? 4 : 10}
              md={wizard.isEditing ? 3 : 6}
            >
              <InstanceName wizard={wizard} />
            </Grid>
            {wizard.isEditing && (
              <Grid item xs={12} sm={4} md={3}>
                <PowerState wizard={wizard} />
              </Grid>
            )}
          </Grid>

          <Checkbox
            half={true}
            label="Run dadi-cli?"
            name="run_dadi_cli"
            wizard={wizard}
          />

          {wizard.values["run_dadi_cli"] && (
            <>
              <Grid
                container
                justifyContent="center"
                alignItems="flex-start"
                spacing={1}
              >
                <Grid
                  item
                  xs={12}
                  sm={wizard.isEditing ? 6 : 10}
                  md={wizard.isEditing ? 4 : 6}
                >
                  <Text
                    name="dadi_cli_parameters"
                    label="Parameters"
                    helperText={
                      <>
                        Fill in the dadi-cli subcommand and arguments/flags you
                        want to be used.
                        <br />
                        e.g. InferDM --fs https://tinyurl.com/u38zv4kw --model
                        two_epoch --lbounds 1e-3 1e-3 --ubounds 10 1
                        --output-prefix two_epoch_demo --nomisid
                      </>
                    }
                    required
                    wizard={wizard}
                  />
                </Grid>
              </Grid>
              <Checkbox
                half={true}
                label="Use workqueue with dadi-cli?"
                name="dadi_cli_use_workqueue"
                wizard={wizard}
              />
            </>
          )}

          <Checkbox
            half={true}
            label="Run workqueue_factory?"
            name="run_workqueue_factory"
            wizard={wizard}
          />

          {((wizard.values["run_dadi_cli"] &&
            wizard.values["dadi_cli_use_workqueue"]) ||
            wizard.values["run_workqueue_factory"]) && (
            <Grid
              container
              justifyContent="center"
              alignItems="flex-start"
              spacing={1}
            >
              <Grid
                item
                xs={12}
                sm={wizard.isEditing ? 6 : 10}
                md={wizard.isEditing ? 4 : 6}
              >
                <div style={{ marginBottom: "1rem" }}>
                  <Text
                    name="workqueue_project_name"
                    label="Project Name"
                    required
                    wizard={wizard}
                  />
                </div>
              </Grid>
            </Grid>
          )}

          {((wizard.values["run_dadi_cli"] &&
            wizard.values["dadi_cli_use_workqueue"]) ||
            wizard.values["run_workqueue_factory"]) && (
            <Grid
              container
              justifyContent="center"
              alignItems="flex-start"
              spacing={1}
            >
              <Grid
                item
                xs={12}
                sm={wizard.isEditing ? 6 : 10}
                md={wizard.isEditing ? 4 : 6}
              >
                <Text
                  name="workqueue_password"
                  label="Workqueue Password"
                  required
                  wizard={wizard}
                />
              </Grid>
            </Grid>
          )}
        </Box>
      }
    </>
  );
};

export default DADIParameters;
