import React from "react";
import { useState, useEffect } from "react";
import { Box, DialogContent } from "@mui/material";

import { FormStepper, FormControls } from "../../forms/FormStepper";

import SelectRegion from "../Steps/SelectRegion";
import DADIParameters from "./DADIParameters";
import DADIReview from "./DADIReview";

import { useAPI, useUser } from "../../../contexts";
import { Alert } from "@mui/material";

import { ValidateDeploymentName } from "../../../utils";
import { useDeploymentWizard } from "../contexts/DeploymentWizardContext";
import TemplateStepControls from "../Steps/TemplateStepControls";

/**
 * The CreateDADIWizard component controls the functionality unique to the
 * DADI template, while logic shared across all wizards is located in
 * CreateWizardController or CreateDeploymentDialog.
 */
const CreateDADIWizard = (props) => {
  const api = useAPI();
  const [user] = useUser();
  const { isSubmitting } = useDeploymentWizard();

  const initialValues = {
    flavor: "m3.medium",
    instance_name: "",
    instance_count: 1,
    image_name: "Featured-Ubuntu22",
    run_dadi_cli: false,
    dadi_cli_use_workqueue: false,
    run_workqueue_factory: false,
  };

  // obj for storing run parameter values
  const [values, setValues] = useState(initialValues);

  // whether user input is valid for current step
  const [isValid, setValid] = useState(false);

  // if invalid deployment name input, the error message (null if name is valid)
  const [nameError, setNameError] = useState(null);

  const [flavors, setFlavors] = useState();
  // const [loadingImages, setLoadingImages] = useState(false);

  let activeStep = props.activeStep;

  /**
   * Update values object upon user input change.
   * @param {string} id: name of parameter to update
   * @param {*} value: new value for parameter
   */
  const handleChange = (id, value) => {
    setValues({ ...values, [id]: value });
  };

  /**
   * Fetches flavors, and presets param values to first results.
   */
  // const fetchFlavors = async () => {
  //   setLoadingImages(true);
  //   try {
  //     let flavors = await api.providerFlavors(props.cloudId, {
  //       credential: props.deploymentValues.credentialId,
  //       region: props.deploymentValues.regionId,
  //     });
  //     flavors = flavors.sort(sortBy("ram"));
  //     setFlavors(flavors);

  //     setValues({
  //       ...values,
  //       flavor: flavors[0].name,
  //     });

  //     setLoadingImages(false);
  //   } catch (e) {
  //     console.log(e); //TODO
  //     setLoadingImages(false);
  //   }
  // };

  /**
   * Prepare deployment params to be submitted to createDeployment API endpoint &
   * prepare run params to be submitted to run createRun API endpoint.
   * @param {obj} deploymentValues
   * @param {obj} values
   * @returns list of {key,value} parameters
   */
  const setupParams = (deploymentValues, values) => {
    console.log("Deployment Values:", deploymentValues);
    console.log("Run Values:", values);

    // setup deployment params
    let deploymentParams = {
      owner: user.username,
      name: values["instance_name"],
      workspace_id: deploymentValues["workspaceId"],
      template_id: deploymentValues["templateId"],
      primary_provider_id: deploymentValues["cloudId"], //provider.id,
      cloud_credentials: [deploymentValues["credentialId"]], //[ credential.id ],
    };

    // setup run params
    let runParams = [
      { key: "flavor", value: values["flavor"] },
      { key: "image_name", value: values["image_name"] },
      { key: "instance_count", value: values["instance_count"].toString() },
      { key: "instance_name", value: values["instance_name"] },
      { key: "username", value: user.username },
      { key: "project", value: deploymentValues["projectId"] },
      { key: "run_dadi_cli", value: values["run_dadi_cli"] ? "true" : "false" },
      {
        key: "dadi_cli_parameters",
        value: values["dadi_cli_parameters"],
      },
      // {
      //   key: "dadi_cli_email",
      //   value: values["dadi_cli_email"],
      // },
      {
        key: "run_workqueue_factory",
        value: values["run_workqueue_factory"] ? "true" : "false",
      },
      {
        key: "workqueue_project_name",
        value: values["workqueue_project_name"],
      },
      {
        key: "workqueue_password",
        value: values["workqueue_password"],
      },
    ];

    return [deploymentParams, runParams];
  };

  // when region is updated, prefetch the images/flavors for associated region
  // useEffect(() => {
  //   if (props.deploymentValues["regionId"]) {
  //     setTimeout(fetchFlavors, 0);
  //   }
  // }, [props.deploymentValues["regionId"]]);

  // validate input for current step to disable/enable next button
  useEffect(() => {
    if (activeStep > -1) {
      const validator = steps[activeStep].validator;
      setValid((!validator || validator(values)) && !props.error);
    }
  }, [props.activeStep, values]);

  const wizard = {
    isEditing: props.isEditing,
    isParamEditable: props.isParamEditable,
    nameError: nameError,
    onChange: handleChange,
    setNameError: setNameError,
    template: props.template,
    values: values,
  };

  // wizard steps
  const steps = [
    {
      title: "Region",
      render: () => (
        <SelectRegion
          loadingRegions={props.loadingRegions}
          regions={props.regions}
          selectedId={props.deploymentValues["regionId"]}
          setRegionId={props.setRegionId}
        />
      ),
    },
    {
      title: "Parameters",
      validator: (values) => {
        return (
          !!values["instance_name"] &&
          !!values["image_name"] &&
          !!values["flavor"] &&
          !ValidateDeploymentName(values["instance_name"]) &&
          (!values["run_dadi_cli"] || !!values["dadi_cli_parameters"]) &&
          ((!values["run_dadi_cli"] && !values["run_workqueue_factory"]) ||
            (!!values["run_dadi_cli"] &&
              !values["dadi_cli_use_workqueue"] &&
              !values["run_workqueue_factory"]) ||
            (!!values["workqueue_project_name"] &&
              !!values["workqueue_password"]))
        );
      },
      render: (values) => (
        <DADIParameters
          // flavors={flavors}
          // loadingImages={loadingImages}
          wizard={wizard}
        />
      ),
    },
    {
      title: "Review & Deploy",
      render: (values) => (
        <DADIReview
          deploymentValues={props.deploymentValues}
          flavors={flavors}
          values={values}
          wizard={wizard}
        />
      ),
    },
  ].filter((step) => step);

  return (
    <>
      <DialogContent style={{ minHeight: "40vh" }}>
        <FormStepper
          activeStep={activeStep}
          steps={steps.map((s) => s.title)}
        />
        <Box mt={6} mb={2}>
          {steps[activeStep].render(values)}
          {props.error && (
            <Box mt={2} display="flex" justifyContent="center">
              <Alert severity="error">{props.error}</Alert>
            </Box>
          )}
        </Box>
      </DialogContent>

      <TemplateStepControls
        handlePrev={props.handlePrev}
        isValid={isValid}
        steps={steps}
        activeStep={activeStep}
        nextHandler={props.handleNext.bind(null, values)}
        backHandler={props.handleBack.bind(null, values)}
        submitHandler={() => {
          const [deploymentParams, runParams] = setupParams(
            props.deploymentValues,
            values
          );
          props.submitHandler(deploymentParams, runParams);
        }}
        isSubmitting={isSubmitting}
      />
    </>
  );
};

export default CreateDADIWizard;
