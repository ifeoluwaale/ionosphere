import { decodeBase64ArrayString } from "../../utils";

import EditDADIWizard from "./DADI/EditDADIWizard";
import EditMetadataWizard from "./MetadataWizard/EditMetadataWizard";
import EditJupyterhubWizard from "./jupyterhub/EditJupyterhubWizard";
import EditK3sWizard from "./k3s/EditK3sWizard";
import EditVMs4WorkshopWizard from "./vms4workshop/EditVMs4WorkshopWizard";

/**
 * The EditWizardController contains functions common to all deployment wizards
 * and displays the correct wizard depending on which template was selected
 * by the user.
 */
const EditWizardController = (props) => {
  let template = props.templates.find(
    (t) => t.id === props.deploymentValues.templateId
  );

  // -------------- begin functions used across all edit wizards --------------- //

  const getInitialValues = (paramList) => {
    let initialValues = {};
    paramList.forEach((param) => {
      let paramValue = props.getDeploymentParam(param.name);
      if (param.type === "string") {
        initialValues[param.name] = paramValue;
      } else if (param.type === "bool") {
        initialValues[param.name] = paramValue === "true";
      } else if (param.type === "integer") {
        initialValues[param.name] = parseInt(paramValue);
      } else if (param.type === "array_string") {
        initialValues[param.name] = paramValue.split(",");
      } else if (param.type === "array_string_base64") {
        let prefix = param.name.split("_base64")[0];
        initialValues[prefix] = paramValue
          ? decodeBase64ArrayString(paramValue)
          : [];
      }
    });
    return initialValues;
  };

  const checkParamChanges = (newParams) => {
    let changedParams = [];

    // check if newParams have changed
    newParams.forEach((newParam) => {
      let oldValue = props.deployment.parameters.find(
        (oldParam) => oldParam.key === newParam.key
      )?.value;

      if (oldValue !== newParam.value) {
        changedParams.push(newParam.key);
      }
    });

    return changedParams;
  };

  const handleNext = async () => {
    props.setActiveStep((prevStep) => prevStep + 1);
  };

  const handleBack = () => {
    props.setActiveStep((prevStep) => prevStep - 1);
  };

  // -------------- end functions used across all edit wizards --------------- /

  const components = {
    DADI: EditDADIWizard,
    jupyterhub: EditJupyterhubWizard,
    "single-image-k3s": EditK3sWizard,
    vms4workshop: EditVMs4WorkshopWizard,
  };

  // render correct wizard based on selected template
  const EditWizard = components[template.name] || EditMetadataWizard;

  return (
    <EditWizard
      activeStep={props.activeStep}
      setActiveStep={props.setActiveStep}
      handleNext={handleNext}
      handleBack={handleBack}
      submitHandler={props.submitHandler}
      template={template}
      cloudId={props.cloudId}
      deploymentValues={props.deploymentValues}
      regions={props.regions}
      loadingRegions={props.loadingRegions}
      setRegionId={props.setRegionId}
      isEditing={true}
      deployment={props.deployment}
      handleClose={props.handleClose}
      classes={props.classes}
      getDeploymentParam={props.getDeploymentParam}
      getInitialValues={getInitialValues}
      checkParamChanges={checkParamChanges}
    />
  );
};

export default EditWizardController;
