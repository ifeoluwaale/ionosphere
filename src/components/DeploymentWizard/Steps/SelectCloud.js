import { useEffect } from "react";
import { Box, DialogContent, Grid, Stack, Typography } from "@mui/material";
import { CloudQueue as CloudOutlineIcon } from "@mui/icons-material";

import { useClouds, useProjects } from "@/contexts";
import LabeledDropdownMenu from "@/components/Common/Form/LabeledDropdownMenu";
import {
  useDeploymentWizard,
  useDeploymentWizardDispatch,
} from "../contexts/DeploymentWizardContext";
import { setCloudId, setProjectId } from "../contexts/actions/actions";

/**
 * Prerequisite step in deployment wizard to select a cloud and project for the deployment.
 */
export default function SelectCloud() {
  // Get clouds and projects from global contexts
  const [clouds] = useClouds();
  const [projects] = useProjects();

  // Get selected cloud and porject from deployment wizard context
  const { cloudId, projectId } = useDeploymentWizard();

  // Update function for deployment wizard context
  const dispatch = useDeploymentWizardDispatch();

  // Set default cloud and project if not already set
  useEffect(() => {
    if (!cloudId) {
      dispatch(setCloudId(clouds[0]));
    }
    if (!projectId) {
      dispatch(setProjectId(projects[0]?.title));
    }
  });

  return (
    <DialogContent style={{ minHeight: "40vh" }}>
      <Box
        mt={2}
        mb={2}
        sx={{
          height: "95%",
          overflowX: "hidden",
          overflowY: "hidden",
        }}
      >
        <Box
          mt={4}
          display={"flex"}
          flexDirection={"column"}
          justifyContent={"center"}
          sx={{ width: "100%" }}
        >
          <Grid container spacing={4} paddingX={16}>
            <Grid item md={12}>
              <Typography style={{ fontSize: "1.25rem" }}>
                Select the cloud you want to deploy to:
              </Typography>
            </Grid>

            <Grid item md={6}>
              <LabeledDropdownMenu
                label={
                  <Stack direction={"row"} alignItems={"flex-end"} spacing={1}>
                    <CloudOutlineIcon />
                    <Typography>Cloud</Typography>
                  </Stack>
                }
                icon={<CloudOutlineIcon />}
                selectedId={cloudId}
                options={clouds}
                handleSelect={(selected) => dispatch(setCloudId(selected))}
              />
            </Grid>
            <Grid item md={6}>
              <LabeledDropdownMenu
                label={"Project"}
                selectedId={projectId}
                options={projects.map((p) => {
                  return { id: p.title, name: p.title };
                })}
                handleSelect={(selected) => dispatch(setProjectId(selected.id))}
              />
            </Grid>
          </Grid>
        </Box>
      </Box>
    </DialogContent>
  );
}
