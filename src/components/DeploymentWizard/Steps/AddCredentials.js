import { Box, DialogContent } from "@mui/material";
import DeployPrerequisiteForm from "@/components/forms/DeployPrerequisiteForm";

/**
 * Prerequisite step in deployment wizard to add a cloud credential and ssh key.
 */
export default function AddCredentials() {
  return (
    <DialogContent>
      <Box m={4}>
        <DeployPrerequisiteForm />
      </Box>
    </DialogContent>
  );
}
