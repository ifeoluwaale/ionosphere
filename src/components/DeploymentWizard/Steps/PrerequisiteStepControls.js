import React from "react";
import { Button, Stack } from "@mui/material";

/**
 * Step controls for the prerequisite steps in the deployment wizard.
 */
const PrerequisiteStepControls = ({
  handleNext,
  handlePrev,
  disableNext,
  disablePrev,
}) => {
  return (
    <Stack
      direction="row"
      spacing={1}
      display="flex"
      justifyContent="center"
      mt={2}
      mb={3}
    >
      {!disablePrev && (
        <Button variant="outlined" onClick={handlePrev} sx={{ width: "8rem" }}>
          Previous
        </Button>
      )}

      <Button
        variant="contained"
        disabled={disableNext ? disableNext : false}
        onClick={handleNext}
        sx={{ width: "8rem" }}
      >
        Next
      </Button>
    </Stack>
  );
};

export default PrerequisiteStepControls;
