import React from "react";
import { Button, Stack } from "@mui/material";

import { FormControls } from "@/components/forms/FormStepper";

const TemplateStepControls = (props) => {
  const {
    activeStep,
    handlePrev,
    isValid,
    steps,
    nextHandler,
    backHandler,
    submitHandler,
    isSubmitting,
  } = props;

  return (
    <Stack
      direction="row"
      spacing={1}
      display="flex"
      justifyContent="center"
      mt={2}
      mb={3}
    >
      {activeStep === 0 && (
        <Button onClick={handlePrev} variant="outlined" sx={{ width: "8rem" }}>
          Previous
        </Button>
      )}
      <FormControls
        disabled={!isValid}
        step={steps[activeStep]}
        activeStep={activeStep}
        numSteps={steps.length}
        nextHandler={nextHandler}
        backHandler={backHandler}
        submitHandler={submitHandler}
        isSubmitting={isSubmitting}
      />
    </Stack>
  );
};

export default TemplateStepControls;
