import { Box, DialogContent } from "@mui/material";
import TemplateList from "./TemplateList";

/**
 * Prerequisite step in the deployment wizard that allows user to select a template.
 */
const SelectTemplate = function SelectTemplate({ templates }) {
  return (
    <DialogContent style={{ minHeight: "40vh" }}>
      <Box
        mt={2}
        mb={2}
        sx={{
          height: "95%",
          overflowX: "hidden",
          overflowY: "hidden",
        }}
      >
        <TemplateList templates={templates} />
      </Box>
    </DialogContent>
  );
};
export default SelectTemplate;
