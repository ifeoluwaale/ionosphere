import { Box, Typography, List, ListItemButton } from "@mui/material";
import {
  useDeploymentWizard,
  useDeploymentWizardDispatch,
} from "../contexts/DeploymentWizardContext";

import { setTemplate } from "../contexts/actions/actions";
/**
 * List of templates to select from.
 */
const TemplateList = function TemplateList({ templates }) {
  const deploymentWizard = useDeploymentWizard();
  const dispatch = useDeploymentWizardDispatch();
  return (
    <Box display="flex" justifyContent="center">
      <List
        style={{ minWidth: "35em" }}
        subheader={
          <Typography
            style={{
              fontSize: "1.25rem",
              marginBottom: "1em",
              marginTop: "2em",
            }}
          >
            Select a template that best describes what you want to do:
          </Typography>
        }
      >
        {templates?.map((template, index) => (
          <div key={index}>
            <ListItemButton
              selected={template.id === deploymentWizard.template?.id}
              onClick={() => dispatch(setTemplate(template))}
            >
              <Box p={1} ml={1} sx={{ width: "100%" }}>
                <Typography>
                  {template.description ? template.description : template.name}
                </Typography>
                <Typography variant="caption">
                  {template.description ? template.name : ""}
                </Typography>
              </Box>
            </ListItemButton>
          </div>
        ))}
      </List>
    </Box>
  );
};
export default TemplateList;
