/* eslint-disable no-unused-vars */
import { useState, useEffect } from "react";
import {
  Box,
  Typography,
  IconButton,
  Dialog,
  Toolbar,
  AppBar,
} from "@mui/material";
import {
  Close as CloseIcon,
  CloudQueue as CloudOutlineIcon,
} from "@mui/icons-material";

import EditWizardController from "./EditWizardController";
import { useClouds, useAPI } from "../../contexts";

export default function EditDeploymentDialog(props) {
  const getDeploymentParam = (paramName) => {
    return props.deployment.parameters.find((p) => p.key === paramName)?.value;
  };

  const [clouds] = useClouds();
  const api = useAPI();

  const [activeStep, setActiveStep] = useState(0);

  const [regions, setRegions] = useState(null);
  const [loadingRegions, setLoadingRegions] = useState(false);

  const [templateId, setTemplateId] = useState(props.deployment.template_id);
  const [regionId, setRegionId] = useState(getDeploymentParam("region"));

  // get cloud name
  const cloud = clouds.find((c) => c.id === props.cloudId);

  // Find existing password/application credential for cloud
  const credentials = props.credentials?.filter((c) => c.type === "openstack");

  let defaultCredentialId = null;
  if (props.projectId) {
    const credsForCloud = credentials.filter((c) =>
      c.tags.includes(props.cloudId)
    );
    defaultCredentialId = credsForCloud.find(
      (c) =>
        c.id.includes(props.projectId) ||
        c.name.includes(props.projectId) ||
        c.tags.includes(props.projectId)
    )?.id;
  }

  // const fetchRegions = async () => {
  //   setLoadingRegions(true);
  //   let regions;
  //   try {
  //     regions = await api.providerRegions(props.cloudId, {
  //       credential: defaultCredentialId,
  //     });
  //     setRegions(regions);
  //     setLoadingRegions(false);
  //   } catch (e) {
  //     console.log(e);
  //     setLoadingRegions(false);
  //   }
  // };

  // we do not need to load regions because region is currently immutable
  // useEffect(() => {
  //   // Prefetch the regions for associated provider
  //   setTimeout(fetchRegions, 0)
  // }, [])

  return (
    <Dialog
      open={props.open}
      onClose={(_, reason) =>
        reason !== "backdropClick" && props.handleClose && props.handleClose()
      }
      maxWidth="md"
      fullWidth
    >
      <AppBar sx={{ position: "relative" }}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={props.handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h6" sx={{ marginLeft: "16px", flex: 1 }}>
            Edit Deployment: {props.deployment.name}
          </Typography>
          <Box display="flex">
            <CloudOutlineIcon />
            <Typography style={{ marginLeft: "0.5em" }}>
              {cloud.name.toUpperCase()} / {props.projectId}
            </Typography>
          </Box>
        </Toolbar>
      </AppBar>

      {activeStep >= 0 && (
        <EditWizardController
          cloudId={props.cloudId}
          activeStep={activeStep}
          setActiveStep={setActiveStep}
          submitHandler={props.handleSubmit}
          templates={props.templates}
          deploymentValues={{
            workspaceId: props.deployment.workspace_id,
            templateId: templateId,
            credentialId: props.deployment.cloud_credentials[0],
            cloudId: props.deployment.primary_provider_id,
            projectId: getDeploymentParam("project"),
            regionId: regionId,
          }}
          deployment={props.deployment}
          setRegionId={setRegionId}
          regions={regions}
          loadingRegions={loadingRegions}
          handleClose={props.handleClose}
          getDeploymentParam={getDeploymentParam}
        ></EditWizardController>
      )}
    </Dialog>
  );
}
