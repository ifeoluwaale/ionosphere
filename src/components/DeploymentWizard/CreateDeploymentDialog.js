import React, { useMemo } from "react";
import getConfig from "next/config";
import { useRouter } from "next/router";
import styled from "@emotion/styled";

import {
  AppBar,
  Avatar,
  Box,
  Dialog,
  IconButton,
  Toolbar,
  Typography,
} from "@mui/material";

import {
  Close as CloseIcon,
  CloudQueue as CloudOutlineIcon,
  RocketLaunch,
} from "@mui/icons-material";

import CreateWizardController from "./CreateWizardController";
import {
  AddCredentials,
  SelectCloud,
  SelectTemplate,
  PrerequisiteStepControls,
  DeploymentSuccess,
} from "./Steps";
import { useAPI, useCredentials, useError } from "@/contexts";
import {
  useDeploymentWizard,
  useDeploymentWizardDispatch,
} from "./contexts/DeploymentWizardContext";

import { setSubmittingDeployment } from "./contexts/actions/actions";

const GradientAvatar = styled(Avatar)({
  // blue gradient background
  background: "linear-gradient(45deg, #26AEFF, #5222FF)",
  // white text
  color: "#ffffff",
});

// Constants for steps
const STEPS = {
  CREDENTIAL: "credentialStep",
  CLOUD: "cloudStep",
  TEMPLATE: "templateStep",
  SUCCESS: "successStep",
};

/**
 * Builds the list of prerequisite steps.
 * @param {Array} credentials list of credentials
 * @param {Object} deploymentWizard deployment wizard context
 * @returns {Array} - List of steps
 */
const buildPrerequisiteSteps = (credentials, deploymentWizard) => {
  const { projectId, cloudId, template } = deploymentWizard;
  let steps = [];

  const addStepIf = (condition, step) => {
    if (condition) steps.push(step);
  };

  addStepIf(
    !credentials.find((c) => c.type === "ssh") ||
      !credentials.find((c) => c.type === "openstack"),
    STEPS.CREDENTIAL
  );
  addStepIf(!projectId || !cloudId, STEPS.CLOUD);
  addStepIf(!template, STEPS.TEMPLATE);

  // After all prerequisite steps, proceed to template steps (starts at 0)
  steps.push(0);

  return steps;
};

export default function CreateDeploymentDialog({
  handleClose,
  handlePostCreate,
  open,
  templates,
}) {
  // Get info from deployment wizard context
  const deploymentWizard = useDeploymentWizard();
  const { cloudId, cloudName, projectId, template, regions } = deploymentWizard;
  const dispatch = useDeploymentWizardDispatch();
  const config = getConfig().publicRuntimeConfig;

  // Get credentials from context
  const [credentials] = useCredentials();
  const [_, setError] = useError();

  const router = useRouter();
  const api = useAPI();

  /**
   * Build prerequisite steps. These include:
   * 1. Add Credentials - if user is missing cloud or ssh credentials
   * 2. Select Workspace - if cloud/project have not been provided
   * 3. Select Template - if template has not been provided
   **/
  const prereqSteps = React.useRef(
    buildPrerequisiteSteps(credentials, deploymentWizard)
  );

  // Start at first prerequisite step
  const [activeStep, setActiveStep] = React.useState(prereqSteps.current[0]);

  // TODO: Move regionId to deploymentWizard context
  const [regionId, setRegionId] = React.useState(null);

  const hasCloudCredential = useMemo(() => {
    return credentials.find((c) => c.type === "openstack") !== undefined;
  }, [credentials]);

  // Handle navigation for prerequisite steps
  const handleNext = () => {
    const steps = prereqSteps.current;
    const currentIndex = steps.indexOf(activeStep);
    if (currentIndex < steps.length - 1) {
      setActiveStep(steps[currentIndex + 1]);
    }
  };
  const handlePrev = () => {
    const steps = prereqSteps.current;
    const currentIndex = steps.indexOf(activeStep);
    if (currentIndex > 0) {
      setActiveStep(steps[currentIndex - 1]);
    }
  };

  // Once regions are loaded, set the default region to IU
  // TODO: Move to deploymentWizard context
  React.useEffect(() => {
    if (regions.length) {
      const defaultRegion = regions.find((r) => r.id === "IU") || regions[0];
      setRegionId(defaultRegion.id);
    }
  }, [regions]);

  /**
   * Calls API to creates a deployment and run using the provided params.
   *
   * Upon successful creation, performs post-create tasks.
   * @param {*} deploymentParams
   * @param {*} runParams
   */
  const handleCreateDeployment = async (deploymentParams, runParams) => {
    // display loading indicator
    dispatch(setSubmittingDeployment(true));

    console.log(deploymentParams);
    console.log(runParams);

    /**
     * If simulation mode is off, create deployment.
     *
     * Simulation mode can be used by setting env var SIMULATE_DEPLOYMENT_SUBMIT=true.
     * It is helpful during development so deployments are not actually created.
     */
    if (!(config.SIMULATE_DEPLOYMENT_SUBMIT === "true")) {
      try {
        // create deployment
        const res = await api.createDeployment(deploymentParams);

        // create run
        await api.createDeploymentRun(res.tid, {
          deployment_id: res.tid,
          parameters: runParams,
        });

        // if not on deployments page, show success confimation and option to redirect to deployments page
        if (router.pathname !== "/deployments") {
          setActiveStep(STEPS.SUCCESS);
        } else {
          handlePostCreate();
        }
      } catch (error) {
        // display error & remove loading indicator
        setError(api.errorMessage(error));
        dispatch(setSubmittingDeployment(false));
      }
    } else {
      if (router.pathname !== "/deployments") {
        setActiveStep(STEPS.SUCCESS);
      } else {
        handlePostCreate();
      }
    }
  };

  return (
    <Dialog
      open={open}
      onClose={(_, reason) =>
        reason !== "backdropClick" && handleClose && handleClose()
      }
      maxWidth="md"
      fullWidth
    >
      <AppBar sx={{ position: "relative" }}>
        <Toolbar>
          <GradientAvatar sx={{ bgcolor: "secondary.main" }}>
            <RocketLaunch />
          </GradientAvatar>
          <Typography variant="h6" sx={{ marginLeft: "16px", flex: 1 }}>
            {template ? "New Deployment: " + template.name : "New Deployment"}
          </Typography>
          <Box display="flex" style={{ marginRight: "2rem" }}>
            {cloudName && (
              <>
                <CloudOutlineIcon />
                <Typography style={{ marginLeft: "0.5em" }}>
                  {cloudName.toUpperCase()}
                  {projectId ? ` / ${projectId}` : ""}
                </Typography>
              </>
            )}
          </Box>
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
        </Toolbar>
      </AppBar>

      {activeStep === STEPS.CREDENTIAL && (
        <>
          <AddCredentials />
          <PrerequisiteStepControls
            {...{ handleNext, handlePrev }}
            disablePrev={activeStep === prereqSteps.current[0]}
            disableNext={!hasCloudCredential}
          />
        </>
      )}

      {activeStep === STEPS.CLOUD && (
        <>
          <SelectCloud></SelectCloud>
          <PrerequisiteStepControls
            {...{ handleNext, handlePrev }}
            disablePrev={activeStep === prereqSteps.current[0]}
            disableNext={!cloudId || !projectId}
          />
        </>
      )}

      {activeStep === STEPS.TEMPLATE && (
        <>
          <SelectTemplate templates={templates}></SelectTemplate>
          <PrerequisiteStepControls
            {...{ handleNext, handlePrev }}
            disablePrev={activeStep === prereqSteps.current[0]}
            disableNext={!template}
          />
        </>
      )}

      {activeStep >= 0 && (
        <CreateWizardController
          activeStep={activeStep}
          setActiveStep={setActiveStep}
          handlePrev={handlePrev}
          submitHandler={handleCreateDeployment}
          regionId={regionId}
          setRegionId={setRegionId}
        ></CreateWizardController>
      )}

      {activeStep === STEPS.SUCCESS && (
        <DeploymentSuccess
          handleClose={handleClose}
          handlePostCreate={handlePostCreate}
        />
      )}
    </Dialog>
  );
}
