import {
  Box,
  Chip,
  Grid,
  ListItem,
  ListItemText,
  Typography,
} from "@mui/material";
import Alert from "@mui/material/Alert";
import AdvancedSettings from "../Fields/AdvancedSettings";
import { BootDiskReview } from "../Fields/BootDisk";

const ReviewItem = ({ label, value, type, highlighted, valueLabel }) => {
  return (
    <Grid xs={12} sm={6} md={6}>
      <ListItem>
        <ListItemText
          primary={
            <Typography
              variant="body"
              color={highlighted && "#ff007f"}
              sx={{ textDecoration: highlighted ? "underline" : "none" }}
            >
              <Box component="span" sx={{ fontWeight: "bold" }}>
                {" "}
                {label}:{" "}
              </Box>
              {type === "bool" ? (value ? "Yes" : "No") : value}
              {valueLabel && " "}
              {valueLabel}
            </Typography>
          }
        />
      </ListItem>
    </Grid>
  );
};

const JupyterhubReview = (props) => {
  return (
    <Box display="flex" justifyContent="center">
      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignContent="flex-start"
        alignItems="flex-start"
        paddingX={2}
      >
        {props.changedParams?.length > 0 && (
          <Grid item xs={12}>
            <Box display="flex" justifyContent="center" mb={1}>
              <Alert severity="info" variant="outlined">
                <Typography>
                  Please review your changes (highlighed in{" "}
                  <Box
                    component={"span"}
                    sx={{
                      color: "#ff007f",
                      fontWeight: "bold",
                      textDecoration: "underline",
                    }}
                  >
                    pink and underlined
                  </Box>
                  ).
                </Typography>{" "}
              </Alert>
            </Box>
          </Grid>
        )}
        {props.changedParams?.length === 0 && (
          <Grid item xs={12}>
            <Box display="flex" justifyContent="center" mb={1}>
              <Alert severity="info">No changes have been made.</Alert>
            </Box>
          </Grid>
        )}
        <ReviewItem
          label={"Region"}
          value={props.deploymentValues["regionId"]}
        />

        <ReviewItem label={"Template"} value={props.template.name} />

        <ReviewItem
          label={"Name"}
          value={props.values.instance_name}
          highlighted={props.changedParams?.includes("instance_name")}
        />
        {props.isEditing && (
          <ReviewItem
            label={"Power State"}
            value={props.values.power_state}
            highlighted={props.changedParams?.includes("power_state")}
          />
        )}
        <ReviewItem
          label={"Image"}
          value={props.values.image_name}
          highlighted={props.changedParams?.includes("image_name")}
        />
        <ReviewItem
          label={"Instances"}
          value={props.values.instance_count}
          highlighted={props.changedParams?.includes("instance_count")}
        />
        <ReviewItem
          label={"Master Flavor"}
          value={props.values.flavor_master}
          valueLabel={
            props.values.flavor_master.startsWith("g3") && (
              <Chip size="small" label="GPU" style={{ marginLeft: ".3em" }} />
            )
          }
          highlighted={props.changedParams?.includes("flavor_master")}
        />
        {props.values.instance_count > 1 && (
          <ReviewItem
            label={"Worker Flavor"}
            value={props.values.flavor}
            valueLabel={
              props.values.flavor.startsWith("g3") && (
                <Chip size="small" label="GPU" style={{ marginLeft: ".3em" }} />
              )
            }
            highlighted={props.changedParams?.includes("flavor")}
          />
        )}
        <ReviewItem
          label={"Users"}
          value={props.values.jupyterhub_allowed_users.length}
          valueLabel={
            props.values.jupyterhub_allowed_users.length === 1
              ? "user"
              : "users"
          }
          highlighted={props.changedParams?.includes(
            "jupyterhub_allowed_users"
          )}
        />
        <ReviewItem
          label={"Admins"}
          value={props.values.jupyterhub_admins.length}
          valueLabel={
            props.values.jupyterhub_admins.length === 1 ? "admin" : "admins"
          }
          highlighted={props.changedParams?.includes("jupyterhub_admins")}
        />
        <ReviewItem
          label={"Auth Type"}
          value={props.values.jupyterhub_authentication}
          highlighted={props.changedParams?.includes(
            "jupyterhub_authentication"
          )}
        />
        {props.values.jupyterhub_authentication === "dummy" && (
          <ReviewItem
            label={"Dummy Password"}
            value={props.values.jupyterhub_dummy_password}
            highlighted={props.changedParams?.includes(
              "jupyterhub_dummy_password"
            )}
          />
        )}
        {props.values.jupyterhub_authentication === "github" && (
          <>
            <ReviewItem
              label={"OAuth2 Client ID"}
              value={props.values.jupyterhub_oauth2_clientid}
              highlighted={props.changedParams?.includes(
                "jupyterhub_oauth2_clientid"
              )}
            />
            <ReviewItem
              label={"OAuth2 Secret"}
              value={props.values.jupyterhub_oauth2_secret}
              highlighted={props.changedParams?.includes(
                "jupyterhub_oauth2_secret"
              )}
            />
          </>
        )}
        <ReviewItem
          label={"Default Image"}
          value={props.values.jupyterhub_singleuser_image}
          highlighted={props.changedParams?.includes(
            "jupyterhub_singleuser_image"
          )}
        />
        <ReviewItem
          label={"Image Tag"}
          value={props.values.jupyterhub_singleuser_image_tag}
          highlighted={props.changedParams?.includes(
            "jupyterhub_singleuser_image_tag"
          )}
        />
        <ReviewItem
          label={"Shared Storage"}
          value={
            props.values.enable_shared_storage
              ? props.values.jh_storage_size + " GB"
              : "None"
          }
          highlighted={props.changedParams?.includes("jh_storage_size")}
        />
        {props.values.enable_shared_storage && (
          <>
            <ReviewItem
              label={"Storage Directory"}
              value={props.values.jh_storage_mount_dir}
              type={"bool"}
              highlighted={props.changedParams?.includes(
                "jh_storage_mount_dir"
              )}
            />
            <ReviewItem
              label={"Storage Readonly"}
              value={props.values.jh_storage_readonly}
              type={"bool"}
              highlighted={props.changedParams?.includes("jh_storage_readonly")}
            />
          </>
        )}
        <Box display="flex" justifyContent="flex-start">
          <AdvancedSettings>
            <Box display="flex" justifyContent="center">
              <Grid
                container
                direction="row"
                justifyContent="flex-start"
                alignContent="center"
                alignItems="center"
              >
                {props.values.instance_count > 1 && (
                  <ReviewItem
                    label={"Disable notebooks on master node"}
                    value={props.values.do_jh_singleuser_exclude_master}
                    type={"bool"}
                    highlighted={props.changedParams?.includes(
                      "do_jh_singleuser_exclude_master"
                    )}
                  />
                )}

                <ReviewItem
                  label={"Enable GPU Sharing"}
                  value={props.values.gpu_timeslice_enable}
                  type={"bool"}
                  highlighted={props.changedParams?.includes(
                    "gpu_timeslice_enable"
                  )}
                />
                {props.values.gpu_timeslice_enable && (
                  <ReviewItem
                    label={"Number of Timeslices"}
                    value={props.values.gpu_timeslice_num}
                    highlighted={props.changedParams?.includes(
                      "gpu_timeslice_num"
                    )}
                  />
                )}
                <ReviewItem
                  label={"Hostname"}
                  value={
                    props.values.jupyterhub_hostname
                      ? props.values.jupyterhub_hostname
                      : "default"
                  }
                  highlighted={props.changedParams?.includes(
                    "jupyterhub_hostname"
                  )}
                />
                <ReviewItem
                  label={"IP Address"}
                  value={
                    props.values.jupyterhub_floating_ip
                      ? props.values.jupyterhub_floating_ip
                      : "default"
                  }
                  highlighted={props.changedParams?.includes(
                    "jupyterhub_floating_ip"
                  )}
                />
              </Grid>
            </Box>
            <Box>
              <BootDiskReview values={props.values} />
            </Box>
          </AdvancedSettings>
        </Box>
      </Grid>
    </Box>
  );
};

export default JupyterhubReview;
