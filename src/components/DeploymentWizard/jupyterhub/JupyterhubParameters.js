import { useState } from "react";
import {
  Box,
  Typography,
  Button,
  Grid,
  TextField,
  Checkbox,
  FormControl,
  FormControlLabel,
  InputLabel,
  Select,
  MenuItem,
  Switch,
  FormHelperText,
} from "@mui/material";
import AllowedUsersList from "./AllowedUserList";
import Papa from "papaparse";
import SizeGB from "../Fields/SizeGB";

export const AddUsernames = ({
  values,
  onChange,
  isEditing,
  isParamEditable,
}) => {
  const [file, setFile] = useState(null);
  const [usersToAddString, setUsersToAddString] = useState("");

  // ---------------------- CSV --------------------------- //

  const removeUpload = () => {
    setFile(null);
  };

  const handleUpload = (event) => {
    const files = Array.from(event.target.files);
    const [file] = files;
    setFile(file);
    event.target.value = "";
  };

  const addUsersFromCsv = () => {
    Papa.parse(file, {
      header: true,
      complete: (results) => {
        let usersToAdd = [];
        results.data.forEach((row) => {
          if (row.username) {
            usersToAdd.push(row.username.trim());
          }
        });
        onChange("jupyterhub_allowed_users", [
          ...values["jupyterhub_allowed_users"],
          ...usersToAdd,
        ]);
        setFile(null);
      },
    });
  };

  // ------------------ Manual Add ----------------------- //

  const addUsers = () => {
    let usersToAdd = usersToAddString.split(",").map((element) => {
      return element.trim();
    });
    onChange("jupyterhub_allowed_users", [
      ...values["jupyterhub_allowed_users"],
      ...usersToAdd,
    ]);
    setUsersToAddString("");
  };

  const addAdmin = (usersToAdd) => {
    onChange("jupyterhub_admins", [
      ...values["jupyterhub_admins"],
      ...usersToAdd,
    ]);
  };

  const removeUsers = (usersToRemove) => {
    let users = values["jupyterhub_allowed_users"].filter((user) => {
      return !usersToRemove.includes(user);
    });

    onChange("jupyterhub_allowed_users", users);
  };

  const removeAdmin = (adminToRemove) => {
    let newAdminList = values["jupyterhub_admins"].filter((user) => {
      return !adminToRemove.includes(user);
    });

    onChange("jupyterhub_admins", newAdminList);
  };

  return (
    <Box sx={{ maxWidth: "90%", margin: "0 auto" }}>
      <Box>
        <Typography
          variant="h6"
          component="h2"
          style={{ fontSize: "1.25rem", marginBottom: ".5em" }}
        >
          Add Usernames
        </Typography>
      </Box>
      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignItems="flex-start"
      >
        <Grid item md={6}>
          <Grid
            container
            direction="column"
            justifyContent="flex-start"
            alignItems="flex-start"
            spacing={3}
            style={{ marginTop: ".5rem" }}
          >
            <Grid item>
              <TextField
                name="usersToAddString"
                value={usersToAddString}
                // autoFocus
                autoComplete="off"
                id="outlined-helperText"
                multiline
                label=" Add Individual Usernames"
                helperText="Separate usernames with a comma"
                variant="outlined"
                onChange={(e) => setUsersToAddString(e.target.value)}
              />
              <Button
                style={{ margin: ".5rem" }}
                color="primary"
                variant="contained"
                onClick={() => addUsers()}
                disabled={!usersToAddString || usersToAddString.length === 0}
              >
                Add
              </Button>
            </Grid>

            <Grid item>
              <Typography variant="subtitle1">
                <b>Upload CSV File</b>
              </Typography>
              <Typography variant="subtitle2">
                File must contain 'username' column.
              </Typography>

              <Box mt={2}>
                <Typography>{file ? file.name : "No File Selected"}</Typography>
              </Box>
              <Grid
                container
                direction="row"
                spacing={3}
                alignItems="flex-start"
                justifyContent="flex-start"
                style={{ marginTop: ".5rem" }}
              >
                <Grid item md={4}>
                  <Button variant="contained" component="label" color="primary">
                    Browse
                    <input
                      type="file"
                      hidden
                      accept=".csv"
                      multiple={false}
                      onChange={handleUpload}
                    />
                  </Button>
                </Grid>
                <Grid item md={4}>
                  <Button
                    color="primary"
                    variant="contained"
                    disabled={!file}
                    onClick={addUsersFromCsv}
                  >
                    Upload
                  </Button>
                </Grid>
                <Grid item md={12}>
                  {
                    <Button size="small" color="primary" onClick={removeUpload}>
                      Remove File
                    </Button>
                  }
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={6}>
          <AllowedUsersList
            users={values["jupyterhub_allowed_users"]}
            admin={values["jupyterhub_admins"]}
            addAdmin={addAdmin}
            removeAdmin={removeAdmin}
            removeUsers={removeUsers}
          ></AllowedUsersList>
        </Grid>
      </Grid>
    </Box>
  );
};

export const Authentication = ({
  values,
  onChange,
  isEditing,
  isParamEditable,
}) => {
  return (
    <Box sx={{ maxWidth: "90%", margin: "0 auto" }}>
      <Box>
        <Typography
          variant="h6"
          component="h2"
          style={{ fontSize: "1.25rem", marginBottom: "1em" }}
        >
          JupyterHub Configuration
        </Typography>
      </Box>
      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignItems="flex-start"
        spacing={3}
      >
        <Grid item md={5} xs={12}>
          <Typography style={{ marginBottom: "1em" }}>
            Authentication Type
          </Typography>
          <Grid container direction="column" spacing={3}>
            <Grid item>
              <FormControl variant="outlined" fullWidth>
                <InputLabel id="demo-simple-select-outlined-label">
                  Auth Type
                </InputLabel>
                <Select
                  name="jupyterhub_authentication"
                  id="jupyterhub_authentication"
                  value={values["jupyterhub_authentication"]}
                  disabled={
                    isEditing && !isParamEditable("jupyterhub_authentication")
                  }
                  onChange={(e) =>
                    onChange("jupyterhub_authentication", e.target.value)
                  }
                  label="Auth Type"
                >
                  <MenuItem value="github">GitHub</MenuItem>
                  <MenuItem value="dummy">Dummy Auth</MenuItem>
                </Select>
              </FormControl>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={7} xs={12}>
          {values["jupyterhub_authentication"] === "github" && (
            <>
              <Typography style={{ marginBottom: "1rem" }}>
                OAuth Credentials
              </Typography>
              <Grid container direction="column" spacing={3}>
                <Grid item>
                  <TextField
                    id="jupyterhub_oauth2_clientid"
                    name="jupyterhub_oauth2_clientid"
                    value={values["jupyterhub_oauth2_clientid"]}
                    disabled={
                      isEditing &&
                      !isParamEditable("jupyterhub_oauth2_clientid")
                    }
                    onChange={(e) =>
                      onChange("jupyterhub_oauth2_clientid", e.target.value)
                    }
                    label=" OAuth Client ID"
                    placeholder="clientid"
                    fullWidth
                    variant="outlined"
                    required={values["jupyterhub_authentication"] === "github"}
                  />
                </Grid>
                <Grid item>
                  <TextField
                    id="jupyterhub_oauth2_secret"
                    name="jupyterhub_oauth2_secret"
                    value={values["jupyterhub_oauth2_secret"]}
                    disabled={
                      isEditing && !isParamEditable("jupyterhub_oauth2_secret")
                    }
                    onChange={(e) =>
                      onChange("jupyterhub_oauth2_secret", e.target.value)
                    }
                    label=" OAuth Client Secret"
                    placeholder="clientsecret"
                    fullWidth
                    variant="outlined"
                    required={values["jupyterhub_authentication"] === "github"}
                  />
                </Grid>
              </Grid>
            </>
          )}

          {values["jupyterhub_authentication"] === "dummy" && (
            <>
              <Typography style={{ marginBottom: "1rem" }}>
                Dummy Password
              </Typography>
              <Grid container direction="column" spacing={3}>
                <Grid item>
                  <TextField
                    id="jupyterhub_dummy_password"
                    name="jupyterhub_dummy_password"
                    value={
                      values["jupyterhub_dummy_password"] !== undefined
                        ? values["jupyterhub_dummy_password"]
                        : ""
                    }
                    disabled={
                      isEditing && !isParamEditable("jupyterhub_dummy_password")
                    }
                    onChange={(e) =>
                      onChange("jupyterhub_dummy_password", e.target.value)
                    }
                    label="Dummy Password"
                    fullWidth
                    variant="outlined"
                    required={values["jupyterhub_authentication"] === "dummy"}
                  />
                </Grid>
              </Grid>
            </>
          )}
        </Grid>
      </Grid>
    </Box>
  );
};

export const Custom = ({ values, onChange, isEditing, isParamEditable }) => {
  const wizard = {
    isEditing: isEditing,
    isParamEditable: isParamEditable,
    onChange: onChange,
    values: values,
  };
  return (
    <>
      <Box sx={{ maxWidth: "90%", margin: "0 auto" }}>
        <Box>
          <Typography
            variant="h6"
            component="h2"
            style={{ fontSize: "1.25rem", marginBottom: "1rem" }}
          >
            Shared Storage
          </Typography>
        </Box>

        <Grid container spacing={3} mb={1}>
          <Grid item md={12}>
            <Box mb={1}>
              <FormControl variant="outlined" fullWidth>
                <FormControlLabel
                  control={
                    <Switch
                      checked={values["enable_shared_storage"]}
                      disabled={
                        isEditing && !isParamEditable("jh_storage_size")
                      }
                      onChange={(e) =>
                        onChange("enable_shared_storage", e.target.checked)
                      }
                    />
                  }
                  label="Enable Shared Storage"
                />
              </FormControl>
              <FormHelperText>
                Enable shared storage to configure storage options.
              </FormHelperText>
            </Box>
          </Grid>
        </Grid>

        {values["enable_shared_storage"] && (
          <>
            <Grid container spacing={3}>
              <SizeGB
                disabled={
                  wizard.isEditing && !wizard.isParamEditable("jh_storage_size")
                }
                half={true}
                label={"Storage in GB"}
                name={"jh_storage_size"}
                wizard={wizard}
              />
              <Grid item sm={6} xs={12}>
                <TextField
                  id="jh_storage_mount_dir"
                  name="jh_storage_mount_dir"
                  value={values["jh_storage_mount_dir"]}
                  disabled={
                    isEditing && !isParamEditable("jh_storage_mount_dir")
                  }
                  onChange={(e) =>
                    onChange("jh_storage_mount_dir", e.target.value)
                  }
                  label="Storage Directory"
                  fullWidth
                  variant="outlined"
                  required
                />
              </Grid>
            </Grid>

            <Grid container spacing={3}>
              <Grid item md={6}>
                <Box mt={2}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        id="jh_storage_readonly"
                        name="jh_storage_readonly"
                        disabled={
                          isEditing && !isParamEditable("jh_storage_readonly")
                        }
                        checked={values["jh_storage_readonly"]}
                        onChange={(e) =>
                          onChange("jh_storage_readonly", e.target.checked)
                        }
                      />
                    }
                    label="Shared Storage Readonly?"
                  />
                </Box>
              </Grid>
            </Grid>
          </>
        )}
      </Box>
    </>
  );
};

export const ImageTag = ({ values, onChange, isEditing, isParamEditable }) => {
  return (
    <>
      <Box sx={{ maxWidth: "90%", margin: "0 auto" }}>
        <Box>
          <Typography
            variant="h6"
            component="h2"
            style={{ fontSize: "1.25rem", marginBottom: "1rem" }}
          >
            Image
          </Typography>
        </Box>
        <Box>
          <Typography style={{ marginBottom: "1rem" }}>
            Default Image & Tag
          </Typography>
        </Box>
        <Grid container direction="row" spacing={3}>
          <Grid item md={6} sm={6} xs={12}>
            <TextField
              id="jupyterhub_singleuser_image"
              name="jupyterhub_singleuser_image"
              value={values["jupyterhub_singleuser_image"]}
              disabled={
                isEditing && !isParamEditable("jupyterhub_singleuser_image")
              }
              onChange={(e) =>
                onChange("jupyterhub_singleuser_image", e.target.value)
              }
              label="Image"
              fullWidth
              variant="outlined"
            />
          </Grid>
          <Grid item md={6} sm={6} xs={12}>
            <TextField
              id="jupyterhub_singleuser_image_tag"
              name="jupyterhub_singleuser_image_tag"
              value={values["jupyterhub_singleuser_image_tag"]}
              disabled={
                isEditing && !isParamEditable("jupyterhub_singleuser_image_tag")
              }
              onChange={(e) =>
                onChange("jupyterhub_singleuser_image_tag", e.target.value)
              }
              label="Tag"
              fullWidth
              variant="outlined"
            />
          </Grid>
        </Grid>
      </Box>
    </>
  );
};
