import {
  Box,
  CircularProgress,
  FormControl,
  FormControlLabel,
  FormHelperText,
  Grid,
  Stack,
  Switch,
  TextField,
} from "@mui/material";
import AdvancedSettings from "../Fields/AdvancedSettings";
import BootDisk from "../Fields/BootDisk";
import Flavor from "../Fields/Flavor";
import InstanceCount from "../Fields/InstanceCount";
import InstanceName from "../Fields/InstanceName";
import Image from "../Fields/Image";
import PowerState from "../Fields/PowerState";
import NumberField from "../Fields/Number";
import { filterFlavorsByMinValues } from "../../../utils";

const JupyterhubParameters = ({
  values,
  onChange,
  nameError,
  setNameError,
  images,
  flavors,
  loadingImages,
  disableMasterSwitch,
  isEditing,
  isParamEditable,
}) => {
  flavors = filterFlavorsByMinValues(
    flavors,
    images?.find((image) => image.name === values["image_name"])?.min_disk,
    images?.find((image) => image.name === values["image_name"])?.min_ram
  );
  const wizard = {
    flavors: flavors,
    images: images,
    isEditing: isEditing,
    isParamEditable: isParamEditable,
    nameError: nameError,
    onChange: onChange,
    setNameError: setNameError,
    values: values,
  };

  return (
    <>
      {loadingImages && (
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <CircularProgress />
        </Box>
      )}

      {!loadingImages && (
        <Box>
          <Grid
            container
            justifyContent="center"
            alignItems="flex-start"
            spacing={2}
            mb={2}
          >
            <Grid
              item
              xs={12}
              sm={wizard.isEditing ? 6 : 12}
              md={wizard.isEditing ? 4 : 8}
            >
              <InstanceName wizard={wizard} />
            </Grid>
            {wizard.isEditing && (
              <Grid item xs={12} sm={6} md={4}>
                <PowerState wizard={wizard} />
              </Grid>
            )}
          </Grid>

          <Grid
            container
            justifyContent="center"
            alignItems="flex-start"
            spacing={2}
            mb={2}
          >
            <Grid item xs={12} md={8}>
              <Image wizard={wizard} />
            </Grid>
          </Grid>

          <Grid
            container
            justifyContent="center"
            alignItems="flex-start"
            spacing={2}
            mb={2}
          >
            <Grid item xs={12} sm={4} md={2}>
              <InstanceCount wizard={wizard} />
            </Grid>
            <Grid item xs={12} sm={4} md={3}>
              <Flavor
                helperText={
                  parseInt(wizard.values["instance_count"]) === 1 &&
                  (wizard.values["flavor_master"]?.startsWith("g3")
                    ? "GPU kubernetes drivers will be installed."
                    : "Selecting a GPU flavor will install GPU kubernetes drivers.")
                }
                label="Master Flavor"
                name="flavor_master"
                wizard={wizard}
              />
            </Grid>
            <Grid item xs={12} sm={4} md={3}>
              {parseInt(values["instance_count"]) > 1 && (
                <>
                  <Flavor
                    helperText={
                      parseInt(wizard.values["instance_count"]) > 1 &&
                      (wizard.values["flavor_master"]?.startsWith("g3")
                        ? "GPU kubernetes drivers will be installed."
                        : "Selecting a GPU flavor will install GPU kubernetes drivers.")
                    }
                    label="Worker Flavor"
                    name="flavor"
                    wizard={wizard}
                  />
                </>
              )}
            </Grid>
          </Grid>
          <Grid container justifyContent="center" spacing={2} mb={2}>
            <Grid item xs={12} md={8}>
              <Box>
                <AdvancedSettings>
                  <Stack spacing={2}>
                    <Box>
                      <Grid
                        container
                        justifyContent="center"
                        columnSpacing={2}
                        rowSpacing={1}
                      >
                        <Grid item xs={12} sm={6}>
                          <TextField
                            id="jupyterhub_hostname"
                            name="jupyterhub_hostname"
                            value={values["jupyterhub_hostname"]}
                            disabled={
                              isEditing &&
                              !isParamEditable("jupyterhub_hostname")
                            }
                            onChange={(e) =>
                              onChange("jupyterhub_hostname", e.target.value)
                            }
                            label="Hostname"
                            fullWidth
                            variant="outlined"
                            helperText="If unknown, leave blank."
                            autoComplete="off"
                          />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                          <TextField
                            id="jupyterhub_floating_ip"
                            name="jupyterhub_floating_ip"
                            value={values["jupyterhub_floating_ip"]}
                            disabled={
                              isEditing &&
                              !isParamEditable("jupyterhub_floating_ip")
                            }
                            onChange={(e) =>
                              onChange("jupyterhub_floating_ip", e.target.value)
                            }
                            label=" IP Address"
                            fullWidth
                            variant="outlined"
                            helperText="Otherwise, an IP address will be generated for the deployment."
                            autoComplete="off"
                          />
                        </Grid>
                      </Grid>
                    </Box>
                    <Box>
                      <Grid
                        container
                        justifyContent="center"
                        columnSpacing={2}
                        rowSpacing={1}
                      >
                        <Grid item xs={12}>
                          {values["instance_count"] > 1 && (
                            <FormControl variant="outlined" fullWidth>
                              <FormControlLabel
                                control={
                                  <Switch
                                    checked={
                                      values["do_jh_singleuser_exclude_master"]
                                    }
                                    disabled={
                                      disableMasterSwitch ||
                                      (isEditing &&
                                        !isParamEditable(
                                          "do_jh_singleuser_exclude_master"
                                        ))
                                    }
                                    onChange={(e) =>
                                      onChange(
                                        "do_jh_singleuser_exclude_master",
                                        e.target.checked
                                      )
                                    }
                                  />
                                }
                                label="Disable notebooks on master node?"
                              />
                              {!disableMasterSwitch && (
                                <FormHelperText style={{ marginLeft: 0 }}>
                                  Check this option to disable the launching of
                                  notebooks on the master node. By default,
                                  notebooks can launch on the master node.
                                </FormHelperText>
                              )}
                              {disableMasterSwitch && (
                                <FormHelperText error={true}>
                                  This option is required because master flavor
                                  is non-GPU and worker flavor is GPU.
                                </FormHelperText>
                              )}
                            </FormControl>
                          )}
                        </Grid>
                      </Grid>
                    </Box>
                    <Box>
                      <Grid container justifyContent="center" rowSpacing={2}>
                        <Grid item xs={12}>
                          <FormControl variant="outlined" fullWidth>
                            <FormControlLabel
                              control={
                                <Switch
                                  checked={values["gpu_timeslice_enable"]}
                                  disabled={
                                    isEditing &&
                                    !isParamEditable("gpu_timeslice_enable")
                                  }
                                  onChange={(e) =>
                                    onChange(
                                      "gpu_timeslice_enable",
                                      e.target.checked
                                    )
                                  }
                                />
                              }
                              label="Enable GPU Sharing"
                            />
                          </FormControl>
                        </Grid>

                        {wizard.values["gpu_timeslice_enable"] && (
                          <>
                            <Grid item xs={12} sm={6} md={6}>
                              <NumberField
                                label={"Number of Timeslices"}
                                name={"gpu_timeslice_num"}
                                minValue={2}
                                numType={"integer"}
                                wizard={wizard}
                              ></NumberField>
                            </Grid>
                            <Grid item xs={12} sm={6} md={6}></Grid>
                          </>
                        )}
                      </Grid>
                    </Box>
                    <Box>
                      <BootDisk wizard={wizard} />
                    </Box>
                  </Stack>
                </AdvancedSettings>
              </Box>
            </Grid>
          </Grid>
        </Box>
      )}
    </>
  );
};

export default JupyterhubParameters;
