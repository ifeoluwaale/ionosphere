import React from "react";
import { useState, useEffect } from "react";

import { Box, DialogContent, Grid, Button } from "@mui/material";

import { FormStepper, FormControls } from "../../forms/FormStepper";

import SelectRegion from "../Steps/SelectRegion";
import JupyterhubParameters from "./JupyterhubStart";
import JupyterhubReview from "./JupyterhubReview";
import {
  AddUsernames,
  Authentication,
  Custom,
  ImageTag,
} from "./JupyterhubParameters";

import { useAPI, useUser } from "../../../contexts";
import {
  js2SortByImageByName,
  sortBy,
  ValidateDeploymentName,
} from "../../../utils";
import Alert from "@mui/material/Alert";
import { BootDiskSetInitialValues } from "../Fields/BootDisk";
import { BootDiskSetRunParams } from "../Fields/BootDisk";
import { BootDiskAddParams } from "../Fields/BootDisk";

/**
 * The EditJupyterhubWizard component controls the edit functionality unique to the
 * jupyterhub template, while logic shared across all wizards is located in
 * EditWizardController or EditDeploymentDialog.
 */
const EditJupyterhubWizard = (props) => {
  // Jupyterhub template only supports Ubuntu 20/22 images
  const ALLOWED_IMAGES = ["Featured-Ubuntu20", "Featured-Ubuntu22"];

  const api = useAPI();
  const [user] = useUser();

  /**
   * jupyterhub params with type, editable specified
   * - type is used to convert param to value to be used in UI fields
   * - editable is used to enable/disable fields in edit wizard
   *
   * TODO: could instead pull from template metadata in the future when
   * 'editable' field is added
   */
  const paramList = [
    { name: "jupyterhub_dummy_password", type: "string", editable: true },
    { name: "username", type: "string", editable: false },
    { name: "do_jh_singleuser_exclude_master", type: "bool", editable: true },
    { name: "flavor", type: "string", editable: false },
    { name: "flavor_master", type: "string", editable: false },
    { name: "jh_storage_mount_dir", type: "string", editable: true },
    { name: "jupyterhub_allowed_users", type: "array_string", editable: true },
    { name: "instance_name", type: "string", editable: true },
    { name: "jh_storage_size", type: "string", editable: true },
    { name: "jupyterhub_oauth2_clientid", type: "string", editable: true },
    { name: "jupyterhub_singleuser_image", type: "string", editable: true },
    { name: "jupyterhub_singleuser_image_tag", type: "string", editable: true },
    { name: "power_state", type: "string", editable: true },
    { name: "project", type: "string", editable: false },
    { name: "do_enable_gpu", type: "bool", editable: true },
    { name: "gpu_timeslice_enable", type: "bool", editable: true },
    { name: "gpu_timeslice_num", type: "integer", editable: true },
    { name: "image_name", type: "string", editable: false },
    { name: "jh_storage_readonly", type: "bool", editable: true },
    { name: "jupyterhub_floating_ip", type: "string", editable: true },
    { name: "jupyterhub_hostname", type: "string", editable: true },
    { name: "instance_count", type: "integer", editable: true },
    { name: "jupyterhub_admins", type: "array_string", editable: true },
    { name: "jupyterhub_authentication", type: "string", editable: true },
    { name: "jupyterhub_oauth2_secret", type: "string", editable: true },
  ];
  BootDiskAddParams(paramList);

  let initialValues = props.getInitialValues(paramList);

  if (initialValues["jh_storage_size"] === "0") {
    initialValues["enable_shared_storage"] = false;
  } else {
    initialValues["enable_shared_storage"] = true;
  }

  // obj for storing run parameter values - populate current params from deployment
  const [values, setValues] = useState(initialValues);
  BootDiskSetInitialValues(values);

  // whether user input is valid for current step
  const [isValid, setValid] = useState(false);

  // if invalid deployment name input, the error message (null if name is valid)
  const [nameError, setNameError] = useState(null);

  const [flavors, setFlavors] = useState();
  const [images, setImages] = useState();
  const [loadingImages, setLoadingImages] = useState(false);

  // list of params that have been updated by the user
  const [changedParams, setChangedParams] = useState([]);

  /**
   * When worker_flavor==GPU and master_flavor!=GPU, do_jh_singleuser_exclude_master
   * field must be set to true. The disableMasterSwitch boolean var disables the
   * UI control in this case.
   */
  const [disableMasterSwitch, setDisableMasterSwitch] = useState(false);

  let activeStep = props.activeStep;

  /**
   * Returns whether given param is editable, pulled from paramList.
   * TODO: Update to pull from template metadata when 'editable' field is added.
   * @param {string} paramName
   */
  const isParamEditable = (paramName) => {
    return paramList.find((p) => p.name === paramName).editable;
  };

  /**
   * Update values object upon user input change.
   * @param {string} id: name of parameter to update
   * @param {*} value: new value for parameter
   */
  const handleChange = (id, value) => {
    setValues({ ...values, [id]: value });
  };

  /**
   * Fetches images and flavors, and sorts by RAM for display.
   */
  const fetchImagesAndFlavors = async () => {
    setLoadingImages(true);
    try {
      let [images, flavors] = await Promise.all([
        api.providerImages(props.cloudId, {
          // pass credential to filter images by allocation
          credential: props.deploymentValues.credentialId,
          region: props.deploymentValues.regionId,
        }),
        api.providerFlavors(props.cloudId, {
          credential: props.deploymentValues.credentialId,
          region: props.deploymentValues.regionId,
        }), //(workspace.default_provider_id),
      ]);

      // Restrict image options to ALLOWED_IMAGES
      images = images.filter((i) => ALLOWED_IMAGES.includes(i.name));

      images = images.sort(js2SortByImageByName);
      setImages(images);
      flavors = flavors.sort(sortBy("id"));
      setFlavors(flavors);
      setLoadingImages(false);
    } catch (e) {
      console.log(e); //TODO
      setLoadingImages(false);
    }
  };

  /**
   * Prepare deployment params to be submitted to updateDeployment API endpoint &
   * prepare run params to be submitted to run createRun API endpoint.
   * @param {obj} deploymentValues
   * @param {obj} values
   * @returns list of {key,value} parameters
   */
  const setupParams = (deploymentValues, values) => {
    // setup deployment edit params (only name is editable)
    let deploymentParams = {};

    // update deployment only if name has changed
    if (props.deployment.name !== values["instance_name"]) {
      deploymentParams.name = values["instance_name"];
    }

    // setup run params
    let runParams = [
      { key: "username", value: user.username },
      { key: "project", value: deploymentValues["projectId"] },
      { key: "region", value: deploymentValues["regionId"] },

      // Parameters step
      { key: "instance_name", value: values["instance_name"] },
      { key: "power_state", value: values["power_state"] },
      { key: "instance_count", value: values["instance_count"].toString() },
      { key: "image_name", value: values["image_name"] },
      { key: "flavor_master", value: values["flavor_master"] },
      {
        key: "do_enable_gpu",
        value: values["do_enable_gpu"] ? "true" : "false",
      },
      {
        key: "gpu_timeslice_enable",
        value: values["gpu_timeslice_enable"] ? "true" : "false",
      },
      {
        key: "gpu_timeslice_num",
        value: values["gpu_timeslice_enable"]
          ? values["gpu_timeslice_num"].toString()
          : "0",
      },

      // Authentication step
      {
        key: "jupyterhub_authentication",
        value: values["jupyterhub_authentication"],
      },

      // Users step
      {
        key: "jupyterhub_allowed_users",
        value: values["jupyterhub_allowed_users"].join(","),
      },
      {
        key: "jupyterhub_admins",
        value: values["jupyterhub_admins"].join(","),
      },

      // Storage step
      {
        key: "jh_storage_size",
        value: values["enable_shared_storage"]
          ? values["jh_storage_size"]
          : "0",
      },
      { key: "jh_storage_mount_dir", value: values["jh_storage_mount_dir"] },
      {
        key: "jh_storage_readonly",
        value: values["jh_storage_readonly"] ? "true" : "false",
      },

      // Image step
      {
        key: "jupyterhub_singleuser_image",
        value: values["jupyterhub_singleuser_image"],
      },
      {
        key: "jupyterhub_singleuser_image_tag",
        value: values["jupyterhub_singleuser_image_tag"],
      },
    ];
    BootDiskSetRunParams(runParams, values);

    /**
     * If instance_count==1, ignore any user input for worker_flavor and
     * do_jh_singleuser_exclude_master, since they are not applicable
     * for a single instance. Because it's required by the API, we set
     * worker flavor to match master.
     *
     * Otherwise, use user-selected values.
     */
    if (values["instance_count"] === "1") {
      runParams.push(
        // set worker flavor to same as master
        // (we must pass a value since worker flavor is required by API)
        { key: "flavor", value: values["flavor_master"] },
        { key: "do_jh_singleuser_exclude_master", value: "false" }
      );
    } else {
      runParams.push(
        { key: "flavor", value: values["flavor"] },
        {
          key: "do_jh_singleuser_exclude_master",
          value: values["do_jh_singleuser_exclude_master"] ? "true" : "false",
        }
      );
    }

    /**
     * All auth fields are required by API, so add default/dummy values for the option
     * that was not selected.  i.e. add dummy values for github auth values if dummy
     * password was selected and vice versa.
     */
    if (values["jupyterhub_authentication"] === "dummy") {
      runParams.push(
        {
          key: "jupyterhub_dummy_password",
          value: values["jupyterhub_dummy_password"],
        },
        { key: "jupyterhub_oauth2_clientid", value: "clientid" },
        { key: "jupyterhub_oauth2_secret", value: "clientsecret" }
      );
    } else if (values["jupyterhub_authentication"] === "github") {
      runParams.push(
        {
          key: "jupyterhub_oauth2_clientid",
          value: values["jupyterhub_oauth2_clientid"],
        },
        {
          key: "jupyterhub_oauth2_secret",
          value: values["jupyterhub_oauth2_secret"],
        },
        { key: "jupyterhub_dummy_password", value: "dummypass987" }
      );
    }

    if (values["jupyterhub_floating_ip"]) {
      runParams.push({
        key: "jupyterhub_floating_ip",
        value: values["jupyterhub_floating_ip"],
      });
    }

    if (values["jupyterhub_hostname"]) {
      runParams.push({
        key: "jupyterhub_hostname",
        value: values["jupyterhub_hostname"],
      });
    }

    return [deploymentParams, runParams];
  };

  // When region is updated, prefetch the images/flavors for associated region
  useEffect(() => {
    if (props.deploymentValues["regionId"]) {
      setTimeout(fetchImagesAndFlavors, 0);
    }
  }, [props.deploymentValues["regionId"]]);

  useEffect(() => {
    // validate input for current step to disable/enable next button
    if (activeStep > -1) {
      const validator = steps[activeStep].validator;
      setValid((!validator || validator(values)) && !props.error);
    }

    // if on the last step, check for changed params to display on review step
    if (activeStep === steps.length - 1) {
      const [deploymentParams, runParams] = setupParams(
        props.deploymentValues,
        values
      );
      setChangedParams(props.checkParamChanges(runParams));
    }
  }, [props.activeStep, values]);

  /**
   * Watch for changes in flavor, flavor_master, and instance_count.
   *
   * When worker flavor==GPU and master_flavor!=GPU, do_jh_singleuser_exclude_master
   * field must be set to true. The disableMasterSwitch boolean var disables the
   * UI control in this case.
   *
   * We also set do_enable_gpu at this time. If instance_count==1 we set GPU based
   * on master flavor, otherwise we use worker flavor.
   */
  useEffect(() => {
    let excludeMaster = values["do_jh_singleuser_exclude_master"];
    let enableGpu;

    if (
      values &&
      values["flavor"]?.startsWith("g") &&
      !values["flavor_master"]?.startsWith("g")
    ) {
      excludeMaster = true;
      setDisableMasterSwitch(true);
    } else {
      setDisableMasterSwitch(false);
    }

    if (values["instance_count"] === 1) {
      if (values["flavor_master"].startsWith("g")) {
        enableGpu = true;
      } else {
        enableGpu = false;
      }
    } else {
      if (values["flavor"].startsWith("g")) {
        enableGpu = true;
      } else {
        enableGpu = false;
      }
    }

    setValues({
      ...values,
      ["do_enable_gpu"]: enableGpu,
      ["do_jh_singleuser_exclude_master"]: excludeMaster,
    });
  }, [values["flavor"], values["flavor_master"], values["instance_count"]]);

  // wizard steps
  const steps = [
    {
      title: "Region",
      render: () => (
        <SelectRegion
          regions={props.regions}
          selectedId={props.deploymentValues["regionId"]}
          setRegionId={props.setRegionId}
          loadingRegions={props.loadingRegions}
          isEditing={true}
          isParamEditable={isParamEditable}
        />
      ),
    },
    {
      title: "Parameters",
      validator: (values) =>
        !!values["instance_name"] &&
        !!values["image_name"] &&
        !!values["flavor"] &&
        !ValidateDeploymentName(values["instance_name"]),
      render: (values) => (
        <JupyterhubParameters
          values={values}
          onChange={handleChange}
          nameError={nameError}
          setNameError={setNameError}
          images={images}
          flavors={flavors}
          loadingImages={loadingImages}
          disableMasterSwitch={disableMasterSwitch}
          isEditing={true}
          isParamEditable={isParamEditable}
        />
      ),
    },
    {
      title: "Authentication",
      validator: (values) => {
        return (
          (values["jupyterhub_authentication"] === "github" &&
            !!values["jupyterhub_oauth2_clientid"] &&
            !!values["jupyterhub_oauth2_secret"]) ||
          (values["jupyterhub_authentication"] === "dummy" &&
            !!values["jupyterhub_dummy_password"]) ||
          values["jupyterhub_authentication"] === "default"
        );
      },
      render: () => (
        <Authentication
          values={values}
          onChange={handleChange}
          isEditing={true}
          isParamEditable={isParamEditable}
        ></Authentication>
      ),
    },
    {
      title: "Users",
      validator: (values) =>
        !!values["jupyterhub_allowed_users"].length &&
        !!values["jupyterhub_admins"].length,
      render: () => (
        <AddUsernames
          values={values}
          onChange={handleChange}
          isEditing={true}
          isParamEditable={isParamEditable}
        ></AddUsernames>
      ),
    },
    {
      title: "Storage",
      validator: (values) =>
        !values["enable_shared_storage"] ||
        (values["jh_storage_mount_dir"] && values["jh_storage_size"] > 0),
      render: () => (
        <Custom
          values={values}
          onChange={handleChange}
          isEditing={true}
          isParamEditable={isParamEditable}
        ></Custom>
      ),
    },
    {
      title: "Image",
      validator: (values) =>
        !!values["jupyterhub_singleuser_image"] &&
        !!values["jupyterhub_singleuser_image_tag"],
      render: () => (
        <ImageTag
          values={values}
          onChange={handleChange}
          isEditing={true}
          isParamEditable={isParamEditable}
        ></ImageTag>
      ),
    },

    {
      title: "Review & Deploy",
      render: (values) => (
        <JupyterhubReview
          deploymentValues={props.deploymentValues}
          values={values}
          flavors={flavors}
          images={images}
          template={props.template}
          isEditing={true}
          changedParams={changedParams}
        />
      ),
      // disable submit if no params have been changed
      disableNext: changedParams.length === 0,
    },
  ].filter((step) => step);

  return (
    <>
      <DialogContent style={{ minHeight: "40vh" }}>
        <FormStepper
          activeStep={activeStep}
          steps={steps.map((s) => s.title)}
        />
        <Box mt={6} mb={2}>
          {steps[activeStep].render(values)}
          {props.error && (
            <Box mt={2} display="flex" justifyContent="center">
              <Alert severity="error">{props.error}</Alert>
            </Box>
          )}
        </Box>
      </DialogContent>
      <Box>
        <Grid container>
          <Grid item xs={4}></Grid>
          <Grid item xs={4}>
            <Box display="flex" justifyContent="center" mt={2} mb={3}>
              <FormControls
                disabled={!isValid}
                step={steps[activeStep]}
                activeStep={activeStep}
                numSteps={steps.length}
                nextHandler={props.handleNext.bind(null, values)}
                backHandler={props.handleBack.bind(null, values)}
                closeHandler={props.handleClose}
                submitHandler={() => {
                  const [deploymentParams, runParams] = setupParams(
                    props.deploymentValues,
                    values
                  );
                  props.submitHandler(
                    props.deployment.id,
                    deploymentParams,
                    runParams
                  );
                }}
              />
            </Box>
          </Grid>
          <Grid item xs={4}></Grid>
        </Grid>
      </Box>
    </>
  );
};

export default EditJupyterhubWizard;
