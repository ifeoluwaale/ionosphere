import React from "react";
import { useState, useEffect } from "react";
import { Box, DialogContent } from "@mui/material";

import { FormStepper, FormControls } from "../../forms/FormStepper";

import SelectRegion from "../Steps/SelectRegion";
import K3sParameters from "./K3sParameters";
import K3sReview from "./K3sReview";

import { useAPI, useUser } from "../../../contexts";
import { js2SortByImageByName, sortBy } from "../../../utils";
import Alert from "@mui/material/Alert";
import {
  BootDiskSetInitialValues,
  BootDiskSetRunParams,
  BootDiskValidate,
} from "../Fields/BootDisk";

import { ValidateDeploymentName } from "../../../utils";
import { useDeploymentWizard } from "../contexts/DeploymentWizardContext";
import TemplateStepControls from "../Steps/TemplateStepControls";

/*
 * The CreateK3sWizard component controls the functionality unique to the
 * k3s-single-image template, while logic shared across all wizards is located in
 * CreateWizardController or CreateDeploymentDialog.
 */

const CreateK3sWizard = (props) => {
  const api = useAPI();
  const [user] = useUser();
  const { isSubmitting } = useDeploymentWizard();

  const initialValues = {
    instance_name: "",
    instance_count: 1,
  };
  BootDiskSetInitialValues(initialValues);

  // obj for storing run parameter values
  const [values, setValues] = useState(initialValues);

  // whether user input is valid for current step
  const [isValid, setValid] = useState(false);

  // if invalid deployment name input, the error message (null if name is valid)
  const [nameError, setNameError] = useState(null);

  const [flavors, setFlavors] = useState();
  const [images, setImages] = useState();
  const [loadingImages, setLoadingImages] = useState(false);

  let activeStep = props.activeStep;

  /**
   * Update values object upon user input change.
   * @param {string} id: name of parameter to update
   * @param {*} value: new value for parameter
   */
  const handleChange = (id, value) => {
    setValues({ ...values, [id]: value });
  };

  /**
   * Fetches images and flavors, and presets param values to first results.
   */
  const fetchImagesAndFlavors = async () => {
    setLoadingImages(true);
    try {
      let [images, flavors] = await Promise.all([
        api.providerImages(props.cloudId, {
          // pass credential to filter images by allocation
          credential: props.deploymentValues.credentialId,
          region: props.deploymentValues.regionId,
        }),
        api.providerFlavors(props.cloudId, {
          credential: props.deploymentValues.credentialId,
          region: props.deploymentValues.regionId,
        }), //(workspace.default_provider_id),
      ]);

      images = images.sort(js2SortByImageByName);
      setImages(images);
      flavors = flavors.sort(sortBy("id"));
      setFlavors(flavors);

      setValues({
        ...values,
        image_name: images[0].name,
        flavor: flavors[0].name,
      });

      setLoadingImages(false);
    } catch (e) {
      console.log(e); //TODO
      setLoadingImages(false);
    }
  };

  /**
   * Prepare deployment params to be submitted to createDeployment API endpoint &
   * prepare run params to be submitted to run createRun API endpoint.
   * @param {obj} deploymentValues
   * @param {obj} values
   * @returns list of {key,value} parameters
   */
  const setupParams = (deploymentValues, values) => {
    console.log("Deployment Values:", deploymentValues);
    console.log("Run Values:", values);

    // setup deployment params
    let deploymentParams = {
      owner: user.username,
      name: values["instance_name"],
      workspace_id: deploymentValues["workspaceId"],
      template_id: deploymentValues["templateId"],
      primary_provider_id: deploymentValues["cloudId"], //provider.id,
      cloud_credentials: [deploymentValues["credentialId"]], //[ credential.id ],
    };

    // setup run params
    let runParams = [
      { key: "flavor", value: values["flavor"] },
      { key: "image_name", value: values["image_name"] },
      { key: "instance_count", value: values["instance_count"].toString() },
      { key: "instance_name", value: values["instance_name"] },
      {
        key: "k3s_traefik_disable",
        value: values["k3s_traefik_disable"] ? "true" : "false",
      },
      { key: "username", value: user.username },
      { key: "project", value: deploymentValues["projectId"] },
      {
        key: "gpu_enable",
        value: values["flavor"].startsWith("g") ? "true" : "false",
      },
      { key: "region", value: deploymentValues["regionId"] },
    ];
    BootDiskSetRunParams(runParams, values);

    return [deploymentParams, runParams];
  };

  // When region is updated, prefetch the images/flavors for associated region
  useEffect(() => {
    if (props.deploymentValues["regionId"]) {
      setTimeout(fetchImagesAndFlavors, 0);
    }
  }, [props.deploymentValues["regionId"]]);

  // validate input for current step to disable/enable next button
  useEffect(() => {
    if (activeStep > -1) {
      const validator = steps[activeStep].validator;
      setValid((!validator || validator(values)) && !props.error);
    }
  }, [props.activeStep, values]);

  // wizard steps
  const steps = [
    {
      title: "Region",
      render: () => (
        <SelectRegion
          regions={props.regions}
          selectedId={props.deploymentValues["regionId"]}
          setRegionId={props.setRegionId}
          loadingRegions={props.loadingRegions}
        />
      ),
    },
    {
      title: "Parameters",
      validator: (values) =>
        !!values["instance_name"] &&
        !!values["image_name"] &&
        !!values["flavor"] &&
        !ValidateDeploymentName(values["instance_name"]) &&
        BootDiskValidate(values),
      render: (values) => (
        <K3sParameters
          values={values}
          onChange={handleChange}
          nameError={nameError}
          setNameError={setNameError}
          images={images}
          flavors={flavors}
          loadingImages={loadingImages}
        />
      ),
    },
    {
      title: "Review & Deploy",
      render: (values) => (
        <K3sReview
          values={values}
          deploymentValues={props.deploymentValues}
          flavors={flavors}
          images={images}
          template={props.template}
        />
      ),
    },
  ].filter((step) => step);

  return (
    <>
      <DialogContent style={{ minHeight: "40vh" }}>
        <FormStepper
          activeStep={activeStep}
          steps={steps.map((s) => s.title)}
        />
        <Box mt={6} mb={2}>
          {steps[activeStep].render(values)}
          {props.error && (
            <Box mt={2} display="flex" justifyContent="center">
              <Alert severity="error">{props.error}</Alert>
            </Box>
          )}
        </Box>
      </DialogContent>

      <TemplateStepControls
        handlePrev={props.handlePrev}
        isValid={isValid}
        steps={steps}
        activeStep={activeStep}
        nextHandler={props.handleNext.bind(null, values)}
        backHandler={props.handleBack.bind(null, values)}
        submitHandler={() => {
          const [deploymentParams, runParams] = setupParams(
            props.deploymentValues,
            values
          );
          props.submitHandler(deploymentParams, runParams);
        }}
        isSubmitting={isSubmitting}
      />
    </>
  );
};

export default CreateK3sWizard;
