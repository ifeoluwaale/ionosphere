import React from "react";
import { useState, useEffect } from "react";
import { Box, DialogContent, Grid } from "@mui/material";

import { FormStepper, FormControls } from "../../forms/FormStepper";

import SelectRegion from "../Steps/SelectRegion";
import K3sParameters from "./K3sParameters";
import K3sReview from "./K3sReview";

import { useAPI, useUser } from "../../../contexts";
import { js2SortByImageByName, sortBy } from "../../../utils";
import { Alert } from "@mui/material";
import {
  BootDiskAddParams,
  BootDiskSetInitialValues,
  BootDiskSetRunParams,
} from "../Fields/BootDisk";

import { ValidateDeploymentName } from "../../../utils";

/**
 * The EditK3sWizard component controls the edit functionality unique to the
 * k3s-single-image template, while logic shared across all wizards is located in
 * EditWizardController or EditDeploymentDialog.
 */
const EditK3sWizard = (props) => {
  const api = useAPI();
  const [user] = useUser();

  /**
   * single-image-k3s params with type, editable specified
   * - type is used to convert param to value to be used in UI fields
   * - editable is used to enable/disable fields in edit wizard
   *
   * TODO: could instead pull from template metadata in the future when
   * 'editable' field is added
   */
  const paramList = [
    { name: "instance_name", type: "string", editable: true },
    { name: "power_state", type: "string", editable: true },
    { name: "instance_count", type: "integer", editable: true },
    { name: "k3s_traefik_disable", type: "bool", editable: true },
    { name: "flavor", type: "string", editable: false },
    { name: "flavor_master", type: "string", editable: false },
    { name: "image_name", type: "string", editable: false },
  ];
  BootDiskAddParams(paramList);

  // obj for storing run parameter values - populate current params from deployment
  const [values, setValues] = useState(props.getInitialValues(paramList));
  BootDiskSetInitialValues(values);

  // whether user input is valid for current step
  const [isValid, setValid] = useState(false);

  // if invalid deployment name input, the error message (null if name is valid)
  const [nameError, setNameError] = useState(null);

  const [flavors, setFlavors] = useState();
  const [images, setImages] = useState();
  const [loadingImages, setLoadingImages] = useState(false);

  // list of params that have been updated by the user
  const [changedParams, setChangedParams] = useState([]);

  let activeStep = props.activeStep;

  /**
   * Returns whether given param is editable, pulled from paramList.
   * TODO: Update to pull from template metadata when 'editable' field is added.
   * @param {string} paramName
   */
  const isParamEditable = (paramName) => {
    return paramList.find((p) => p.name === paramName).editable;
  };

  /**
   * Update values object upon user input change.
   * @param {string} id: name of parameter to update
   * @param {*} value: new value for parameter
   */
  const handleChange = (id, value) => {
    setValues({ ...values, [id]: value });
  };

  /**
   * Fetches images and flavors, and sorts by RAM for display.
   */
  const fetchImagesAndFlavors = async () => {
    setLoadingImages(true);
    try {
      let [images, flavors] = await Promise.all([
        api.providerImages(props.cloudId, {
          // pass credential to filter images by allocation
          credential: props.deploymentValues.credentialId,
          region: props.deploymentValues.regionId,
        }),
        api.providerFlavors(props.cloudId, {
          credential: props.deploymentValues.credentialId,
          region: props.deploymentValues.regionId,
        }), //(workspace.default_provider_id),
      ]);

      images = images.sort(js2SortByImageByName);
      setImages(images);
      flavors = flavors.sort(sortBy("id"));
      setFlavors(flavors);
      setLoadingImages(false);
    } catch (e) {
      console.log(e); //TODO
      setLoadingImages(false);
    }
  };

  /**
   * Prepare deployment params to be submitted to updateDeployment API endpoint &
   * prepare run params to be submitted to run createRun API endpoint.
   * @param {obj} deploymentValues
   * @param {obj} values
   * @returns list of {key,value} parameters
   */
  const setupParams = (deploymentValues, values) => {
    // setup deployment params
    let deploymentParams = {};

    // update deployment only if name has changed
    if (props.deployment.name !== values["instance_name"]) {
      deploymentParams.name = values["instance_name"];
    }

    // setup run params
    let runParams = [
      { key: "flavor", value: values["flavor"] },
      { key: "image_name", value: values["image_name"] },
      { key: "instance_count", value: values["instance_count"].toString() },
      { key: "instance_name", value: values["instance_name"] },
      { key: "power_state", value: values["power_state"] },
      {
        key: "k3s_traefik_disable",
        value: values["k3s_traefik_disable"] ? "true" : "false",
      },
      { key: "username", value: user.username },
      { key: "project", value: deploymentValues["projectId"] },
      {
        key: "gpu_enable",
        value: values["flavor"].startsWith("g") ? "true" : "false",
      },
      { key: "region", value: deploymentValues["regionId"] },
    ];
    BootDiskSetRunParams(runParams, values);

    return [deploymentParams, runParams];
  };

  // When region is updated, prefetch the images/flavors for associated region
  useEffect(() => {
    if (props.deploymentValues["regionId"]) {
      setTimeout(fetchImagesAndFlavors, 0);
    }
  }, [props.deploymentValues["regionId"]]);

  useEffect(() => {
    // validate input for current step to disable/enable next button
    if (activeStep > -1) {
      const validator = steps[activeStep].validator;
      setValid((!validator || validator(values)) && !props.error);
    }

    // if on the last step, check for changed params to display on review step
    if (activeStep === steps.length - 1) {
      const [deploymentParams, runParams] = setupParams(
        props.deploymentValues,
        values
      );
      setChangedParams(props.checkParamChanges(runParams));
    }
  }, [props.activeStep, values]);

  // wizard steps
  const steps = [
    {
      title: "Region",
      render: () => (
        <SelectRegion
          regions={props.regions}
          selectedId={props.deploymentValues["regionId"]}
          setRegionId={props.setRegionId}
          loadingRegions={props.loadingRegions}
          isEditing={true}
          isParamEditable={isParamEditable}
        />
      ),
    },
    {
      title: "Parameters",
      validator: (values) =>
        !!values["instance_name"] &&
        !!values["image_name"] &&
        !!values["flavor"] &&
        !ValidateDeploymentName(values["instance_name"]),
      render: (values) => (
        <K3sParameters
          values={values}
          onChange={handleChange}
          nameError={nameError}
          setNameError={setNameError}
          images={images}
          flavors={flavors}
          loadingImages={loadingImages}
          isEditing={true}
          isParamEditable={isParamEditable}
        />
      ),
    },
    {
      title: "Review & Deploy",
      render: (values) => (
        <K3sReview
          deploymentValues={props.deploymentValues}
          values={values}
          flavors={flavors}
          images={images}
          template={props.template}
          isEditing={true}
          changedParams={changedParams}
        />
      ),
      // disable submit if no params have been changed
      disableNext: changedParams.length === 0,
    },
  ].filter((step) => step);

  return (
    <>
      <DialogContent style={{ minHeight: "40vh" }}>
        <FormStepper
          activeStep={activeStep}
          steps={steps.map((s) => s.title)}
        />
        <Box mt={6} mb={2}>
          {steps[activeStep].render(values)}
          {props.error && (
            <Box mt={2} display="flex" justifyContent="center">
              <Alert severity="error">{props.error}</Alert>
            </Box>
          )}
        </Box>
      </DialogContent>
      <Box>
        <Grid container>
          <Grid item xs={4}></Grid>
          <Grid item xs={4}>
            <Box display="flex" justifyContent="center" mt={2} mb={3}>
              <FormControls
                disabled={!isValid}
                step={steps[activeStep]}
                activeStep={activeStep}
                numSteps={steps.length}
                nextHandler={props.handleNext.bind(null, values)}
                backHandler={props.handleBack.bind(null, values)}
                closeHandler={props.handleClose}
                submitHandler={() => {
                  const [deploymentParams, runParams] = setupParams(
                    props.deploymentValues,
                    values
                  );
                  props.submitHandler(
                    props.deployment.id,
                    deploymentParams,
                    runParams
                  );
                }}
              />
            </Box>
          </Grid>
          <Grid item xs={4}></Grid>
        </Grid>
      </Box>
    </>
  );
};

export default EditK3sWizard;
