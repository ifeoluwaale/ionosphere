import { Box, Grid, CircularProgress } from "@mui/material";
import { filterFlavorsByMinValues } from "../../../utils";
import AdvancedSettings from "../Fields/AdvancedSettings";
import Checkbox from "../Fields/Checkbox";
import BootDisk from "../Fields/BootDisk";
import Flavor from "../Fields/Flavor";
import InstanceCount from "../Fields/InstanceCount";
import InstanceName from "../Fields/InstanceName";
import Image from "../Fields/Image";
import PowerState from "../Fields/PowerState";

const K3sParameters = ({
  values,
  onChange,
  nameError,
  setNameError,
  images,
  flavors,
  loadingImages,
  isEditing,
  isParamEditable,
}) => {
  const powerStateOptions = ["active", "shutoff", "shelved_offloaded"];

  flavors = filterFlavorsByMinValues(
    flavors,
    images?.find((image) => image.name === values["image_name"])?.min_disk,
    images?.find((image) => image.name === values["image_name"])?.min_ram
  );
  const wizard = {
    flavors: flavors,
    images: images,
    isEditing: isEditing,
    isParamEditable: isParamEditable,
    nameError: nameError,
    onChange: onChange,
    setNameError: setNameError,
    values: values,
  };

  return (
    <>
      {loadingImages && (
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <CircularProgress />
        </Box>
      )}

      {!loadingImages && (
        <Box>
          <Grid
            container
            justifyContent="center"
            alignItems="flex-start"
            spacing={2}
            mb={2}
          >
            <Grid
              item
              xs={12}
              sm={wizard.isEditing ? 4 : 10}
              md={wizard.isEditing ? 3 : 6}
            >
              <InstanceName wizard={wizard} />
            </Grid>
            {wizard.isEditing && (
              <Grid item xs={12} sm={4} md={3}>
                <PowerState wizard={wizard} />
              </Grid>
            )}
          </Grid>

          <Grid
            container
            justifyContent="center"
            alignItems="flex-start"
            spacing={2}
            mb={2}
          >
            <Grid item xs={10} md={6}>
              <Image disclaimer={true} wizard={wizard} />
            </Grid>
          </Grid>

          <Grid
            container
            justifyContent="center"
            alignItems="flex-start"
            spacing={2}
            mb={2}
          >
            <Grid item xs={12} sm={4} md={2}>
              <InstanceCount wizard={wizard} />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Flavor default_value={"m3.medium"} wizard={wizard} />
            </Grid>
          </Grid>
          <Grid
            container
            justifyContent="center"
            alignItems="flex-start"
            spacing={2}
            mb={2}
          >
            <Grid item xs={12} sm={10} md={6}>
              <AdvancedSettings>
                <Box mb={1}>
                  <Checkbox
                    label="Disable Traefik Ingress?"
                    name="k3s_traefik_disable"
                    wizard={wizard}
                  />
                </Box>
                <BootDisk wizard={wizard} />
              </AdvancedSettings>
            </Grid>
          </Grid>
        </Box>
      )}
    </>
  );
};

export default K3sParameters;
