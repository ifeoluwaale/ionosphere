import React from "react";
import { useState, useEffect } from "react";
import {
  Box,
  CircularProgress,
  DialogContent,
  Grid,
  Stack,
} from "@mui/material";

import { FormStepper, FormControls } from "../../forms/FormStepper";

import {
  FetchImagesAndFlavors,
  GetWizard,
  SetupParams,
  Validate,
} from "./common";
import RenderUIItems from "./RenderUIItems";
import MetadataReview from "./MetadataReview";

import { useAPI, useUser } from "../../../contexts";
import { decodeBase64ArrayString } from "../../../utils";
import { Alert } from "@mui/material";
import { BootDiskSetInitialValues } from "../Fields/BootDisk";
/**
 * The EditMetadataWizard component controls the edit functionality of any unknown (i.e. not custom)
 * templates, while logic shared across all wizards is located in
 * EditWizardController or EditDeploymentDialog.
 */
const EditMetadataWizard = (props) => {
  const api = useAPI();
  const [user] = useUser();

  // obj for storing run parameter values - populate current params from deployment
  let initialValues = {};
  props.template.metadata.parameters.forEach((p) => {
    let paramValue = props.getDeploymentParam(p.name);
    if (p.type === "bool") {
      initialValues[p.name] = paramValue === "true";
    } else if (p.type === "array_string") {
      initialValues[p.name] = paramValue.split(",");
    } else if (p.type === "array_string_base64") {
      let prefix = p.name.split("_base64")[0];
      initialValues[prefix] = decodeBase64ArrayString(paramValue);
    } else if (p.base64) {
      initialValues[p.name] = Buffer.from(paramValue, "base64").toString();
    } else {
      initialValues[p.name] = paramValue;
    }
  });

  const [values, setValues] = useState(initialValues);
  BootDiskSetInitialValues(values);

  // whether user input is valid for current step
  const [isValid, setValid] = useState(false);

  // if invalid deployment name input, the error message (null if name is valid)
  const [nameError, setNameError] = useState(null);

  const [flavors, setFlavors] = useState();
  const [images, setImages] = useState();
  const [loadingImages, setLoadingImages] = useState(false);

  // list of params that have been updated by the user
  const [changedParams, setChangedParams] = useState([]);

  let activeStep = props.activeStep;

  /**
   * Gets `editable` value for param from template metadata.
   *
   * If `editable` is false or not present, the field is disabled for editing.
   * @param {string} paramName
   * @returns {boolean}
   */
  const isParamEditable = (paramName) => {
    const param = props.template.metadata.parameters.find(
      (p) => p.name === paramName
    );
    return param?.editable ? true : false;
  };

  useEffect(() => {
    // When region is updated, prefetch the images/flavors for associated region
    if (props.deploymentValues["regionId"]) {
      setTimeout(function () {
        FetchImagesAndFlavors(
          props,
          api,
          values,
          setFlavors,
          setImages,
          setLoadingImages
        );
      }, 0);
    }
  }, [props.deploymentValues["regionId"]]);

  useEffect(() => {
    // validate input for current step to disable/enable next button
    if (activeStep > -1) {
      const validator = steps[activeStep].validator;
      setValid((!validator || validator(values)) && !props.error);
    }

    // if on the last step, check for changed params to display on review step
    if (activeStep === steps.length - 1) {
      const [deploymentParams, runParams] = SetupParams(user, props.deployment);
      setChangedParams(props.checkParamChanges(runParams));
    }
  }, [props.activeStep, values]);

  const wizard = GetWizard(
    props,
    images,
    flavors,
    loadingImages,
    nameError,
    setNameError,
    setValues,
    values
  );
  wizard.isEditing = true;
  wizard.isParamEditable = isParamEditable;

  const steps = wizard.template.ui_metadata.steps.slice();
  steps[0].validator = (values) => Validate(steps[0], values);
  steps.push({
    title: "Review & Update",
    render: (values) => (
      <MetadataReview
        changedParams={changedParams}
        deploymentValues={props.deploymentValues}
        wizard={wizard}
      />
    ),
    // disable submit if no params have been changed
    disableNext: changedParams.length === 0,
  });
  return (
    <>
      <DialogContent style={{ minHeight: "40vh" }}>
        <FormStepper
          activeStep={activeStep}
          steps={steps.map((s) => s.title)}
        />
        {steps[activeStep].render && steps[activeStep].render(values)}
        {!steps[activeStep].render && (
          <Stack marginX={10} mt={6} spacing={2}>
            {(wizard.loadingImages || wizard.loadingRegions) && (
              <Box sx={{ display: "flex", justifyContent: "center" }}>
                <CircularProgress />
              </Box>
            )}
            {!(wizard.loadingImages || wizard.loadingRegions) && (
              <RenderUIItems item={steps[activeStep]} wizard={wizard} />
            )}
            {props.error && (
              <Box mt={2} display="flex" justifyContent="center">
                <Alert severity="error">{props.error}</Alert>
              </Box>
            )}
          </Stack>
        )}
      </DialogContent>
      <Box>
        <Grid container>
          <Grid item xs={4}></Grid>
          <Grid item xs={4}>
            <Box display="flex" justifyContent="center" mt={2} mb={3}>
              <FormControls
                disabled={!isValid}
                step={steps[activeStep]}
                activeStep={activeStep}
                numSteps={steps.length}
                nextHandler={props.handleNext.bind(null, values)}
                backHandler={props.handleBack.bind(null, values)}
                closeHandler={props.handleClose}
                submitHandler={() => {
                  const [deploymentParams, runParams] = SetupParams(
                    user,
                    props.deployment
                  );
                  props.submitHandler(
                    props.deployment.id,
                    deploymentParams,
                    runParams
                  );
                }}
              />
            </Box>
          </Grid>
          <Grid item xs={4}></Grid>
        </Grid>
      </Box>
    </>
  );
};

export default EditMetadataWizard;
