import { Box, Grid } from "@mui/material";
import AdvancedSettings from "../Fields/AdvancedSettings";
import BootDisk from "../Fields/BootDisk";
import Flavor from "../Fields/Flavor";
import InstanceCount from "../Fields/InstanceCount";
import InstanceName from "../Fields/InstanceName";
import Image from "../Fields/Image";
import ImageID from "../Fields/ImageID";
import Number from "../Fields/Number";
import PowerState from "../Fields/PowerState";
import RadioButtons from "../Fields/RadioButtons";
import Region from "../Fields/Region";
import SelectField from "../Fields/SelectField";
import Text from "../Fields/Text";
import ToggleSwitch from "@/components/DeploymentWizard/Fields/ToggleSwitch";

const RenderUIItem = ({ item, wizard }) => {
  if (item.type === "advanced_settings")
    return (
      <AdvancedSettings item={item} wizard={wizard}>
        <RenderUIItems item={item} wizard={wizard} />
      </AdvancedSettings>
    );
  else if (item.type === "row")
    return (
      <Grid container spacing={2}>
        {item.items.map((i) => (
          <Grid item xs={12 / item.items.length} key={i.name}>
            <RenderUIItem item={i} wizard={wizard} />
          </Grid>
        ))}
      </Grid>
    );
  // remove name check in next line after all templates have been updated
  if (
    item.field_type === "cacao_root_storage" ||
    item.name === "root_storage"
  ) {
    return <BootDisk wizard={wizard} />;
  }
  const p = wizard.get_param(item);

  switch (p.type) {
    case "cacao_instance_count":
      return <InstanceCount item={item} wizard={wizard} />;
    case "cacao_instance_name":
      return <InstanceName item={item} wizard={wizard} />;
    case "cacao_power_state":
      return <PowerState item={item} wizard={wizard} />;
    case "cacao_provider_image":
      return <ImageID item={item} wizard={wizard} />;
    case "cacao_provider_image_name":
      return <Image item={item} wizard={wizard} />;
    case "cacao_provider_flavor":
      return <Flavor item={item} wizard={wizard} />;
    case "cacao_provider_region":
      return <Region item={item} wizard={wizard} />;
    default:
      break;
  }
  // remove this switch after all templates have been updated to use corresponding types instead of names
  switch (p.name) {
    case "instance_count":
      return <InstanceCount item={item} wizard={wizard} />;
    case "instance_name":
      return <InstanceName item={item} wizard={wizard} />;
    case "power_state":
      return <PowerState item={item} wizard={wizard} />;
    default:
      break;
  }
  if (p.type === "string") {
    return item.field_type === "radio" ? (
      <RadioButtons item={item} wizard={wizard} />
    ) : item.field_type === "select" ? (
      <SelectField item={item} wizard={wizard} />
    ) : (
      <Text item={item} wizard={wizard} />
    );
  }
  if (p.type === "bool") {
    return <ToggleSwitch item={item} wizard={wizard} />;
  }
  if (p.type === "float" || p.type === "integer" || p.type === "number") {
    return <Number item={item} wizard={wizard} />;
  }
  // fall back to Text field
  return <Text item={item} wizard={wizard} />;
};

const RenderUIItems = ({ item, wizard }) => {
  let components = [];
  item.items.forEach((i) => {
    if (i.name !== "power_state" || wizard.isEditing)
      components.push(
        <Box key={i.name || i.type}>
          {<RenderUIItem item={i} wizard={wizard} />}
        </Box>
      );
  });
  return components;
};

export default RenderUIItems;
