import React from "react";
import { useState, useEffect } from "react";
import {
  Alert,
  Box,
  Button,
  CircularProgress,
  DialogContent,
  Stack,
} from "@mui/material";

import { FormStepper, FormControls } from "../../forms/FormStepper";

import {
  FetchImagesAndFlavors,
  GetWizard,
  SetupParams,
  Validate,
} from "./common";
import RenderUIItems from "./RenderUIItems";
import MetadataReview from "./MetadataReview";

import { useAPI, useUser } from "../../../contexts";

import { BootDiskSetInitialValues } from "../Fields/BootDisk";
import { useDeploymentWizard } from "../contexts/DeploymentWizardContext";
import TemplateStepControls from "../Steps/TemplateStepControls";

/**
 * The CreateMetadataWizard creates a wizard based on the template parameters. logic shared across all wizards is located in
 * CreateWizardController or CreateDeploymentDialog.
 */
const CreateMetadataWizard = (props) => {
  const api = useAPI();
  const [user] = useUser();
  const { isSubmitting } = useDeploymentWizard();

  const initialValues = {};
  props.template.metadata.parameters.forEach((p) => {
    if (p.type === "integer" || p.type === "float" || p.type === "number") {
      initialValues[p.name] = p.default.toString();
    } else {
      initialValues[p.name] = p.default;
    }
  });
  BootDiskSetInitialValues(initialValues);

  // obj for storing run parameter values
  const [values, setValues] = useState(initialValues);

  // whether user input is valid for current step
  const [isValid, setValid] = useState(false);

  // if invalid deployment name input, the error message (null if name is valid)
  const [nameError, setNameError] = useState(null);

  const [flavors, setFlavors] = useState();
  const [images, setImages] = useState();
  const [loadingImages, setLoadingImages] = useState(true);

  let activeStep = props.activeStep;

  // when region is updated, prefetch the images/flavors for associated region
  useEffect(() => {
    if (props.deploymentValues["regionId"]) {
      setTimeout(function () {
        FetchImagesAndFlavors(
          props,
          api,
          values,
          setFlavors,
          setImages,
          setLoadingImages,
          setValues
        );
      }, 0);
    }
  }, [props.deploymentValues["regionId"]]);

  // validate input for current step to disable/enable next button
  useEffect(() => {
    if (activeStep > -1) {
      const validator = steps[activeStep].validator;
      setValid((!validator || validator(values)) && !props.error);
    }
  }, [props.activeStep, values]);

  const wizard = GetWizard(
    props,
    images,
    flavors,
    loadingImages,
    nameError,
    setNameError,
    setValues,
    values
  );

  const steps = wizard.template.ui_metadata.steps.slice();
  steps[0].validator = (values) => Validate(steps[0], values);
  steps.push({
    title: "Review & Deploy",
    render: (values) => (
      <MetadataReview
        deploymentValues={props.deploymentValues}
        wizard={wizard}
      />
    ),
  });

  return (
    <>
      <DialogContent style={{ minHeight: "40vh" }}>
        <FormStepper
          activeStep={activeStep}
          steps={steps.map((s) => s.title)}
        />
        {steps[activeStep].render && steps[activeStep].render(values)}
        {!steps[activeStep].render && (
          <Stack marginX={10} mt={6} spacing={2}>
            {(wizard.loadingImages || wizard.loadingRegions) && (
              <Box sx={{ display: "flex", justifyContent: "center" }}>
                <CircularProgress />
              </Box>
            )}
            {!(wizard.loadingImages || wizard.loadingRegions) && (
              <RenderUIItems item={steps[activeStep]} wizard={wizard} />
            )}
            {props.error && (
              <Box mt={2} display="flex" justifyContent="center">
                <Alert severity="error">{props.error}</Alert>
              </Box>
            )}
          </Stack>
        )}
      </DialogContent>

      <TemplateStepControls
        handlePrev={props.handlePrev}
        isValid={isValid}
        steps={steps}
        activeStep={activeStep}
        nextHandler={props.handleNext.bind(null, values)}
        backHandler={props.handleBack.bind(null, values)}
        submitHandler={() => {
          const [deploymentParams, runParams] = SetupParams(user);
          props.submitHandler(deploymentParams, runParams);
        }}
        isSubmitting={isSubmitting}
      />
    </>
  );
};

export default CreateMetadataWizard;
