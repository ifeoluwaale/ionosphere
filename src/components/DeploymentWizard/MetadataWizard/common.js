import { BootDiskValidate } from "../Fields/BootDisk";
import {
  filterFlavorsByMinValues,
  js2SortByImageByName,
  sortBy,
  sortByName,
  ValidateDeploymentName,
} from "../../../utils";

import { BootDiskSetRunParams } from "../Fields/BootDisk";

let wizard = null;

/**
 * Fetches images and flavors, and presets param values to first results.
 */
export async function FetchImagesAndFlavors(
  props,
  api,
  values,
  setFlavors,
  setImages,
  setLoadingImages,
  setValues
) {
  setLoadingImages(true);
  try {
    let [images, flavors] = await Promise.all([
      api.providerImages(props.cloudId, {
        // pass credential to filter images by allocation
        credential: props.deploymentValues.credentialId,
        region: props.deploymentValues.regionId,
      }),
      api.providerFlavors(props.cloudId, {
        credential: props.deploymentValues.credentialId,
        region: props.deploymentValues.regionId,
      }), //(workspace.default_provider_id),
    ]);

    images = images.sort(js2SortByImageByName);
    setImages(images);
    flavors = flavors.sort(sortBy("id"));
    flavors = filterFlavorsByMinValues(
      flavors,
      images?.find((image) => image.name === values["image_name"])?.min_disk,
      images?.find((image) => image.name === values["image_name"])?.min_ram
    );
    setFlavors(flavors);

    if (setValues)
      setValues({
        ...values,
        image_name: images[0].name,
        flavor: flavors[0].name,
      });

    setLoadingImages(false);
  } catch (e) {
    console.log(e); //TODO
    setLoadingImages(false);
  }
}

const get_item = (param, item) => {
  if (item.name === param.name) return item;
  if (item.items)
    for (let j = 0; j < item.items.length; j++) {
      let i = get_item(param, item.items[j]);
      if (i) return i;
    }
  return null;
};

export function GetWizard(
  props,
  images,
  flavors,
  loadingImages,
  nameError,
  setNameError,
  setValues,
  values
) {
  wizard = {
    deploymentValues: props.deploymentValues,
    flavors: flavors,
    images: images,
    loadingImages: loadingImages,
    loadingRegions: props.loadingRegions,
    nameError: nameError,
    onChange: (id, value) => {
      setValues({ ...values, [id]: value });
    },
    regions: props.regions,
    setNameError: setNameError,
    setRegionId: props.setRegionId,
    template: props.template,
    values: values,
    get_item: (param) => {
      let steps = props.template.ui_metadata.steps;
      for (let i = 0; i < steps.length; i++) {
        let item = get_item(param, steps[i]);
        if (item) return item;
      }
      return null;
    },
    get_item_parameter: (item, key) => {
      if (item.parameters)
        for (let i = 0; i < item.parameters.length; i++)
          if (item.parameters[i].key === key) return item.parameters[i];
      return null;
    },
    get_param: (item) =>
      props.template.metadata.parameters.find((p) => p.name === item.name),
  };
  return wizard;
}

// deployment only needed when isEditing
export function SetupParams(user, deployment) {
  let deploymentParams = wizard.isEditing
    ? {}
    : {
        owner: user.username,
        name: wizard.values["instance_name"],
        workspace_id: wizard.deploymentValues["workspaceId"],
        template_id: wizard.deploymentValues["templateId"],
        primary_provider_id: wizard.deploymentValues["cloudId"], //provider.id,
        cloud_credentials: [wizard.deploymentValues["credentialId"]], //[ credential.id ],
      };
  if (wizard.isEditing && deployment.name !== wizard.values["instance_name"]) {
    deploymentParams.name = wizard.values["instance_name"];
  }

  let runParams = [
    { key: "username", value: user.username },
    { key: "project", value: wizard.deploymentValues["projectId"] },
    { key: "region", value: wizard.deploymentValues["regionId"] },
    { key: "power_state", value: wizard.values["power_state"] },
  ];
  BootDiskSetRunParams(runParams, wizard.values);
  wizard.template.metadata.parameters.forEach((p) => {
    if (
      !p.ignore &&
      !runParams.find((x) => x.key === p.name) &&
      wizard.values[p.name]
    ) {
      let value = wizard.values[p.name].toString();
      if (p.base64) value = Buffer.from(value).toString("base64");
      runParams.push({ key: p.name, value: value });
    }
  });

  return [deploymentParams, runParams];
}

export function Validate(item, values) {
  for (const i of item.items) {
    if (i.type === "advanced_settings" || i.type === "row")
      if (!Validate(i, values)) return false;
      else continue;
    // remove name check in next line after templates have been updated
    if (i.field_type === "cacao_root_storage" || i.name === "root_storage")
      if (!BootDiskValidate(values)) return false;
      else continue;
    const p = wizard.get_param(i);
    // remove name check in next line after templates have been updated
    if (p.type === "cacao_instance_name" || p.name === "instance_name")
      if (!values[p.name] || ValidateDeploymentName(values[p.name]))
        return false;
      else continue;
    if (p.required && !values[p.name]) return false;
  }
  return true;
}
