import React from "react";

import CreateDADIWizard from "./DADI/CreateDADIWizard";
import CreateMetadataWizard from "./MetadataWizard/CreateMetadataWizard";
import CreateJupyterhubWizard from "./jupyterhub/CreateJupyterhubWizard";
import CreateK3sWizard from "./k3s/CreateK3sWizard";
import CreateVMs4WorkshopWizard from "./vms4workshop/CreateVMs4WorkshopWizard";
import { Alert, Box, DialogContent } from "@mui/material";
import { useDeploymentWizard } from "./contexts/DeploymentWizardContext";

/**
 * The WizardController contains functions common to all deployment wizards
 * and displays the correct wizard depending on which template was selected
 * by the user.
 */
const WizardController = (props) => {
  const deploymentWizard = useDeploymentWizard();
  const { template } = deploymentWizard;

  // -------------- functions used across all wizards --------------- //

  const handleNext = async () => {
    props.setActiveStep((prevStep) => prevStep + 1);
  };

  const handleBack = () => {
    props.setActiveStep((prevStep) => prevStep - 1);
  };

  const wizards = {
    DADI: CreateDADIWizard,
    jupyterhub: CreateJupyterhubWizard,
    "single-image-k3s": CreateK3sWizard,
    vms4workshop: CreateVMs4WorkshopWizard,
  };

  // render correct wizard based on selected template
  const CreateWizard =
    wizards[template.name] ||
    (hasUIMetadata(template) ? CreateMetadataWizard : MissingUIMetadataWizard);

  // TODO: refactor all create wizards to use DeploymentWizardContext instead of props
  return (
    <CreateWizard
      activeStep={props.activeStep}
      handlePrev={props.handlePrev}
      cloudId={deploymentWizard.cloudId}
      deploymentValues={{
        workspaceId: deploymentWizard.workspaceId,
        templateId: deploymentWizard.template.id,
        credentialId: deploymentWizard.credentialId,
        cloudId: deploymentWizard.cloudId,
        projectId: deploymentWizard.projectId,
        regionId: props.regionId,
      }}
      handleBack={handleBack}
      handleNext={handleNext}
      regions={deploymentWizard.regions}
      loadingRegions={deploymentWizard.loadingRegions}
      setActiveStep={props.setActiveStep}
      setRegionId={props.setRegionId}
      submitHandler={props.submitHandler}
      template={template}
    />
  );
};

/**
 * Check if template has UI metadata to be launched from UI with metadata wizard.
 * @param {Object} template
 * @returns {boolean}
 */
const hasUIMetadata = (template) => {
  return Boolean(template?.ui_metadata?.steps);
};

/**
 * Static Wizard for error message.
 * Not all template will have UI metadata, so these template are not launch-able from UI.
 *
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
const MissingUIMetadataWizard = (props) => {
  return (
    <DialogContent style={{ minHeight: "40vh" }}>
      <Box mt={2} display="flex" justifyContent="center">
        <Alert severity="error">
          Template is missing UI metadata, this template cannot be launched from
          UI.
          <br />
          Contact template author to add UI metadata, or launch deployment from
          CLI.
        </Alert>
      </Box>
    </DialogContent>
  );
};

export default WizardController;
