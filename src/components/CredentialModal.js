import React from "react";
import { Dialog, DialogContent, Button, Typography, Box } from "@mui/material";
import Link from "next/link";
import SvgMissingCloud from "./icons/missingCloud";

export default function CredentialModal(props) {
  return (
    <Dialog
      open={props.open}
      onClose={props.handleClose}
      aria-labelledby="credential-modal-title"
      aria-describedby="credential-modal-description"
    >
      <DialogContent dividers>
        <Box sx={{ borderRadius: 5, padding: "2em" }}>
          <Box
            justifyContent="center"
            p={1}
            style={{ maxWidth: "20rem", margin: "0 auto" }}
          >
            <SvgMissingCloud />
          </Box>
          <Typography
            align="center"
            variant="h4"
            component="h2"
            id="credential-modal-title"
            color="primary"
          >
            We need a Cloud to get started.
          </Typography>
          <Box justifyContent="center" m={2}>
            <Typography
              align="center"
              variant="h6"
              component="p"
              id="credential-modal-description"
            >
              Let's add one to your account now.
            </Typography>
          </Box>
          <Box display="flex" justifyContent="center" m={3}>
            <Link href="/credentials">
              <Button
                variant="contained"
                color="primary"
                onClick={props.navigateToWizard}
              >
                Manage Credentials
              </Button>
            </Link>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}
