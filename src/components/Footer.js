import React from "react";
import { Box, Grid, Typography } from "@mui/material";

export default function Footer() {
  return (
    <Grid
      container
      spacing={3}
      flexDirection={"row"}
      justifyContent={"center"}
      alignContent={"center"}
      alignItems={"center"}
    >
      <Grid item md={6}>
        <Box
          display={"flex"}
          flexDirection="column"
          justifyContent={"center"}
          alignItems={"center"}
        >
          <Box>
            <Typography
              variant="body2"
              textAlign="center"
              color="textSecondary"
              sx={{ marginLeft: "1rem" }}
            >
              {"Copyright © "}
              CACAO {new Date().getFullYear()}
              {"."}
            </Typography>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
}
