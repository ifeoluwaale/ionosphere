import { isAlphanumeric, isLowercase, isAlpha } from "validator";

export function datetimeString(timestamp) {
  return new Date(timestamp).toLocaleString([], { timeZoneName: "short" });
}

export function decodeBase64ArrayString(arrayStringBase64) {
  let arrayBase64 = [];

  if (arrayStringBase64.length) {
    arrayBase64 = arrayStringBase64.split(",");
  }

  let decodedKeys = [];
  arrayBase64.forEach((sshKey) => decodedKeys.push(atob(sshKey)));
  return decodedKeys;
}

export function filterFlavorsByMinValues(all_flavors, min_disk, min_ram) {
  let filtered_result = [];
  if (!all_flavors) {
    return filtered_result;
  }
  if (!min_disk && !min_ram) {
    // min_disk and min_ram may not load (=== undefined) when this is called.
    return all_flavors;
  }

  for (let flavor of all_flavors) {
    if (flavor.disk >= min_disk && flavor.ram >= min_ram) {
      filtered_result.push(flavor);
    }
  }

  return filtered_result;
}

export function sortBy(field) {
  return (a, b) => {
    return a[field] - b[field];
  };
}

export function sortByName(a, b) {
  return a.name.localeCompare(b.name);
}

/**
 * @param a.name {string}
 * @param b.name {string}
 * @returns {number}
 */
export function js2SortByImageByName(a, b) {
  const aFeatured = a.name.startsWith("Featured-");
  const bFeatured = b.name.startsWith("Featured-");
  if (aFeatured && !bFeatured) {
    return -1;
  } else if (!aFeatured && bFeatured) {
    return 1;
  }
  return a.name.localeCompare(b.name);
}

// export function sortByDate(a, b) {
//   return new Date(a.created_at) - new Date(b.created_at);
// }

export function sortByDateDesc(a, b) {
  return new Date(b.created_at) - new Date(a.created_at);
}

// export function sortFlavorsByName(a) {
//   let g3 = [];
//   let m3 = [];
//   let r3 = [];

//   for (let flavor of a) {
//     if (flavor.name.includes("g3")) {
//       g3.push(flavor);
//     }
//     if (flavor.name.includes("m3")) {
//       m3.push(flavor);
//     }
//     if (flavor.name.includes("r3")) {
//       r3.push(flavor);
//     }
//   }

//   g3.sort(sortBy("ram"));
//   m3.sort(sortBy("ram"));
//   r3.sort(sortBy("ram"));

//   return g3.concat(m3, r3);
// }

export function timeDiffString(start, end) {
  const diff = Math.abs(new Date(end) - new Date(start));
  const secDiff = Math.floor(diff / 1000);
  const minDiff = Math.floor(secDiff / 60);
  const hourDiff = Math.floor(minDiff / 60);
  const dayDiff = Math.floor(hourDiff / 24);

  if (dayDiff > 0) return `${dayDiff} day` + (dayDiff !== 1 ? "s" : "");
  if (hourDiff > 0) return `${hourDiff} hour` + (hourDiff !== 1 ? "s" : "");
  if (minDiff > 0) return `${minDiff} min`;
  if (secDiff > 0) return `${secDiff} sec`;

  return `${diff} ms`;
}

export function ValidateDeploymentName(name) {
  if (!isAlpha(name.charAt(0))) {
    return "Name must begin with a letter.";
  } else if (
    !isAlphanumeric(name, ["en-US"], { ignore: "-" }) ||
    !isLowercase(name)
  ) {
    return "Name must be all lowercase and contain only letters, numbers, and hyphens (-).";
  } else {
    return null;
  }
}

// return true if template is a prerequisite template
export function isPrerequisiteTemplate(template) {
  // TODO use a better way (not hard-code name) to check if template is a prerequisite.
  return (
    template.name !== "openstack-prerequisite" &&
    template.name !== "aws-prerequisite"
  );
}

/**
 * Find a matching credential for a given template.
 *
 * @param {string} templateType - The type of the template (e.g., 'openstack_terraform').
 * @param {Array} credentials - Array of credentials (e.g., [{ type: 'openstack', ... }]).
 * @return {Object|null} The matching credential object or null if no match is found.
 */
export function findCredentialForTemplate(templateType, credentials) {
  // Mapping of template types to credential types
  const typeMapping = {
    openstack_terraform: "openstack",
    aws_terraform: "aws",
  };

  // Extract the base type from the template type
  // (e.g., 'openstack_terraform' -> 'openstack')
  const baseType = templateType.split("_")[0];

  // Look up the credential type using the mapping
  const credentialType = typeMapping[templateType] || baseType;

  // Search for a credential with the matching type
  const matchingCredential = credentials.find(
    (cred) => cred.type === credentialType
  );

  return matchingCredential || null;
}

/**
 * Capitalize first letter of a string.
 * @param {string} string
 * @returns {string} Capitalized string.
 */
export function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function stringToColor(string) {
  let hash = 0;
  let i;

  /* eslint-disable no-bitwise */
  for (i = 0; i < string.length; i += 1) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }

  let color = "#";

  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    color += `00${value.toString(16)}`.slice(-2);
  }
  /* eslint-enable no-bitwise */

  return color;
}
