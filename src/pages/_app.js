import React from "react";
import Head from "next/head";
import getConfig from "next/config";
import * as Sentry from "@sentry/nextjs";
import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import {
  ConfigProvider,
  APIProvider,
  ErrorProvider,
  UserProvider,
  EventsProvider,
  CloudsProvider,
  ProjectsProvider,
  CredentialsProvider,
  WorkspacesProvider,
} from "../contexts";
import theme from "../theme";
import "../styles/global.css";

// import { CookiesProvider } from 'react-cookie' //FIXME do not use this module!  Caused problems with WelcomeBanner in User Portal
export default function MyApp(props) {
  const {
    sentryDSN,
    sentryRelease,
    sentryEnv,
    Component,
    pageProps,
    isDevelopment,
    user,
    clouds,
    projects,
    credentials,
    workspaces,
    baseUrl,
    token,
  } = props;
  const config = getConfig().publicRuntimeConfig;
  config.isDevelopment = isDevelopment;

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  // const theme =
  //   config.THEME && themes[config.THEME]
  //     ? themes[config.THEME]
  //     : themes.default;

  if (sentryDSN) {
    // we need to init Sentry separately for frontend.
    Sentry.init({
      dsn: sentryDSN,
      tracesSampleRate: 1.0,
      environment: sentryEnv,
      release: sentryRelease,
    });
  }

  return (
    <>
      <Head>
        <title>CyVerse Cacao | Jetstream2</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <ConfigProvider config={config}>
            <APIProvider baseUrl={baseUrl} token={token}>
              <UserProvider user={user}>
                <EventsProvider username={user && user.username}>
                  <CloudsProvider clouds={clouds}>
                    <ProjectsProvider projects={projects}>
                      <CredentialsProvider credentials={credentials}>
                        <WorkspacesProvider workspaces={workspaces}>
                          <ErrorProvider>
                            <Component {...pageProps} />
                          </ErrorProvider>
                        </WorkspacesProvider>
                      </CredentialsProvider>
                    </ProjectsProvider>
                  </CloudsProvider>
                </EventsProvider>
              </UserProvider>
            </APIProvider>
          </ConfigProvider>
        </ThemeProvider>
      </StyledEngineProvider>
    </>
  );
}

MyApp.getInitialProps = async ({ Component, ctx }) => {
  const req = ctx.req;
  const api = req && req.api;

  let user;
  let clouds;
  let projects;
  let credentials;
  let workspaces;
  let pageProps;

  try {
    user = api && api.token && (await api.user());
    clouds = api && api.token && (await api.providers());
    projects = api && api.token && (await api.js2allocations());
    credentials = api && api.token && (await api.credentials());
    workspaces = api && api.token && (await api.workspaces());
    pageProps = Component.getInitialProps
      ? await Component.getInitialProps(ctx)
      : {};
  } catch (e) {
    console.error(e);
  }

  return {
    sentryDSN: req && req.sentryDSN,
    sentryRelease: req && req.sentryRelease,
    sentryEnv: req && req.sentryEnv,
    isDevelopment: req && req.isDevelopment,
    baseUrl: api && api.baseUrl,
    token: api && api.token, //FIXME still necessary?
    user: user,
    clouds: clouds,
    projects: projects,
    credentials: credentials,
    workspaces: workspaces,
    pageProps: pageProps,
  };
};
