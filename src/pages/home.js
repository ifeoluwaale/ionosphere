import { useState } from "react";
import { useRouter } from "next/router";
import * as Sentry from "@sentry/nextjs";
import { Box, Divider, Grid, Link, Stack, Typography } from "@mui/material";
import DeploymentCard from "../components/Cards/DeploymentCard";
import LearningCard from "../components/Cards/LearningCard";
import AllocationCard from "../components/Cards/AllocationCard";
import { sortByDateDesc } from "../utils";
import Layout from "../components/Layout";
import WelcomeBanner from "../components/welcomeBanner";
import CredentialModal from "../components/CredentialModal";
import { useCredentials, useProjects } from "@/contexts";

const Home = ({ deployments }) => {
  const router = useRouter();
  const [projects] = useProjects();
  const [credentials] = useCredentials();

  // show credential reminder modal if user has no credentials
  const [credentialModalOpen, setCredentialModalOpen] = useState(
    credentials.length === 0
  );

  const navigateToCredentials = () => {
    router.push({
      pathname: "/credentials",
    });
  };

  return (
    <Layout title="Home">
      <CredentialModal
        open={credentialModalOpen}
        handleClose={() => setCredentialModalOpen(false)}
        navigateToCredentials={navigateToCredentials}
      />

      <Box mt={1} mb={4}>
        <WelcomeBanner />
      </Box>

      <Stack spacing={4}>
        {projects?.length > 0 && (
          <Box>
            <Box mb={1}>
              <Typography variant="h6">Allocations</Typography>
            </Box>
            <Divider />
            <br />
            <Grid container direction="row" spacing={3}>
              {projects.map((allocation, index) => (
                <Grid item key={index} xs={12} sm={12} md={6}>
                  <AllocationCard {...allocation} />
                </Grid>
              ))}
            </Grid>
          </Box>
        )}

        {deployments?.length > 0 && (
          <Box>
            <Box mb={0.5}>
              <Typography variant="h6">Deployments</Typography>
            </Box>
            <Divider />
            <br />
            <Grid
              container
              spacing={3}
              direction="row"
              justifyContent="flex-start"
              sx={{ alignItems: "flex-start" }}
            >
              {deployments.map((deployment, index) => (
                <Grid item key={index} xs={12} sm={12} md={6} lg={4} xl={4}>
                  <Link underline="none" href={`/deployments/${deployment.id}`}>
                    <DeploymentCard deployment={deployment} />
                  </Link>
                </Grid>
              ))}
            </Grid>
          </Box>
        )}
        <Box mt={4}>
          <Box pb={3}>
            <Box mb={1}>
              <Typography variant="h6">Featured Learning</Typography>
            </Box>
            <Divider />
          </Box>
          <Grid
            container
            justifyContent="flex-start"
            spacing={3}
            direction="row"
            alignItems="flex-start"
            alignContent="flex-start"
          >
            <Grid item xs={12} sm={12} md={6} lg={4}>
              <Link
                href={
                  "https://docs.jetstream-cloud.org/ui/cacao/getting_started/"
                }
                underline="none"
                target="_blank"
              >
                <LearningCard
                  title="Continuous Analysis 101"
                  description="Learn how Jetstream2 can help you with your research goals."
                />
              </Link>
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={4}>
              <Link
                href={
                  "https://docs.jetstream-cloud.org/ui/cacao/getting_started/"
                }
                underline="none"
                target="_blank"
              >
                <LearningCard
                  title="Jetstream2 Basics"
                  description="Learn about workspaces, deployments, providers and templates."
                />
              </Link>
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={4}>
              <Link
                href={"https://docs.jetstream-cloud.org/alloc/overview/"}
                underline="none"
                target="_blank"
              >
                <LearningCard
                  title="Manage Resources"
                  description="Learn about how to manage resources to maximize your research."
                />
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Stack>
    </Layout>
  );
};

export async function getServerSideProps({ req, res }) {
  try {
    let [deployments] = await Promise.all([
      req.api.deployments({ "full-run": true }),
    ]);

    deployments = deployments
      ? deployments.filter((d) => d.current_status === "active")
      : [];

    return {
      props: {
        deployments:
          deployments &&
          deployments.length > 0 &&
          deployments.sort(sortByDateDesc).slice(0, 3),
      },
    };
  } catch (e) {
    Sentry.captureException(e);
    console.error(e);
    if (e?.response?.status === 401) {
      res.redirect("/401");
    } else {
      res.redirect("/500");
    }
    return { props: {} };
  }
}

export default Home;
