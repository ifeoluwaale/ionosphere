import { useState } from "react";
import { useRouter } from "next/router";
import * as Sentry from "@sentry/nextjs";
import {
  Link,
  Button,
  Box,
  Divider,
  CardHeader,
  CardContent,
  Typography,
  Card,
  IconButton,
} from "@mui/material";
import DeploymentSummary from "../../components/Deployment/DeploymentSummary";
import { Edit as EditIcon } from "@mui/icons-material";
import { useClouds, useAPI, useError } from "../../contexts";
import { Layout, FormDialog, ConfirmationDialog } from "../../components";
import { datetimeString } from "../../utils";

const Workspace = (props) => {
  const router = useRouter();
  const api = useAPI();
  const [clouds] = useClouds();
  // eslint-disable-next-line no-unused-vars
  const [_, setError] = useError();

  const [workspace, setWorkspace] = useState(props.workspace);
  const [busy, setBusy] = useState(false);
  const [showEditNameDialog, setShowEditNameDialog] = useState(false);
  const [showDeleteConfirmationDialog, setShowDeleteConfirmationDialog] =
    useState(false);
  const defaultCloud = clouds.find(
    (c) => c.id === workspace.default_provider_id
  );

  const handleError = (error) => {
    console.error(error);
    setBusy(false);
    setError(api.errorMessage(error));
  };

  const deleteHandler = async () => {
    setBusy(true);
    try {
      const res = await api.deleteWorkspace(workspace.id);
      if (!res) setError("An error occurred");
      else setTimeout(() => router.push("/workspaces"), 500); //FIXME replace with event detection
    } catch (error) {
      handleError(error);
    }
  };

  const updateHandler = async (values) => {
    try {
      const res = await api.updateWorkspace(workspace.id, values);
      if (!res) setError("An error occurred");
      else setWorkspace({ ...workspace, ...values }); //FIXME replace with event detection
    } catch (error) {
      handleError(error);
    }
  };

  return (
    <Layout title={workspace.name} breadcrumbs busy={busy}>
      <Box mt={4}>
        <Card>
          <CardHeader
            title={
              <Typography variant="h6">
                {workspace.name}{" "}
                <IconButton onClick={() => setShowEditNameDialog(true)}>
                  <EditIcon color="primary" fontSize="small" />
                </IconButton>
              </Typography>
            }
            subheader={
              <Typography variant="body2">{workspace.description}</Typography>
            }
            action={
              <>
                {/* <Button color="primary" size="small">
                  Request More Resources
                </Button> */}
                <Button
                  color="warning"
                  size="small"
                  onClick={() => setShowDeleteConfirmationDialog(true)}
                >
                  Delete Workspace
                </Button>
              </>
            }
          />
          <CardContent>
            <Typography variant="body2">
              <div>
                <span>Owner:</span>{" "}
                {`${props.owner.first_name} ${props.owner.last_name} (${props.owner.username})`}
              </div>
              <div>
                <span>Cloud:</span>{" "}
                {(defaultCloud && defaultCloud.name) || "Unknown"}
              </div>
              <div>
                <span>Created:</span> {datetimeString(workspace.created_at)}
              </div>
            </Typography>
          </CardContent>
        </Card>
      </Box>
      <Box mt={4}>
        <Typography variant="h6">Deployments</Typography>
      </Box>
      <Divider />
      <Box>
        {props.deployments && props.deployments.length > 0 ? (
          props.deployments.map((deployment, index) => (
            <Box mt={2}>
              <Link underline="none" href={`/deployments/${deployment.id}`}>
                <DeploymentSummary deployment={deployment} summaryOnly />
              </Link>
            </Box>
          ))
        ) : (
          <Box mt={2}>
            <Typography>
              You have not created a deployment in this workspace yet.
            </Typography>
          </Box>
        )}
      </Box>
      <FormDialog
        open={showEditNameDialog}
        title="Edit workspace"
        handleClose={() => setShowEditNameDialog(false)}
        handleSubmit={updateHandler}
        fields={[
          //FIXME add min/max length validation
          {
            id: "name",
            label: "Name",
            type: "text",
            required: true,
            defaultValue: workspace.name,
          },
          {
            id: "description",
            label: "Description",
            type: "text",
            required: true,
            defaultValue: workspace.description,
          },
        ]}
      />
      <ConfirmationDialog
        open={showDeleteConfirmationDialog}
        title="Delete workspace"
        handleClose={() => setShowDeleteConfirmationDialog(false)}
        handleSubmit={deleteHandler}
      />
    </Layout>
  );
};

export async function getServerSideProps({ req, query }) {
  try {
    const workspace = await req.api.workspace(query.id);
    //const deployments = await req.api.workspaceDeployments(query.id) //FIXME endpoint not implemented yet
    const deployments = await req.api.deployments();
    const owner = await req.api.user(workspace.owner);

    return {
      props: {
        workspace,
        owner,
        deployments: deployments.filter((d) => d.workspace_id === workspace.id),
      },
    };
  } catch (e) {
    Sentry.captureException(e);
    //FIXME replace with "redirect" feature in Next.js 10
    console.error(e);
    return {
      redirect: {
        destination: "/404",
        permanent: false,
      },
    };
  }
}

export default Workspace;
