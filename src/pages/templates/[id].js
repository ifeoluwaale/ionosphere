import * as Sentry from "@sentry/nextjs";
import { useState } from "react";
import {
  Code as CodeIcon,
  RocketLaunch as RocketLaunchIcon,
  Share as ShareIcon,
  Widgets as WidgetsIcon,
  EmailOutlined as EmailOutlinedIcon,
} from "@mui/icons-material";
import {
  Avatar,
  Box,
  Button,
  Chip,
  Divider,
  Grid,
  Link,
  Paper,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableContainer,
  Typography,
} from "@mui/material";
import { Layout } from "../../components";
import { DateTime } from "@/components/Common/DateTime";
import Accordion from "@/components/Common/Accordion";
import { stringToColor } from "@/utils";
import CreateDeploymentDialog from "@/components/DeploymentWizard/CreateDeploymentDialog";
import { DeploymentWizardProvider } from "@/components/DeploymentWizard/contexts/DeploymentWizardContext";

const Template = ({ template }) => {
  const [showDeploymentWizard, setShowDeploymentWizard] = useState(false);

  const handleClose = async () => {
    setShowDeploymentWizard(false);
  };

  return (
    <Layout title={template.name} breadcrumbs>
      <Paper variant="outlined">
        <Box p={3}>
          <Grid
            container
            spacing={2}
            justifyContent={"space-between"}
            paddingBottom={2}
          >
            <Grid item>
              <Stack direction="row" spacing={2} alignItems={"center"}>
                <Avatar
                  sx={{
                    bgcolor: stringToColor(template.name),
                  }}
                >
                  <WidgetsIcon />
                </Avatar>
                <Typography variant="h6">{template.name}</Typography>
              </Stack>
            </Grid>
            <Grid item>
              <Grid container spacing={1}>
                <Grid item>
                  <Button
                    startIcon={<RocketLaunchIcon />}
                    variant="contained"
                    size="small"
                    onClick={() => setShowDeploymentWizard(true)}
                  >
                    Deploy
                  </Button>
                </Grid>
                {/* <Grid item>
                    <Button startIcon={<ShareIcon />} variant="outlined">
                      Share
                    </Button>
                  </Grid> */}
                <Grid item>
                  <Button
                    startIcon={<CodeIcon />}
                    variant="outlined"
                    size="small"
                    href={template.source.uri}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Source Code
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Divider />
          <Stack spacing={4} marginTop={2}>
            <Grid container spacing={4}>
              <Grid item>Author: {template.metadata.author}</Grid>
              <Grid item>
                <Stack direction={"row"} spacing={1}>
                  <EmailOutlinedIcon color="primary" />
                  <Box>{template.metadata.author_email}</Box>
                </Stack>
              </Grid>
            </Grid>

            <Box>
              <Grid container spacing={1} direction="row">
                <Grid item>
                  <Chip label={template.metadata.purpose} size="small" />
                </Grid>
                <Grid item>
                  <Chip label={template.metadata.template_type} size="small" />
                </Grid>
                <Grid item>
                  <Chip
                    label={template.public ? "public" : "private"}
                    size="small"
                  />
                </Grid>
              </Grid>
            </Box>

            <Box mt={1}>
              <Typography variant="h6" mb={1}>
                Description
              </Typography>
              <Typography>{template.description}</Typography>
            </Box>

            <Box mt={1}>
              <Typography variant="h6" mb={1}>
                Parameters
              </Typography>
              <Accordion title={"View Template Parameters"}>
                <TableContainer>
                  <Table size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell>Parameter Name</TableCell>
                        <TableCell>Parameter Type</TableCell>
                        <TableCell>Description</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {template.metadata.parameters.map((p) => (
                        <TableRow key={p.name}>
                          <TableCell>{p.name}</TableCell>
                          <TableCell>{p.type}</TableCell>
                          <TableCell>{p.description}</TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </Accordion>
            </Box>

            <Stack spacing={1}>
              <Typography variant="body2">
                Source code:{" "}
                <Link href={template.source.uri}>{template.source.uri}</Link>
              </Typography>
              <Typography variant="body2">
                Schema version: {template.metadata.schema_version}
              </Typography>
              <Typography variant="body2">
                Template last updated:{" "}
                <DateTime datetime={template.updated_at} />
              </Typography>
              <Typography variant="body2">
                Imported by: {template.owner}
              </Typography>
            </Stack>
          </Stack>
        </Box>
      </Paper>

      {showDeploymentWizard && (
        <DeploymentWizardProvider
          initialValues={{
            template: template,
          }}
        >
          <CreateDeploymentDialog
            open={!!showDeploymentWizard}
            handleClose={handleClose}
            handlePostCreate={handleClose}
          />
        </DeploymentWizardProvider>
      )}
    </Layout>
  );
};

export async function getServerSideProps({ req, res, query }) {
  try {
    const template = await req.api.template(query.id);
    return {
      props: {
        template,
      },
    };
  } catch (e) {
    Sentry.captureException(e);
    console.error(e);
    return {
      redirect: {
        destination: "/404",
        permanent: false,
      },
    };
  }
}

export default Template;
