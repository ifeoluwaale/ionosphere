import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import * as Sentry from "@sentry/nextjs";
import {
  Box,
  Button,
  Grid,
  Menu,
  MenuItem,
  Skeleton,
  Tab,
  Tabs,
} from "@mui/material";
import { Add as AddIcon } from "@mui/icons-material";

import ProviderAuthDialog from "../components/forms/ProviderAuthDialog";
import AddCredentialDialog from "../components/forms/AddCredentialDialog";
import EditCredentialDialog from "../components/forms/EditCredentialDialog";

import { useAPI, useCredentials, useError, useProjects } from "@/contexts";
import { Layout } from "@/components";
import CreateApiTokenDialog from "@/components/Credentials/ApiTokens/CreateApiTokenDialog";
import CredentialList from "@/components/Credentials/CredentialList";
import {
  credentialCategories,
  sshCredentialType,
  cloudCredentialType,
  apiTokenCredentialType,
} from "@/components/Credentials/CredentialTypes";

const Credentials = ({ initialApiTokens }) => {
  const router = useRouter();
  const api = useAPI();

  const [credentials, setCredentials] = useCredentials();

  // show cloud credentials tab by default
  const [credentialCategory, setCredentialCategory] =
    useState(cloudCredentialType);
  const [sshKeys, setSshKeys] = useState([]);
  const [cloudCredentials, setCloudCredentials] = useState([]);
  const [apiTokens, setApiTokens] = useState(initialApiTokens);

  const [projects, setProjects] = useProjects();

  const [busy, setBusy] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const [_, setError] = useError();

  // for handling redirect
  const [provider, setProvider] = useState();
  const [wizardStep, setWizardStep] = useState();

  const [addCredentialType, setAddCredentialType] = useState();
  const [editCredentialId, setEditCredentialId] = useState();
  const [anchorEl, setAnchorEl] = useState(null); // for dropdown menu

  /**
   * If user was redirected to ACCESS login during credential creation
   * wizard, reopen wizard to next step.
   */
  useEffect(() => {
    if (router.query.provider) {
      setAddCredentialType(cloudCredentialType);
      setWizardStep(1);
      setProvider(router.query.provider);
      router.replace("/credentials", undefined, { shallow: true });
    }
  }, []);

  /**
   * Filter credentials by type and set state.
   */
  useEffect(() => {
    const sshFilter = credentialCategories.filter(
      (c) => c.value === sshCredentialType
    )[0].filter;
    const cloudCredentialFilter = credentialCategories.filter(
      (c) => c.value === cloudCredentialType
    )[0].filter;

    const sshKeys = credentials.filter(sshFilter);
    const cloudCredentials = credentials.filter(cloudCredentialFilter);

    setSshKeys(sshKeys);
    setCloudCredentials(cloudCredentials);
  }, [credentials]);

  /**
   * Closes provider auth wizard and refreshes credentials list.
   */
  const closeProviderAuthWizard = () => {
    setAddCredentialType();
    setProvider();
    setWizardStep();
    fetchCredentials();
  };

  const handleError = (msg, error, hideDetails) => {
    console.error(msg, error);
    setBusy(false);
    setError(
      <>
        {msg}
        {error && !hideDetails && (
          <Box mt={2}>
            Details: {error.response ? error.response.data : error.message}
          </Box>
        )}
      </>
    );
  };

  const fetchCredentials = async () => {
    try {
      const res = await api.credentials();
      setCredentials(res);
    } catch (error) {
      handleError("Could not retrieve credential list", error);
    } finally {
      setBusy(false);
    }
  };

  // force refresh of js2projects cache
  const refreshProjects = async () => {
    try {
      const projects = await api.js2allocations({ nocache: true });
      setProjects(projects);
    } catch (error) {
      console.log("Error refreshing js2 allocations.", error);
    }
  };

  /**
   * Makes API call to edit credential, then refreshes credentials list and closes edit dialog.
   * @param {string} id ID of credential to edit
   * @param {Object} credentialUpdate credential update request params
   */
  const editCredential = async (id, credentialUpdate) => {
    try {
      await api.updateCredential(id, credentialUpdate);
    } catch (error) {
      handleError("Could not edit credential.", error.response.data);
    } finally {
      await fetchCredentials();
      setEditCredentialId();
    }
  };

  const deleteCredential = async (credential) => {
    setBusy(true);

    try {
      // this will clean up application credential in openstack as well.
      await api.deleteCredential(credential.id);
    } catch (error) {
      handleError(
        "Could not delete credential from CACAO.",
        error.response.data
      );
    }

    // force refresh of js2projects cache
    refreshProjects();

    // update credentials list
    setTimeout(fetchCredentials, 500);
  };

  const handleCredentialCreate = async () => {
    // force refresh of js2projects cache
    refreshProjects();

    // refresh credentials and close modal
    fetchCredentials();
    setAddCredentialType();
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  /**
   * Fetches API tokens and updates state.
   */
  const fetchTokens = async () => {
    try {
      const res = await api.getTokens();
      setApiTokens(res);
    } catch (error) {
      handleError("Could not retrieve tokens", error);
    } finally {
      setBusy(false);
    }
  };

  /**
   * Deletes API token and refresh API tokens list.
   * @param {*} token
   */
  const deleteToken = async (token) => {
    setBusy(true);

    try {
      // this will clean up application credential in openstack as well.
      await api.deleteToken(token.id);
    } catch (error) {
      handleError("Could not delete token from CACAO.", error.response.data);
    }

    // update credentials list
    setTimeout(fetchTokens, 500);
  };

  /**
   * Handle user selection within "Add Credential" menu. Closes menu and opens
   * appropriate credential creation dialog.
   * @param {string} credType
   */
  const selectCredentialTypeToCreate = (credType) => {
    handleClose();
    setAddCredentialType(credType);
    setCredentialCategory(credType);
  };

  return (
    <Layout title="Credentials" busy={busy}>
      <Box display="flex" justifyContent="space-between" paddingBottom={1}>
        <Tabs
          value={credentialCategory}
          onChange={(_, newValue) => setCredentialCategory(newValue)}
        >
          {credentialCategories.map((category) => (
            <Tab
              value={category.value}
              label={category.plural_label}
              key={category.value}
            />
          ))}
        </Tabs>
        <div>
          <Button
            aria-controls="credential-menu"
            aria-haspopup="true"
            onClick={(e) => setAnchorEl(e.currentTarget)}
            color="primary"
            variant="contained"
            startIcon={<AddIcon />}
            size="small"
          >
            Add Credential
          </Button>
          <Menu
            id="credential-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            {credentialCategories.map((category) => (
              <MenuItem
                onClick={() => selectCredentialTypeToCreate(category.value)}
                key={category.value}
              >
                {category.singular_label}
              </MenuItem>
            ))}
          </Menu>
        </div>
      </Box>

      {/* ----------- Credential lists ------------ */}
      {(credentialCategory === cloudCredentialType ||
        credentialCategory === sshCredentialType) && (
        <>
          {sshKeys && cloudCredentials ? (
            <CredentialList
              credentials={
                credentialCategory === cloudCredentialType
                  ? cloudCredentials
                  : sshKeys
              }
              credentialCategory={credentialCategory}
              onDelete={deleteCredential}
              setEditCredentialId={setEditCredentialId}
            />
          ) : (
            <Grid container spacing={3} direction="row" jusitfy="flex-end">
              <Grid item xs={12} sm={12} md={8} lg={6} xl={4}>
                <Skeleton height={300} animation="wave"></Skeleton>
              </Grid>
            </Grid>
          )}
        </>
      )}

      {credentialCategory === apiTokenCredentialType && (
        <CredentialList
          credentials={apiTokens}
          credentialCategory={credentialCategory}
          onDelete={deleteToken}
        />
      )}

      {/* ----------- Create credential dialogs ------------ */}
      {addCredentialType === cloudCredentialType && (
        <ProviderAuthDialog
          open={true}
          handleClose={closeProviderAuthWizard}
          setAddCredentialType={setAddCredentialType}
          fetchCredentials={fetchCredentials}
          wizardStep={wizardStep}
          provider={provider}
          setWizardStep={setWizardStep}
          handleSubmit={closeProviderAuthWizard}
        />
      )}
      {addCredentialType === apiTokenCredentialType && (
        <CreateApiTokenDialog
          open={true}
          handleClose={() => {
            setAddCredentialType();
            setTimeout(fetchTokens, 0);
          }}
        ></CreateApiTokenDialog>
      )}
      {(addCredentialType === sshCredentialType ||
        addCredentialType === "application") && (
        <AddCredentialDialog
          open={!!addCredentialType}
          type={addCredentialType}
          credentials={credentials}
          handleClose={(_, reason) =>
            reason !== "backdropClick" && setAddCredentialType()
          }
          handleCredentialCreate={handleCredentialCreate}
        />
      )}

      {/* ----------- Edit credential dialogs ------------ */}
      {editCredentialId && (
        <EditCredentialDialog
          open={editCredentialId !== null}
          title="Edit Credential"
          handleClose={() => setEditCredentialId()}
          handleEdit={editCredential}
          credential={credentials?.filter((c) => c.id === editCredentialId)[0]}
        />
      )}
    </Layout>
  );
};

export async function getServerSideProps({ req, res }) {
  try {
    let apiTokens = await req.api.getTokens();

    return {
      props: {
        initialApiTokens: apiTokens,
      },
    };
  } catch (e) {
    Sentry.captureException(e);
    //FIXME replace with "redirect" feature in Next.js 10
    console.error(e);
    if (e?.response?.status === 401) {
      res.redirect("/401");
    } else {
      res.redirect("/500");
    }
    return { props: {} };
  }
}

export default Credentials;
