/* eslint-disable no-unused-vars */
import { useState } from "react";
import Link from "next/link";
import {
  Container,
  Grid,
  Paper,
  Typography,
  TextField,
  CircularProgress,
  TableRow,
  TableCell,
} from "@mui/material";
import { Layout, PaginatedTable } from "../../components";
import { useAPI } from "../../contexts/api";

//TODO move pagination code into shared component
const Users = (props) => {
  const api = useAPI();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [keyword, setKeyword] = useState();
  const [count, setCount] = useState(props.count);
  const [rows, setRows] = useState(props.results || []);
  const [debounce, setDebounce] = useState(null);
  const [searching, setSearching] = useState(false);

  const handleChangePage = async (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = async (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeKeyword = async (event) => {
    setSearching(true);
    setKeyword(event.target.value);
    setPage(0);
  };

  // useEffect(() => {
  //   if (debounce) clearTimeout(debounce)
  //   setDebounce(
  //     setTimeout(async () => {
  //         const { count, results } = await api.users({
  //           offset: page * rowsPerPage,
  //           limit: rowsPerPage,
  //           keyword: keyword
  //         })
  //         setCount(count)
  //         setRows(results)
  //         setSearching(false)
  //       }, 500)
  //   )},
  //   [page, rowsPerPage, keyword]
  // )

  return (
    <Layout breadcrumbs>
      <Container maxWidth="lg">
        <br />
        <Paper elevation={3}>
          <Grid container justifyContent="space-between">
            <Grid item>
              <Typography component="h1" variant="h4" gutterBottom>
                Users
              </Typography>
            </Grid>
            <Grid item>
              <TextField
                style={{ width: "20em" }}
                placeholder="Search ..."
                onChange={handleChangeKeyword}
                InputProps={{
                  endAdornment: (
                    <>
                      {searching && (
                        <CircularProgress color="inherit" size={20} />
                      )}
                    </>
                  ),
                }}
              />
            </Grid>
          </Grid>
          <Typography color="textSecondary" gutterBottom>
            Search by name, username, email, institution, occupation, region,
            and country.
            <br />
            Enter multiple keywords separated by spaces.
          </Typography>
          <br />
          <PaginatedTable
            columnNames={["Name", "Username", "Email"]}
            rows={rows}
            rowsPerPage={rowsPerPage}
            count={count}
            page={page}
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
            renderRow={(user, index) => {
              // const d = new Date(user.date_joined)
              return (
                <Link
                  key={index}
                  href={`/administrative/users/${user.username}`}
                >
                  <TableRow hover style={{ cursor: "pointer" }}>
                    <TableCell>
                      {user.first_name} {user.last_name}
                    </TableCell>
                    <TableCell>{user.username}</TableCell>
                    <TableCell>{user.primary_email}</TableCell>
                  </TableRow>
                </Link>
              );
            }}
          />
        </Paper>
      </Container>
    </Layout>
  );
};

export async function getServerSideProps({ req }) {
  const results = await req.api.users();

  return {
    props: {
      results,
      count: results ? results.length : 0,
    },
  };
}

export default Users;
