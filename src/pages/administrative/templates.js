/* eslint-disable no-unused-vars */
import { useState, useEffect } from "react";
import Link from "next/link";
import {
  Container,
  Grid,
  Paper,
  Typography,
  TextField,
  CircularProgress,
  TableRow,
  TableCell,
} from "@mui/material";
import { Layout, PaginatedTable } from "../../components";
import { useAPI } from "../../contexts/api";

//TODO move pagination code into shared component
const Templates = (props) => {
  const api = useAPI();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [keyword, setKeyword] = useState();
  const [count, setCount] = useState(props.count);
  const [rows, setRows] = useState(props.results || []);
  const [debounce, setDebounce] = useState(null);
  const [searching, setSearching] = useState(false);

  const handleChangePage = async (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = async (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeKeyword = async (event) => {
    setSearching(true);
    setKeyword(event.target.value);
    setPage(0);
  };

  // useEffect(() => {
  //   if (debounce) clearTimeout(debounce)
  //   setDebounce(
  //     setTimeout(async () => {
  //         const { count, results } = await api.users({
  //           offset: page * rowsPerPage,
  //           limit: rowsPerPage,
  //           keyword: keyword
  //         })
  //         setCount(count)
  //         setRows(results)
  //         setSearching(false)
  //       }, 500)
  //   )},
  //   [page, rowsPerPage, keyword]
  // )

  return (
    <Layout breadcrumbs>
      <Container maxWidth="lg">
        <br />
        <Paper elevation={3}>
          <Grid container justifyContent="space-between">
            <Grid item>
              <Typography component="h1" variant="h4" gutterBottom>
                Templates
              </Typography>
            </Grid>
            <Grid item>
              <TextField
                style={{ width: "20em" }}
                placeholder="Search ..."
                onChange={handleChangeKeyword}
                InputProps={{
                  endAdornment: (
                    <>
                      {searching && (
                        <CircularProgress color="inherit" size={20} />
                      )}
                    </>
                  ),
                }}
              />
            </Grid>
          </Grid>
          <Typography color="textSecondary" gutterBottom>
            Search by ID, name, owner, and type.
            <br />
            Enter multiple keywords separated by spaces.
          </Typography>
          <br />
          <PaginatedTable
            columnNames={["Name", "Owner", "Source Type", "Created"]}
            rows={rows}
            rowsPerPage={rowsPerPage}
            count={count}
            page={page}
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
            renderRow={(template, index) => {
              return (
                // <Link key={index} href={`/administrative/templates/${template.id}`}>
                <TableRow hover style={{ cursor: "pointer" }}>
                  <TableCell>{template.name}</TableCell>
                  <TableCell>{template.owner}</TableCell>
                  <TableCell>
                    <a href={template.source.uri} target="_blank">
                      {template.source.type}
                    </a>
                  </TableCell>
                  <TableCell>{template.created_at}</TableCell>
                </TableRow>
                // </Link>
              );
            }}
          />
        </Paper>
      </Container>
    </Layout>
  );
};

export async function getServerSideProps({ req }) {
  const results = await req.api.templates();
  const count = results ? results.length : 0;

  return {
    props: {
      results,
      count,
    },
  };
}

export default Templates;
