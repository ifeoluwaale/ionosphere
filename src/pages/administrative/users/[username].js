import { useRouter } from "next/router";
import { useState } from "react";
import { Container, Box, Paper, Button, Typography } from "@mui/material";
import { Layout, ConfirmationDialog } from "../../../components";
import { useAPI } from "../../../contexts/api";
import { useError } from "../../../contexts/error";
import { useUser } from "../../../contexts/user";

const User = ({ user }) => {
  const router = useRouter();
  const [me] = useUser();
  const api = useAPI();
  const [_, setError] = useError();

  const [showDeleteConfirmationDialog, setShowDeleteConfirmationDialog] =
    useState(false);
  const [deletingUser, setDeletingUser] = useState(false);

  const deleteUser = async () => {
    try {
      setDeletingUser(true);
      setShowDeleteConfirmationDialog(false);
      await api.deleteUser(user.id);
      router.push("/administrative/users");
    } catch (error) {
      console.log(error);
      setError(api.errorMessage(error));
    }
  };

  // const openPasswordResetDialog = async () => {
  //   try {
  //     const resp = await api.adminResetPassword(user.id)
  //     setHMAC(resp)
  //     setShowPasswordResetDialog(true)
  //   }
  //   catch(error) {
  //     console.log(error)
  //     setError(api.errorMessage(error))
  //   }
  // }

  return (
    <Layout title={user.username} breadcrumbs busy={deletingUser}>
      <Container maxWidth="lg">
        <Box mt={4} display="flex">
          <Box flexGrow={1}>
            <Typography
              component="div"
              variant="h5"
            >{`${user.first_name} ${user.last_name} (${user.username})`}</Typography>
            <Typography>{user.primary_email}</Typography>
          </Box>
          <Box>
            <Button
              variant="contained"
              style={{ color: "red" }} // color="error" // not working
              disabled={!me.admin}
              onClick={() => setShowDeleteConfirmationDialog(true)}
            >
              DELETE USER
            </Button>
          </Box>
        </Box>
        <br />

        <Paper elevation={3}>
          <Typography component="div" variant="h5">
            Workspaces
          </Typography>
          <br />
          TODO
        </Paper>

        <Paper elevation={3}>
          <Typography component="div" variant="h5">
            Deployments
          </Typography>
          <br />
          TODO
        </Paper>

        <Paper elevation={3}>
          <Typography component="div" variant="h5">
            Storage
          </Typography>
          <br />
          TODO
        </Paper>

        <Paper elevation={3}>
          <Typography component="div" variant="h5">
            Credentials
          </Typography>
          <br />
          TODO
        </Paper>

        <Paper elevation={3}>
          <Typography component="div" variant="h5">
            Templates
          </Typography>
          <br />
          TODO
        </Paper>
      </Container>
      <ConfirmationDialog
        open={showDeleteConfirmationDialog}
        title="Delete user"
        handleClose={() => setShowDeleteConfirmationDialog(false)}
        handleSubmit={deleteUser}
      />
    </Layout>
  );
};

export async function getServerSideProps({ req, query }) {
  const user = await req.api.user(query.username);
  return { props: { user } };
}

export default User;
