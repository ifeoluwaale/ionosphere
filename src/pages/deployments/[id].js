import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import getConfig from "next/config";
import * as Sentry from "@sentry/nextjs";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  Grid,
  IconButton,
  List,
  Tooltip,
  Typography,
} from "@mui/material";
import LinearProgress, {
  linearProgressClasses,
} from "@mui/material/LinearProgress";
import {
  Stop as StopIcon,
  Delete as DeleteIcon,
  PowerSettingsNew as PowerIcon,
  PlayArrow as PlayIcon,
  TextSnippet as LogsIcon,
} from "@mui/icons-material";
import { mdiRocketLaunch } from "@mdi/js";
import Icon from "@mdi/react";
import { useAPI, useError, useEvents } from "../../contexts";
import { Layout, StatusIndicator, ConfirmationDialog } from "../../components";
import {
  Resource,
  needToRenderResource,
} from "../../components/Resources/Resource";
import { matchAWSInstanceIP } from "../../components/Resources/AWSResource";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { timeDiffString, datetimeString } from "../../utils";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import { styled } from "@mui/material/styles";
import DeploymentErrorAlert from "../../components/Deployment/DeploymentErrorAlert";
import ShelveConfirmationDialog from "../../components/ShelveConfirmationDialog";

//custom IconButton

const ActionButton = styled(IconButton)(({ theme }) => ({
  color: theme.palette.primary.main,
  width: "2em",
  height: "2em",
  top: "0.2em",
}));

const ErrorLinearProgress = styled(LinearProgress)(({ theme }) => ({
  root: {
    height: 10,
    borderRadius: 5,
  },
  [`&.${linearProgressClasses.colorPrimary}`]: {
    borderRadius: 5,
    backgroundColor: "#f57c00",
  },
}));

const StandardLinearProgress = styled(LinearProgress)(({ theme }) => ({
  root: {
    height: 10,
    borderRadius: 5,
  },
}));

//TODO move into src/components for general use
const ProgressDialog = ({
  open,
  busy,
  title,
  busyText,
  doneText,
  handleClose,
}) => {
  return (
    <Dialog open={open} onClose={handleClose} fullWidth>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{busy ? busyText : doneText}</DialogContentText>
      </DialogContent>
      <DialogActions>
        {!busy && (
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              handleClose();
            }}
          >
            OK
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

const LogsButton = ({ deploymentId, runId }) => {
  const router = useRouter();
  const navigateToLogs = ({ deploymentId, runId }) => {
    router.push({
      pathname: `/deployments/${deploymentId}/logs`,
    });
  };
  return (
    <Tooltip title="View logs">
      <ActionButton onClick={() => navigateToLogs({ deploymentId, runId })}>
        <LogsIcon />
      </ActionButton>
    </Tooltip>
  );
};

const Deployment = (props) => {
  const config = getConfig().publicRuntimeConfig;
  const router = useRouter();
  const api = useAPI();
  const [_, setError] = useError();
  const [event] = useEvents();
  const [deployment, setDeployment] = useState(props.deployment);
  const [run, setRun] = useState(props.run);
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
  const [busy, setBusy] = useState(false);
  const [showProgress, setShowProgress] = useState(false);
  const [sessionUrl, setSessionUrl] = useState();
  const [showParams, setShowParams] = useState(false);
  const [shelveDialog, setShelveDialog] = useState(false);

  const handleError = (error) => {
    console.error(error);
    setBusy(false);
    setError(api.errorMessage(error));
  };

  async function fetchRun() {
    const r = await api.deploymentRun(deployment.id, run.id);
    setRun(r);
  }

  useEffect(() => {
    if (!event) return;

    if (
      event.type === "DeploymentRunStatusUpdated" &&
      event.data.deployment === deployment.id
    ) {
      if (event.data.status === "active") fetchRun();
      else setRun({ ...run, status: event.data.status });
    }
  }, [event]);

  const handleDeleteDeployment = async () => {
    setBusy(true);
    try {
      await api.deleteDeployment(deployment.id);
      setTimeout(() => router.push("/deployments"), 500); //FIXME replace with event detection
    } catch (error) {
      handleError(error);
    }
  };

  const handleUpdatedDeploymentState = async (newState) => {
    setBusy(true);
    const parameters = JSON.parse(JSON.stringify(run.parameters)); // deep copy
    const powerStateParam = parameters.find((p) => p.key === "power_state");
    powerStateParam.value = newState;

    try {
      const res = await api.createDeploymentRun(deployment.id, {
        deployment_id: deployment.id,
        // cloud_credentials: [ credential.id ],
        // git_credential_id: "", //FIXME
        parameters,
      });
      console.log("res:", res);
      setTimeout(() => router.push("/deployments"), 500); //FIXME replace with event detection
    } catch (error) {
      handleError(error);
    }
  };

  // shelving/unshelving confirmation dialog

  const handleShelve = () => {
    handleUpdatedDeploymentState("shelved_offloaded");
  };

  const handleActive = () => {
    handleUpdatedDeploymentState("active");
  };

  // create isession

  const createISession = async (props) => {
    try {
      setShowProgress(true);
      setBusy(true);
      setSessionUrl(null);

      const req = {
        instance_id: props.id,
        instance_address: props.attributes.floating_ip,
        instance_admin_username: deployment.owner.split("@")?.[0],
        cloud_id: "default", //FIXME move into config
        protocol: props.protocol,
      };

      // Try to reuse existing session
      const isessions = await api.isessions();
      let isession = isessions?.find(
        (s) =>
          s.state === "active" &&
          s.cloud_id === req.cloud_id &&
          s.instance_id === req.instance_id &&
          s.instance_address === req.instance_address &&
          s.protocol === req.protocol
      );

      // Create new session
      if (!isession) {
        if (props.type === "vnc")
          req.password = config.ISESSION_VNC_DEFAULT_PASSWORD || "";

        isession = await api.createISession(req);
        if (!isession?.tid)
          throw new ("Could not create session" +
            (props.type === "vnc"
              ? ". Check that VNC is installed in the image."
              : ""))();
      }

      // Open session in new tab
      isession = await api.isession(isession.tid || isession.id);
      if (isession?.state === "failed") throw "Session creation failed";
      if (!isession?.redirect_url) throw "Could not retrieve session url";
      setSessionUrl(isession.redirect_url);
      console.log(isession.redirect_url);
      window.open(isession.redirect_url, "_blank");
    } catch (error) {
      handleError(error);
      setShowProgress(false);
    } finally {
      setBusy(false);
    }
  };
  const getStatus = (run) => {
    let status = run.status;
    if (status === "running" || status === "pre-flight") {
      let ps = run.parameters.find((p) => p.key === "power_state")?.value;
      if (ps === "shelved_offloaded") return "Shelving";
      if (ps === "shutoff") return "Shutting Off";
    }
    return "Deploying (" + (status || "pending") + ")";
  };

  const notActive = (deployment) => {
    let ps = deployment.parameters.find((p) => p.key === "power_state")?.value;
    if (
      deployment.current_status === "creation_errored" ||
      (deployment.pending_status === "none" &&
        (ps === "shelved_offloaded" || ps === "shutoff"))
    )
      return ps;
    return null;
  };

  return (
    <Layout title={deployment.id} breadcrumbs busy={busy && !showProgress}>
      <Box mt={4}>
        <Card>
          <CardHeader
            avatar={
              <Avatar>
                <Icon path={mdiRocketLaunch} size={1} />
              </Avatar>
            }
            action={
              <Grid container alignItems="center">
                <Box display="flex" alignItems="center">
                  <Box mt={1} mr={1}>
                    <StatusIndicator deployment={deployment} />
                  </Box>
                  <Divider
                    orientation="vertical"
                    flexItem
                    style={{ margin: "0 0.5em 0 0.5em" }}
                  />
                  <LogsButton deploymentId={deployment.id} runId={run.id} />
                  <Divider
                    orientation="vertical"
                    flexItem
                    style={{ margin: "0 0.5em 0 0.5em" }}
                  />
                  {notActive(deployment) ? (
                    <Box>
                      <Tooltip
                        title={
                          notActive(deployment) === "shelved_offloaded"
                            ? "Unshelve"
                            : "Start"
                        }
                      >
                        <ActionButton onClick={() => setShelveDialog(true)}>
                          <PlayIcon />
                        </ActionButton>
                      </Tooltip>
                      <ShelveConfirmationDialog
                        open={shelveDialog}
                        title="Unshelve Deployment"
                        handleClose={() => setShelveDialog(false)}
                        handleSubmit={handleActive}
                        infoText={
                          "Unshelving will restart the virtual machine(s), which will resume the consumption of compute resources."
                        }
                        confirmText={"Unshelve"}
                      />
                    </Box>
                  ) : (
                    <Box display="flex">
                      <Tooltip title="Shelve">
                        <ActionButton onClick={() => setShelveDialog(true)}>
                          <StopIcon />
                        </ActionButton>
                      </Tooltip>
                      <ShelveConfirmationDialog
                        open={shelveDialog}
                        title="Shelve Deployment"
                        handleClose={() => setShelveDialog(false)}
                        handleSubmit={handleShelve}
                        infoText={
                          "Shelving your virtual machine(s) will turn them off and preserve their data. A shelved deployment continues to use storage allocation but doesn't consume any compute allocation."
                        }
                        confirmText={"Shelve"}
                      />
                      <Tooltip title="Shutdown">
                        <ActionButton
                          onClick={() =>
                            handleUpdatedDeploymentState("shutoff")
                          }
                        >
                          <PowerIcon />
                        </ActionButton>
                      </Tooltip>
                    </Box>
                  )}
                  <Divider
                    orientation="vertical"
                    flexItem
                    style={{ margin: "0 0.5em 0 0.5em" }}
                  />
                  <Tooltip title="Remove">
                    <ActionButton
                      onClick={() => setShowDeleteConfirmation(true)}
                    >
                      <DeleteIcon />
                    </ActionButton>
                  </Tooltip>
                </Box>
              </Grid>
            }
            title={
              <Typography>
                <b>{deployment.name || "<Unnamed>"}</b>{" "}
                <span>
                  {run.parameters.find((e) => e.key === "region").value}
                </span>
              </Typography>
            }
            subheader={
              <Typography variant="body2">
                <b>Started:</b> {datetimeString(deployment.created_at)}
                {run.start && run.end && run.end > run.start && (
                  <>
                    , <b>Build time:</b> {timeDiffString(run.start, run.end)}
                  </>
                )}
                , <b>Updated:</b> {datetimeString(deployment.updated_at)}
                <br />
                <b>Template</b>: {props.template.description} <b>Template ID</b>
                : {deployment.template_id}
              </Typography>
            }
          />
          <CardContent>
            {deployment.current_status === "creation_errored" && (
              <DeploymentErrorAlert deployment={deployment} />
            )}
            <Accordion elevation={0}>
              <Button color="primary" size="small">
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon color="primary" />}
                >
                  <Typography variant="body2">
                    View Deployment Parameters
                  </Typography>
                </AccordionSummary>
              </Button>
              <AccordionDetails>
                <TableContainer>
                  <Table size="small">
                    <TableBody>
                      {run.parameters
                        .filter((p) => p.key !== "user_data")
                        .sort(function (a, b) {
                          return a.key.localeCompare(b.key);
                        })
                        .map((p) => (
                          <TableRow key={p.key}>
                            <TableCell>{p.key}</TableCell>
                            <TableCell>{p.value}</TableCell>
                          </TableRow>
                        ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </AccordionDetails>
            </Accordion>
            {run && run.status === "active" ? (
              <List>
                {run.last_state.resources &&
                  run.last_state.resources
                    .filter((r) => needToRenderResource(r))
                    .map((resource, index) => (
                      <Box key={index}>
                        <Divider />
                        <Resource
                          state={
                            run.parameters.find((p) => p.key === "power_state")
                              ?.value
                          }
                          {...resource}
                          sessionHandler={createISession}
                        />
                      </Box>
                    ))}
              </List>
            ) : (
              <Box ml={7} mr={2} mb={2}>
                {run.status === "errored" ? (
                  <>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      gutterBottom
                    >
                      Error:{" "}
                      {run.status_msg ? run.status_msg : "An error occurred"}
                    </Typography>
                    <Box display="flex" alignItems="center">
                      <Box width="100%">
                        <ErrorLinearProgress
                          variant="determinate"
                          value={100}
                        />
                      </Box>
                    </Box>
                  </>
                ) : (
                  <>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      gutterBottom
                    >
                      {getStatus(run)} ...
                    </Typography>
                    <Box display="flex" alignItems="center">
                      <Box width="100%">
                        <StandardLinearProgress variant="indeterminate" />
                      </Box>
                    </Box>
                  </>
                )}
              </Box>
            )}
          </CardContent>
        </Card>
      </Box>
      <ConfirmationDialog
        open={showDeleteConfirmation}
        title="Delete deployment"
        handleClose={() => setShowDeleteConfirmation(false)}
        handleSubmit={() => handleDeleteDeployment()}
      />
      <ProgressDialog
        open={showProgress}
        busy={busy}
        title="Web Shell/Desktop"
        busyText={
          <Box display="flex">
            <CircularProgress
              color="inherit"
              size={20}
              style={{ marginRight: "1em" }}
            />
            <Typography>Creating session ...</Typography>
          </Box>
        }
        doneText={
          <>
            Session opened in new tab or click{" "}
            <a href={sessionUrl} target="_blank" rel="noreferrer">
              this link
            </a>
            .
          </>
        }
        handleClose={() => setShowProgress(false)}
      />
    </Layout>
  );
};

export async function getServerSideProps({ req, res, query }) {
  try {
    const deployment = await req.api.deployment(query.id);
    const template = await req.api.template(deployment.template_id);
    const run = await req.api.deploymentRun(
      deployment.id,
      deployment.current_run.id
    );

    if (template.metadata.template_type === "aws_terraform" && run.last_state) {
      // transform the resources for AWS before send to client, so that it is easier to render.
      matchAWSInstanceIP(run.last_state.resources);
    }

    return {
      props: {
        deployment,
        template,
        run,
      },
    };
  } catch (e) {
    Sentry.captureException(e);
    console.error(e);

    let redirectDestination;
    if (e?.response?.status === 401) {
      redirectDestination = "/401";
    } else if (e?.response?.status === 404) {
      redirectDestination = "/404";
    } else {
      redirectDestination = "/500";
    }
    return {
      redirect: {
        destination: redirectDestination,
        permanent: false,
      },
    };
  }
}

export default Deployment;
