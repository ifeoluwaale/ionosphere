import { useState, useEffect } from "react";
import * as Sentry from "@sentry/nextjs";
import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  ToggleButton,
  ToggleButtonGroup,
  TextField,
  Typography,
} from "@mui/material";
import { mdiRocketLaunch } from "@mdi/js";
import Icon from "@mdi/react";
import { Layout } from "../../../components";
import { useAPI, useError } from "../../../contexts";

// logsType constants
const logsTypeDeletion = "deletion";
const logsTypePreflight = "pre-flight";
const logsTypeExecution = "running";

/**
 *
 * @param api {Cacao}
 * @param logsType {string}
 * @param deploymentId {string}
 * @param runId {string}
 * @param runStatus {string}
 * @param setLogs {function(string)}
 * @param setLoadingLogs {function(boolean)}
 * @param setError {function(string)}
 * @returns {Promise<void>}
 */
const downloadLogs = async (
  api,
  logsType,
  deploymentId,
  runId,
  runStatus,
  setLogs,
  setLoadingLogs,
  setError
) => {
  setLoadingLogs(true);
  try {
    let logs = "";
    switch (logsType) {
      case logsTypeDeletion:
        logs = await api.deploymentDeletionLogs(deploymentId);
        break;
      case logsTypePreflight:
        if (
          !runId ||
          !(
            runStatus === "running" ||
            runStatus === "active" ||
            runStatus === "errored"
          )
        ) {
          logs = "deployment run has not finished pre-flight stage yet";
        } else {
          logs = await api.deploymentRunLogs(deploymentId, runId, logsType);
        }
        break;
      case logsTypeExecution:
        if (!runId || !(runStatus === "active" || runStatus === "errored")) {
          logs = "deployment run has not finished execution stage yet";
        } else {
          logs = await api.deploymentRunLogs(deploymentId, runId, logsType);
        }
        break;
      default:
        console.error("unknown logsType", logsType);
        logs = "no logs for this deployment run";
        break;
    }
    // workaround a backend bug, sometimes backend will return 200 but it actually errored, and we get back an object instead of string.
    if (typeof logs === "string" || logs instanceof String) setLogs(logs);
    else setLogs(JSON.stringify(logs));
  } catch (e) {
    let msg;
    switch (e?.response?.status) {
      case 404:
        msg = `no ${logsType} logs`;
        setLogs(msg);
        setError(msg);
        break;
      default:
        msg = "failed to fetch logs, " + e.toString();
        setLogs(msg);
        setError(msg);
        break;
    }
  } finally {
    setLoadingLogs(false);
  }
};

// use <TextField> to display logs
const LogsBlock1 = ({ logs }) => {
  return (
    <TextField
      color={"info"}
      inputProps={{ style: { color: "#fbfafd" } }}
      sx={{
        backgroundColor: "#111111",
      }}
      multiline
      value={logs}
    />
  );
};

// use <code> to display logs
const LogsBlock = ({ logs }) => {
  return (
    <Box
      sx={{
        color: "#fbfafd",
        backgroundColor: "#111111",
        code: { display: "block", whiteSpace: "pre-wrap" },
        padding: "1em",
      }}
    >
      <code>{logs}</code>
    </Box>
  );
};

const Deployment = (props) => {
  const api = useAPI();
  const [_, setError] = useError();
  const deployment = props.deployment;
  const [logsType, setLogsType] = useState("pre-flight");
  const [logs, setLogs] = useState("");
  const [loadingLogs, setLoadingLogs] = useState(false);
  const runStatus = props?.run?.status;
  useEffect(() => {
    downloadLogs(
      api,
      logsType,
      deployment.id,
      deployment.current_run.id,
      runStatus,
      setLogs,
      setLoadingLogs,
      setError
    ).then();
  }, [logsType, deployment.id, deployment.current_run.id, runStatus]);

  const deploymentDeletedOrDeleting =
    deployment?.current_status === "deleted" ||
    deployment?.pending_status === "deleting";

  return (
    <Layout title={`logs`} breadcrumbs>
      <Box mt={4}>
        <Card>
          <CardHeader
            avatar={
              <Avatar>
                <Icon path={mdiRocketLaunch} size={1} />
              </Avatar>
            }
            action={
              <Box m={1}>
                <ToggleButtonGroup
                  color="primary"
                  value={logsType}
                  exclusive
                  onChange={(event, newLogsType) => {
                    setLogsType(newLogsType);
                  }}
                  aria-label="Select logs type"
                >
                  <ToggleButton value={logsTypePreflight}>
                    Pre-flight
                  </ToggleButton>
                  <ToggleButton value={logsTypeExecution}>
                    Execution
                  </ToggleButton>
                  <ToggleButton value={logsTypeDeletion}>Deletion</ToggleButton>
                </ToggleButtonGroup>
              </Box>
            }
            title={
              <Typography>
                <b>{deployment.name || "<Unnamed>"}</b>{" "}
              </Typography>
            }
          />
          <CardContent>
            {loadingLogs ? (
              <CircularProgress />
            ) : (
              <Box>
                <LogsBlock logs={logs} />
              </Box>
            )}
          </CardContent>
        </Card>
      </Box>
    </Layout>
  );
};

export async function getServerSideProps({ req, res, query }) {
  try {
    const deployment = await req.api.deployment(query.id);
    let run = null;
    if (deployment?.current_run.id) {
      run = await req.api.deploymentRun(
        deployment.id,
        deployment.current_run.id // TODO we currently only show logs the current run, we should allow viewing logs of previous runs.
      );
    }

    return {
      props: {
        deployment,
        run,
      },
    };
  } catch (e) {
    Sentry.captureException(e);
    console.error(e);

    let redirectDestination;
    if (e?.response?.status === 401) {
      redirectDestination = "/401";
    } else if (e?.response?.status === 404) {
      redirectDestination = "/404";
    } else {
      redirectDestination = "/500";
    }
    return {
      redirect: {
        destination: redirectDestination,
        permanent: false,
      },
    };
  }
}

export default Deployment;
