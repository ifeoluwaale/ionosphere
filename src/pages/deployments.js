import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import getConfig from "next/config";
import * as Sentry from "@sentry/nextjs";
import cookie from "cookie";
import {
  Alert,
  Box,
  Button,
  Divider,
  FormControl,
  FormControlLabel,
  Grid,
  Link,
  Paper,
  Radio,
  RadioGroup,
  Stack,
  Typography,
} from "@mui/material";

import {
  Add as AddIcon,
  CloudQueue as CloudOutlineIcon,
} from "@mui/icons-material";

import { ConfirmationDialog, Layout } from "../components";
import { isPrerequisiteTemplate, sortByDateDesc, sortByName } from "../utils";
import {
  useAPI,
  useClouds,
  useCredentials,
  useError,
  useEvents,
} from "../contexts";

import DropdownMenu from "../components/DropdownMenu";
import CredentialModal from "../components/CredentialModal";
import DeploymentSummary from "../components/Deployment/DeploymentSummary";
import CreateDeploymentDialog from "../components/DeploymentWizard/CreateDeploymentDialog";
import EditDeploymentDialog from "../components/DeploymentWizard/EditDeploymentDialog";
import ResourceAlert from "../components/Resources/ResourceAlert";

import { DeploymentWizardProvider } from "@/components/DeploymentWizard/contexts/DeploymentWizardContext";

const SETTINGS_COOKIE = "settings"; //TODO move to config or constants.js

const sortDeployments = (deployments, sort) => {
  const order = {
    active: 0,
    "pre-flight": 1,
    errored: 2,
    "": 3,
  };

  if (sort === "name") return deployments.sort(sortByName);
  if (sort === "status")
    return deployments.sort(
      (a, b) => order[a.current_run.status] - order[b.current_run.status]
    );

  return deployments.sort(sortByDateDesc);
};

/**
 * Check if a cloud provider is JS2 provider
 * @param cloud
 * @returns {boolean}
 */
const isJS2Cloud = (cloud) => {
  if (!cloud) {
    return false;
  }
  if (cloud["type"] !== "openstack") {
    return false;
  }
  if (typeof cloud["url"] !== "string") {
    return false;
  }
  return cloud["url"].startsWith("https://js2.jetstream-cloud.org");
};

/**
 * Menu at top of deployments page which allows users to select
 * cloud and project.
 *
 * Project concept is limited to JS2 provider currently, so for
 * other providers, the project dropdown should be disabled.
 */
const CloudMenu = ({
  clouds,
  projects,
  selectedCloudId,
  selectedProjectId,
  onClick,
}) => {
  const handleClick = (source, value) => {
    if (onClick) onClick(source, value);
  };

  const selectedCloud = clouds.find((c) => {
    return c.id === selectedCloudId;
  });
  const isJS2 = isJS2Cloud(selectedCloud);
  const projectDropDownLabel = isJS2
    ? projects.length
      ? selectedProjectId
      : "Select Project"
    : "N/A";

  return (
    <Stack display="flex" direction={"row"} spacing={1}>
      <DropdownMenu
        label={
          clouds.length
            ? clouds.find((c) => c.id === selectedCloudId)?.name
            : "Select Cloud"
        }
        icon={<CloudOutlineIcon />}
        header="Clouds"
        selectedCloudId={selectedCloudId}
        selectedProjectId={selectedProjectId}
        options={clouds}
        onClick={(option) => handleClick("cloud", option)}
      />
      <DropdownMenu
        label={projectDropDownLabel}
        header="Projects"
        selectedCloudId={selectedCloudId}
        selectedProjectId={selectedProjectId}
        options={projects.map((p) => {
          return { id: p.title, name: p.title };
        })}
        onClick={(option) => handleClick("project", option)}
        enabled={isJS2}
      />
    </Stack>
  );
};

/**
 * Currently one workspace per cloud is supported.
 *
 * In a Workspace, users can:
 * - View all deployments for the selected cloud, filtered by project.
 * - Create new deployments for the selected cloud & project.
 *
 * NOTE: Current implementation of Workspace component may seem odd
 * because it was originally designed for users to have multiple workspaces
 * per cloud, but this functionality has been removed for now.
 *
 * Workspace structure was left in place for if/when we return to allowing
 * multiple workspaces/cloud.  If we don't support multiple workspaces,
 * this should be refactored.
 */
const Workspace = ({
  id,
  name,
  description,
  deployments,
  templates,
  showAllDeployments,
  handleDeleteDeployment,
  openEditDeploymentDialog,
  openCreateDeploymentDialog,
  selectedProjectId,
  selectedCloudName,
}) => {
  const [deploymentIdToDelete, setDeploymentIdToDelete] = useState();
  const [sort, setSort] = useState();
  const MAX_SUMMARY_DEPLOYMENTS = 5;
  const isDefaultWorkspace = name === "Default Workspace";
  const numDeploymentsToShow = showAllDeployments
    ? deployments.length
    : MAX_SUMMARY_DEPLOYMENTS;
  return (
    <div>
      <Paper variant="outlined" sx={{ padding: "2em" }}>
        <Grid container spacing={4}>
          <Grid
            container
            item
            xs={12}
            justifyContent="space-between"
            alignItems="flex-start"
          >
            <Grid item>
              <Stack alignItems={"center"} direction={"row"}>
                <Typography
                  component="h1"
                  variant="h5"
                  style={{ paddingRight: "1em" }}
                >
                  {isDefaultWorkspace && (
                    <>
                      {selectedProjectId
                        ? selectedProjectId
                        : "Default Workspace"}
                    </>
                  )}
                </Typography>
              </Stack>
            </Grid>
            <Grid item>
              {selectedProjectId && (
                <Button
                  variant="contained"
                  color="primary"
                  style={{ marginLeft: "2em" }}
                  startIcon={<AddIcon />}
                  onClick={openCreateDeploymentDialog}
                  size="small"
                >
                  New Deployment
                </Button>
              )}
            </Grid>
          </Grid>
        </Grid>
        <Box marginTop={2}>
          <ResourceAlert cloudName={selectedCloudName}></ResourceAlert>
        </Box>
        <Grid container>
          <Box mt={1} mb={2.5}>
            <Typography
              color="textPrimary"
              sx={{
                display: "-webkit-box",
                WebkitLineClamp: 1,
                WebkitBoxOrient: "vertical",
                overflow: "hidden",
              }}
            >
              {description}
            </Typography>
          </Box>
        </Grid>
        {selectedProjectId ? (
          <>
            <Grid
              container
              justifyContent="space-between"
              alignItems="center"
              style={{ paddingBottom: "0.5em" }}
            >
              <Grid item>
                <Box display="flex">
                  <Typography variant="h6">
                    Deployments
                    {deployments.length > 0 && (
                      <small> ({deployments.length})</small>
                    )}
                  </Typography>
                </Box>
              </Grid>
              {sortDeployments(deployments, sort)?.length > 1 && (
                <Grid item>
                  <Box
                    display="flex"
                    flexWrap="wrap"
                    alignSelf="flex-end"
                    alignItems="center"
                  >
                    <Typography color="textSecondary">Sort by:</Typography>
                    <FormControl
                      component="fieldset"
                      style={{ marginLeft: "1.5em" }}
                    >
                      <RadioGroup
                        row
                        aria-label="sort by"
                        name="sort by"
                        defaultValue="newest"
                        onChange={(event) => setSort(event.target.value)}
                      >
                        <FormControlLabel
                          value="name"
                          control={<Radio color="primary" />}
                          label="Name"
                        />
                        <FormControlLabel
                          value="newest"
                          control={<Radio color="primary" />}
                          label="Newest"
                        />
                        <FormControlLabel
                          value="status"
                          control={<Radio color="primary" />}
                          label="Status"
                        />
                      </RadioGroup>
                    </FormControl>
                  </Box>
                </Grid>
              )}
            </Grid>
            <Divider />
            {sortDeployments(deployments, sort)?.length > 0 ? (
              <div>
                {sortDeployments(deployments, sort)
                  ?.slice(0, numDeploymentsToShow)
                  .map((deployment, index) => (
                    <Link
                      key={index}
                      href={`/deployments/${deployment.id}`}
                      underline="none"
                    >
                      <DeploymentSummary
                        deployment={deployment}
                        template={templates.find(
                          (t) => t.id === deployment.template_id
                        )}
                        handleDelete={() =>
                          setDeploymentIdToDelete(deployment.id)
                        }
                        handleEdit={() =>
                          openEditDeploymentDialog(deployment.id)
                        }
                      />
                      <Divider />
                    </Link>
                  ))}
                <br />
                {!showAllDeployments &&
                  deployments.length > MAX_SUMMARY_DEPLOYMENTS && (
                    <Box display="flex" justifyContent="center">
                      <Button
                        size="small"
                        color="primary"
                        href={`/deployments?workspace=${id}`}
                      >
                        View All
                      </Button>
                    </Box>
                  )}
              </div>
            ) : (
              <Box mt={2}>
                <Typography color="textSecondary">
                  This project does not have any deployments yet. Select "Add
                  Deployment" to create one.{" "}
                </Typography>
              </Box>
            )}
          </>
        ) : (
          <Box m={4}>
            <Alert severity="error">
              You do not have any projects. Create a credential on the{" "}
              <Link href="/credentials">Credentials page</Link> to add a
              project.
            </Alert>
          </Box>
        )}

        <ConfirmationDialog
          open={!!deploymentIdToDelete}
          title="Delete deployment"
          handleClose={() => setDeploymentIdToDelete()}
          handleSubmit={() => handleDeleteDeployment(deploymentIdToDelete)}
        />
      </Paper>
    </div>
  );
};

const filterTemplateForSelection = (t, selectedCloudType, templateTypesMap) => {
  if (
    selectedCloudType &&
    templateTypesMap &&
    templateTypesMap[selectedCloudType]
  ) {
    // filter out templates that does not match the provider/cloud type
    const matchedTemplateTypeName = templateTypesMap[selectedCloudType].find(
      (templateTypeName) => templateTypeName === t.metadata.template_type
    );
    if (!matchedTemplateTypeName) {
      return false;
    }
  }
  return isPrerequisiteTemplate(t);
};

const Deployments = (props) => {
  const api = useAPI();
  const router = useRouter();
  const config = getConfig().publicRuntimeConfig;

  const [_, setError] = useError();
  const [event] = useEvents();
  const [clouds] = useClouds();
  const [projects] = useState(props.projects);
  const [workspaces, setWorkspaces] = useState(props.workspaces);
  const [deployments, setDeployments] = useState(props.deployments);

  const [selectedCloudId, setSelectedCloudId] = useState(props.settings.cloud);
  let selectedCloudType = null;
  let selectedCloudIsJS2 = false;
  let selectedCloudName = null;
  {
    let selectedCloud = clouds.find((c) => c.id === selectedCloudId);
    if (selectedCloud) {
      selectedCloudType = selectedCloud.type;
      selectedCloudName = selectedCloud.name;
    }
    if (selectedCloud && isJS2Cloud(selectedCloud)) {
      selectedCloudIsJS2 = true;
    }
  }

  const [selectedProjectId, setSelectedProjectId] = useState(
    props.settings.project
  );
  const [credentials, setCredentials] = useCredentials();
  const [showCreateDeploymentDialog, setShowCreateDeploymentDialog] =
    useState(false);
  const [showEditDeploymentDialog, setShowEditDeploymentDialog] = useState();
  const [showCredentialModal, setShowCredentialModal] = useState(
    !(credentials?.length > 0)
  );

  // deployment selected for editing
  const [deployment, setDeployment] = useState();

  // when creating a deployment, the workspace new deployment is to be created in
  const [selectedWorkspaceId, setSelectedWorkspaceId] = useState();

  const [busy, setBusy] = useState(false);

  const handleError = (error) => {
    console.error(error);
    setBusy(false);
    setError(api.errorMessage(error));
  };

  const navigateToCredentials = () => {
    router.push({
      pathname: "/credentials",
    });
  };

  /**
   * Handles creation of new deployment and run. Invoked from CreateDeploymentDialog.
   *
   * @param {array of {key, value} pairs} deploymentParams: params for creating deployment
   * @param {array of {key, value} pairs} runParams: params for creating deployment run
   */
  const handlePostCreateDeployment = async () => {
    setBusy(true);
    try {
      setTimeout(async () => {
        //FIXME replace with DeploymentRunCreated event detection
        let deployments = await api.deployments({ "full-run": true });
        setDeployments(deployments);
        setShowCreateDeploymentDialog(false);
        setBusy(false);
      }, 500);
    } catch (error) {
      handleError(error);
    }
  };

  /**
   * Handles editing of deployment and run. Invoked from EditDeploymentDialog.
   *
   * @param {string} deploymentId: ID of deployment to be edited
   * @param {array of {key, value} pairs} deploymentParams: params for editing existing deployment
   * @param {array of {key, value} pairs} runParams: params for creating new deployment run
   */
  const handleEditDeployment = async (
    deploymentId,
    deploymentParams,
    runParams
  ) => {
    console.log("Deployment Params:", deploymentParams);
    console.log("Run Params:", runParams);

    if (!(config.SIMULATE_DEPLOYMENT_SUBMIT === "true")) {
      setBusy(true);

      try {
        if (deploymentParams.name) {
          const res = await api.updateDeployment(
            deploymentId,
            deploymentParams
          );
        }

        // create run
        if (runParams) {
          const res2 = await api.createDeploymentRun(deploymentId, {
            deployment_id: deploymentId,
            parameters: runParams,
          });
        }

        setTimeout(async () => {
          // refresh deployments
          let deployments = await api.deployments({ "full-run": true });
          deployments = deployments.filter(
            (d) => d.pending_status !== "deleting"
          );

          setDeployments(deployments);
          setShowEditDeploymentDialog(false);
          setBusy(false);
        }, 500);
      } catch (error) {
        handleError(error);
      }
    }
  };

  /**
   * Handles deletion of deployments.
   *
   * @param {string} id ID of deployment to delete.
   * */
  const handleDeleteDeployment = async (id) => {
    setBusy(true);
    try {
      const res = await api.deleteDeployment(id);
      if (!res) setError("An error occurred");
      else {
        const deployment = deployments.find((d) => d.id === id);
        deployment.pending_status = "deleting";
        setDeployment(deployment);
        setBusy(false);
      }
    } catch (error) {
      handleError(error);
    }
  };

  /**
   * Opens EditDeploymentDialog w/ selected deployment.
   *
   * @param {string} id ID of deployment to edit.
   */
  const openEditDeploymentDialog = async (id) => {
    const deployment = deployments.find((d) => d.id === id);

    setDeployment(deployment);
    setShowEditDeploymentDialog(id);
  };

  const handleSelectCloud = (source, option) => {
    if (source === "cloud") setSelectedCloudId(option.id);
    else if (source === "project") setSelectedProjectId(option.id);
  };

  /**
   * Filters deployments by workspace ID and currently selected project.
   * @param {string} workspaceId
   */
  const filterDeployments = (workspaceId) => {
    let workspaceDeployments = deployments.filter(
      (d) => d.workspace_id === workspaceId
    );

    if (!selectedProjectId) {
      return workspaceDeployments;
    }
    if (!selectedCloudIsJS2) {
      return workspaceDeployments;
    }

    // FIXME: update second condition once credentials are tagged w/ project name
    return workspaceDeployments.filter((d) => {
      const projectParam = d.parameters?.find(
        (param) => param.key === "project"
      );
      if (projectParam && projectParam.value === selectedProjectId) {
        return true;
      }
      return d.cloud_credentials[0].includes(selectedProjectId);
    });
  };

  async function fetchDeployment(id) {
    const deployment = await api.deployment(id);
    setDeployments(
      deployments.map((d) => {
        if (d.id === id) return deployment;
        else return d;
      })
    );
  }

  useEffect(() => {
    if (!event) return;

    if (event.type === "DeploymentRunStatusUpdated") {
      const deployment = deployments.find(
        (d) => d.id === event.data.deployment
      );
      if (!deployment)
        console.error(
          "Event received for unknown deployment",
          event.data.deployment
        );
      else fetchDeployment(event.data.deployment);
    }
  }, [event]);

  useEffect(() => {
    document.cookie =
      SETTINGS_COOKIE +
      "=" +
      JSON.stringify({ cloud: selectedCloudId, project: selectedProjectId }); // create cookie
  }, [selectedCloudId, selectedProjectId]);

  return (
    <Layout
      title={
        props.workspaceId && workspaces?.length > 0
          ? workspaces[0].name
          : "Deployments"
      }
      breadcrumbs={!!props.workspaceId}
      parts={["deployments", ""]}
      busy={busy}
    >
      <CredentialModal
        open={showCredentialModal}
        handleClose={() => setShowCredentialModal(false)}
        navigateToCredentials={navigateToCredentials}
      />
      <Box style={{ minWidth: "20rem", maxWidth: "80rem" }}>
        <Grid container justifyContent="space-between">
          <Grid item>
            <CloudMenu
              selectedCloudId={selectedCloudId}
              selectedProjectId={selectedProjectId}
              clouds={clouds}
              projects={projects}
              onClick={handleSelectCloud}
            />
          </Grid>
        </Grid>
        <br />

        {workspaces?.length > 0 ? (
          workspaces
            .filter((w) => w.default_provider_id === selectedCloudId)
            .map((workspace, index) => (
              <Box key={index} mb={4}>
                <Workspace
                  {...workspace}
                  selectedProjectId={selectedProjectId}
                  selectedCloudName={selectedCloudName}
                  deployments={filterDeployments(workspace.id)}
                  templates={props.templates}
                  showAllDeployments={workspaces.length === 1}
                  handleDeleteDeployment={handleDeleteDeployment}
                  openEditDeploymentDialog={openEditDeploymentDialog}
                  openCreateDeploymentDialog={() => {
                    setSelectedWorkspaceId(workspace.id);
                    setShowCreateDeploymentDialog(true);
                  }}
                />
              </Box>
            ))
        ) : (
          <Box mt={4}>
            <Alert severity="error">
              You do not have any workspaces. Select "Add Workspace" to create
              one.
            </Alert>
          </Box>
        )}
        {showCreateDeploymentDialog && (
          <DeploymentWizardProvider
            initialValues={{
              cloudId: selectedCloudId,
              cloudName: clouds.find((c) => c.id === selectedCloudId)?.name,
              projectId: selectedProjectId,
            }}
          >
            <CreateDeploymentDialog
              open={true}
              templates={props.templates?.filter((t) =>
                filterTemplateForSelection(
                  t,
                  selectedCloudType,
                  props.templateTypesMap
                )
              )}
              handlePostCreate={handlePostCreateDeployment}
              handleClose={() =>
                setSelectedWorkspaceId() || setShowCreateDeploymentDialog(false)
              }
            />
          </DeploymentWizardProvider>
        )}

        {showEditDeploymentDialog && (
          <EditDeploymentDialog
            open={true}
            deployment={deployment}
            cloudId={selectedCloudId}
            projectId={selectedProjectId}
            workspaceId={selectedWorkspaceId}
            workspaces={workspaces}
            templates={props.templates?.filter((t) =>
              filterTemplateForSelection(
                t,
                selectedCloudType,
                props.templateTypesMap
              )
            )}
            credentials={credentials}
            handleSubmit={handleEditDeployment}
            handleClose={() =>
              setDeployment() || setShowEditDeploymentDialog(false)
            }
          />
        )}
      </Box>
    </Layout>
  );
};

/**
 * Returns a Map of provider type to list of template types. This can be used to lookup list of template types for a provider type.
 * e.g.
 * {
 *     "openstack": ["openstack_terraform"],
 *     "aws": ["aws_terraform"]
 * }
 * @param {[Object]} templateTypes
 * @returns {{}}
 */
function providerTypeTemplateTypesMap(templateTypes) {
  let m = {};
  for (let i = 0; i < templateTypes.length; i++) {
    templateTypes[i].provider_types.forEach((pt) => {
      if (m[pt]) {
        m[pt].push(templateTypes[i].name);
      } else {
        m[pt] = [templateTypes[i].name];
      }
    });
  }
  return m;
}

export async function getServerSideProps({ req, res }) {
  const cookies = cookie.parse(req.headers.cookie || "");

  let providers,
    projects,
    workspaces,
    deployments,
    templates,
    templateTypes,
    settings,
    settingsCookie;

  try {
    [providers, projects, workspaces, deployments, templates, templateTypes] =
      await Promise.all([
        req.api.providers(),
        req.api.js2allocations(),
        req.api.workspaces(),
        req.api.deployments({ "full-run": true }),
        req.api.templates(),
        req.api.templateTypes(),
      ]);
    const known_templates = [
      "DADI",
      "juypterhub",
      "single-image-k3s",
      "vm4workshop",
    ];
    templates.forEach((t) => {
      if (known_templates.includes(t.name)) {
        return;
      }
      if (!t.metadata.parameters) {
        return;
      }
      ["instance_count", "instance_name", "power_state"].forEach((name) => {
        let p = t.metadata.parameters.find((p) => {
          return p.name === name;
        });
        if (p) p.editable = true;
      });

      // ignore based on parameter type
      [
        "cacao_provider_external_network",
        "cacao_provider_key_pair",
        "cacao_provider_project",
        "cacao_provider_region",
        "cacao_username",
        "cacao_cloud_init",
      ].forEach((param_type) => {
        let p = t.metadata.parameters.find((p) => {
          return p.type === param_type;
        });
        if (p) p.ignore = true;
      });
      // ignore based on parameter name
      [
        "power_state",
        "root_storage_type",
        "root_storage_size",
        "root_storage_delete_on_termination",
      ].forEach((name) => {
        let p = t.metadata.parameters.find((p) => {
          return p.name === name;
        });
        if (p) p.ignore = true;
      });

      // edge case ignore image id field for openstack.
      if (templates.type === "openstack_terraform") {
        // TODO ideally, this should only be ignored if we have image name field, but since there can be multiple pair
        // of image (id/name) field, associate one with another is not possible with the current metadata.
        ["cacao_provider_image"].forEach((param_type) => {
          let p = t.metadata.parameters.find((p) => {
            return p.type === param_type;
          });
          if (p) p.ignore = true;
        });
      }
    });

    // parse existing settings cookie, if any
    if (SETTINGS_COOKIE in cookies && cookies[SETTINGS_COOKIE])
      settingsCookie = JSON.parse(cookies[SETTINGS_COOKIE]);

    settings = { cloud: null, project: null };

    // check if user has projects
    if (projects.length) {
      // if project set in cookie and still exists, set project to cookie value
      if (
        settingsCookie?.project &&
        projects.find((element) => element.title === settingsCookie.project)
      ) {
        settings.project = settingsCookie.project;
        // otherwise set project to first
      } else {
        settings.project = projects[0].title;
      }
    }

    // check if user has clouds
    if (providers.length) {
      // if cloud set in cookie and still exists, set cloud to cookie value
      if (
        settingsCookie?.cloud &&
        providers.find((element) => element.id === settingsCookie.cloud)
      ) {
        settings.cloud = settingsCookie.cloud;
        // otherwise set cloud to first provider
      } else {
        settings.cloud = providers[0].id;
      }
    }

    // Ensure default workspace exists for each provider
    let updated = false;
    for (const p of providers) {
      if (
        !workspaces?.find(
          (w) =>
            w.default_provider_id === p.id && w.name === "Default Workspace"
        )
      ) {
        await req.api.createWorkspace({
          name: "Default Workspace",
          default_provider_id: p.id,
        });
        updated = true;
      }
    }
    if (updated) workspaces = await req.api.workspaces();
  } catch (e) {
    Sentry.captureException(e);
    //FIXME replace with "redirect" feature in Next.js 10
    console.error(e);
    if (e?.response?.status === 401) {
      res.redirect("/401");
    } else {
      res.redirect("/500");
    }
    return { props: {} };
  }

  return {
    props: {
      projects,
      workspaces,
      deployments,
      templates,
      settings,
      templateTypesMap: providerTypeTemplateTypesMap(templateTypes),
    },
  };
}

export default Deployments;
