import * as Sentry from "@sentry/nextjs";
import { useState, useEffect } from "react";
import {
  Box,
  Divider,
  Alert,
  Typography,
  AlertTitle,
  Button,
  Grid,
} from "@mui/material";
import { isPrerequisiteTemplate } from "../utils";
import Layout from "../components/Layout";
import TemplateCard from "../components/Templates/TemplateCard";
import CreateDeploymentDialog from "@/components/DeploymentWizard/CreateDeploymentDialog";
import { DeploymentWizardProvider } from "@/components/DeploymentWizard/contexts/DeploymentWizardContext";
import TemplateSearch from "@/components/Templates/TemplateSearch";

const Templates = ({ templates }) => {
  const [showTemplateWizard, setShowTemplateWizard] = useState(null); // templateId
  const [search, setSearch] = useState(""); // search holds the value of the search input
  const [filteredTemplates, setFilteredTemplates] = useState([]);

  const scoreTemplate = (template) => {
    let score = 0;
    if (!template) return score;
    const searchLower = search.toLowerCase();
    if (searchLower.length < 0) {
      return score;
    }
    if (template.name && template.name.toLowerCase().includes(searchLower))
      score += 6;
    if (
      template.metadata.description &&
      template.metadata.description.toLowerCase().includes(searchLower)
    )
      score += 5;
    if (
      template.metadata.author &&
      template.metadata.author.toLowerCase().includes(searchLower)
    )
      score += 4;
    if (template.owner && template.owner.toLowerCase().includes(searchLower))
      score += 3;
    if (
      template.metadata.purpose &&
      template.metadata.purpose.toLowerCase().includes(searchLower)
    )
      score += 2;
    if (
      template.metadata.template_type &&
      template.metadata.template_type.toLowerCase().includes(searchLower)
    )
      score += 1;
    return score;
  };

  useEffect(() => {
    const newFilteredTemplates = templates
      .map((template) => ({
        ...template,
        score: scoreTemplate(template),
      }))
      .filter((template) => template.score > 0)
      .sort((a, b) => b.score - a.score);

    setFilteredTemplates(newFilteredTemplates);
  }, [templates, search]);

  const handleClose = async () => {
    setShowTemplateWizard(false);
  };

  return (
    <Layout title="Templates">
      <Divider />
      <Box mt={1} paddingTop={2}>
        <Box my={2}>
          <TemplateSearch search={search} setSearch={setSearch} />
        </Box>
        <Grid container spacing={3} justify="flex-end">
          {filteredTemplates.length === 0 ? (
            <Grid item xs={12}>
              <Box>
                <Alert severity="info">
                  <AlertTitle>No matching templates found</AlertTitle>
                  <Typography variant="body1">
                    {" "}
                    Try different keywords or{" "}
                    <Button
                      onClick={() => setSearch("")}
                      color="primary"
                      size="small"
                      variant="text"
                    >
                      view all templates
                    </Button>
                  </Typography>
                </Alert>
              </Box>
            </Grid>
          ) : (
            filteredTemplates
              .filter((t) => isPrerequisiteTemplate(t))
              .map((template, index) => (
                <Grid
                  key={index}
                  item
                  xs={12}
                  sm={12}
                  md={6}
                  lg={4}
                  xl={3}
                  style={{ display: "flex" }}
                >
                  <TemplateCard
                    template={template}
                    search={search}
                    launchWizard={() => setShowTemplateWizard(template.id)}
                  />
                </Grid>
              ))
          )}
        </Grid>
      </Box>
      {showTemplateWizard && (
        <DeploymentWizardProvider
          initialValues={{
            template: templates.find((t) => t.id === showTemplateWizard),
          }}
        >
          <CreateDeploymentDialog
            open={!!showTemplateWizard}
            handleClose={handleClose}
            templates={templates}
            handlePostCreate={handleClose}
          />
        </DeploymentWizardProvider>
      )}
    </Layout>
  );
};

export async function getServerSideProps({ req, res }) {
  try {
    const templates = await req.api.templates();

    return {
      props: {
        templates: templates && templates.length > 0 && templates.sort(),
      },
    };
  } catch (e) {
    Sentry.captureException(e);
    console.error(e);
    if (e?.response?.status === 401) {
      res.redirect("/401");
    } else {
      res.redirect("/500");
    }
    return { props: {} };
  }
}

export default Templates;
